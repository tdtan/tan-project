import { browser } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageAdminTrapDestinations } from '../PageObject/PageAdminTrapDestinations.po';
import { PageAddAdminTrapDestinations } from '../PageObject/PageAddTrapDestinations.po';
// import { PageRBACRoles } from '../PageObject/PageRBACRoles.po';
// const data = require('../../../e2e/VNFM_DB.e2e.json');
describe('workspace-project Trap Destination Page', () => {
    let pageAdminTrapDestinations: PageAdminTrapDestinations;
    let pageAddAdminTrapDestinations: PageAddAdminTrapDestinations;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
        pageAdminTrapDestinations = new PageAdminTrapDestinations();
        pageAddAdminTrapDestinations = new PageAddAdminTrapDestinations();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // Verify that the empty form was displayed when user click on Add Trap Destination button
    it('Verify that the empty form was displayed when user click on Add Trap Destination button- 1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/trapDestinations');
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickAddTrapButton();
        await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeTruthy();
        const txt_TrapName = await pageAddAdminTrapDestinations.getAttributeInTrapName();
        const txt_TrapAddress = await pageAddAdminTrapDestinations.getAttributeInTrapAddress();
        const txt_PortNumber = await pageAddAdminTrapDestinations.getAttributeInPortNumber();
        const txt_TrapCommunity = await pageAddAdminTrapDestinations.getAttributeInTrapCommunity();
        const txt_Version = await pageAddAdminTrapDestinations.getTextInTrapVersion();
        await expect(Boolean(txt_TrapName === '')).toBeTruthy();
        await expect(Boolean(txt_TrapAddress === '')).toBeTruthy();
        await expect(Boolean(txt_PortNumber === '')).toBeTruthy();
        await expect(Boolean(txt_TrapCommunity === '')).toBeTruthy();
        await expect(Boolean(txt_Version === 'Select Version')).toBeTruthy();
    });

    // Verify that the add trap destination function can not complete when missing required field
    it('Verify that the add trap destination function can not complete when missing required field-2', async function () {
        await pageAddAdminTrapDestinations.clickSaveButton();
        await browser.waitForAngularEnabled(true);
        await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyDisplayMessageErrorTrapName()).toBeTruthy();
        // await expect(pageAddAdminTrapDestinations.verifyDisplayMessageErrorTrapIPAddress()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyDisplayMessageErrorTrapPortNumber()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyDisplayMessageErrorTrapCommunity()).toBeTruthy();
        await pageAddAdminTrapDestinations.clickBackPageTrapButton();
    });

    // Verify that the add trap destination form was updated when user select the version of SNMP trap is v3
    it('Verify that the add trap destination form was updated when user select the version of SNMP trap is v3-3', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickAddTrapButton();
        await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeTruthy();
        await pageAddAdminTrapDestinations.clickDropdownTrapVersion();
        await pageAddAdminTrapDestinations.ClickChooseOptionTrapVersion('V3');
        await expect(pageAddAdminTrapDestinations.verifyTextSecurityIsPresent()).toBeTruthy();
    });

    // Verify that the Authentication passphrase and protocol fields were displayed when user enable Authentication
    it('Verify that the Authentication passphrase and protocol fields were displayed when user enable Authentication-4', async function () {
        await pageAddAdminTrapDestinations.clickAuthenticationCheckbox();
        await expect(pageAddAdminTrapDestinations.verifyTextAuthPassphraseDisplay()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyAuthPassphraseTextboxIsPresent()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyTextAuthProtocolIsPresent()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyAuthProtocolDropdownIsPresent()).toBeTruthy();
        await pageAddAdminTrapDestinations.clickAuthenticationCheckbox();
        await expect(pageAddAdminTrapDestinations.verifyTextAuthPassphraseDisplay()).toBeFalsy();
        await expect(pageAddAdminTrapDestinations.verifyAuthPassphraseTextboxIsPresent()).toBeFalsy();
        await expect(pageAddAdminTrapDestinations.verifyTextAuthProtocolIsPresent()).toBeFalsy();
        await expect(pageAddAdminTrapDestinations.verifyAuthProtocolDropdownIsPresent()).toBeFalsy();
    });

    // Verify that the Privacy passphrase and protocol fields were displayed when user enable Privacy
    it('Verify that the Privacy passphrase and protocol fields were displayed when user enable Privacy-5', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAddAdminTrapDestinations.clickPrivacyCheckbox();
        await expect(pageAddAdminTrapDestinations.verifyPrivacyPassphraseTextboxIsPresent()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyPrivacyProtocolDropdownIsPresent()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyTextPrivacyPassphraseDisplay()).toBeTruthy();
        await expect(pageAddAdminTrapDestinations.verifyTextPrivacyProtocolIsPresent()).toBeTruthy();
        await pageAddAdminTrapDestinations.clickPrivacyCheckbox();
        await expect(pageAddAdminTrapDestinations.verifyPrivacyPassphraseTextboxIsPresent()).toBeFalsy();
        await expect(pageAddAdminTrapDestinations.verifyPrivacyProtocolDropdownIsPresent()).toBeFalsy();
        await expect(pageAddAdminTrapDestinations.verifyTextPrivacyPassphraseDisplay()).toBeFalsy();
        await expect(pageAddAdminTrapDestinations.verifyTextPrivacyProtocolIsPresent()).toBeFalsy();
    });

     // Verify that the user can close the Add trap destination form when click on Cancel button
     it('Verify that the user can close the Add trap destination form when click on Cancel button-6', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAddAdminTrapDestinations.clickCancelButton();
        await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeFalsy();
    });

     // Verify that the table was updated with new SNMP destination when user click on Save button
     it('Verify that the table was updated with new SNMP destination when user click on Save button-7', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickAddTrapButton();
        await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeTruthy();
        await pageAddAdminTrapDestinations.setTrapName('Trap 1');
        await pageAddAdminTrapDestinations.setTrapAddress('1.2.3.4');
        await pageAddAdminTrapDestinations.setTrapPortNumber('32');
        await pageAddAdminTrapDestinations.setTrapCommunity('community test');
        await pageAddAdminTrapDestinations.clickDropdownTrapVersion();
        await pageAddAdminTrapDestinations.ClickChooseOptionTrapVersion('V2');
        await pageAddAdminTrapDestinations.clickSaveButton();
        await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeFalsy();
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Trap 1');
        await expect(await pageAdminTrapDestinations.verifyDisplay1RecordInPage()).toBeTruthy();

    });

    // Verify that the user can click on the row to edit the SNMP trap destination
    it('Verify that the user can click on the row to edit the SNMP trap destination-8', async function () {
        await pageAdminTrapDestinations.ClickLinkOnRow();
        await expect(pageAdminTrapDestinations.verifyEditTrapDestinationPageDisplay()).toBeTruthy();
    });

    it('Verify that the filter row of table is displayed when user click on the “Filter” button -9', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/trapDestinations');
        await browser.waitForAngularEnabled(true);
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeFalsy();
        await pageAdminTrapDestinations.ClickFilterButton();
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeTruthy();
    });

    // Verify that the filter row of table is hided when user click on the “Filter” button again
    it('Verify that the filter row of table is hided when user click on the “Filter” button again -10', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickFilterButton();
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeFalsy();
    });

    // Verify that the filter function work properly
    it('Verify that the filter function work properly -11', async function () {
        await pageAdminTrapDestinations.ClickFilterButton();
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeTruthy();
        const txtTextInColumn = await pageAdminTrapDestinations.getTextInColumn(2);
        await pageAdminTrapDestinations.SelectFilter(2, txtTextInColumn);
        await expect(pageAdminTrapDestinations.verifyShow1RecordIsDisplayInPage()).toBeTruthy();
        await pageAdminTrapDestinations.SelectFilter(2, 'All');
        const txtTextInColumn2 = await pageAdminTrapDestinations.getTextInColumn(3);
        await pageAdminTrapDestinations.SelectFilter(3, txtTextInColumn2);
        await expect(pageAdminTrapDestinations.verifyShow1RecordIsDisplayInPage()).toBeTruthy();
        await pageAdminTrapDestinations.SelectFilter(3, 'All');
    });

    // Verify that the filter result should be kept when user hide/display the filer row
    it('Verify that the filter result should be kept when user hide/display the filer row-12', async function () {
        await browser.waitForAngularEnabled(true);
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeTruthy();
        const txtTextInColumn = await pageAdminTrapDestinations.getTextInColumn(2);
        await pageAdminTrapDestinations.SelectFilter(2, txtTextInColumn);
        await pageAdminTrapDestinations.ClickFilterButton();
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeFalsy();
        await pageAdminTrapDestinations.ClickFilterButton();
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeTruthy();
        const txtTextFilter = await pageAdminTrapDestinations.getTextLabelFilter(2);
        await expect(Boolean(txtTextFilter === txtTextInColumn)).toBe(true);
    });

    // // Verify that the user can clear the result filter when click on the "X" in filter field
    // it('Verify that the user can clear the result filter when click on the "X" in filter field-13', async function () {
    //     const txtTextFilter1 = await pageAdminTrapDestinations.getTextLabelFilter(2);
    //     await console.log('text 123: ' + txtTextFilter1);
    //     await expect(Boolean(txtTextFilter1 === '')).toBe(false);
    //     await pageAdminTrapDestinations.ClickClearFilterButton();
    //     await browser.waitForAngularEnabled(true);
    //     const txtTextFilter = await pageAdminTrapDestinations.getTextLabelFilter(2);
    //     await console.log('text 123 All: ' + txtTextFilter);
    //     await expect(Boolean(txtTextFilter === 'All')).toBe(true);
    // });

    // Verify that the Section/table is minimized when user click on the down-arrow
    it('Verify that the Section/table is minimized when user click on the down-arrow- 14', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/trapDestinations');
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickDownArrow();
        await expect(pageAdminTrapDestinations.verifyDisplay1RecordInPage()).toBeTruthy();
        await expect(pageAdminTrapDestinations.verifyShow1RecordIsDisplayInPage()).toBeFalsy();
    });

    // Verify that the Section/table is expended  when user click on the right-arrow
    it('Verify that the Section/table is expended  when user click on the right-arrow- 15', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickRightArrow();
        await expect(pageAdminTrapDestinations.verifyDisplay1RecordInPage()).toBeTruthy();
        await expect(pageAdminTrapDestinations.verifyShow1RecordIsDisplayInPage()).toBeTruthy();
    });

    // Verify that the Search function work properly
    it('Verify that the Search function work properly- 16', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Text Invalid');
        await expect(pageAdminTrapDestinations.isTableHasValueAfterSearch()).toBeFalsy();
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Trap 1');
        await expect(pageAdminTrapDestinations.isTableHasValueAfterSearch()).toBeTruthy();
    });

    // Verify that the user can clear the Search input when click on the "X" in search field
    it('Verify that the user can clear the Search input when click on the "X" in search field- 17', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminTrapDestinations.ClickClearSearchButton();
        const txtSearch = await pageAdminTrapDestinations.getAttributeOfTextboxSearch();
        await expect(Boolean(txtSearch === '')).toBe(true);
    });

    // Verify that the user can hide/unhide columns
    it('Verify that the user can hide/unhide columns- 18', async function () {
        await pageAdminTrapDestinations.ClickShowCloumnButton();
        await pageAdminTrapDestinations.selectOptionNamefromShowHideColumnsList('Destination Name');
        await pageAdminTrapDestinations.ClickCloseButton();
        await expect(pageAdminTrapDestinations.verifyShowHidecolumns('Destination Name')).toBeFalsy();
        await pageAdminTrapDestinations.ClickShowCloumnButton();
        await pageAdminTrapDestinations.selectOptionNamefromShowHideColumnsList('Destination Name');
        await pageAdminTrapDestinations.ClickCloseButton();
        await expect(pageAdminTrapDestinations.verifyShowHidecolumns('Destination Name')).toBeTruthy();
    });

    // Verify the number row of page was display correctly
    it('Verify the number row of page was display correctly- 19', async function () {
        const txtNumber = await pageAdminTrapDestinations.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtNumber === '5')).toBe(true);
        await pageAdminTrapDestinations.selectNumberRowOnTable(10);
        const txtNumber2 = await pageAdminTrapDestinations.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtNumber2 === '10')).toBe(true);
        await pageAdminTrapDestinations.selectNumberRowOnTable(15);
        const txtNumber3 = await pageAdminTrapDestinations.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtNumber3 === '15')).toBe(true);
    });

    // Verify the table should back to default setting when refresh the page
    it('Verify the table should back to default setting when refresh the page- 20', async function () {
        await pageAdminTrapDestinations.ClickFilterButton();
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeTruthy();
        await browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await expect(pageAdminTrapDestinations.verifyDisplayDropdownFilterInPage()).toBeFalsy();
        const txtNumber = await pageAdminTrapDestinations.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtNumber === '5')).toBe(true);
    });

    // Verify that the pupop confirm delete display when click on the trashcan icon
    it('Verify that the pupop confirm delete display when click on the trashcan icon- 21', async function () {
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Trap 1');
        await pageAdminTrapDestinations.ClickDeleteButton();
        await browser.waitForAngularEnabled(true);
        await expect(pageAdminTrapDestinations.verifyDeletePupopDisplayInPage()).toBeTruthy();
    });

    // Verify that the user can't delete the SNMP trap destination when click No Button
    it('Verify that the user can not delete the SNMP trap destination when click No Button- 22', async function () {
        await pageAdminTrapDestinations.ClickConfirmNoDeleteButton();
        await expect(pageAdminTrapDestinations.verifyDeletePupopDisplayInPage()).toBeFalsy();
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Trap 1');
        await expect(pageAdminTrapDestinations.isTableHasValueAfterSearch()).toBeTruthy();
    });

    // Verify that the user can delete the SNMP trap destination when click Yes Button
    it('Verify that the user can delete the SNMP trap destination when click Yes Button- 23', async function () {
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Trap 1');
        await pageAdminTrapDestinations.ClickDeleteButton();
        await pageAdminTrapDestinations.ClickConfirmYesDeleteButton();
        await pageAdminTrapDestinations.ClickRefreshButton();
        await pageAdminTrapDestinations.ClearTextSearch();
        await pageAdminTrapDestinations.settextSearch('Trap 1');
        await expect(pageAdminTrapDestinations.isTableHasValueAfterSearch()).toBeFalsy();
    });

    // // Verify that the alarm, event was generated when user add/edit/delete a SNMP trap destination
    // it('Verify that the alarm, event was generated when user delete a SNMP trap destination- 24', async function () {
    //     await browser.get(browser.baseUrl + '/#/vnfm/trapDestinations');
    //     await browser.waitForAngularEnabled(true);

    //     // Add Trap Destination
    //     await pageAdminTrapDestinations.ClickAddTrapButton();
    //     await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeTruthy();
    //     await pageAddAdminTrapDestinations.setTrapName(data.TRAPDESTINATION.TC_1.TRAPNAME);
    //     await pageAddAdminTrapDestinations.setTrapAddress(data.TRAPDESTINATION.TC_1.TRAPIPADDRESS);
    //     await pageAddAdminTrapDestinations.setTrapPortNumber(data.TRAPDESTINATION.TC_1.TRAPPORT);
    //     await pageAddAdminTrapDestinations.setTrapCommunity(data.TRAPDESTINATION.TC_1.TRAPCOMMUNITY);
    //     await pageAddAdminTrapDestinations.clickDropdownTrapVersion();
    //     await pageAddAdminTrapDestinations.ClickChooseOptionTrapVersion('V2');
    //     await pageAddAdminTrapDestinations.clickSaveButton();
    //     await expect(pageAddAdminTrapDestinations.verifySwitchButtonDisplay()).toBeFalsy();
    //     // await browser.sleep(3000);
    //     await browser.waitForAngularEnabled(false);
    //     await expect(pageAdminTrapDestinations.verifyAlarmDisplayWhenTrapAdded()).toBeTruthy();

        // // Edit Trap Destination
        // await browser.waitForAngularEnabled(true);
        // await pageAdminTrapDestinations.ClearTextSearch();
        // await pageAdminTrapDestinations.settextSearch(data.TRAPDESTINATION.TC_1.TRAPNAME);
        // await pageAdminTrapDestinations.ClickLinkOnRow();
        // await expect(pageAdminTrapDestinations.verifyEditTrapDestinationPageDisplay()).toBeTruthy();
        // await pageAddAdminTrapDestinations.setTrapName(data.TRAPDESTINATION.TC_1.TRAPNAMECHANGE);
        // await pageAddAdminTrapDestinations.clickSaveButton();
        // await expect(pageAdminTrapDestinations.verifyEditTrapDestinationPageDisplay()).toBeFalsy();
        // await pageAdminTrapDestinations.ClearTextSearch();
        // await browser.sleep(3000);
        // await browser.waitForAngularEnabled(false);
        // await expect(pageAdminTrapDestinations.verifyAlarmPresentInPageWhenTrapEdit()).toBeTruthy();

        // // Detele Trap Destination
        // await browser.waitForAngularEnabled(true);
        // await pageAdminTrapDestinations.ClearTextSearch();
        // await pageAdminTrapDestinations.settextSearch(data.TRAPDESTINATION.TC_1.TRAPNAMECHANGE);
        // await pageAdminTrapDestinations.ClickDeleteButton();
        // await pageAdminTrapDestinations.ClickConfirmYesDeleteButton();
        // await pageAdminTrapDestinations.ClearTextSearch();
        // await browser.sleep(2000);
        // await browser.waitForAngularEnabled(false);
        // await expect(pageAdminTrapDestinations.verifyAlarmDisplayWhenTrapToDelete()).toBeTruthy();
    // });


});
