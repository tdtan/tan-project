import { browser, ExpectedConditions} from 'protractor';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';
import { PageVNFMConfTenants } from '../PageObject/PageVNFMConfTenants.po';
import { PageAddTenant } from '../PageObject/PageAddTenant.po';
import { PageEditTenant } from '../PageObject/PageEditTenant.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('testcase vnf VNFM Configution Tenant', () => {
  let confCloud: PageVNFMConfCloudSystems;
  let navigation: PageNavigation;
  let confTenant: PageVNFMConfTenants;
  let addTenant: PageAddTenant;
  let editTenant: PageEditTenant;
  let pageError: PageError;


  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
    confCloud = new PageVNFMConfCloudSystems();
    navigation = new PageNavigation();
    confTenant = new PageVNFMConfTenants();
    addTenant = new PageAddTenant();
    editTenant = new PageEditTenant();
    pageError = new PageError();
  });
  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if ( result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
  });

  // 1 - Verify that the Empty form is displayed when user click on Add Tenant button
  it('Verify that the Empty form is displayed when user click on Add Tenant button - 1 ', async function () {
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 3000);
    await browser.waitForAngularEnabled(true);
    await confTenant.clickAddTenantbtn();
    await addTenant.verifyEmptyDisplayed('cloudTenantName');
    await addTenant.verifyEmptyDisplayed('cloudUserName');
    await addTenant.verifyEmptyDisplayed('userPassword');
    // await addTenant.clickCancel();
  });

  // 2 - Verify that the "missing required info" message was displayed when user forgot to input the required field when add tenant
  it('"missing required info" message was displayed when user forgot to input the required field when add tenant - 2', async function () {
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await addTenant.clickSaveTenant();
    await addTenant.verifyMissingRequiredInfo('Cloud Tenant Name');
    await addTenant.verifyMissingRequiredInfo('Cloud User Name');
    await addTenant.verifyMissingRequiredInfo('User Password');
  });

  // 3 - Verify that the Add tenant function can not complete when missing required info
  it('Verify that the Add tenant function can not complete when missing required info - 3', async function () {
    await addTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_3.TENANT);
    await addTenant.inputTenantInfo(3, data.TENANT_SYSTEM.TC_3.USER);
    await addTenant.clickAssociatedCloud();
    await addTenant.SelectCloud(data.TENANT_SYSTEM.TC_3.ASSOCIATED);
    await addTenant.clickSaveTenant();
    await addTenant.verifyMissingRequiredInfo('User Password');
    await addTenant.clickCancel();
  });

  // 4 - Verify that the "Associated Cloud" display with all of added cloud
  it('Verify that the "Associated Cloud" display with all of added cloud - 4', async function () {
    // await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const numRecord = await confCloud.getRecordNumber();
    const numRecordNew = numRecord.slice(13);
    await console.log('number record: ' + numRecordNew);
    const numberRecordTotal = Number(numRecordNew.replace(' records' , ''));
    await console.log('string: ' + numberRecordTotal);

    // await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 3000);
    await browser.waitForAngularEnabled(true);
    await confTenant.clickAddTenantbtn();
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if ( result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
    await addTenant.clickAssociatedCloud();
    const cloud = await addTenant.countRowNumber();
    await console.log('number cloud: ' + cloud);
    await expect(Boolean(numberRecordTotal === cloud)).toBe(true);
    // await addTenant.clickCancel();
  });

  // 5 - Verify that the Add tenant function can close the table when click on cancel button
  it('Verify that the Add tenant function can close the table when click on cancel button - 5 ', async function () {
    // await navigation.selectLeftNavSecond(4, 2);
    // await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 3000);
    // await browser.waitForAngularEnabled(true);
    // await confTenant.clickAddTenantbtn();
    await addTenant.verifyShowTextAddTenant();
    await addTenant.clickCancel();
    await addTenant.verifyAddCloudNotPresent();
  });

  // 6 - Verify that the Add tenant function can work successfully when fill all the field with correct info
  it('Verify that the Add tenant function can work successfully when fill all the field with correct info - 6', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.clickAddTenantbtn();
    await addTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_6.TENANT);
    await addTenant.inputTenantInfo(3, data.TENANT_SYSTEM.TC_6.USER);
    await addTenant.inputTenantInfo(4, data.TENANT_SYSTEM.TC_6.PASSWORD);
    await addTenant.clickAssociatedCloud();
    await addTenant.SelectCloud(data.TENANT_SYSTEM.TC_6.ASSOCIATED);
    await addTenant.clickSaveTenant();
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_6.TENANT);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_6.TENANT);
    await confTenant.cleartextSearch();
    // await confTenant.clickCleartTextSearch();
  });

  // 7 - Verify that the Tenants table can update the values after Added tenant
  it('Verify that the Tenants table can update the values after Added tenant - 7', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.clickAddTenantbtn();
    await addTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_7.TENANT);
    await addTenant.inputTenantInfo(3, data.TENANT_SYSTEM.TC_7.USER);
    await addTenant.inputTenantInfo(4, data.TENANT_SYSTEM.TC_7.PASSWORD);
    await addTenant.clickAssociatedCloud();
    await addTenant.SelectCloud(data.TENANT_SYSTEM.TC_7.ASSOCIATED);
    await addTenant.clickSaveTenant();
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if ( result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_7.TENANT);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_7.TENANT);
    await confTenant.clickCleartTextSearch();
  });

  // 8 - Verify that the Show Hide Filter button work properly
  it(' Verify that the Show Hide Filter button work properly - 8', async function () {
    // browser.driver.navigate().refresh();
    await browser.waitForAngularEnabled(true);
    await confTenant.clickShowHideFilter();
    await confTenant.verifyFilterDisplayed();
    await confTenant.clickShowHideFilter();
    await confTenant.verifyFilterNotDisplayed();
  });

  // 9 - Verify that the dropdown filter and sort functions work properly with UID column
  it('the dropdown filter and sort functions work properly with UID column - 9', async function () {
    // browser.driver.navigate().refresh();
    await browser.waitForAngularEnabled(true);
    await confTenant.clickShowHideColumn();
    await confTenant.selectMultipleOptionNamefromShowHideColumnsList(data.TENANT_SYSTEM.TC_9.UID);
    await confTenant.selectMultipleOptionNamefromShowHideColumnsList(data.TENANT_SYSTEM.TC_9.STATUS);
    // await confTenant.selectMultipleOptionNamefromShowHideColumnsList(data.TENANT_SYSTEM.TC_9.DATEADDED);
    await confTenant.clickCloseShowHide();
    await confTenant.verifysort(1);

    const GetUID1 = await confTenant.GetTextInTd(1, 1);
    await confTenant.clickShowHideFilter();
    await confTenant.clickLableFilter(2);
    await confTenant.settextSearchInFilter(GetUID1);
    await confTenant.clickCheckBoxSelectAll();
    await confTenant.clickCloseFilter();
    const recordFilter1 = await confTenant.GetRecordCurrent();
    await expect(Boolean(recordFilter1 === 'Showing 1 of 1 record')).toBe(true);
    await confTenant.ClickClearFilterButton();
  });

  // 13 - Verify that the dropdown filter and sort functions work properly with Status column
  it('Verify that the dropdown filter and sort functions work properly with Status column - 13', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.SelectFilter(5, data.TENANT_SYSTEM.TC_13.STATUS);
    await confTenant.verifyText(5, data.TENANT_SYSTEM.TC_13.STATUS);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_13.RECORD);
    await confTenant.ClickClearFilterButton();
    // await confTenant.verifysort(5);
  });

  // 10 - Verify that the dropdown filter and sort functions work properly with Tenant column
  it('Verify that the dropdown filter and sort functions work properly with Tenant column - 10', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.ClickClearFilterButton();
    await confTenant.verifysort(2);
    await confTenant.SelectFilter(2, data.TENANT_SYSTEM.TC_10.TENANT);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_10.TENANT);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_10.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 11 - Verify that the dropdown filter and sort functions work properly with Cloud User column
  it('the dropdown filter and sort functions work properly with Cloud User column - 11', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(3);
    await confTenant.SelectFilter(3, data.TENANT_SYSTEM.TC_11.USER);
    await confTenant.verifyText(3, data.TENANT_SYSTEM.TC_11.USER);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_11.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 12 - Verify that the dropdown filter and sort functions work properly with Target Cloud column
  it('Verify that the dropdown filter and sort functions work properly with Target Cloud column - 12', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(4);
    await confTenant.SelectFilter(4, data.TENANT_SYSTEM.TC_12.TARGET);
    await confTenant.verifyText(4, data.TENANT_SYSTEM.TC_12.TARGET);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_12.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 14 - Verify that the dropdown filter and sort functions work properly with VMs column
  it('Verify that the dropdown filter and sort functions work properly with VMs column - 14', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(7);
    const txtVMs = await confTenant.GetTextInTd(1, 7);
    await console.log('Text VMs: ' + txtVMs);
    await confTenant.SelectFilter(7, txtVMs);
    await confTenant.verifyText(7, txtVMs);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_14.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 15 - Verify that the dropdown filter and sort functions work properly with vCPU column
  it('Verify that the dropdown filter and sort functions work properly with vCPU column - 15', async function () {
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(8);
    const txtvCPU = await confTenant.GetTextInTd(1, 8);
    await console.log('Text vCPU: ' + txtvCPU);
    await confTenant.SelectFilter(8, txtvCPU);
    await confTenant.verifyText(8, txtvCPU);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_15.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 16 - Verify that the dropdown filter and sort functions work properly with RAM column
  it('Verify that the dropdown filter and sort functions work properly with RAM column - 16', async function () {
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(9);
    const txtRAM = await confTenant.GetTextInTd(1, 9);
    await console.log('Text RAM: ' + txtRAM);
    await confTenant.SelectFilter(9, txtRAM);
    await confTenant.verifyText(9, txtRAM);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_16.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 17 - Verify that the dropdown filter and sort functions work properly with Floating column
  it('Verify that the dropdown filter and sort functions work properly with Floating column -17', async function () {
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(10);
    const txtFloatingIP = await confTenant.GetTextInTd(1, 10);
    await console.log('Text FloatingIP: ' + txtFloatingIP);
    await confTenant.SelectFilter(10, txtFloatingIP);
    await confTenant.verifyText(10, txtFloatingIP);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_17.RECORD);
    await confTenant.ClickClearFilterButton();
  });

  // 18 - Verify that the dropdown filter and sort functions work properly with VNFs column
  it('Verify that the dropdown filter and sort functions work properly with VNFs column - 18', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.verifysort(11);
    const txtVNFs = await confTenant.GetTextInTd(1, 11);
    await console.log('Text FloatingIP: ' + txtVNFs);
    await confTenant.SelectFilter(11, txtVNFs);
    await confTenant.verifyText(11, txtVNFs);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_18.RECORD);
    await confTenant.ClickClearFilterButton();
  });

   // 19 - Verify that the Date filter work properly with Date Added column
   it('Verify that the Date filter work properly with Date Added column -19', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.clickShowHideColumn();
    await confTenant.selectMultipleOptionNamefromShowHideColumnsList(data.TENANT_SYSTEM.TC_21.DATEADDED);
    await confTenant.clickCloseShowHide();
    await browser.waitForAngularEnabled(true);
    await confTenant.clickDateAdded();
    await confTenant.clickLast5HourButtonInForm();
    await browser.waitForAngularEnabled(true);
    const result = await confTenant.isTableNoValueAfterSearch();
    await expect(Boolean(result === true)).toBe(true);
    await confTenant.clickDateAdded();
    await confTenant.clickDateClear();
  });

  // 20 - Verify that the Clear Filter button work properly
  it(' Verify that the Clear Filter button work properly - 20', async function () {
    await browser.waitForAngularEnabled(true);
    const record = await confTenant.GetRecordCurrent();
    await confTenant.SelectFilter(5, data.TENANT_SYSTEM.TC_13.STATUS);
    await confTenant.verifyText(5, data.TENANT_SYSTEM.TC_13.STATUS);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_13.RECORD);
    await confTenant.ClickClearFilterButton();
    const record1 = await confTenant.GetRecordCurrent();
    await expect(Boolean(record === record1)).toBe(true);
  });

  // 21 - Verify that user can select the number of displayed columns
  it('Verify that user can select the number of displayed columns - 21', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await navigation.selectLeftNavSecond(4, 2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 3000);
    await browser.waitForAngularEnabled(true);
    const result2 = await confTenant.verifyShowHidecolumns(data.TENANT_SYSTEM.TC_21.UID);
    console.log('column 1:' + result2);
    await expect(Boolean(result2 === true)).toBe(true);

    await confTenant.clickShowHideColumn();
    await confTenant.selectMultipleOptionNamefromShowHideColumnsList(data.TENANT_SYSTEM.TC_21.UID);
    await confTenant.clickCloseShowHide();
    await browser.waitForAngularEnabled(true);
    const result5 = await confTenant.verifyShowHidecolumns(data.TENANT_SYSTEM.TC_21.UID);
    console.log('column 1:' + result5);
    await expect(Boolean(result5 === false)).toBe(true);
  });

  // 22 - Verify that the Search function work properly
  it(' Verify that the Search function work properly - 22', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.cleartextSearch();
    await confTenant.settextSearch('send error text');
    const result = await confTenant.isTableNoValueAfterSearch();
    await expect(Boolean(result === false)).toBe(true);
    await confTenant.cleartextSearch();
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_22.SEARCH_NAME);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_22.SEARCH_NAME);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_22.RECORD);
    await confTenant.clickCleartTextSearch();
  });

  // 23 - Verify that the Clear Search button work properly
  it(' Verify that the Clear Search button work properly - 23', async function () {
    await browser.waitForAngularEnabled(true);
    const record = await confTenant.GetRecordCurrent();
    await confTenant.cleartextSearch();
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_23.SEARCH);
    await confTenant.verifyRecord(data.TENANT_SYSTEM.TC_23.RECORD);
    await confTenant.clickCleartTextSearch();
    const record1 = await confTenant.GetRecordCurrent();
    await expect(Boolean(record === record1)).toBe(true);
  });

  // // 16.Verify that the Refresh button work properly
  // it(' Verify that the Refresh button work properly - 16', async function () {
  //   await navigation.selectLeftNavSecond(4, 2);
  //   await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 3000);
  //   const record = await confTenant.GetRecordCurrent();
  //   // await confTenant.SelectFilter(2, data.TENANT_SYSTEM.TC_16.SEARCH);
  //   await confTenant.settextSearch(data.TENANT_SYSTEM.TC_16.SEARCH);
  //   await confTenant.clickRefesh();
  //   await confTenant.verifyRecord(record);
  // });

  // 24 - Verify that the table display with maximum 5 entries in one page as default
  it('the table display with maximum 5 entries in one page as default - 24', async function () {
    const numbRow = await confTenant.GetNumberRowInPage();
    await expect(Boolean(numbRow === '5')).toBe(true);
    await console.log('Record default: ' + numbRow);
    const totalrow = await confTenant.countRowNumber();
    await expect(Boolean(totalrow <= 5)).toBe(true);
    await console.log('Record current: ' + totalrow);
  });

  // 25 - Verify that the user can choose the number of entries displayed in one page
  it('the user can choose the number of entries displayed in one page -25', async function () {
    await confTenant.SelectNumberRowDisplay(15);
    const result2 = await confTenant.countRowNumber();
    await expect(Boolean(result2 <= 15)).toBe(true);
    await confTenant.SelectNumberRowDisplay(5);
    const result3 = await confTenant.countRowNumber();
    await expect(Boolean(result3 <= 5)).toBe(true);
  });

  // 26 - Verify that the user can move to specified page when click on the page number
  it('the user can move to specified page when click on the page number - 26', async function () {
    const record = await confTenant.GetRecordCurrent();
    const numRecord = Number(record.slice(0, 1));
    const numbRow = Number(await confTenant.GetNumberRowInPage());
    if (numRecord > numbRow) {
      await confTenant.clickNumbertoMovePage(2);
      const numPage = await confTenant.getCurentPageActive();
      await expect(Boolean(numPage === '2')).toBe(true);
      await confTenant.clickNumbertoMovePage(1);
    }
  });

  // 27 - Verify that the info of selected tenant is displayed when user click on View/Edit action
  it('Verify that the info of selected tenant is displayed when user click on View/Edit action - 27', async function () {
    // await navigation.selectLeftNav(4);
    // await navigation.selectLeftNavSecond(4, 2);
    // await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 3000);
    await browser.waitForAngularEnabled(true);
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_27.TENANT);
    await confTenant.clickEditTenant();
    await editTenant.verifyInfoDisplayed('cloudTenantName');
    await editTenant.verifyInfoDisplayed('cloudUserName');
    await editTenant.clickCancel();
  });

  // 28 - Verify that the Tenant Usage can display the resource of selected tenant
  it('Verify that the Tenant Usage  can display the resource of selected tenant - 28', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.clickEditTenant();
    await editTenant.verifyUsageInfo('Instance Count (VNFCs)');
    await editTenant.verifyUsageInfo('vCPU Count');
    await editTenant.verifyUsageInfo('RAM Usage');
    await editTenant.verifyUsageInfo('Floating IP Count');
    await editTenant.verifyUsageInfo('VNFs');
    // await editTenant.clickCancel();
  });

  // 29 - Verify that the Tenant Usage chart displayed with corresponds to the color-coded table columns
  it('Verify that the Tenant Usage chart displayed with corresponds to the color-coded table columns - 29', async function () {
    // await confTenant.clickEditTenant();
    const rgbColor = await editTenant.getColor(5);
    const hexColor = await editTenant.convertRgb2hex(rgbColor);
    console.log('hex color: ', hexColor);
    const percent1 = await editTenant.getUsageNumber(5);
    const percent2 = await editTenant.getUsageNumber2(5);
    const Green = '#4f8a10';
    const Yellow = '#d1c101';
    const Orange = '#fe7f0e';
    const Red = '#ca3f3f';
    if (percent1 <= 9) {
      expect(Boolean(hexColor === Green)).toBe(true);
    } else if (percent2 <= 25) {
      expect(Boolean(hexColor === Green)).toBe(true);
    } else if (percent2 <= 50) {
      expect(Boolean(hexColor === Yellow)).toBe(true);
    } else if (percent2 <= 75) {
      expect(Boolean(hexColor === Orange)).toBe(true);
    } else if (percent2 > 75) {
      expect(Boolean(hexColor === Red)).toBe(true);
    }
    await editTenant.clickCancel();
  });

  // 30 - Verify that the "missing required info" message was displayed when user forgot to input the required field when edit tenant
  it('the "missing required info" message was displayed when user forgot to input the required field -30', async function () {
    await confTenant.clickEditTenant();
    await editTenant.clickTenant(1);
    await editTenant.ClearInfoTenantEdit(1);
    await editTenant.clickTenant(3);
    await editTenant.ClearInfoTenantEdit(3);
    await editTenant.clickSaveTenant();
    await expect(editTenant.verifyMissingRequiredInfo('Cloud Tenant Name')).toBeTruthy();
    await expect(editTenant.verifyMissingRequiredInfo('Cloud User Name')).toBeTruthy();
    // await editTenant.clickCancel();
  });

  // 31 - Verify that the Edit tenant function can not complete when missing required info
  it('Verify that the Edit tenant function can not complete when missing required info - 31', async function () {
    // await confTenant.clickEditTenant();
    await editTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_31.TENANT);
    await editTenant.clickTenant(3);
    await editTenant.ClearInfoTenantEdit(3);
    await editTenant.clickSaveTenant();
    await expect(editTenant.verifyMissingRequiredInfo('Cloud User Name')).toBeTruthy();
    // await editTenant.clickCancel();
  });

  // 32 - Verify that the Edit Tenant function can close the table when click on cancel button
  it('Verify that the Edit Tenant function can close the table when click on cancel button -32', async function () {
    // await confTenant.clickEditTenant();
    await editTenant.verifyShowTextEditTenant();
    await editTenant.clickCancel();
    await editTenant.verifyEditCloudNoPresent();
  });

  // // 33 - Verify that the Edit tenant function can work successfully when fill all the fields with correct info
  // it('Verify that the Edit tenant function can work successfully when fill all the fields with correct info - 33', async function () {
  //   await browser.waitForAngularEnabled(true);
  //   await confTenant.clickEditTenant();
  //   await editTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_33.TENANT);
  //   await editTenant.inputTenantInfo(3, data.TENANT_SYSTEM.TC_33.USER);
  //   await editTenant.inputTenantInfo(4, data.TENANT_SYSTEM.TC_33.PASSWORD);
  //   await editTenant.clickAssociatedCloud();
  //   await editTenant.SelectCloud(data.TENANT_SYSTEM.TC_33.ASSOCIATED);
  //   await editTenant.clickSaveTenant();
  //   // await confTenant.cleartextSearch();
  //   // await confTenant.settextSearch(data.TENANT_SYSTEM.TC_33.TENANT);
  //   // await confTenant.verifyText(1, data.TENANT_SYSTEM.TC_33.TENANT);
  // });

  // // 34 - Verify that the Tenants table can update the values after Edited tenant
  // it('Verify that the Tenants table can update the values after Edited tenant - 34', async function () {
  //   await confTenant.cleartextSearch();
  //   await confTenant.settextSearch(data.TENANT_SYSTEM.TC_34.TENANT);
  //   await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_34.TENANT);
  //   await confTenant.clickCleartTextSearch();
  // });

  // 35 - Verify that the Delete tenant function can terminate when click on cancel button
  it('Verify that the Delete tenant function can terminate when click on cancel button - 35', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/tenant');
    await browser.waitForAngularEnabled(true);
    await confTenant.cleartextSearch();
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_35.TENANT);
    await confTenant.clickSelect();
    await confTenant.clickDelete();
    await confTenant.clickConfirmCancelDelete();
    await confTenant.cleartextSearch();
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_35.TENANT);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_35.TENANT);

  });

  // 36 - Verify that the Delete tenant function can work properly when click on delete button
  it('Verify that the Delete tenant function can work properly when click on delete button - 36', async function () {
    await confTenant.clickSelect();
    await confTenant.clickDelete();
    await confTenant.clickConfirmDelete();
    await confTenant.cleartextSearch();
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_36.TENANT);
    const result = await confTenant.isTableNoValueAfterSearch();
    await expect(Boolean(result === false)).toBe(true);
    // await confTenant.clickCleartTextSearch();
  });

  // 37 - Add Tenant for deploy VNF
  it('Add Tenant for deploy VNF - 37', async function () {
    await browser.waitForAngularEnabled(true);
    await confTenant.clickAddTenantbtn();
    await addTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_37.TENANT);
    await addTenant.inputTenantInfo(3, data.TENANT_SYSTEM.TC_37.USER);
    await addTenant.inputTenantInfo(4, data.TENANT_SYSTEM.TC_37.PASSWORD);
    await addTenant.clickAssociatedCloud();
    await addTenant.SelectCloud(data.TENANT_SYSTEM.TC_37.ASSOCIATED);
    await addTenant.clickSaveTenant();
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if ( result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_37.TENANT);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_37.TENANT);

    await browser.waitForAngularEnabled(true);
    await confTenant.clickAddTenantbtn();
    await addTenant.inputTenantInfo(1, data.TENANT_SYSTEM.TC_37.TENANT2);
    await addTenant.inputTenantInfo(3, data.TENANT_SYSTEM.TC_37.USER2);
    await addTenant.inputTenantInfo(4, data.TENANT_SYSTEM.TC_37.PASSWORD2);
    await addTenant.clickAssociatedCloud();
    await addTenant.SelectCloud(data.TENANT_SYSTEM.TC_37.ASSOCIATED2);
    await addTenant.clickSaveTenant();
    const result4 = await pageError.verifyMessegeError();
    const result5 = await pageError.verifyMessegeWarning();
    const result6 = await pageError.verifyMessegeInfo();
    if ( result4 === true || result5 === true || result6 === true) {
      await pageError.cliclClearALL();
    }
    await confTenant.settextSearch(data.TENANT_SYSTEM.TC_37.TENANT2);
    await confTenant.verifyText(2, data.TENANT_SYSTEM.TC_37.TENANT2);
  });


});
