import { PageNavigation } from '../PageObject/PageNavigation.po';
import { browser, ExpectedConditions} from 'protractor';
import { PageBulkAction } from '../PageObject/PageBullkAction.po';
// const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Security Realms', () => {
  let pageNavigation: PageNavigation;
  let pageBulkAction: PageBulkAction;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageNavigation = new PageNavigation();
    pageBulkAction = new PageBulkAction();
  });

  // 1- Verify that the Bulk Action drop-list is disable by default
  it('Verify that the Bulk Action drop-list is disable by default- 1', async function () {
    await pageNavigation.selectLeftNav(3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 4000);
    await browser.waitForAngularEnabled(true);
    await expect(pageBulkAction.verifyElementDisable()).toBeFalsy();
  });

  // 2- Verify that the Bulk Action drop-list only enable when two or more VNF checkboxes selected
  it('Verify that the Bulk Action drop-list only enable when two or more VNF checkboxes selected- 2', async function () {
    await browser.waitForAngularEnabled(true);
    const record = await pageBulkAction.getRecord();
    const numRecord = Number(record.replace(' records found', ''));
    await expect(Boolean(numRecord > 1)).toBe(true);
    for (let i = 1 ; i <= numRecord ; i++) {
        await pageBulkAction.clickVNFcheckbox(i);
    }
    await expect(pageBulkAction.verifyElementDisable()).toBeTruthy();
  });
});
