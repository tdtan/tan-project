// import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageEditUser } from '../PageObject/PageEditUser.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageUsers } from '../PageObject/PageUsers.po';
import { PageAdminRoles } from '../PageObject/PageAdminRole.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin Edit Users', () => {
    let pageNavigation: PageNavigation;
    let pageEditUser: PageEditUser;
    let pageUsers: PageUsers;
    let pageAdminRoles: PageAdminRoles;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageNavigation = new PageNavigation();
        pageEditUser = new PageEditUser();
        pageUsers = new PageUsers();
        pageAdminRoles = new PageAdminRoles();
        pageError = new PageError();
    });
    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });


    // 1 - Verify that the View/Edit user function can work successfully when fill all the field with correct info
    it('Verify that the View/Edit user function can work successfully when fill all the field with correct info - 1 ', async function () {
        await pageNavigation.selectLeftNav(2);
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/users'), 3000);
        await browser.waitForAngularEnabled(true);

        await pageUsers.setSearch(data.ADMIN_EDITUSER.TC_1.INPUT_TEXT);
        await pageUsers.verifyRecord(data.ADMIN_EDITUSER.TC_1.RECORD);
        await pageEditUser.clickUser();
        await pageEditUser.verifyAfterClickUserName();
        await pageEditUser.setPassword(data.ADMIN_EDITUSER.TC_1.INPUT_PASSWORD);
        await pageEditUser.verifyAfterClickSave();
    });

    // // 2 - Verify that the View/Edit user function can not complete when missing required info
    // it('Verify that the View/Edit user function can not complete when missing required info - 2 ', async function () {
    //     await browser.waitForAngularEnabled(true);
    //     await pageEditUser.clickUser();
    //     await pageEditUser.verifyAfterClickUserName();
    //     await pageEditUser.clickSave();
    //     await expect(pageEditUser.verifyMissingInfo()).toBeTruthy();
    // });

    // // 3 - Verify that the "missing required info" message was displayed when user forgot to input the required field when edit user
     // tslint:disable-next-line:max-line-length
    // it('Verify that the "missing required info" message was displayed when user forgot to input the required field when edit user - 3 ', async function () {
    //     await pageEditUser.verifyDisplayMissingInfoMessage();
    // });

    // 4 - Verify that can not edit user name of an user
    it('Verify that can not edit user name of an user - 4 ', async function () {
        await browser.waitForAngularEnabled(true);
        await pageEditUser.clickUser();
        await expect(pageEditUser.verifyUserNameIsEnabled()).toBeFalsy();
    });

    // 5 - Verify that the View/Edit function can close the table when click on cancel button
    it('Verify that the View/Edit function can close the table when click on cancel button - 5 ', async function () {
        await browser.waitForAngularEnabled(true);
        await pageEditUser.clickCancel();
        await expect(pageEditUser.verifyMissingInfo()).toBeFalsy();
    });

    // 6 - Verify that the Delete user function can terminate when click on cancel button
    it('Verify that the Delete user function can terminate when click on cancel button - 6 ', async function () {
        await pageUsers.setSearch(data.ADMIN_EDITUSER.TC_6.INPUT_TEXT);
        await pageUsers.verifyRecord(data.ADMIN_EDITUSER.TC_6.RECORD);

        await pageUsers.clickDelete();
        await pageUsers.clickConfirmCancel();
        await pageUsers.setSearch(data.ADMIN_EDITUSER.TC_6.INPUT_TEXT);
        await pageUsers.verifyRecord(data.ADMIN_EDITUSER.TC_6.RECORD);
    });

    // 7 - Verify that the Delete user function can work properly when click on delete button
    it('Verify that the Delete user function can work properly when click on delete button - 7 ', async function () {
        await pageUsers.clickDelete();
        await pageUsers.clickConfirmDelete();
        await pageUsers.setSearch(data.ADMIN_EDITUSER.TC_7.INPUT_TEXT);
        await pageUsers.verifyRecord(data.ADMIN_EDITUSER.TC_7.RECORD);
    });

    it('Clear Role Page', async function () {
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 2);
        browser.wait(ExpectedConditions.urlContains('vnfm/roles'), 3000);
        await browser.waitForAngularEnabled(true);

        await pageAdminRoles.removeAllRole();
        const result = await pageAdminRoles.isTableHasValueAfterSearch();
        await expect(Boolean(result === false)).toBeTruthy();
    });
});
