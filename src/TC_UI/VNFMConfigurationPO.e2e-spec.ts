import { browser, ExpectedConditions } from 'protractor';
import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';
import { PageAddCloud } from '../PageObject/PageAddCloud.po';
import { PageEditCloud } from '../PageObject/PageEditCloud.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('testcase vnf VNFM Configution Cloud', () => {
  let login: PageLogin;
  let confCloud: PageVNFMConfCloudSystems;
  let navigation: PageNavigation;
  let addCloud: PageAddCloud;
  let editCloud: PageEditCloud;
  let pageError: PageError;

  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
    login = new PageLogin();
    confCloud = new PageVNFMConfCloudSystems();
    navigation = new PageNavigation();
    addCloud = new PageAddCloud();
    editCloud = new PageEditCloud();
    pageError = new PageError();

  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if ( result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
  });

  it('login VNFM', async function () {

    await browser.restart();
    await browser.get(data.INFOLOGIN.URL);
    browser.driver.manage().window().maximize();
    await browser.waitForAngularEnabled(true);
    await login.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
  });

  it('VNFM Configuration', async function () {
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.waitForAngularEnabled(true);
  });


  // 0.Verify that the Add cloud function can work successfully when fill all the fields with correct info
  it('the Add cloud function can work successfully when fill all the fields with correct info -0', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_0.CLOUD);
    const result2 = await confCloud.isTableHasValueAfterSearch();
    if (result2) {
      await confCloud.clickSelect();
      await confCloud.clickDelete();
      await confCloud.clickConfirmDelete();
      await confCloud.clearTextSearch();
      await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_0.CLOUD);
      const result3 = await confCloud.isTableHasValueAfterSearch();
      await expect(Boolean(result3 === false)).toBe(true);
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.inputCloudConfig(3, data.CLOUD_SYSTEM.TC_0.CLOUD);
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_0.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_0.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_0.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_0.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_0.IMAGE);

    await addCloud.clickGetVersion();
    await browser.waitForAngularEnabled(true);
    await addCloud.selectVersionCloud(5, 'v2.0');
    await addCloud.selectVersionCloud2(7, 'v2');
    await addCloud.selectVersionCloud(9, 'v2.0');
    await addCloud.selectVersionCloud(11, 'v2');
    await addCloud.selectVersionCloud(13, 'v2.0');
    await addCloud.clickCloudConfig(3);
    await addCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_0.CLOUD);
    const record = await confCloud.getRecordNumber();
    await expect(Boolean(record === 'Showing 1 of 1 record')).toBe(true);
  });

 // 36 - Verify that the filter row of table is show/hide when user click on the “Filter” button
 it('Verify that the filter row of table is show/hide when user click on the “Filter” button -36', async function () {
  await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.waitForAngularEnabled(true);
  await expect(confCloud.verifyFilterRowDisplay()).toBeFalsy();
  await confCloud.clickShowHideFilter();
  await expect(confCloud.verifyFilterRowDisplay()).toBeTruthy();
 });

  // 1.Verify that the dropdown filter and sort functions work properly with UID column
  it('the dropdown filter and sort functions work properly with UID column -1', async function () {
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickShowHideColumn();
    await confCloud.selectMultiOptionNamefromShowHideColumnsList('UID');
    await confCloud.selectMultiOptionNamefromShowHideColumnsList('Date Added');
    await confCloud.clickCloseShowHide();

    await confCloud.verifysort(1);
    const getUID = await confCloud.GetTextInTd(1);
    // await confCloud.clickShowHideFilter();
    await confCloud.SelectFilterWithCheckbox(1, getUID);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_1.RECORD);
    await confCloud.SelectFilterWithCheckbox(1, getUID);
  });


  // 2.Verify that the dropdown filter and sort functions work properly with Cloud column
  it('the dropdown filter and sort functions work properly with Cloud column -2', async function () {
    await confCloud.verifysort(2);
    await confCloud.SelectFilterWithCheckbox(2, data.CLOUD_SYSTEM.TC_2.CLOUD);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_2.RECORD);
    await confCloud.SelectFilterWithCheckbox(2, data.CLOUD_SYSTEM.TC_2.CLOUD);
  });


  // 3.Verify that the dropdown filter and sort functions work properly with Keystone column
  it('the dropdown filter and sort functions work properly with Keystone column -3', async function () {
    await confCloud.SelectFilterWithCheckbox(3, data.CLOUD_SYSTEM.TC_3.KEYSTONE);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_3.RECORD);
    await confCloud.SelectFilterWithCheckbox(3, data.CLOUD_SYSTEM.TC_3.KEYSTONE);
    await confCloud.verifysort(3);
  });

  // 4.Verify that the dropdown filter and sort functions work properly with Compute column
  it('the dropdown filter and sort functions work properly with Compute column -4', async function () {
    await confCloud.verifysort(4);
    await confCloud.SelectFilterWithCheckbox(4, data.CLOUD_SYSTEM.TC_4.COMPUTE);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_4.RECORD);
    await confCloud.SelectFilterWithCheckbox(4, data.CLOUD_SYSTEM.TC_4.COMPUTE);
  });

  // 5.Verify that the dropdown filter and sort functions work properly with Network column
  it('the dropdown filter and sort functions work properly with Network column -5', async function () {
    await confCloud.verifysort(5);
    await confCloud.SelectFilterWithCheckbox(5, data.CLOUD_SYSTEM.TC_5.NETWORK);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_5.RECORD);
    await confCloud.SelectFilterWithCheckbox(5, data.CLOUD_SYSTEM.TC_5.NETWORK);
  });

  // 6.Verify that the dropdown filter and sort functions work properly with Image column
  it('the dropdown filter and sort functions work properly with Image column -6', async function () {
    await confCloud.verifysort(6);
    await confCloud.SelectFilterWithCheckbox(6, data.CLOUD_SYSTEM.TC_6.IMAGE);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_6.RECORD);
    await confCloud.SelectFilterWithCheckbox(6, data.CLOUD_SYSTEM.TC_6.IMAGE);
  });

  // 7.Verify that the dropdown filter and sort functions work properly with Cinder column
  it('the dropdown filter and sort functions work properly with Cinder column -7', async function () {
    await confCloud.verifysort(7);
    await confCloud.SelectFilterWithCheckbox(7, data.CLOUD_SYSTEM.TC_7.CINDER);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_7.RECORD);
    await confCloud.SelectFilterWithCheckbox(7, data.CLOUD_SYSTEM.TC_7.CINDER);
  });

  // 8.Verify that the sort functions work properly with Date Added column
  it('the sort functions work properly with Date Added column- 8', async function () {
    await confCloud.verifysort(8);
  });

  // 9.Verify that user can select the number of displayed columns
  it('Verify that user can select the number of displayed columns -9', async function () {
    // await expect(confCloud.verifyShowHidecolumns(data.CLOUD_SYSTEM.TC_9.UID)).toBeTruthy();
    await expect(confCloud.verifyShowHidecolumns(data.CLOUD_SYSTEM.TC_9.DATE)).toBeTruthy();
    await confCloud.clickShowHideColumn();
    // await confCloud.selectMultiOptionNamefromShowHideColumnsList(data.CLOUD_SYSTEM.TC_9.UID);
    await confCloud.selectMultiOptionNamefromShowHideColumnsList(data.CLOUD_SYSTEM.TC_9.DATE);
    await confCloud.clickCloseShowHide();
    // await expect(confCloud.verifyShowHidecolumns(data.CLOUD_SYSTEM.TC_9.UID)).toBeFalsy();
    await expect(confCloud.verifyShowHidecolumns(data.CLOUD_SYSTEM.TC_9.DATE)).toBeFalsy();
  });

  // 10.Verify that the Search function work properly
  it(' Verify that the Search function work properly - 10', async function () {

    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clearTextSearch();
    await confCloud.settextSearch('send error text');
    await confCloud.isTableNoValueAfterSearch();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_10.SEARCH_NAME);
    await confCloud.verifyText(2, data.CLOUD_SYSTEM.TC_10.SEARCH_NAME);
    await confCloud.verifyRecord(data.CLOUD_SYSTEM.TC_10.RECORD);
    await confCloud.clearTextSearch();
  });

  // 11.Verify that the Clear table button work properly
  it(' Verify that the Clear table button work properly - 11', async function () {
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_11.SEARCH);
    await confCloud.clickButtonClear();
    const value = await confCloud.getAttributeSearch();
    await expect(Boolean(value === '')).toBe(true);
  });

  // // 12.Verify that the filter row of table is displayed the Refresh button work properly
  // it(' Verify that the filter row of table is displayed the Refresh button work properly - 12', async function () {

  //   await confCloud.SelectFilterWithCheckbox(2, data.CLOUD_SYSTEM.TC_2.CLOUD);
  //   await confCloud.clickClearButtonToClearTextFilter();
  //   const textFilter =  await confCloud.getTextFilter(2);
  //   await expect(Boolean(textFilter === 'All')).toBe(true);
  // });

  // 13.Verify that the user can move to specified page when click on the page number
  it('the user can move to specified page when click on the page number -13', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.verifyMovePage();
    await confCloud.clickRefesh();
  });

  // 14.Verify that the table display with maximum 5 entries in one page as default
  it('the table display with maximum 5 entries in one page as default -14', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    const numrow = await confCloud.getNumberRow();
    const numberRow = Number(numrow);
    await console.log('Default row display in one page: ' + numberRow);
    await expect(Boolean(numberRow === 5)).toBe(true);
  });

  // 15.Verify that the user can choose the number of entries displayed in one page
  it('the user can choose the number of entries displayed in one page -15', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.verifyNumberRowDisplayOnTable(data.CLOUD_SYSTEM.TC_15.NUMBER);
  });

  // 16.Verify that the Empty form is displayed when user click on Add cloud button
  it('the Empty form is displayed when user click on Add cloud button -16', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();

    await addCloud.verifyEmptyDisplayed(data.CLOUD_SYSTEM.TC_16.NAMEINPUT);
    const valueName = await confCloud.getAttributeOfElementInForm(data.CLOUD_SYSTEM.TC_16.NAMEINPUT);
    await expect(Boolean(valueName === '')).toBe(true);

    await addCloud.verifyEmptyDisplayed(data.CLOUD_SYSTEM.TC_16.IDENTITYINPUT);
    const valueIdentity = await confCloud.getAttributeOfElementInForm(data.CLOUD_SYSTEM.TC_16.IDENTITYINPUT);
    await expect(Boolean(valueIdentity === '')).toBe(true);

    await addCloud.verifyEmptyDisplayed(data.CLOUD_SYSTEM.TC_16.COMPUTERINPUT);
    const valueComputer = await confCloud.getAttributeOfElementInForm(data.CLOUD_SYSTEM.TC_16.COMPUTERINPUT);
    await expect(Boolean(valueComputer === '')).toBe(true);

    await addCloud.verifyEmptyDisplayed(data.CLOUD_SYSTEM.TC_16.NETWORKINPUT);
    const valueNetwok = await confCloud.getAttributeOfElementInForm(data.CLOUD_SYSTEM.TC_16.NETWORKINPUT);
    await expect(Boolean(valueNetwok === '')).toBe(true);

    await addCloud.verifyEmptyDisplayed(data.CLOUD_SYSTEM.TC_16.STRORAGEINPUT);
    const valueStorage = await confCloud.getAttributeOfElementInForm(data.CLOUD_SYSTEM.TC_16.STRORAGEINPUT);
    await expect(Boolean(valueStorage === '')).toBe(true);

    await addCloud.verifyEmptyDisplayed(data.CLOUD_SYSTEM.TC_16.IMAGEINPUT);
    const valueImage = await confCloud.getAttributeOfElementInForm(data.CLOUD_SYSTEM.TC_16.IMAGEINPUT);
    await expect(Boolean(valueImage === '')).toBe(true);
    await addCloud.clickCancel();
  });

  // 17.Verify that the Add cloud function can not complete when missing required info
  it('the Add cloud function can not complete when missing required info -17', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.clickCloudConfig(3);
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_17.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_17.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_17.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_17.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_17.IMAGE);
    await addCloud.clickGetVersion();
    await expect(addCloud.verifyButtonSaveDisable()).toBeFalsy();
    await addCloud.clickCancel();
  });

  // 18.Verify that the Add cloud function can close the table when click on cancel button
  it('the Add cloud function can close the table when click on cancel button- 18', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.verifyShowTextAddCloud();
    await addCloud.clickCancel();
    await addCloud.verifyAddCloudNoPresent();
  });

  // 19.Verify that the "Cloud Name" fields allow user to input everything when add cloud
  it('"Cloud Name" fields allow user to input everything -19', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.inputCloudConfig(3, data.CLOUD_SYSTEM.TC_19.CLOUD_INVALID);
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_19.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_19.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_19.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_19.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_19.IMAGE);
    await addCloud.clickGetVersion();
    await addCloud.selectVersionCloud(5, 'v2.0');
    await addCloud.selectVersionCloud2(7, 'v2');
    await addCloud.selectVersionCloud(9, 'v2.0');
    await addCloud.selectVersionCloud(11, 'v2');
    await addCloud.selectVersionCloud(13, 'v2.0');
    await addCloud.clickCloudConfig(3);
    await addCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_19.CLOUD_INVALID);
    const result2 = await confCloud.isTableHasValueAfterSearch();
    await expect(result2 === true).toBe(true);
  });

  // 20.Verify that all of API address fields only allow user to input the IP format info when add cloud
  it('All of API address fields only allow user to input the IP format info when add cloud -20', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.inputCloudConfig(3, 'Cloud_Test');
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_20.KEYSTONE_INVALID);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_20.NOVA_INVALID);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_20.NEUTRON_INVALID);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_20.CINDER_INVALID);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_20.IMAGE_INVALID);
    await addCloud.clickCloudConfig(3);
    await expect(addCloud.verifyButtonGetVersionDisable()).toBeFalsy();
    await addCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_20.MESSEGEKEY_INVALID);
    await addCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_20.MESSEGENOVA_INVALID);
    await addCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_20.MESSEGENEUTRON_INVALID);
    await addCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_20.MESSEGECINDER_INVALID);
    await addCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_20.MESSEGEIMAGE_INVALID);
    await addCloud.clickCancel();

    await confCloud.clickAddCloudbtn();
    await addCloud.inputCloudConfig(3, 'Cloud_Test');
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_20.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_20.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_20.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_20.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_20.IMAGE);
    await addCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_20.MESSEGEKEY_INVALID);
    await addCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_20.MESSEGENOVA_INVALID);
    await addCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_20.MESSEGENEUTRON_INVALID);
    await addCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_20.MESSEGECINDER_INVALID);
    await addCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_20.MESSEGEIMAGE_INVALID);
    await expect(addCloud.verifyButtonGetVersionDisable()).toBeTruthy();
    await addCloud.clickCancel();

  });

  // 21.Verify that the version of API address were displayed with correct values when add cloud
  it('Verify that the version of API address were displayed with correct values when add cloud -21', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await browser.waitForAngularEnabled(true);
    await addCloud.inputCloudConfig(3, data.CLOUD_SYSTEM.TC_21.CLOUD);
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_21.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_21.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_21.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_21.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_21.IMAGE);
    await addCloud.clickGetVersion();
    await addCloud.selectVersionCloud(5, 'v2.0');
    await addCloud.selectVersionCloud2(7, 'v2');
    await addCloud.selectVersionCloud(9, 'v2.0');
    await addCloud.selectVersionCloud(11, 'v2');
    await addCloud.selectVersionCloud(13, 'v2.0');
    await addCloud.clickCloudConfig(3);
    await addCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_21.CLOUD);
    await confCloud.verifyText(3, data.CLOUD_SYSTEM.TC_21.VERSION);
  });

  // 22.Verify that the "missing required info" message was displayed when user forgot to input the required field when add cloud
  // tslint:disable-next-line:max-line-length
  it('Verify that the "missing required info" message was displayed when user forgot to input the required field when add cloud- 22', async function () {
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.clickCloudConfig(3);
    await addCloud.clickCloudConfig(5);
    await addCloud.clickCloudConfig(7);
    await addCloud.clickCloudConfig(9);
    await addCloud.clickCloudConfig(11);
    await addCloud.clickCloudConfig(13);
    await addCloud.clickCloudConfig(3);

    await expect(addCloud.verifyButtonSaveDisable()).toBeFalsy();
    await addCloud.verifyMissingRequiredInfo('Instance Name');
    await addCloud.verifyMissingRequiredInfo('Identity');
    await addCloud.verifyMissingRequiredInfo('Compute');
    await addCloud.verifyMissingRequiredInfo('Networking');
    await addCloud.clickCancel();
  });

  // 23.Verify that the Colud System table can update the values after added cloud
  it('Verify that the Colud System table can update the values after added cloud -23', async function () {
    await navigation.selectLeftNav(2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
    await browser.waitForAngularEnabled(true);
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.inputCloudConfig(3, data.CLOUD_SYSTEM.TC_23.CLOUD);
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_23.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_23.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_23.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_23.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_23.IMAGE);
    await addCloud.clickGetVersion();
    await addCloud.clickCloudConfig(3);
    await addCloud.clickSaveCloud();
    // await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_23.CLOUD);
    await confCloud.verifyText(2, data.CLOUD_SYSTEM.TC_23.CLOUD);
  });

  // 24.Verify that the previous information of cloud is displayed when user click on Edit cloud button
  it('Verify that the previous information of cloud is displayed when user click on Edit cloud button -24', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_24.CLOUD);
    await confCloud.clickEditCloud(1);
    await editCloud.verifyInfoDisplayed('instanceNameInput');

    await editCloud.verifyInfoDisplayed('identityInput');
    const valueKey = await editCloud.getAttributeOfElementInForm('identityInput');
    await console.log('IP in form : ' + valueKey);
    await expect(Boolean(valueKey === data.CLOUD_SYSTEM.TC_24.KEYSTONE)).toBe(true);
    await editCloud.verifyInfoDisplayed('computerInput');
    await editCloud.verifyInfoDisplayed('netwokingInput');
    await editCloud.verifyInfoDisplayed('imageInput');
    await editCloud.verifyInfoDisplayed('storageInput');
    await editCloud.clickCancel();
  });

  // 25.Verify that the Edit cloud function can work successfully when fill all the field with correct info
  it('the Edit cloud function can work successfully when fill all the field with correct info -25', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickRefesh();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_25.CLOUD);
    await browser.waitForAngularEnabled(true);
    await confCloud.clickEditCloud(1);
    await editCloud.verifyInfoDisplayed('instanceNameInput');
    await editCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_25.KEYSTONE);
    await editCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_25.NOVA);
    await editCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_25.NEUTRON);
    await editCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_25.CINDER);
    await editCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_25.IMAGE);
    await editCloud.clickCloudConfig(5);
    await editCloud.clickGetVersion();
    await editCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_25.CLOUD);
    await confCloud.verifyText(3, data.CLOUD_SYSTEM.TC_25.KEYSTONE);
  });

  // 26.Verify that the Edit cloud function can not work successfully when missing required info
  it('the Edit cloud function can not work successfully when missing required info -26', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickRefesh();
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_26.CLOUD);
    const result2 = await confCloud.isTableHasValueAfterSearch();
    await expect(result2 === true).toBe(true);
    await confCloud.clickEditCloud(1);
    await editCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_26.KEYSTONE);
    await editCloud.clickCloudConfig(7);
    await editCloud.clearCloudConfig(7);
    await editCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_26.NEUTRON);
    await editCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_26.CINDER);
    await editCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_26.IMAGE);
    await editCloud.clickCloudConfig(7);
    await editCloud.clickCloudConfig(5);
    await expect(addCloud.verifyAddCloudNoPresent()).toBeFalsy();
    await editCloud.clickCancel();
  });

  // 27.Verify that the Edit cloud function can close the table when click on cancel button
  it('the Edit cloud function can close the table when click on cancel button -27', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickRefesh();
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_26.CLOUD);
    await confCloud.clickEditCloud(1);
    await editCloud.verifyShowTextEditCloud();
    await editCloud.clickCancel();
    await editCloud.verifyEditCloudNoPresent();
  });

  // 28.Verify that all of API address fields only allow user to input the IP format info when edit cloud
  it('Verify that all of API address fields only allow user to input the IP format info when edit cloud -28', async function () {
    await navigation.selectLeftNav(2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_28.CLOUD);
    await confCloud.clickEditCloud(1);
    await editCloud.inputCloudConfig(5, 'xgx172.28');
    await editCloud.inputCloudConfig(7, 'ydh172.2');
    await editCloud.inputCloudConfig(9, 'h172.2');
    await editCloud.inputCloudConfig(11, 'hdss172.22');
    await editCloud.inputCloudConfig(13, '172.28');
    await editCloud.clickSaveCloud();
    await editCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_28.MESSEGEKEY_INVALID);
    await editCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_28.MESSEGENOVA_INVALID);
    await editCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_28.MESSEGENEUTRON_INVALID);
    await editCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_28.MESSEGECINDER_INVALID);
    await editCloud.verifyInvalidInfo(data.CLOUD_SYSTEM.TC_28.MESSEGEIMAGE_INVALID);
    await editCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_28.KEYSTONE);
    await editCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_28.NOVA);
    await editCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_28.NEUTRON);
    await editCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_28.CINDER);
    await editCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_28.IMAGE);
    await editCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_28.MESSEGEKEY_INVALID);
    await editCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_28.MESSEGENOVA_INVALID);
    await editCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_28.MESSEGENEUTRON_INVALID);
    await editCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_28.MESSEGECINDER_INVALID);
    await editCloud.verifyInvalidInfoNoPresent(data.CLOUD_SYSTEM.TC_28.MESSEGEIMAGE_INVALID);
    await editCloud.clickCancel();
  });

  // 29.Verify that the version of API address were displayed with correct values when edit cloud
  it('Verify that the version of API address were displayed with correct values when edit cloud -29', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_29.CLOUD);
    await confCloud.clickEditCloud(1);
    await addCloud.selectVersionCloud(5, 'v3');
    await addCloud.selectVersionCloud2(7, 'v2.1');
    await addCloud.selectVersionCloud(9, 'v2.0');
    await addCloud.selectVersionCloud(11, 'v1');
    await addCloud.selectVersionCloud(13, 'v2.1');

    await editCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_29.CLOUD);
    await confCloud.verifyText(3, 'v3');
    await confCloud.verifyText(4, 'v2.1');
    await confCloud.verifyText(5, 'v2.0');
    await confCloud.verifyText(6, 'v2.1');
    await confCloud.verifyText(7, 'v1');
  });



  // 30.Verify that the missing required info reminder was displayed when user forgot to input the required field when edit cloud
  // tslint:disable-next-line:max-line-length
  it('the missing required info reminder was displayed when user forgot to input the required field when edit cloud 30', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clickRefesh();
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_30.CLOUD);
    await confCloud.clickEditCloud(1);
    await editCloud.clickCloudConfig(7);
    await editCloud.clearCloudConfig(7);
    await editCloud.clickCloudConfig(3);
    await expect(addCloud.verifyAddCloudNoPresent()).toBeFalsy();
    await editCloud.verifyMissingRequiredInfo('Compute');
    await editCloud.clickCancel();
  });

  // // 31.Verify that the "Cloud Instance Name" can not edit when edit cloud
  // it('Verify that the "Cloud Instance Name" can not edit when edit cloud -31', async function () {
  //   await navigation.selectLeftNavSecond(4, 1);
  //   await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
  //   await browser.waitForAngularEnabled(true);
  //   const result1 = await pageError.verifyMessegeError();
  //   if ( result1 === true ) {
  //     await pageError.cliclClearALL();
  //   }
  //   await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_31.CLOUD);
  //   await confCloud.clickEditCloud(1);
  //   await editCloud.verifyInstanceCanNotEdit();
  //   await editCloud.clickCancel();
  // });

  // 32.Verify that the Colud System table can update the values after edited cloud
  it('Verify that the Colud System table can update the values after edited cloud -32', async function () {
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    // await confCloud.clickRefesh();
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch('wrong cloud');
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_32.CLOUD);
    const txtrecord2 = await confCloud.getRecordNumber();
    await expect(Boolean(txtrecord2 === data.CLOUD_SYSTEM.TC_32.RECORD)).toBeTruthy();
    await confCloud.clickEditCloud(1);
    await editCloud.inputCloudConfig(3, data.CLOUD_SYSTEM.TC_32.CLOUD_NAME);
    await editCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_32.CLOUD_NAME);
    await confCloud.verifyText(2, data.CLOUD_SYSTEM.TC_32.CLOUD_NAME);
  });

  // 33.Verify that the Delete cloud function can terminate when click on cancel button
  it('Verify that the Delete cloud function can terminate when click on cancel button -33', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_33.CLOUD);
    await confCloud.clickSelect();
    await confCloud.clickDelete();
    await confCloud.clickConfirmCancelDelete();
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_33.CLOUD);
    await confCloud.verifyText(2, data.CLOUD_SYSTEM.TC_33.CLOUD);

  });


  // 34.Verify that the Delete cloud function can work properly when click on delete button
  it('Verify that the Delete cloud function can work properly when click on delete button -34', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_34.CLOUD);
    await confCloud.clickSelect();
    await confCloud.clickDelete();
    await confCloud.clickConfirmDelete();
    await browser.waitForAngularEnabled(true);
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_34.CLOUD);
    const record = await confCloud.getRecordNumber();
    await expect(Boolean(record === 'No records found')).toBe(true);

    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_34.CLOUD_NAME);
    await confCloud.clickSelect();
    await confCloud.clickDelete();
    await confCloud.clickConfirmDelete();
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_34.CLOUD_NAME);
    const record1 = await confCloud.getRecordNumber();
    await expect(Boolean(record1 === 'No records found')).toBe(true);

  });

  // 35.Verify that the Add cloud to Deploy VNF
  it('Verify that the Add cloud to Deploy VNF- 35', async function () {
    await navigation.selectLeftNavSecond(4, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    await browser.waitForAngularEnabled(true);
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_35.CLOUD);
    const result2 = await confCloud.isTableHasValueAfterSearch();
    if (result2) {
      await confCloud.clickSelect();
      await confCloud.clickDelete();
      await confCloud.clickConfirmDelete();
      await confCloud.clearTextSearch();
      await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_0.CLOUD);
      const result3 = await confCloud.isTableHasValueAfterSearch();
      await expect(Boolean(result3 === false)).toBe(true);
    }
    await confCloud.clickAddCloudbtn();
    await addCloud.inputCloudConfig(3, data.CLOUD_SYSTEM.TC_35.CLOUD);
    await addCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_35.KEYSTONE);
    await addCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_35.NOVA);
    await addCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_35.NEUTRON);
    await addCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_35.CINDER);
    await addCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_35.IMAGE);

    await addCloud.clickGetVersion();
    await browser.waitForAngularEnabled(true);
    await addCloud.selectVersionCloud(5, 'v2.0');
    await addCloud.selectVersionCloud2(7, 'v2');
    await addCloud.selectVersionCloud(9, 'v2.0');
    await addCloud.selectVersionCloud(11, 'v2');
    await addCloud.selectVersionCloud(13, 'v2.0');
    await addCloud.clickCloudConfig(3);
    await addCloud.clickSaveCloud();
    await confCloud.clearTextSearch();
    await confCloud.settextSearch(data.CLOUD_SYSTEM.TC_35.CLOUD);
    const record = await confCloud.getRecordNumber();
    await expect(Boolean(record === data.CLOUD_SYSTEM.TC_35.RECORD)).toBe(true);
    await confCloud.clearTextSearch();
  });



});
