import { PageEditRole } from '../PageObject/PageEditRole.po';
import { PageRole } from '../PageObject/PageRole.po';
import { browser } from 'protractor';

let roleNameDuplicate = '';
describe('workspace-project page edit role', () => {
    let pageEditRole: PageEditRole;
    let pageRole: PageRole;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageEditRole = new PageEditRole();
        pageRole = new PageRole();
    });

    // 1. Verify that the user can not Edit/Duplicate/Delete the sysadmin role
    it('1. Verify that the user can not Edit/Duplicate/Delete the sysadmin role', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        await expect(await pageEditRole.checkFindKeyword('sysadmin')).toEqual(0);
    });

    // 2. Verify that the user can not Edit/Duplicate/Delete the sysadmin role
    it('2. Verify that the permissions of current role was displayed when duplicate role', async function () {
        const rowWork = 1;
        await pageRole.clickRowTable(rowWork);
        await expect(await pageEditRole.verifyPageEditRoleDisplay()).toBeTruthy();
        await console.log('permissions list before duplicate');
        const permissionsList1 = await pageEditRole.getPermissionsList();
        await pageEditRole.selectItemInDropdown(rowWork, 'Duplicate');
        await console.log('permissions list after select duplicate');
        const permissionsList2 = await pageEditRole.getPermissionsList();
        await expect(permissionsList1).toEqual(permissionsList2);
    });

    // Verify that the Permissions List be updated when user select a Permission Control for role when duplicate role
    // tslint:disable-next-line:max-line-length
    it('3. Verify that the Permissions List be updated when user select a Permission Control for role when duplicate role', async function () {
        // TODO: using browser.waitForAngular() replace
        const permissionsAll =  ['All Permissions'];
        await pageEditRole.onAllPermissions();
        const permissionsList1 = await pageEditRole.getPermissionsList();
        await expect(permissionsList1).toEqual(permissionsAll);
        await pageEditRole.offAllPermissions();
        const permissionsList2 = await pageEditRole.getPermissionsList();
        await expect(permissionsList2.length).toEqual(0);
    });

    // Verify that the Role table be update when a role duplicated
    it('4. Verify that the Role table be update when a role duplicated', async function () {
        const roleName = await pageEditRole.getTextInputRoleName();
        await pageEditRole.clickSaveFormRole();
        roleNameDuplicate = roleName;
        await expect(await pageEditRole.checkFindKeyword(roleName)).toEqual(1);
    });

    // Verify that the Role table be update when a role deleted
    it('5. Verify that the Role table be update when a role deleted', async function () {
        const indexRow = await pageEditRole.getIndexRowContainKeyWork(roleNameDuplicate);
        await pageEditRole.selectItemInDropdown(indexRow, 'Delete');
        await pageEditRole.clickBtnDeleteRole();
        await expect(await pageEditRole.checkFindKeyword(roleNameDuplicate)).toEqual(0);
    });
});
