// import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageVNFMs } from '../PageObject/PageVNFMs.po';
import { browser, ExpectedConditions } from 'protractor';


const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project VNFMs', () => {
    // let pageLogin: PageLogin;
    let pageNavigation: PageNavigation;
    let pageVNFMs: PageVNFMs;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        // pageLogin = new PageLogin();
        pageNavigation = new PageNavigation();
        pageVNFMs = new PageVNFMs();
    });


    // 1 - Verify that the confirm dialog was displayed when click on "Place VNFM in Maintenance mode"
    it('Verify that the confirm dialog was displayed when click on "Place VNFM in Maintenance mode" - 1 ', async function () {

        await pageNavigation.selectLeftNav(6);
        await pageNavigation.selectLeftNavSecond(6, 5);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfms'), 3000);

        await pageVNFMs.setSearch(data.ADMIN_VNFMs.TC_1.VNFM_NAME);
        await pageVNFMs.verifyRecord(data.ADMIN_VNFMs.TC_1.RECORD);
        await pageVNFMs.setVNFMInMaintenanceMode();
        await pageVNFMs.verifyDisplayTheConfirmDialog();
    });

    // 2 - Verify that the user exit the confirm action when click on cancel button
    it('Verify that the user exit the confirm action when click on cancel button - 2 ', async function () {

        await pageVNFMs.verifyExitTheConfirmDialog();
    });

    // 3 - Verify that the VNFM can enter to the Maintenance mode when click on confirm button
    it('Verify that the VNFM can enter to the Maintenance mode when click on confirm button - 3', async function () {

        await pageVNFMs.setVNFMInMaintenanceMode();
        await browser.sleep(3000);
        await pageVNFMs.verifyVNFMEnterToTheMantenanceMode();
    });

    // 4 - Verify that the confirm dialog was displayed when click on "Complete VNFM Maitenance"
    it('Verify that the confirm dialog was displayed when click on "Complete VNFM Maitenance" - 4 ', async function () {

        await pageVNFMs.removeVNFMFromMaintenanceMode();
        await browser.sleep(3000);
        await pageVNFMs.verifyDisplayTheConfirmDialog();
    });

    // 5 - Verify that the user can exit the confirm action when click on cancel button
    it('Verify that the user can exit the confirm action when click on cancel button - 5', async function () {

        await browser.sleep(3000);
        await pageVNFMs.verifyExitTheConfirmDialog();
    });

    // 6 - Verify that the VNFM can remove from the Maintenance mode when click on confirm button
    it('Verify that the VNFM can remove from the Maintenance mode when click on confirm button - 6 ', async function () {

        await pageVNFMs.removeVNFMFromMaintenanceMode();
        await browser.sleep(3000);
        await pageVNFMs.verifyVNFMIsRemovedFromMantenanceMode();
    });




 });


