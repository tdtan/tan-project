import { PageNavigation } from '../PageObject/PageNavigation.po';
import { browser, ExpectedConditions, protractor } from 'protractor';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { PageInstantiateVNF } from '../PageObject/PageInstantiateVNF.po';
import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
import { PageError } from '../PageObject/PageError.po';
import { PageTerminate } from '../PageObject/PageTerminate.po';
import { PageAdminContinuityPlan } from '../PageObject/PageAdminContinuityPlan.po';
import { PageAddContinuityPlan } from '../PageObject/PageAddContinuityPlan.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Instantiate VNF', () => {
    let pageNavigation: PageNavigation;
    let pageOnboardedVNFs: PageOnboardedVNFs;
    let pageInstantiateVNF: PageInstantiateVNF;
    let pageFailedtoOnboard: PageFailedtoOnboard;
    let pageError: PageError;
    let pageLiveVNFs: PageLiveVNFs;
    let pageAddContinuityPlan: PageAddContinuityPlan;
    let pageAdminContinuityPlan: PageAdminContinuityPlan;
    let pageTerminate: PageTerminate;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
        pageNavigation = new PageNavigation();
        pageOnboardedVNFs = new PageOnboardedVNFs();
        pageInstantiateVNF = new PageInstantiateVNF();
        pageFailedtoOnboard = new PageFailedtoOnboard();
        pageError = new PageError();
        pageAddContinuityPlan = new PageAddContinuityPlan();
        pageAdminContinuityPlan = new PageAdminContinuityPlan();
        pageLiveVNFs = new PageLiveVNFs();
        pageTerminate = new PageTerminate();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });


    // 1- Verify that the empty form was displayed when user click on Instantiate function
    it('Verify that the empty form was displayed when user click on Instantiate function- 1', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        // upload ssbc
        await pageOnboardedVNFs.settextSearch(data.INSTANTIATE.TC_1.TEXTSEARCH);
        const result1 = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        await pageOnboardedVNFs.clickShowHideFilter();
        if (result1 === false) {
            // upload file CSAR
            await pageOnboardedVNFs.clickOnboardbtn();
            await pageFailedtoOnboard.setSearch(data.INSTANTIATE.TC_1.TEXTSEARCH);
            const afterSearch = await pageFailedtoOnboard.isTableHasValueAfterSearch();
            if (afterSearch === false) {
                await pageFailedtoOnboard.funcUploadCSAR(data.INSTANTIATE.TC_1.PATH);
            }
            const result5 = await pageError.verifyMessegeError();
            if (result5 === true) {
                await pageError.cliclClearALL();
            }
            await pageFailedtoOnboard.clickCancelVNF();
        }
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickDropdownActions();
        await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_1.FUNCTION);
        const txtInstantiate2 = await pageInstantiateVNF.getTextInstantiate();
        await console.log('get text: ' + txtInstantiate2);
        await expect(Boolean(txtInstantiate2 === data.INSTANTIATE.TC_1.VERIFYTEXT)).toBe(true);

        const VNFName = await pageInstantiateVNF.getAttributeVNFName();
        const CloudName = await pageInstantiateVNF.gettextCloud();
        const TenantName = await pageInstantiateVNF.gettextTenant();
        const ZoneName = await pageInstantiateVNF.gettextZone();
        await expect(Boolean(VNFName === '')).toBe(true);
        await expect(Boolean(CloudName === 'Select Cloud')).toBe(true);
        await expect(Boolean(TenantName === 'Select Tenant')).toBe(true);
        await expect(Boolean(ZoneName === 'Select Zone')).toBe(true);
    });

    // 2 -Verify that the Cloud filed and Tenant field are required
    it('Verify that the Cloud filed and Tenant field are required- 2', async function () {
        await pageInstantiateVNF.clickdbxCloud();
        await pageInstantiateVNF.clickVNFName();
        const strMessCloud = await pageInstantiateVNF.gettextMessageCloud();
        await expect(Boolean(strMessCloud === data.INSTANTIATE.TC_2.MESSAGECLOUD)).toBe(true);

        await pageInstantiateVNF.clickdbxTenant();
        await pageInstantiateVNF.clickVNFName();
        const strMessTenant = await pageInstantiateVNF.gettextMessageTenant();
        await expect(Boolean(strMessTenant === data.INSTANTIATE.TC_2.MESSAGETENANT)).toBe(true);
        await pageInstantiateVNF.clickCancel();
    });

    // 3 - Verify that the VNFM Preference table was displayed after select Cloud and Tenant when Instantiate a VNF
    // tslint:disable-next-line:max-line-length
    // it('Verify that the VNFM Preference table was displayed after select Cloud and Tenant when Instantiate a VNF - 3', async function () {
    //     await expect(pageInstantiateVNF.verifyVNFMPerferredDisplay()).toBeFalsy();
    //     await pageInstantiateVNF.selectCloud(data.INSTANTIATE.TC_3.CLOUDNAME);
    //     await pageInstantiateVNF.selectTenant(data.INSTANTIATE.TC_3.TENANTNAME);
    //     await expect(pageInstantiateVNF.verifyVNFMPerferredDisplay()).toBeTruthy();
    //     await pageInstantiateVNF.clickCancel();
    // });

    // // 4 - Verify that user can select preferred VNFM in Preference table
    // it('Verify that user can select preferred VNFM in Preference table - 4', async function () {
    //     await pageNavigation.selectLeftNav(5);
    //     await pageNavigation.selectLeftNavSecond(5, 6);
    //     await browser.wait(ExpectedConditions.urlContains('vnfm/vnfms'), 5000);
    //     await browser.waitForAngularEnabled(true);
    //     const VNFMName = await pageVNFMs.getVNFMNameinTable();
    //     await console.log('get text: ' + VNFMName);
    //     await pageNavigation.selectLeftNav(2);
    //     await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
    //     await browser.waitForAngularEnabled(true);
    //     await pageOnboardedVNFs.clickDropdownActions();
    //     await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_4.FUNCTION);
    //     await pageInstantiateVNF.selectCloud(data.INSTANTIATE.TC_4.CLOUDNAME);
    //     await pageInstantiateVNF.selectTenant(data.INSTANTIATE.TC_4.TENANTNAME);
    //     await pageInstantiateVNF.clickVNFMPerferred();
    //     const numberIterm = await pageInstantiateVNF.countItermInForm();
    //     await pageInstantiateVNF.clickChooseOptionVNFMPerferred(numberIterm);
    //     const txtVNFMName = await pageInstantiateVNF.gettextVNFMPerferred();
    //     await console.log('get text: ' + txtVNFMName);
    //     await expect(Boolean(txtVNFMName === VNFMName)).toBe(true);
    //     await pageInstantiateVNF.clickCancel();
    // });

    // 5 - Verify that user can not select the tenant if not select the Cloud first
    it('Verify that user can not select the tenant if not select the Cloud first - 5', async function () {
        await pageOnboardedVNFs.clickDropdownActions();
        await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_5.FUNCTION);
        const txtInstantiate2 = await pageInstantiateVNF.getTextInstantiate();
        await console.log('get text: ' + txtInstantiate2);
        await expect(Boolean(txtInstantiate2 === data.INSTANTIATE.TC_5.VERIFYTEXT)).toBe(true);
        await pageInstantiateVNF.clickdbxTenant();
        const count = await pageInstantiateVNF.countItermInForm();
        await console.log('Number tenant in from: ' + count);
        await expect(Boolean(count === 1)).toBe(true);
        // await pageInstantiateVNF.clickCancel();
    });

    // 6 - Verify that the user can choose the Cloud that was added before
    it('Verify that the user can choose the Cloud that was added before - 6', async function () {
        await pageInstantiateVNF.selectCloud(data.INSTANTIATE.TC_6.CLOUDNAME);
        const CloudName = await pageInstantiateVNF.gettextCloud();
        await expect(Boolean(CloudName === data.INSTANTIATE.TC_6.CLOUDNAME)).toBe(true);
        // await pageInstantiateVNF.clickCancel();
    });

    // 7 - Verify that the user can choose the Tenant belong to the Cloud was chosen in previous
    it('Verify that the user can choose the Tenant belong to the Cloud was chosen in previous - 7', async function () {
        await pageInstantiateVNF.selectTenant(data.INSTANTIATE.TC_7.TENANTNAME);
        const TenantName = await pageInstantiateVNF.gettextTenant();
        await expect(Boolean(TenantName === data.INSTANTIATE.TC_7.TENANTNAME)).toBe(true);
        // await pageInstantiateVNF.clickCancel();
    });

    // 8 - Verify that the Environment field is optional and default value is nova
    it('Verify that the Environment field is optional and default value is nova - 8', async function () {
        const ZoneName = await pageInstantiateVNF.gettextZone();
        await console.log('Zone: ' + ZoneName);
        await expect(Boolean(ZoneName === data.INSTANTIATE.TC_8.ZONENAME)).toBe(true);
        // await pageInstantiateVNF.clickCancel();
    });

    // 9 - Verify that the network display with correct info such as: name, subnet...
    it('Verify that the network display with correct info such as: name, subnet...- 9', async function () {
        await browser.waitForAngularEnabled(true);
        await pageInstantiateVNF.clickShowFormNetwork();
        await pageInstantiateVNF.clickNetworkInForm(1);
        await expect(pageInstantiateVNF.verifyElementDisplayInForm(data.INSTANTIATE.TC_9.ETH0)).toBeTruthy();
        await pageInstantiateVNF.clickNetworkInForm(1);
        await pageInstantiateVNF.clickNetworkInForm(2);
        await expect(pageInstantiateVNF.verifyElementDisplayInForm(data.INSTANTIATE.TC_9.ETH1)).toBeTruthy();
        await pageInstantiateVNF.clickNetworkInForm(2);
    });

    // 10 - Verify that the VNFM can display all of eth(s) of VNF that need to assign network
    it('Verify that the VNFM can display all of eth(s) of VNF that need to assign network - 10', async function () {
        await browser.waitForAngularEnabled(true);
        const Mgt0Network = await pageInstantiateVNF.gettextNetwork(1);
        const Pkt0Network = await pageInstantiateVNF.gettextNetwork(2);
        const Pkt1Network = await pageInstantiateVNF.gettextNetwork(3);
        await console.log('Network 1: ' + Mgt0Network);
        await console.log('Network 2: ' + Pkt0Network);
        await console.log('Network 3: ' + Pkt1Network);
        await expect(Boolean(Mgt0Network === data.INSTANTIATE.TC_10.TXTMGT0)).toBe(true);
        await expect(Boolean(Pkt0Network === data.INSTANTIATE.TC_10.TXTPKT0)).toBe(true);
        await expect(Boolean(Pkt1Network === data.INSTANTIATE.TC_10.TXTPKT1)).toBe(true);
    });

    // 11 - Verify that user can add unlimited the new SOL001 Custom Parameters
    it('Verify that user can add unlimited the new SOL001 Custom Parameters - 11', async function () {
        await browser.waitForAngularEnabled(true);
        await pageInstantiateVNF.clickShowVNFSpecificForm();
        await pageInstantiateVNF.addUnlimittedSolCustomParameters(data.INSTANTIATE.TC_11.NUMBER_NEW_SOL001);
        const txtNumSol = await pageInstantiateVNF.verifyNewSolParameter();
        await expect(Boolean(txtNumSol >= data.INSTANTIATE.TC_11.NUMBER_NEW_SOL001)).toBe(true);
    });

    // 12 - Verify that the new SOL001 Custom Parameters can be added
    it('Verify that the new SOL001 Custom Parameters can be added - 12', async function () {
        const txtNumSol = await pageInstantiateVNF.verifyNewSolParameter();
        await pageInstantiateVNF.clickAddSol();
        const txtNumSolNew = await pageInstantiateVNF.verifyNewSolParameter();
        await expect(Boolean(txtNumSolNew > txtNumSol)).toBe(true);
    });

    // 13 - Verify that the new SOL001 Custom Parameters can be removed
    it('Verify that the new SOL001 Custom Parameters can be removed - 13', async function () {
        await expect(pageInstantiateVNF.verifyElementDisplayWhenRemoveSOL001()).toBeFalsy();
        await pageInstantiateVNF.clickRemoveSolParameter();
        await browser.waitForAngularEnabled(true);
        await expect(pageInstantiateVNF.verifyElementDisplayWhenRemoveSOL001()).toBeTruthy();
    });

    // 14 - Verify that user can edit anything into the new SOL001 Custom Parameters
    it('Verify that user can edit anything into the new SOL001 Custom Parameters - 14', async function () {
        await browser.waitForAngularEnabled(true);
        await pageInstantiateVNF.EditNewParameter(data.INSTANTIATE.TC_14.KEY_VALUE, data.INSTANTIATE.TC_14.KEY_PAIR_VALUE);
        const isEditIntoNewSolParameters = await pageInstantiateVNF.isEditInputSuccessfully();
        await expect(isEditIntoNewSolParameters).toBe(true);
    });

    // 15 - Verify that the init data info of VNF was display with correct info as vnfd defined
    it('Verify that the init data info of VNF was display with correct info as vnfd defined - 15', async function () {
        const txtInitData = await pageInstantiateVNF.getAttributeInitData();
        await console.log('Init Data: ' + txtInitData);
        await expect(Boolean(txtInitData === '')).toBe(false);
    });

    // 16- Verify that the user can edit the init data of VNF
    it('Verify that the user can edit the init data of VNF - 16', async function () {
        await pageInstantiateVNF.setInitData('csdsfsdfs');
        const txtInitData = await pageInstantiateVNF.getAttributeInitData();
        await console.log('Init Data: ' + txtInitData);
        await expect(Boolean(txtInitData === 'csdsfsdfs')).toBe(true);
        await pageInstantiateVNF.clickCancel();
    });

    // 17 - Verify that the Candidate Flavor field is optional an the default value is auto-assign
    it('Verify that the Candidate Flavor field is optional an the default value is auto-assign - 17', async function () {
        // search Vnfs in table live-VNFS
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.INSTANTIATE.TC_17.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result === true) {
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions('Terminate');
            await pageTerminate.clickConfirmYes();
        }
        // navigate to vnf-catalog
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickDropdownActions();
        await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_17.FUNCTION);
        // tslint:disable-next-line:max-line-length
        await pageInstantiateVNF.funcDeploySBC(data.INSTANTIATE.TC_17.VNFNAME, data.INSTANTIATE.TC_17.CLOUD_SELECT, data.INSTANTIATE.TC_17.TENANT_SELECT, data.INSTANTIATE.TC_17.ETH0, data.INSTANTIATE.TC_17.ETH1, data.INSTANTIATE.TC_17.PKT1, data.INSTANTIATE.TC_17.ZONE);
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
        await pageInstantiateVNF.clickInstantiate();
        // verify Vnfcan be deployed without assign flavor
        await expect(pageInstantiateVNF.verifyInstaniateFormNotDisplayed());
    });

    // 18 - Verify that the new SOL001 Custom Parameters is optional when instantiate VNF
    it('Verify that the new SOL001 Custom Parameters is optional when instantiate VNF - 18', async function () {
        // search Vnfs in table live-VNFS
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.INSTANTIATE.TC_18.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result === true) {
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions('Terminate');
            await pageTerminate.clickConfirmYes();
        }
        // navigate to vnf-catalog
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickDropdownActions();
        await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_18.FUNCTION);
        // instantiate wihthout  SOL001
        // tslint:disable-next-line:max-line-length
        await pageInstantiateVNF.funcDeploySBC(data.INSTANTIATE.TC_18.VNFNAME, data.INSTANTIATE.TC_18.CLOUD_SELECT, data.INSTANTIATE.TC_18.TENANT_SELECT, data.INSTANTIATE.TC_18.ETH0, data.INSTANTIATE.TC_18.ETH1, data.INSTANTIATE.TC_18.PKT1, data.INSTANTIATE.TC_18.ZONE);
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
        await pageInstantiateVNF.clickInstantiate();
        await expect(pageInstantiateVNF.verifyInstaniateFormNotDisplayed());

        // verify Vnfs reach to Ready status
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.INSTANTIATE.TC_18.VNFNAME);
        const txtRecord = await pageLiveVNFs.getRecord();
        await expect(txtRecord).toEqual(data.INSTANTIATE.TC_18.RECORD);
        const checkStatus = await pageLiveVNFs.waitVNFReady(1, 7, 'Ready', 3600000, 'TC failed due to ready time out');
        await expect(checkStatus).toBeTruthy();
    }, 3610000);

    // 19 - Verify that the empty form is displayed when user click on instantiate VNF Manual Cinder
    it('Verify that the empty form is displayed when user click on instantiate VNF Manual Cinder - 19', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        // upload ems
        await pageOnboardedVNFs.settextSearch(data.INSTANTIATE.TC_19.TEXTSEARCH_EMS);
        const result2 = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (result2 === false) {
            // upload file CSAR
            await pageOnboardedVNFs.clickOnboardbtn();
            await pageFailedtoOnboard.setSearch(data.INSTANTIATE.TC_19.TEXTSEARCH_EMS);
            const afterSearch = await pageFailedtoOnboard.isTableHasValueAfterSearch();
            if (afterSearch === false) {
                await pageFailedtoOnboard.funcUploadCSAR(data.INSTANTIATE.TC_19.PATH_EMS);
                await pageFailedtoOnboard.clickBackToPageVNFCatalog();
            }
        }
        await pageOnboardedVNFs.clickDropdownActions();
        await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_19.FUNCTION_EMS);
        const txtInstantiate1 = await pageInstantiateVNF.getTextInstantiate();
        await console.log('get text: ' + txtInstantiate1);
        await expect(Boolean(txtInstantiate1 === data.INSTANTIATE.TC_19.VERIFYTEXT_EMS)).toBe(true);

        await pageInstantiateVNF.selectCloud(data.INSTANTIATE.TC_19.CLOUDNAME);
        await pageInstantiateVNF.selectTenant(data.INSTANTIATE.TC_19.TENANTNAME);

        await browser.waitForAngularEnabled(true);
        await pageInstantiateVNF.clickShowStorageVolumesFrom();
        const txtVolum = await pageInstantiateVNF.gettextVolume();
        await expect(Boolean(txtVolum === data.INSTANTIATE.TC_19.TEXTVOLUME2)).toBe(true);
        // const txtVolume = await pageInstantiateVNF.gettextVolume2();
        // await expect(Boolean(txtVolume === data.INSTANTIATE.TC_19.TEXTVOLUME2)).toBe(true);
    });

    // 20 - Verify that the Manual Cinder instantiate can list all the volume of VNF that need to assign
    it('Verify that the Manual Cinder instantiate can list all the volume of VNF that need to assign - 20', async function () {
        const txtVolume = await pageInstantiateVNF.getVolumeAssign();
        const txtverify = txtVolume.slice(0, 3);
        await expect(Boolean(txtverify === 'ems')).toBe(true);
    });

    // 21 - Verify that the empty form is displayed when user click on instantiate VNF Address/Port
    it('Verify that the empty form is displayed when user click on instantiate VNF Address/Port - 21', async function () {
        await pageInstantiateVNF.clickShowFormAddressPort();
        await pageInstantiateVNF.selectPortOrAddressInForm(1);
        await pageInstantiateVNF.selectPortOrAddressInForm(2);
        const txtPort1 = await pageInstantiateVNF.gettextPort(1);
        const txtPort2 = await pageInstantiateVNF.gettextPort(2);
        await console.log('port1: ' + txtPort1);
        await console.log('port2: ' + txtPort2);
        await expect(Boolean(txtPort1 === 'Select Port')).toBe(true);
        await expect(Boolean(txtPort2 === 'Select Port')).toBe(true);
    });

    // 22 - Verify that the Manual Port instantiate can list all the ports of VNF that need to assign
    it('Verify that the Manual Port instantiate can list all the ports of VNF that need to assign - 22', async function () {
        await expect(pageInstantiateVNF.verifyElementdisplayInTD(data.INSTANTIATE.TC_22.PORTNAME1)).toBeTruthy();
        await expect(pageInstantiateVNF.verifyElementdisplayInTD(data.INSTANTIATE.TC_22.PORTNAME2)).toBeTruthy();
        // await expect(pageInstantiateVNF.verifyElementdisplayInTD(data.INSTANTIATE.TC_14.PORTNAME2)).toBeTruthy();
    });

    // 23 - Verify that the user can not select 1 port for 2 NIC VNF name
    it('Verify that the user can not select 1 port for 2 NIC VNF name - 23', async function () {
        await browser.waitForAngularEnabled(true);
        await pageInstantiateVNF.ClickDropdownPort(1);
        await pageInstantiateVNF.ClickOptionPort();
        const txtPort1 = await pageInstantiateVNF.gettextPort(1);
        await pageInstantiateVNF.ClickDropdownPort(2);
        await expect(pageInstantiateVNF.verifyElementDisable()).toBeTruthy();
        const txtPort2 = await pageInstantiateVNF.gettextPortDisable();
        await pageInstantiateVNF.ClickOptionPort();
        const txtPort3 = await pageInstantiateVNF.gettextPort(2);
        await expect(Boolean(txtPort1 === txtPort3)).toBe(false);
        await expect(Boolean(txtPort1 === txtPort2)).toBe(true);
        // await pageInstantiateVNF.clickCancel();
    });

    // 24 - Verify that the VNFM can display the list of flavor in tenant when instantiate VNF
    it('Verify that the VNFM can display the list of flavor in tenant when instantiate VNF - 24', async function () {
        await pageInstantiateVNF.clickShowFormFlavor();
        await pageInstantiateVNF.clickFlavor();
        const numberOfListFlavor = await pageInstantiateVNF.checkDisplayFlavor();
        await expect(numberOfListFlavor >= 1).toBe(true);
        // await pageInstantiateVNF.clickFlavor();
    });

    // 25 - Verify that the VNFM can show detail of flavor such as RAM,CPU when hover on it
    it('Verify that the VNFM can show detail of flavor such as RAM,CPU when hover on it - 25', async function () {
        // await pageInstantiateVNF.clickFlavor();
        await pageInstantiateVNF.moveMouseToFlavorLabel(data.INSTANTIATE.TC_25.OPTION_DETAIL, data.INSTANTIATE.TC_25.NUMBER_OPTION);
        // await pageInstantiateVNF.clickCancel();
    });

    // 26 - Verify that the Instantiate function can close the form when click on cancel button
    it('Verify that the Instantiate function can close the form when click on cancel button - 26', async function () {
        await pageInstantiateVNF.clickCancel();
        await expect(pageInstantiateVNF.verifyInstaniateFormNotDisplayed());
    });

    // 27 - Verify that user can deploy VNF when click on Instatiate  button
    it('Verify that user can deploy VNF when click on Instatiate  button- 27', async function () {
        // search Vnfs in table live-VNFS
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.INSTANTIATE.TC_27.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result === true) {
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions('Terminate');
            await pageTerminate.clickConfirmYes();
        }
        // navigate to vnf-catalog
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.INSTANTIATE.TC_27.TEXTSEARCH);
        const result_vnfs = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (result_vnfs) {
            await pageOnboardedVNFs.clickDropdownActions();
            await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_27.FUNCTION);
            const txtInstantiate = await pageInstantiateVNF.getTextInstantiate();
            await console.log('get text: ' + txtInstantiate);
            await expect(Boolean(txtInstantiate === data.INSTANTIATE.TC_27.VERIFYTEXT)).toBe(true);
        }
        // tslint:disable-next-line:max-line-length
        await pageInstantiateVNF.funcDeploySBC(data.INSTANTIATE.TC_27.VNFNAME, data.INSTANTIATE.TC_27.CLOUD_SELECT, data.INSTANTIATE.TC_27.TENANT_SELECT, data.INSTANTIATE.TC_27.ETH0, data.INSTANTIATE.TC_27.ETH1, data.INSTANTIATE.TC_27.PKT1, data.INSTANTIATE.TC_27.ZONE);
        await pageInstantiateVNF.clickInstantiate();
        await expect(pageInstantiateVNF.verifyInstaniateFormNotDisplayed());

        // verify Vnfs reach to Ready status
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.INSTANTIATE.TC_27.VNFNAME);
        const txtRecord = await pageLiveVNFs.getRecord();
        await expect(txtRecord).toEqual(data.INSTANTIATE.TC_27.RECORD);
        const checkStatus = await pageLiveVNFs.waitVNFReady(1, 7, 'Ready', 3600000, 'TC failed due to ready time out');
        await expect(checkStatus).toBeTruthy();
    }, 3610000);

    // 28 - Verify that user can choose Continuity plan from the drop list when instantiate VNF
    it('Verify that user can choose Continuity plan from the drop list when instantiate VNF - 28', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await pageAdminContinuityPlan.search_global_txt.sendKeys(data.INSTANTIATE.TC_28.CONTINUITYNAME);
        let flag = await pageAdminContinuityPlan.isTableHasValueAfterSearch();
        if (flag === false ) {
            await pageAdminContinuityPlan.ClickAddButton();
            await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeTruthy();
            await pageAddContinuityPlan.setContinuityPlanName(data.INSTANTIATE.TC_28.CONTINUITYNAME);
            const ltVNFM = await pageAddContinuityPlan.getListVNFMNameInAvailableVNFMsTable();
            await console.log('List VNFM Name In Available VNFMs Table: ', ltVNFM);
            for (let index = 0; index < ltVNFM.length; index++) {
                await pageAddContinuityPlan.chooseVNFMNameOnAvailableVNFMs(ltVNFM[index]);
            }
            await browser.waitForAngularEnabled(true);
            await pageAddContinuityPlan.clickSaveButton();
            await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeFalsy();
            await pageAdminContinuityPlan.search_global_txt.sendKeys(data.INSTANTIATE.TC_28.CONTINUITYNAME);
            let flag = await pageAdminContinuityPlan.CheckGlobalSearch(data.INSTANTIATE.TC_28.CONTINUITYNAME);
            await expect(flag).toBe(true);
            await console.log(data.INSTANTIATE.TC_28.CONTINUITYNAME, 'add to table');
        }

        await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.INSTANTIATE.TC_27.TEXTSEARCH);
        const result_vnfs = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (result_vnfs) {
            await pageOnboardedVNFs.clickDropdownActions();
            await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_27.FUNCTION);
            const txtInstantiate = await pageInstantiateVNF.getTextInstantiate();
            await console.log('get text: ' + txtInstantiate);
            await expect(Boolean(txtInstantiate === data.INSTANTIATE.TC_27.VERIFYTEXT)).toBe(true);
        }
        await expect(pageInstantiateVNF.verifyContinuityPlanFormDisplay()).toBeTruthy();
        let textContinuityPlan = await pageInstantiateVNF.gettextContinuityPlan();
        await console.log('text: ', textContinuityPlan);
        await expect(Boolean(textContinuityPlan === data.INSTANTIATE.TC_28.TEXTVERIFY)).toBeTruthy();

    });

    // 29 - Verify that the Continuity plan option was setted default as first
    it('Verify that the Continuity plan option was setted default as first - 29', async function () {
        const txtDefault = await pageInstantiateVNF.gettextOptionContinuityPlan();
        await console.log('text option continuity plan: ', txtDefault);
        await expect(Boolean(txtDefault === 'Default')).toBeTruthy();
    });

    // 30 - Verify that user can change Continuity plan option
    it('Verify that user can change Continuity plan option - 30', async function () {
        await pageInstantiateVNF.ClickContinuityPlanDropdownInFrom();
        await pageInstantiateVNF.ClickOptionContinuityPlan(data.INSTANTIATE.TC_28.CONTINUITYNAME);
        const txtOptionContinuity = await pageInstantiateVNF.gettextOptionContinuityPlan();
        await console.log('text option continuity plan: ', txtOptionContinuity);
        await expect(Boolean(txtOptionContinuity === data.INSTANTIATE.TC_28.CONTINUITYNAME)).toBeTruthy();
    });

    // 31 - Verify that the Continuity plan option drop list will appear when pressed
    it('Verify that the Continuity plan option drop list will appear when pressed - 31', async function () {
        await pageInstantiateVNF.ClickContinuityPlanDropdownInFrom();
        await pageInstantiateVNF.ClickOptionContinuityPlan(data.INSTANTIATE.TC_31.OPTIONCONTINUITY);
        const txtOptionContinuity = await pageInstantiateVNF.gettextOptionContinuityPlan();
        await console.log('text option continuity plan: ', txtOptionContinuity);
        await expect(Boolean(txtOptionContinuity === data.INSTANTIATE.TC_31.OPTIONCONTINUITY)).toBeTruthy();
    });

    // 32 -Verify that the Continuity Plan drop list was displayed correctly
    it('Verify that the Continuity Plan drop list was displayed correctly - 31', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await pageAdminContinuityPlan.search_global_txt.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await pageAdminContinuityPlan.search_global_txt.sendKeys(protractor.Key.BACK_SPACE);
        const listName = await pageAdminContinuityPlan.getListNameContinuityPlanInTable();
        await listName.push('Default');
        await listName.sort();
        await console.log('list Name Continuity Plan', listName);

        await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.INSTANTIATE.TC_27.TEXTSEARCH);
        const result_vnfs = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (result_vnfs) {
            await pageOnboardedVNFs.clickDropdownActions();
            await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.INSTANTIATE.TC_27.FUNCTION);
            const txtInstantiate = await pageInstantiateVNF.getTextInstantiate();
            await console.log('get text: ' + txtInstantiate);
            await expect(Boolean(txtInstantiate === data.INSTANTIATE.TC_27.VERIFYTEXT)).toBe(true);
        }
        await pageInstantiateVNF.ClickContinuityPlanDropdownInFrom();
        const listName2 = await pageInstantiateVNF.getTextOptionInDrodown();
        await expect(listName).toEqual(listName2);

    });
});
