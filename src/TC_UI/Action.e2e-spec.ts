import { PageActions } from '../PageObject/PageActions.po';
// import { browser, ExpectedConditions } from 'protractor';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { browser, ExpectedConditions } from 'protractor';
  const data = require('../../../e2e/VNFM_DB.e2e.json');
describe('workspace-project Dashboard', () => {
    // let pageLogin: PageLogin;
     let pageActions: PageActions;
   let pageNavigation: PageNavigation;
     beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageActions = new PageActions();
        pageNavigation = new PageNavigation();
    });
    // 1 - Verify that the Actions tab displayed in left navigation bar
    it('Verify that the Actions tab displayed in left navigation bar -1', async function() {
    await browser.sleep(3000);
    const actionTab = await pageActions.VerifyLeftNav(4);
    await expect(actionTab).toEqual('Actions');
    });
    // 2 -Verify that the dropdown filter and sort functions work properly with Name column
    it('Verify that the dropdown filter and sort functions work properly with Name column -2', async function() {
      await pageNavigation.selectLeftNav(4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
      const numRecord = await pageActions.getRecord();
      const name = await pageActions.getTextName(1);
      await console.log(name);
      await pageActions.SelectMultiFilter(1, name);
      const numberRecord = await pageActions.countRowSearch();
      const record = numberRecord +  ' records found';
      await pageActions.verifyRecord(record);
      await pageActions.clickFilter(1);
      await pageActions.SelectMultiFilter(1, name);
      const numRecord2 = await pageActions.getRecord();
      await expect(Boolean( numRecord === numRecord2)).toBe(true);
      await pageActions.verifysort(1);
      });
      // 3 -Verify that the dropdown filter and sort functions work properly with Action column
    it('Verify that the dropdown filter and sort functions work properly with  Action column -3', async function() {
      await pageNavigation.selectLeftNav(4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
      const numRecord = await pageActions.getRecord();
      const action = await pageActions.getTextName(2);
      await console.log(action);
      await pageActions.SelectFilter(2, action);
      const numberRecord = await pageActions.countRowSearch();
      const record = numberRecord +  ' records found';
      await pageActions.verifyRecord(record);
      await pageActions.SelectFilter(2, 'All');
      const numRecord2 = await pageActions.getRecord();
      await expect(Boolean( numRecord === numRecord2)).toBe(true);
      await pageActions.verifysort(2);
      });
      // 4 -Verify that the dropdown filter and sort functions work properly with User column
    it('Verify that the dropdown filter and sort functions work properly with User column -4', async function() {
      await pageNavigation.selectLeftNav(4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
      const numRecord = await pageActions.getRecord();
      const user = await pageActions.getTextName(3);
      await pageActions.SelectFilter(3, user);
      const numberRecord = await pageActions.countRowSearch();
      const record = numberRecord +  ' records found';
      await pageActions.verifyRecord(record);
      await pageActions.SelectFilter(3, 'All');
      const numRecord2 = await pageActions.getRecord();
      await expect(Boolean( numRecord === numRecord2)).toBe(true);
      await pageActions.verifysort(3);
      });
      // 5 -Verify that the dropdown filter and sort functions work properly with Name column
    it('Verify that the dropdown filter and sort functions work properly with Status column -5', async function() {
      await pageNavigation.selectLeftNav(4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
      const numRecord = await pageActions.getRecord();
      const status = await pageActions.getTextName(5);
      await pageActions.SelectFilter(5, status);
      const numberRecord = await pageActions.countRowSearch();
      const record = numberRecord +  ' records found';
      await pageActions.verifyRecord(record);
      await pageActions.SelectFilter(5, 'All');
      const numRecord2 = await pageActions.getRecord();
      await expect(Boolean( numRecord === numRecord2)).toBe(true);
      await pageActions.verifysort(5);
      });
      // 6 - Verify that the sort functions work properly with Progress column
      it('Verify that the sort functions work properly with Progress column -6', async function() {
         await pageNavigation.selectLeftNav(4);
         await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
         await pageActions.verifysort(6);
      });
       // 7 - Verify that the date filter and sort functions work properly with Date/Time column
       it('Verify that the date filter and sort functions work properly with Date/Time column -7', async function() {
         await pageNavigation.selectLeftNav(4);
         await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
         await pageActions.verifysort(4);
      });
      // 8- Verify that the Search function work properly
      it('Verify that the Search function work properly -8', async function() {
         await pageNavigation.selectLeftNav(4);
         await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
         const name = await pageActions.getTextName(1);
         await pageActions.setSearch('wrong text');
        await pageActions.verifyRecord('No records found');
        await pageActions.setSearch(name);
        const numberRecord = await pageActions.countRowSearch();
        const record = numberRecord +  ' records found';
        await pageActions.verifyRecord(record);
        await pageActions.click_Clear();
      });
      // 9- Verify that the Clear table button work properly
      it('Verify that the Clear table button work properly -9', async function() {
         const contentInputSearch = await pageActions.getValue();
         await console.log(contentInputSearch);
         await pageActions.setSearch('wrong text');
        await pageActions.verifyRecord('No records found');
        await pageActions.click_Clear();
        const contentInputSearch2 = await pageActions.getValue();
        await console.log(contentInputSearch);
        await expect(Boolean( contentInputSearch === contentInputSearch2)).toBe(true);
        await pageActions.clickRefresh();
      });
      // 10-  Verify that the Refresh button work properly
      it('Verify that the Refresh button work properly -10', async function() {
         const record = await pageActions.getRecord();
         const status = await pageActions.getTextName(5);
         await pageActions.SelectFilter(5, status);
         await pageActions.setSearch(status);
          await browser.sleep(3000);
         await pageActions.clickRefresh();
          // await browser.sleep(2000);
         const value = await pageActions.getValue();
         await expect(Boolean(value === '')).toBe(true);
         await pageActions.verifyRecord(record);
        // await pageActions.clickMinimize();
      });
      // 11- Verify that the tables can be expanded or minimized when click on right-top button
      it('Verify that the tables can be expanded or minimized when click on right-top button -11', async function() {
         // await pageNavigation.selectLeftNav(4);
         // await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
         // await browser.waitForAngularEnabled(false);
         await pageActions.clickMinimizeTable();
         await expect(await pageActions.verifyTableMinimized()).toBeFalsy();
         await pageActions.clickExpandedTable();
         await expect(await pageActions.verifyTableExpanded()).toBeTruthy();
      });
   // 12- Verify that the user can move to specified page when click on the page number
         it('Verify that the user can move to specified page when click on the page number -12', async function() {
            await pageActions.MoveToPageNumber();
         });
   // 13- Verify that the tables display with maximum 5 entries in one page as default
         it('Verify that the tables display with maximum 5 entries in one page as default -13', async function() {
            await pageNavigation.selectLeftNav(4);
            await browser.wait(ExpectedConditions.urlContains('vnfm/actions'), 3000);
            const numberR =  await pageActions.GetNumberRow();
            await expect(Boolean(numberR === '5')).toBe(true);
    });
   // 14- Verify that the user can choose the number of entries displayed in one page
         it('Verify that the user can choose the number of entries displayed in one page -14', async function() {
         await pageActions.verifyNumberRowDisplayOnTable(data.ACTIONS.TC_14.NUMBER_ENTRIES);
         await pageActions.clickRefresh();
         });
   // 15- Verify that user can select the number of displayed columns
         it('Verify that user can select the number of displayed columns', async function() {
            await pageActions.selectOptionNamefromShowHideColumnsList(data.ACTIONS.TC_15.COLUMNNAME);
            await pageActions.verifyShowHidecolumns(data.ACTIONS.TC_15.COLUMNNAME);
         });
   // 16-  Verify the Actions drop-list has: Pause, Resume, Cancel, Clear actions
      it('Verify the Actions drop-list has: Pause, Resume, Cancel, Clear actions', async function() {
          await browser.sleep(3000);
          await pageActions.clickRefresh();
          const valueSearch = await pageActions.getTextforSearch();
          await pageActions.setSearch(valueSearch);
         await console.log('start click Dropdown Action');
         await pageActions.clickSelect();
         // tslint:disable-next-line:max-line-length
         const isCheckDropdown = await pageActions.verifyDropDownActions(data.ACTIONS.TC_16.PAUSE, data.ACTIONS.TC_16.RESUME, data.ACTIONS.TC_16.CANCEL);
         await expect(isCheckDropdown).toBeTruthy();
         await pageActions.clickSelect();
      });

});
