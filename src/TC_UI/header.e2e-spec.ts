import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
// import { browser } from 'protractor';

describe('workspace-project Header', () => {
  let pageNavigation: PageNavigation;
  let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageNavigation = new PageNavigation();
    pageError = new PageError();
  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await pageError.cliclClearALL();
    }
  });

  // Verify that show version of VNFM work properly
  it('Verify that show version of VNFM work properly-1', async function () {
   // await browser.waitForAngularEnabled(true);
   const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageNavigation.clickUserProfile();
    await pageNavigation.VerifyVersion('Ribbon VNF Manager');
  });

  // Verify that Logout function work properly
  it('Verify that Logout function work properly-3', async function () {
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageNavigation.clickUserProfile();
    await pageNavigation.VerifyLogout();
  });
});
