import { browser} from 'protractor';
import { PageVNFMConfTenants } from '../PageObject/PageVNFMConfTenants.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { PageError } from '../PageObject/PageError.po';
import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
import { PageLogin } from '../PageObject/PageLogin.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Clear Resource', () => {

  let pageError: PageError;
  let confCloud: PageVNFMConfCloudSystems;
  let confTenant: PageVNFMConfTenants;
  let pageOnboardedVNFs: PageOnboardedVNFs;
  let pageFailedtoOnboard: PageFailedtoOnboard;
  let login: PageLogin;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageError = new PageError();
    confCloud = new PageVNFMConfCloudSystems();
    confTenant = new PageVNFMConfTenants();
    pageOnboardedVNFs = new PageOnboardedVNFs();
    pageFailedtoOnboard = new PageFailedtoOnboard();
    login = new PageLogin();
  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await pageError.cliclClearALL();
    }
  });

  it('login VNFM', async function () {

    await browser.restart();
    await browser.get(data.INFOLOGIN.URL);
    browser.driver.manage().window().maximize();
    await browser.waitForAngularEnabled(true);
    await login.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
  });

  // Clear tenant
  it('Clear tenant', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/tenant');
    await browser.waitForAngularEnabled(true);
    await confTenant.removeAllTenant();
    const result = await confTenant.isTableNoValueAfterSearch();
    await expect(Boolean(result === false)).toBeTruthy();

  });

  // Clear Cloud
  it('Clear Cloud', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/cloud');
    await browser.waitForAngularEnabled(true);
    await confCloud.removeAllCloud();
    const result = await confCloud.isTableHasValueAfterSearch();
    await expect(Boolean(result === false)).toBeTruthy();

  });

  // Clear VNF Catalog Onboarded
  it('Clear VNF Catalog Onboarded', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
    await browser.waitForAngularEnabled(true);
    await pageOnboardedVNFs.removeAllVnfOnboard();
    const result = await pageOnboardedVNFs.isTableHasValueAfterSearch();
    await expect(Boolean(result === false)).toBeTruthy();

  });

  // Clear VNF in Failed Onboard VNF Catalog Page
  it('Clear VNF in Failed Onboard VNF Catalog Page', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
    await browser.waitForAngularEnabled(true);
    await pageOnboardedVNFs.clickOnboardbtn();
    await pageFailedtoOnboard.removeAllVnfFailedOnboardVNF();
    const result = await pageFailedtoOnboard.isTableHasValueAfterSearch();
    await expect(Boolean(result === false)).toBeTruthy();

  });
});
