import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
import { browser, ExpectedConditions, protractor } from 'protractor';
import { PageShowEvents } from '../PageObject/PageShowEvents.po';
import { PageError } from '../PageObject/PageError.po';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
import { PageAdminContinuityPlan } from '../PageObject/PageAdminContinuityPlan.po';
import { PageUpdateContinuityPlanInVNF } from '../PageObject/PageUpdateContinuityPlanInVNF.po';
// import { PageLogin } from '../PageObject/PageLogin.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Live VNFs', () => {
    let pageNavigation: PageNavigation;
    let pageLiveVNFs: PageLiveVNFs;
    let pageShowEvents: PageShowEvents;
    let pageOnboardedVNFs: PageOnboardedVNFs;
    let pageFailedtoOnboard: PageFailedtoOnboard;
    let pageAdminContinuityPlan: PageAdminContinuityPlan;
    let pageUpdateContinuityPlanInVNF: PageUpdateContinuityPlanInVNF;
    let pageError: PageError;
    // let login: PageLogin;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
        pageNavigation                  = new PageNavigation();
        pageLiveVNFs                    = new PageLiveVNFs();
        pageShowEvents                  = new PageShowEvents();
        pageOnboardedVNFs               = new PageOnboardedVNFs();
        pageFailedtoOnboard             = new PageFailedtoOnboard();
        pageAdminContinuityPlan         = new PageAdminContinuityPlan();
        pageUpdateContinuityPlanInVNF   = new PageUpdateContinuityPlanInVNF();
        pageError = new PageError();
        // login = new PageLogin();
    });

    beforeEach( async() => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
    });

    // 1 - Verify that search function for VNF works properly
    it('Verify that search function for VNF works properly - 1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageLiveVNFs.setSearch('wrong text');
        await pageLiveVNFs.verifyRecord('No records found');
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_1.TEXT_SEARCH);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_1.RECORD);
    });

    // 2 - Verify that Clear Search function for Live VNF table work properly
    it('Verify that Clear Search function for Live VNF table work properly - 2', async function () {
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageLiveVNFs.setSearch('wrong text');
        await pageLiveVNFs.clickClearSearchVNFTable();
        await pageLiveVNFs.VerifyAttributeInSearch();
    });

    // 3 - Verify that the dropdown filter and sort functions work properly with Name column
    it('Verify that the dropdown filter and sort functions work properly with Name column - 3', async function () {
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        await pageLiveVNFs.clickShowHideFilterButton();
        await pageLiveVNFs.SelectFilterWithCheckbox(1, data.LIVEVNFs.TC_3.VNFNAME);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_3.RECORD);
        await pageLiveVNFs.SelectFilterWithCheckbox(1, data.LIVEVNFs.TC_3.VNFNAME);
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(1);
    });

    // 4 - Verify that the dropdown filter and sort functions work properly with ID column
    it('Verify that the dropdown filter and sort functions work properly with ID column - 4', async function () {
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        const textUID = await pageLiveVNFs.GetTextInTd(2);
        await pageLiveVNFs.SelectFilterWithCheckbox(2, textUID);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_4.RECORD);
        await pageLiveVNFs.SelectFilterWithCheckbox(2, textUID);
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(2);
    });

    // 5 - Verify that the dropdown filter and sort functions work properly with Type column
    it('Verify that the dropdown filter and sort functions work properly with Type column - 5', async function () {
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        await pageLiveVNFs.SelectFilter(3, data.LIVEVNFs.TC_5.VNFTYPE);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_5.RECORD);
        await pageLiveVNFs.SelectFilter(3, 'All');
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(3);
    });

    // 6 - Verify that the dropdown filter and sort functions work properly with Version column
    it('Verify that the dropdown filter and sort functions work properly with Version column - 6', async function () {
        const numRecord = await pageLiveVNFs.getRecord();
        await pageLiveVNFs.SelectFilter(4, data.LIVEVNFs.TC_6.VNFVERSION);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_6.RECORD);
        await pageLiveVNFs.SelectFilter(4, 'All');
        await browser.waitForAngularEnabled(true);
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(4);
    });

    // 7 - Verify that the dropdown filter and sort functions work properly with Tenant column
    it('Verify that the dropdown filter and sort functions work properly with Tenant column - 7', async function () {
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        await pageLiveVNFs.SelectFilter(6, data.LIVEVNFs.TC_7.VNFTENANT);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_7.RECORD);
        await pageLiveVNFs.SelectFilter(6, 'All');
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(6);
    });

    // 8 - Verify that the dropdown filter and sort functions work properly with Status column
    it('Verify that the dropdown filter and sort functions work properly with Status column - 8', async function () {
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        // const txtStatus = await pageLiveVNFs.GetTextInTd(7);
        await pageLiveVNFs.SelectFilter(7, data.LIVEVNFs.TC_8.VNFSTATUS);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_8.RECORD);
        await pageLiveVNFs.SelectFilter(7, 'All');
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(7);
    });

    // 9 - Verify that the dropdown filter and sort functions work properly with Cloud column
    it('Verify that the dropdown filter and sort functions work properly with Cloud column - 9', async function () {
        await pageLiveVNFs.clickShowHideColumn();
        await pageLiveVNFs.selectOptionCloumnShowOrhide('Cloud');
        await pageLiveVNFs.selectOptionCloumnShowOrhide('Created by');
        await pageLiveVNFs.selectOptionCloumnShowOrhide('Date Created');
        await pageLiveVNFs.clickCloseShowHideCloumn();
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        await pageLiveVNFs.SelectFilter(5, data.LIVEVNFs.TC_9.VNFCLOUD);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_9.RECORD);
        await pageLiveVNFs.SelectFilter(5, 'All');
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(5);
    });

    // 10- Verify that the dropdown filter and sort functions work properly with Created by column
    it('Verify that the dropdown filter and sort functions work properly with Created by column - 10', async function () {
        await browser.waitForAngularEnabled(true);
        const numRecord = await pageLiveVNFs.getRecord();
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        // const result2 = await pageLiveVNFs.verifyShowHidecolumns('Created by');
        // if (result2 === false) {
        //     await pageLiveVNFs.clickShowHideColumn();
        //     await pageLiveVNFs.selectOptionCloumnShowOrhide('Created by');
        //     await pageLiveVNFs.clickCloseShowHideCloumn();
        // }
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.SelectFilter(8, data.LIVEVNFs.TC_10.VNFCREATEDBY);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_10.RECORD);
        await pageLiveVNFs.SelectFilter(8, 'All');
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);
        await pageLiveVNFs.verifysort(8);

    });

    // // 11 - Verify that search function with "Date created" option for VNF works properly
    // it('Verify that search function with "Date created" option for VNF works properly- 11', async function() {
    //     await browser.waitForAngularEnabled(true);
    //     // const result2 = await pageLiveVNFs.verifyShowHidecolumns('Date Created');
    //     // if (result2 === false) {
    //     //     await pageLiveVNFs.clickShowHideColumn();
    //     //     await pageLiveVNFs.selectOptionCloumnShowOrhide('Date Created');
    //     //     await pageLiveVNFs.clickCloseShowHideCloumn();
    //     // }
    //     // const date = new Date();
    //     // const day = date.getDate();
    //     await pageLiveVNFs.clickbuttonDate();
    //     await pageLiveVNFs.selectDateInTable('Last hour');
    //     const result = await pageLiveVNFs.isTableHasValueAfterSearch();
    //     await expect(Boolean(result)).toBe(true);
    //     await pageLiveVNFs.clickbuttonDate();
    //     await pageLiveVNFs.selectDateInTable('Last minute');
    //     const result6 = await pageLiveVNFs.isTableHasValueAfterSearch();
    //     await expect(Boolean(result6)).toBe(false);
    //     await pageLiveVNFs.clickbuttonDate();
    //     await pageLiveVNFs.clickResetButton();
    //     await pageLiveVNFs.clickCancelDateButton();

    // });

    // 12 - Verify that Clear filter function for VNF work properly
    it('Verify that Clear filter function for VNF work properly - 12', async function() {
        browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        const numRecord = await pageLiveVNFs.getRecord();
        await pageLiveVNFs.clickShowHideFilterButton();
        await pageLiveVNFs.SelectFilterWithCheckbox(1, data.LIVEVNFs.TC_12.VNFNAME);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_12.RECORD);
        await pageLiveVNFs.clickClearFilterButton();
        const numRecord2 = await pageLiveVNFs.getRecord();
        await expect(Boolean( numRecord === numRecord2)).toBe(true);

        const result3 = await pageLiveVNFs.verifyShowHidecolumns('Date Created');
        if (result3 === true) {
            await pageLiveVNFs.clickShowHideColumn();
            await pageLiveVNFs.selectOptionCloumnShowOrhide('Date Created');
            await pageLiveVNFs.clickCloseShowHideCloumn();
        }
        const result4 = await pageLiveVNFs.verifyShowHidecolumns('Created by');
        if (result4 === true) {
            await pageLiveVNFs.clickShowHideColumn();
            await pageLiveVNFs.selectOptionCloumnShowOrhide('Created by');
            await pageLiveVNFs.clickCloseShowHideCloumn();
        }
        const result5 = await pageLiveVNFs.verifyShowHidecolumns('Cloud');
        if (result5 === true) {
            await pageLiveVNFs.clickShowHideColumn();
            await pageLiveVNFs.selectOptionCloumnShowOrhide('Cloud');
            await pageLiveVNFs.clickCloseShowHideCloumn();
        }
    });

    // 13- Verify that user can show all events of the VNFs
    it('Verify that user can show all events of the VNFs- 13', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_13.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_13.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        const txtShowEvent = await pageShowEvents.getTextShowEvent();
        expect(Boolean(txtShowEvent === data.LIVEVNFs.TC_13.TEXTVERIFY)).toBe(true);
    });

    // 14 - Verify that search function for VNF event works properly
    it('Verify that search function for VNF event works properly- 14', async function() {
        await pageShowEvents.setSearch('wrong text search');
        await pageShowEvents.verifyRecord('No records found');
        await pageShowEvents.setSearch('INFO');
        const result = await pageShowEvents.isTableHasValueAfterSearch();
        await expect(Boolean(result)).toBe(true);
        await pageShowEvents.clickClearSearch();
    });

    // 15 - Verify that user can change the event number of an event VNF page
    it('Verify that user can change the event number of an event VNF page- 15', async function() {
        await pageShowEvents.selectRowOnPage(data.LIVEVNFs.TC_15.NUMBERROWS);
        const numbers = await pageShowEvents.countNumberRow();
        await expect(Boolean(numbers <= data.LIVEVNFs.TC_15.NUMBERROWS)).toBe(true);
        await pageShowEvents.selectRowOnPage(data.LIVEVNFs.TC_15.NUMBERROW);
        const number = await pageShowEvents.countNumberRow();
        await expect(Boolean(number <= data.LIVEVNFs.TC_15.NUMBERROW)).toBe(true);
    });

    // 16 - Verify that the number total of the event is equal with the total record
    it('Verify that the number total of the event is equal with the total record- 16', async function() {
        await pageShowEvents.selectRowOnPage(data.LIVEVNFs.TC_16.NUMBERROW);
        const txttext = await pageShowEvents.getRecord();
        const numRecord = txttext.slice(13);
        await console.log('number record: ' + numRecord);
        const numberRecordTotal = Number(numRecord.replace(' records' , ''));
        await console.log('number record: ' + numberRecordTotal);
        if (numberRecordTotal <= data.LIVEVNFs.TC_16.NUMBERROW) {
            const count = await pageShowEvents.countNumberRow();
            await expect(Boolean(count === numberRecordTotal)).toBe(true);
        } else {
            await pageShowEvents.clickbtnPageEnd();
            const numPage = await pageShowEvents.getNumberPageEnd();
            const numberPage = Number(numPage);
            const rowinpageEnd = await pageShowEvents.countNumberRow();
            const totalRowNoPageEnd = ((numberPage - 1) * data.LIVEVNFs.TC_16.NUMBERROW);
            await console.log('total 1: ' + totalRowNoPageEnd);
            const totalRow = (totalRowNoPageEnd + rowinpageEnd);
            await console.log('total 2: ' + totalRow);
        }
    });

    // 17 - Verify that user can sort the event of the VNF by date or severity
    it('Verify that user can sort the event of the VNF by date or severity- 17', async function() {
        await pageShowEvents.clickShowHideFilter();
        await pageShowEvents.SelectFilter(4, 'INFO');
        await browser.waitForAngularEnabled(true);
        const result2 = await pageShowEvents.isTableHasValueAfterSearch();
        await expect(Boolean(result2)).toBe(true);
        // date
        // const date = new Date();
        // const day = date.getDate();
        await pageShowEvents.clickDate();
        await pageShowEvents.selectDateInTable('Last 5 hours');
        const result3 = await pageShowEvents.isTableHasValueAfterSearch();
        await expect(Boolean(result3)).toBe(true);
        await pageLiveVNFs.clickCloseShowEvent();
    });

    // 18 - Verify that drop-list can not be closed
    it('Verify that drop-list can not be closed - 18', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_18.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.verifyFormDisplayed();
            // await pageLiveVNFs.clickSelectActions();
        }
    });

    // 18 - Verify that the event items only is displayed with specific selected VNF
    it('Verify that the event items only is displayed with specific selected VNF- 18', async function() {
        // await pageLiveVNFs.clickSelectActions();
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.SHOWEVENT);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.GETKEY);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.TERMINATE);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.MAINTENANCE);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.REENABLE);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.CONTINUITYPLAN);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.RESYNC);
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_18.UPGRADE);
        await pageLiveVNFs.clickSelectActions();
    });

    // 19 - Verify that drop-list function available on live VNF table
    it('Verify that drop-list function available on live VNF table - 19', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_19.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.SHOWEVENT);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.GETKEY);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.TERMINATE);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.MAINTENANCE);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.REENABLE);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.CONTINUITYPLAN);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.RESYNC);
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_19.UPGRADE);
            await pageLiveVNFs.clickSelectActions();
        }
    });

    // 20 - Verify the VNF drop-list can return to default list after executing an action
    it('Verify the VNF drop-list can return to default list after executing an action - 20', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_20.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_20.FUNCTION);
            await pageLiveVNFs.clickCloseGetKey();
        }
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_20.VNFNAME);
        const textFunction = await pageLiveVNFs.getTextActionDefault();
        await expect(Boolean(textFunction === 'Select Action')).toBe(true);
    });

    // 21- Verify that user can't choose disable option from VNF drop-list
    it('Verify that user can not chose disable option from VNF drop-list - 21', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_21.STATUS);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
        }
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_21.FUNCTION);
        const result2 = await pageLiveVNFs.verifyEventItemDisable(data.LIVEVNFs.TC_21.FUNCTION);
        await expect(Boolean(result2)).toBe(false);
    });


    // 22 - Verify that reenable option is disable on drop-list when the VNF is Ready state
    it('Verify that reenable option is disable on drop-list when the VNF is Ready state - 22', async function() {
        await pageNavigation.selectLeftNav(2);
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_22.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_22.FUNCTION);
            const result2 = await pageLiveVNFs.verifyEventItemDisable(data.LIVEVNFs.TC_22.FUNCTION);
            await expect(Boolean(result2)).toBe(false);
        }
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_22.FUNCTION);
        const textFunction = await pageLiveVNFs.getTextActionDefault();
        await expect(Boolean(textFunction === 'Select Action')).toBe(true);
    });

    // 23- Verify that user can get ssh key from the VNF
    it('Verify that user can get ssh key from the VNF- 23', async function() {
        // await pageNavigation.selectLeftNav(3);
        // browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_23.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_23.FUNCTION);
            const result2 = await pageLiveVNFs.verifyGetKeyInForm();
            await expect(Boolean(result2)).toBe(true);
        }
        await pageLiveVNFs.clickCloseGetKey();
    });

    // 24 - Verify that user can execution get key actions multiple times on live VNF table
    it('Verify that user can execution get key actions multiple times on live VNF table - 24', async function() {
        // browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_24.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_24.FUNCTION);
            const result4 = await pageLiveVNFs.verifyGetKeyInForm();
            await expect(Boolean(result4)).toBe(true);
            await pageLiveVNFs.clickGetKey();
            const result10 = await pageLiveVNFs.verifyDisplayPupopErrorGetKey();
            if ( result10 === true) {
                await pageLiveVNFs.clickOkWhenGetKeyError();
            }
            await browser.waitForAngularEnabled(true);
            const result1 = await pageError.verifyMessegeError();
            const result2 = await pageError.verifyMessegeWarning();
            const result3 = await pageError.verifyMessegeInfo();
            if ( result1 === true || result2 === true || result3 === true) {
                await pageError.cliclClearALL();
            }
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_24.FUNCTION);
            const result5 = await pageLiveVNFs.verifyGetKeyInForm();
            await expect(Boolean(result5)).toBe(true);
            await pageLiveVNFs.clickGetKey();
            const result11 = await pageLiveVNFs.verifyDisplayPupopErrorGetKey();
            if ( result11 === true) {
                await pageLiveVNFs.clickOkWhenGetKeyError();
            }
        }
    });

    // 25 - Verify that user can put the VNF to Maintenance mode from Ready status
    it('Verify that user can put the VNF to Maintenance mode from Ready status - 25', async function() {
        await pageNavigation.selectLeftNav(2);
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_25.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_25.FUNCTION);
            await pageLiveVNFs.clickConfirmMaintenance();
        }
        await browser.waitForAngularEnabled(true);
        await browser.sleep(5000);
        const textStatus = await pageLiveVNFs.GetTextInTd(7);
        await expect(Boolean(textStatus === data.LIVEVNFs.TC_25.VERIFYTEXT)).toBe(true);
    });

    // 26 - Verify the VNF can leave the Maintenance mode with "re-enable" function
    it('Verify the VNF can leave the Maintenance mode with "re-enable" function - 26', async function() {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_26.FUNCTION);
        await pageLiveVNFs.clickConfirmReEnable();
        const checkStatus = await pageLiveVNFs.waitVNFReady(1, 7, 'Degraded', 30000, 'TC failed due to Degraded time out');
        await expect(checkStatus).toBeTruthy();
    }, 31000);

    // 27 - Verify that the VNFM can resync with VNFM using "Resync" option
    it('Verify that the VNFM can resync with VNFM using "Resync" option - 27', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_27.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_27.FUNCTION);
            await pageLiveVNFs.clickConfirmReSync();
        }
        await browser.waitForAngularEnabled(true);
        await browser.sleep(5000);
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
        const checkStatus = await pageLiveVNFs.waitVNFReady(1, 7, 'Ready', 30000, 'TC failed due to Ready time out');
        await expect(checkStatus).toBeTruthy();
    }, 31000);

    // 28 - Verify that continuity plan drop list will appear when clicked in VNF Life-cycle.
    it('Verify that continuity plan drop list will appear when clicked in VNF Life-cycle - 28', async function() {
        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_28.VNFNAME);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_28.RECORD);
        const textStatus = await pageLiveVNFs.GetTextInTd(7);
        await expect(Boolean(textStatus === 'Ready')).toBe(true);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.verifyEventDisplayInForm(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        const txtContinuity = await pageUpdateContinuityPlanInVNF.getTextContinuityPlanPupop();
        await expect(txtContinuity).toEqual(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
    });

    // 29 - Verify that the continuity plan drop list display correctly the available list
    it('Verify that the continuity plan drop list display correctly the available list- 29', async function() {
        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await pageAdminContinuityPlan.search_global_txt.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await pageAdminContinuityPlan.search_global_txt.sendKeys(protractor.Key.BACK_SPACE);
        const listName = await pageAdminContinuityPlan.getListNameContinuityPlanInTable();
        await console.log('list Name Continuity Plan', listName);

        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_28.VNFNAME);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_28.RECORD);
        const textStatus = await pageLiveVNFs.GetTextInTd(7);
        await expect(Boolean(textStatus === 'Ready')).toBe(true);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        const txtContinuity = await pageUpdateContinuityPlanInVNF.getTextContinuityPlanPupop();
        await expect(txtContinuity).toEqual(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        await pageUpdateContinuityPlanInVNF.clickContinuityPlanDropdown();
        const listName2 = await pageUpdateContinuityPlanInVNF.getTextOptionInDrodown();
        await expect(listName).toEqual(listName2);
        await pageUpdateContinuityPlanInVNF.clickbtnNo();
    });

    // 30 - Verify that user can choose continuity plan from the drop list
    it('Verify that user can choose continuity plan from the drop list - 30', async function() {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_28.VNFNAME);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_28.RECORD);
        const textStatus = await pageLiveVNFs.GetTextInTd(7);
        await expect(Boolean(textStatus === 'Ready')).toBe(true);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        const txtContinuity = await pageUpdateContinuityPlanInVNF.getTextContinuityPlanPupop();
        await expect(txtContinuity).toEqual(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        await pageUpdateContinuityPlanInVNF.clickContinuityPlanDropdown();
        const listName2 = await pageUpdateContinuityPlanInVNF.getTextOptionInDrodown();
        await pageUpdateContinuityPlanInVNF.clickChooseOptionContinuityPlan(listName2[1]);
        const txt_Name = await pageUpdateContinuityPlanInVNF.getTextContinuityPlanOption();
        await expect(txt_Name).toEqual(listName2[1]);
        await pageUpdateContinuityPlanInVNF.clickbtnNo();
    });

    // 31 - Verify that user can re-choose another continuity plan in the drop list
    it('Verify that user can re-choose another continuity plan in the drop list - 31', async function() {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_28.VNFNAME);
        await pageLiveVNFs.verifyRecord(data.LIVEVNFs.TC_28.RECORD);
        const textStatus = await pageLiveVNFs.GetTextInTd(7);
        await expect(Boolean(textStatus === 'Ready')).toBe(true);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        const txtContinuity = await pageUpdateContinuityPlanInVNF.getTextContinuityPlanPupop();
        await expect(txtContinuity).toEqual(data.LIVEVNFs.TC_28.CONTINUITYPLAN);
        await pageUpdateContinuityPlanInVNF.clickContinuityPlanDropdown();
        const listName2 = await pageUpdateContinuityPlanInVNF.getTextOptionInDrodown();
        await pageUpdateContinuityPlanInVNF.clickChooseOptionContinuityPlan(listName2[2]);
        const txt_Name = await pageUpdateContinuityPlanInVNF.getTextContinuityPlanOption();
        await expect(txt_Name).toEqual(listName2[2]);
    });

    // 32 - Verify that user can choose upgrade function from UI
    it('Verify that user can choose upgrade function from UI - 32', async function() {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.LIVEVNFs.TC_32.VERSIONCSAR);
        const afterSearch = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (afterSearch === false) {
            await pageOnboardedVNFs.clickOnboardbtn();
            await pageFailedtoOnboard.setSearch(data.LIVEVNFs.TC_32.VERSIONCSAR);
            const result1 = await pageFailedtoOnboard.isTableHasValueAfterSearch();
            if (result1 === true) {
                await pageFailedtoOnboard.clickActionsDopdown();
                await pageFailedtoOnboard.clickFunctionActionsDopdown('Delete');
                await pageFailedtoOnboard.clickConfirmDelete();
            }
            await browser.sleep(10000);
            await pageFailedtoOnboard.funcUploadCSAR(data.LIVEVNFs.TC_32.PATH);
            await pageFailedtoOnboard.clickBackToPageVNFCatalog();
        }
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFs.TC_32.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            const textStatus = await pageLiveVNFs.GetTextInTd(7);
            await expect(Boolean(textStatus === 'Ready')).toBe(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_32.FUNCTION);
            const txtVNFMPreference = await pageLiveVNFs.getTextUpgrade();
            expect(Boolean(txtVNFMPreference === 'Upgrade VNF')).toBe(true);
            await pageLiveVNFs.clickOptionSelectVersionUpgrade(data.LIVEVNFs.TC_32.VERSIONCSAR);
            await pageLiveVNFs.clickConfirm();
            const checkStatus = await pageLiveVNFs.waitVNFReady(1, 7, 'Upgrading', 30000, 'TC failed due to Upgrading time out');
            await expect(checkStatus).toBeTruthy();
        }
    }, 51000);

    // 33 - Verify that user can cancel upgrade function from UI
    it('Verify that user can cancel upgrade function from UI - 33', async function() {
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFs.TC_33.FUNCTION);
        const txtVNFMPreference = await pageLiveVNFs.getTextCancelUpgrade();
        expect(Boolean(txtVNFMPreference === 'Cancel VNF Upgrade')).toBe(true);
        await pageLiveVNFs.clickConfirm();
        const checkStatus = await pageLiveVNFs.waitVNFReady(1, 7, 'Ready', 30000, 'TC failed due to Ready time out');
        await expect(checkStatus).toBeTruthy();
    }, 51000);


});
