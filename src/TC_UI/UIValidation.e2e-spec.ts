import { browser, /*by, element*/ ExpectedConditions } from 'protractor';
// import { protractor } from 'protractor';
import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('testcase vnf', () => {
  let login: PageLogin;
  let confCloud: PageVNFMConfCloudSystems;
  let navigation: PageNavigation;


  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    login = new PageLogin();
    confCloud = new PageVNFMConfCloudSystems();
    navigation = new PageNavigation();

  });






  it('login VNFM', async function () {

    await browser.restart();
    await browser.get(data.INFOLOGIN.URL);
    await browser.waitForAngularEnabled(false);
    browser.driver.manage().window().maximize();
    await browser.sleep(3000);
    await login.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
  });


  it('VNFM Configuration', async function () {
    browser.waitForAngularEnabled(false);
    await navigation.selectLeftNav(4);
    await navigation.selectLeftNavSecond(4, 1);
    await browser.sleep(2000);
  });



  // Verify that the VNFM UI returns to default display when refreshing the UI
  it('Verify that the VNFM UI returns to default display when refreshing the UI', async function () {
    await navigation.selectLeftNavSecond(5, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
    browser.driver.sleep(5000);
    browser.driver.navigate().refresh();
    browser.driver.sleep(60000);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 4000);
    await confCloud.verifyShowTextCloud('Cloud Systems');

  });


  // Verify that after 30 min inactive session will be time out and redirect to login page
  it('Verify that after 30 min inactive session will be time out and redirect to login page', async function () {
    await navigation.selectLeftNav(5);
    await navigation.selectLeftNavSecond(5, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 4000);
    browser.driver.sleep(1800000);
    await browser.wait(ExpectedConditions.urlContains('login'), 3000);

  });



});
