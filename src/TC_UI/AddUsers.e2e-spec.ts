import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageRole } from '../PageObject/PageRole.po';
import { PageAddUser } from '../PageObject/PageAddUser.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageVNFMConfTenants } from '../PageObject/PageVNFMConfTenants.po';
import { PageError } from '../PageObject/PageError.po';
import { PageUsers } from '../PageObject/PageUsers.po';
import { PageEditUser } from '../PageObject/PageEditUser.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Add Users', () => {
  let pageRole: PageRole;
  let confTenant: PageVNFMConfTenants;
  let pageNavigation: PageNavigation;
  let pageAddUser: PageAddUser;
  let pageError: PageError;
   let pageUsers: PageUsers;
   let pageEditUser: PageEditUser;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageRole = new PageRole();
    confTenant = new PageVNFMConfTenants();
    pageNavigation = new PageNavigation();
    pageAddUser = new PageAddUser();
    pageError = new PageError();
    pageUsers = new PageUsers();
    pageEditUser = new PageEditUser();
  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await pageError.cliclClearALL();
    }
  });

  // 1 - Verify that the Empty form is displayed when user click on Add User button
  it('Verify that the Empty form is displayed when user click on Add User button - 1 ', async function () {

    await browser.get(browser.baseUrl + '/#/vnfm/users');
    await browser.waitForAngularEnabled(true);
    await pageUsers.clickAddUser();
    await pageAddUser.verifyAfterClickAddUser();
  });

  // 2 - Verify that the "missing required info" message was displayed when user forgot to input the required field when add user
  // tslint:disable-next-line:max-line-length
  it('Verify that the "missing required info" message was displayed when user forgot to input the required field when add user - 2', async function () {
    await pageAddUser.clickbuttonSave();
    await pageAddUser.verifyMissingUsername();
    await pageAddUser.verifyMissingPassword();
    await pageAddUser.verifyMissingRoleAndTenant();
    // await pageAddUser.clickbuttonCancel();
  });

  // 3 - Verify that the Add user function can not complete when missing required info
  it('Verify that the Add user function can not complete when missing required info - 3 ', async function () {
    // await pageAddUser.clickAddUser();
    await pageAddUser.setUserName(data.ADMIN_ADDUSER.TC_3.USERNAME);
    await pageAddUser.setPassword(data.ADMIN_ADDUSER.TC_3.PASSWORD);
    await pageAddUser.clickbuttonSave();
    await pageAddUser.verifyMissingPasswordConfirm();
    await pageAddUser.verifyMissingRoleAndTenant();
    // await pageAddUser.clickbuttonCancel();
  });

  // 4- Verify that the "missing required info" message was displayed when user forgot to select the Role and Tenant
  it('Verify that the "missing required info" message was displayed when user forgot to select the Role and Tenant - 4', async function () {
    // await pageAddUser.clickAddUser();
    await pageAddUser.setUserName(data.ADMIN_ADDUSER.TC_4.USERNAME);
    await pageAddUser.setPassword(data.ADMIN_ADDUSER.TC_4.PASSWORD);
    await pageAddUser.clickbuttonSave();
    await pageAddUser.verifyMissingRoleAndTenant();
    // await pageAddUser.clickbuttonCancel();
  });

  // 5 - Verify that the Add user form has the section to assign the role and tenant
  it('Verify that the Add user form has the section to assign the role and tenant - 5', async function () {
    // await pageAddUser.clickAddUser();
    await pageAddUser.verifyDropdownTenantandRole();
    // await pageAddUser.clickbuttonCancel();
  });

  // 6 - Verify that click on "+" button can add the new Role and Tenant row
  it('Verify that click on "+" button can add the new Role and Tenant row - 6', async function () {
    const numberRow = await pageAddUser.verifyNewRoleAndTenant();
    await pageAddUser.clickAddRoleAndTenant();
    const numberRowAfterAdd = await pageAddUser.verifyNewRoleAndTenant();
    await console.log(numberRowAfterAdd);
    await expect(numberRowAfterAdd > numberRow).toBeTruthy();
  });

  // 7 - Verify that click on "-" button can subtract the Role and Tenant row
  it('Verify that click on "-" button can subtract the Role and Tenant row - 7', async function () {
    const numberRow = await pageAddUser.verifyNewRoleAndTenant();
    await pageAddUser.clickSubTractRoleAndTenant();
    const numberRowAndSubTract = await pageAddUser.verifyNewRoleAndTenant();
    await console.log(numberRowAndSubTract);
    await expect(numberRowAndSubTract < numberRow).toBeTruthy();
  });

  // 8 - Verify that the last Role and Tenant row can not be subtracted
  it('Verify that the last Role and Tenant row can not be subtracted - 8', async function () {
    await pageAddUser.verifyLastTenantAndRoleCanNotSubtract();
    await pageAddUser.clickbuttonCancel();
  });

  // 9 - Verify that the Role drop-down list can list all the current role of system
  it('Verify that the Role drop-down list can list all the current role of system - 9', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/roles'), 6000);
    await browser.waitForAngularEnabled(true);
    const numRecord = await pageRole.getRecordNumber();
    const numRecordNew = numRecord.slice(13);
    await console.log('number record: ' + numRecordNew);
    const numberRecordTotal = Number(numRecordNew.replace(' record' , ''));
    const numberRole = (numberRecordTotal + 1);
    await console.log('numberRole: ' + numberRole);
    // await pageAddUser.getListRole();
    await pageNavigation.selectLeftNavSecond(5, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/users'), 6000);
    await browser.waitForAngularEnabled(true);
    await pageAddUser.clickAddUser();
    await pageAddUser.clickSelectRole();
    const numberItemRole = await pageAddUser.getNumBerRole();
    await console.log(numberRole);
    await expect(numberItemRole === numberRole).toBeTruthy();
    await pageAddUser.clickbuttonCancel();
  });

  // 10 - Verify that the Tenant drop-down list can list all the current tenant of system
  it('Verify that the Tenant drop-down list can list all the current tenant of system - 10', async function () {
    await pageNavigation.selectLeftNav(4);
    await pageNavigation.selectLeftNavSecond(4, 2);
    await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 6000);
    await browser.waitForAngularEnabled(true);
    const numRecord = await confTenant.getRecordNumber();
    const numRecordNew = numRecord.slice(13);
    await console.log('number record: ' + numRecordNew);
    const numberRecordTotal = Number(numRecordNew.replace(' records' , ''));
    await console.log('string: ' + numberRecordTotal);
    // await pageAddUser.getListRole();
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 1);
    await browser.wait(ExpectedConditions.urlContains('vnfm/users'), 6000);
    await browser.waitForAngularEnabled(true);
    await pageAddUser.clickAddUser();
    await pageAddUser.clickSelectTenant();
    const numberItemRole = await pageAddUser.getNumBerTenant();
    await console.log(numberItemRole);
    await expect(numberRecordTotal === numberItemRole).toBeTruthy();
    await pageAddUser.clickSelectTenant();
  });

  // 11 - Verify that the Add user function can close the table when click on cancel button
  it('Verify that the Add user function can close the table when click on cancel button - 11', async function () {
    await pageAddUser.clickbuttonCancel();
    await pageAddUser.verifyAfterClickCancel();
  });

  // 12 - Verify that the Add user function can work successfully when fill all the field with correct info
  it('Verify that the Add user function can work successfully when fill all the field with correct info - 12', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 1);
    await browser.waitForAngularEnabled(true);
    await pageUsers.setSearch(data.ADMIN_ADDUSER.TC_12.USERNAME);
    await browser.waitForAngularEnabled(true);
    const result = await pageUsers.isTableHasNoValueAfterSearch();
    if (result === true) {
      await pageEditUser.clickDelete();
      await pageEditUser.clickConfirmDelete();
    }
    await pageAddUser.clickAddUser();
    await pageAddUser.funcAddUser(data.ADMIN_ADDUSER.TC_12.USERNAME, data.ADMIN_ADDUSER.TC_12.PASSWORD);
  });

  // 13 - Verify that the Users table can update values after added user
  it('Verify that the Users table can update values after added user - 13 ', async function () {
    await browser.waitForAngularEnabled(true);
    await pageUsers.setSearch(data.ADMIN_ADDUSER.TC_13.USERNAME);
    await pageUsers.verifyRecord(data.ADMIN_ADDUSER.TC_13.RECORD);
  });


});



