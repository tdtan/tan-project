import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageAdminSetting } from '../PageObject/PageAdminSetting.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageError } from '../PageObject/PageError.po';


const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin Setting', () => {
    let pageNavigation: PageNavigation;
    let pageAdminSetting: PageAdminSetting;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageNavigation = new PageNavigation();
        pageAdminSetting = new PageAdminSetting();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // 1 - Verify that the sort functions work properly with Element column
    it('Verify that the sort functions work properly with Element column - 1', async function () {
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageAdminSetting.verifysort(1);
    });

    // 2 - Verify that the sort functions work properly with Value column
    it('Verify that the sort functions work properly with Value column - 2', async function () {

        await pageAdminSetting.verifysort(2);
    });

    // 3 - Verify that user can select the number of displayed columns
    it('Verify that user can select the number of displayed columns - 3', async function () {

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageAdminSetting.selectOptionNamefromShowHideColumnsList(data.ADMIN_SETTINGS.TC_3.COLUMNNAME);
        await pageAdminSetting.verifyShowHidecolumns(data.ADMIN_SETTINGS.TC_3.COLUMNNAME);
    });

    // 4 - Verify that the Search function work properly
    it('Verify that the Search function work properly - 4 ', async function () {

        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 5000);
        await browser.waitForAngularEnabled(true);
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
        await browser.waitForAngularEnabled(true);

        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageAdminSetting.setSearch('wrong text');
        await pageAdminSetting.verifyRecord('No records found');
        await pageAdminSetting.setSearch(data.ADMIN_SETTINGS.TC_4.INPUT_TEXT);
        await pageAdminSetting.verifyRecord(data.ADMIN_SETTINGS.TC_4.RECORD);
        await pageAdminSetting.clickRefesh();
    });

    // 5 - Verify that the Clear text search in table work properly
    it('Verify that the Clear text search in table work properly - 5 ', async function () {

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageAdminSetting.setSearch('wrong text');
        await pageAdminSetting.verifyRecord('No records found');
        await pageAdminSetting.clickClear();
        await browser.waitForAngularEnabled(true);
        const Value2 = await pageAdminSetting.getTextSearch();
        await expect(Boolean(Value2 === '')).toBe(true);
    });

    // 6 - Verify that table can Show or Hide filter form when click button Hide Filter
    it('Verify that table can Show or Hide filter form when click button Hide Filter - 6 ', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminSetting.clickShowHideFilterButtonInTable();
        await expect(pageAdminSetting.verifyInputInFilterDisplay()).toBeTruthy();
        await pageAdminSetting.clickShowHideFilterButtonInTable();
        await expect(pageAdminSetting.verifyInputInFilterDisplay()).toBeFalsy();
    });

    // 7 - Verify that the clear function of filter work properly
    it('Verify that the clear function of filter work properly - 7 ', async function () {

        await pageAdminSetting.clickShowHideFilterButtonInTable();
        await expect(pageAdminSetting.verifyInputInFilterDisplay()).toBeTruthy();
        await pageAdminSetting.setTextIntoElementInput('text search value');
        await pageAdminSetting.clickClearTextOfFilter();
        const textFilter = await pageAdminSetting.getAttributeOfTextFilter();
        await expect(Boolean(textFilter === '')).toBe(true);
        await pageAdminSetting.clickShowHideFilterButtonInTable();
    });

    // 8 - Verify that the user can move to specified page when click on the page number
    it('Verify that the user can move to specified page when click on the page number - 8', async function () {
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageAdminSetting.MoveToPageNumber(data.ADMIN_SETTINGS.TC_8.PAGE_NUMBER);
        await pageAdminSetting.clickRefesh();
    });

    // 9 - Verify that the tables display with maximum 5 entries in one page as default
    it('Verify that the tables display with maximum 5 entries in one page as default - 9 ', async function () {

        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        const rowNumber = await pageAdminSetting.verifyDisplayEntriesDefault();
        await expect(Boolean(rowNumber === 5)).toBe(true);
    });

    // 10 - Verify that the user can choose the number of entries diplayed in one page
    it('Verify that the user can choose the number of entries displayed in one page - 10 ', async function () {
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }

        await pageAdminSetting.selectNumberRowOnTable(data.ADMIN_SETTINGS.TC_10.NUMBER_ENTRIES);
        await browser.waitForAngularEnabled(true);
        const rowNumber = await pageAdminSetting.verifyDisplayEntriesDefault();
        await console.log('abc: ' + rowNumber);
        await expect(Boolean(rowNumber === 15)).toBe(true);
    });


});

