import { browser} from 'protractor';
//import { PageError } from '../PageObject/PageError.po';
import { PageAdminContinuityPlan } from '../PageObject/PageAdminContinuityPlan.po';
// import { PageAddContinuityPlan } from '../PageObject/PageAddContinuityPlan.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');
describe('workspace-project Continuity Plan Page', () => {
   // let pageAdminContinuityPlan: PageAdminContinuityPlan;
    let ACP = new PageAdminContinuityPlan();
  //  let add = new PageAddContinuityPlan();
    //let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
         ACP = new PageAdminContinuityPlan();
      //   add = new PageAddContinuityPlan();
       // pageError = new PageError();
    });

    // beforeEach(async () => {
    //     const result1 = await pageError.verifyMessegeError();
    //     const result2 = await pageError.verifyMessegeWarning();
    //     const result3 = await pageError.verifyMessegeInfo();
    //     if (result1 === true || result2 === true || result3 === true) {
    //         await pageError.cliclClearALL();
    //     }
    // });


    // Verify that the empty form was displayed when user click on add plan button
    it('19. Verify that user can sort by pressing up symbol in the plan name', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await ACP.ClickSortIconPlanNameHeader();
        await expect(await ACP.sort_up_icon.isDisplayed()).toBeTruthy();
        await console.log('Sort Up icon displays on Plan Name Header');
    });
    it('20. Verify that user can sort by pressing down symbol in the plan name', async function () {
        await browser.waitForAngularEnabled(true);
        await ACP.sort_up_icon.click();
        await expect(await ACP.sort_down_icon.isDisplayed()).toBeTruthy();
        await console.log('Sort Down icon displays on Plan Name Header');
    });
    it('21. Verify that the filter table will appear when pressing filter button', async function () {
        await browser.waitForAngularEnabled(true);
        await ACP.ClickOnFilterIcon();
        await expect(await ACP.clear_filter_icon.isDisplayed()).toBeTruthy();
        await console.log('clear filter icon displays on UI');
    });
    it('22. Verify that user can sort data by name and Description', async function () {
        await browser.waitForAngularEnabled(true);
        await ACP.ClickOnPlanNameHeader();
        await expect(await ACP.sort_up_icon.isDisplayed()).toBeTruthy();
        await console.log('Sort Up icon displays on Plan Name Header');
        await ACP.ClickOnDescriptionHeader();
        await expect(await ACP.sort_up_icon.isDisplayed()).toBeTruthy();
        await console.log('Sort Up icon displays on Description Header');
    });
    it('23. Verify that the list of plan name was displayed correctly in global searching', async function () {
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.search_global_txt.sendKeys(data.CONTINUITYPLAN.TC_23.GLOBAL_KEY);
        let flag = await ACP.CheckGlobalSearch(data.CONTINUITYPLAN.TC_23.GLOBAL_KEY);
        await expect(flag).toBe(true);
        await console.log(data.CONTINUITYPLAN.TC_23.GLOBAL_KEY, 'display on table');
     });
     it('25. Verify that user can cancel filter by press filter button again', async function () {
        //  await browser.refresh();
        await browser.waitForAngularEnabled(true);
        // await ACP.ClickOnFilterIcon();
        await ACP.ClickOnFilterIcon();
        await expect(await ACP.clear_filter_icon.isPresent()).toBe(false);
        await console.log('clear filter icon is not display on table');
     });
    it('26. Verify that user can filter by plan name with correctly result', async function () {
        // await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.add_btn.click();
        await ACP.AddCP(data.CONTINUITYPLAN.TC_26.PLANNAME_KEY,data.CONTINUITYPLAN.TC_27.DESCRIPTION_KEY);
        
        await ACP.ClickOnFilterIcon();
        await ACP.plan_name_txt.sendKeys(data.CONTINUITYPLAN.TC_26.PLANNAME_KEY);
        let flag = await ACP.CheckPlanNameFilter(data.CONTINUITYPLAN.TC_26.PLANNAME_KEY);
        await expect(flag).toBe(true);
        await browser.sleep(10000);
        await console.log(data.CONTINUITYPLAN.TC_26.PLANNAME_KEY, 'display on table');
    });
    it('27. Verify that user can filter by description with correctly result', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.ClickOnFilterIcon();
        await ACP.description_txt.sendKeys(data.CONTINUITYPLAN.TC_27.DESCRIPTION_KEY);
        let flag = await ACP.CheckDescriptionFilter(data.CONTINUITYPLAN.TC_27.DESCRIPTION_KEY);
        await expect(flag).toBe(true);
        await console.log(data.CONTINUITYPLAN.TC_27.DESCRIPTION_KEY, 'display on table');
    });
    it('28. Verify that user can close the searching table by click "x" symbol', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.ClickOnFilterIcon();
        await ACP.plan_name_txt.sendKeys(data.CONTINUITYPLAN.TC_26.PLANNAME_KEY);
        await ACP.description_txt.sendKeys(data.CONTINUITYPLAN.TC_27.DESCRIPTION_KEY);
        await ACP.clear_filter_icon.click();
        await expect(await ACP.plan_name_txt.getText()).toEqual('');
        await expect(await ACP.description_txt.getText()).toEqual('');

    });
    it('30. Verify that the show columns table will appear when user press system button', async function () {
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.show_hide_col_icon.click();
        await expect(await ACP.close_column_popup.isPresent()).toBe(true);
        await console.log('Close button displays on UI');
    });
    it('31. Verify that user can close the form by pressing close button', async function () {
        //  await browser.refresh();
        await browser.waitForAngularEnabled(true);
        //  await ACP.show_hide_col_icon.click();
        await ACP.close_column_popup.click();
        await expect(await ACP.close_column_popup.isPresent()).toBe(false);
        await console.log('Close button does not display on UI');
    });
    it('32. Verify that every square in show columns table can be tick', async function () {
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.show_hide_col_icon.click();
        await ACP.plan_name_checkbox.click();
        await expect(await ACP.plan_name_checkbox.isSelected()).toBe(false);
        await console.log('Plan name checkbox is not selected');
    });
    it('33. Verify that all square in show columns table can be tick at a time', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.show_hide_col_icon.click();
        await ACP.plan_name_checkbox.click();
        await expect(await ACP.plan_name_checkbox.isSelected()).toBe(false);
        await console.log('Plan name checkbox is not selected');
        await ACP.description_checkbox.click();
        await expect(await ACP.description_checkbox.isSelected()).toBe(false);
        await console.log('Description checkbox is not selected');
        await ACP.delete_checkbox.click();
        await expect(await ACP.delete_checkbox.isSelected()).toBe(false);
        await console.log('Delete checkbox is not selected');
    });
    it('34. Verify that columns which were not ticked will not appear in Continuity plans dashboard', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.show_hide_col_icon.click();
        await ACP.plan_name_checkbox.click();
        let txt = await ACP.header_PlanName.getAttribute(data.CONTINUITYPLAN.TC_34.ATTRIBUTE);
        await expect(txt).toEqual(data.CONTINUITYPLAN.TC_34.STYLE);
        await console.log('Plan name header is not displayed on table');
    });
    it('35. Verify that all columns were ticked can appear at a time in Continuity plans dashboard', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.show_hide_col_icon.click();
        await ACP.plan_name_checkbox.click();
        await ACP.plan_name_checkbox.click();
        let txt = await ACP.header_PlanName.getAttribute(data.CONTINUITYPLAN.TC_35.ATTRIBUTE);
        await expect(txt).toEqual(data.CONTINUITYPLAN.TC_35.STYLE);
        await console.log('Plan name header is displayed on table');
    });
    it('36. Verify that user can close show columns table by click "x" symbol', async function () {

        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.show_hide_col_icon.click();
        await ACP.x_icon.click();
        await expect(await ACP.x_icon.isPresent()).toBe(false);
        await console.log('X button of popup does not display on UI');
    });
    it('37. Verify that the showing number record option are defaulted as 5', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.VerifyCurrentPageNumber(data.CONTINUITYPLAN.TC_37.NUMBER);
    });
    it('38. Verify that the record option per page table will appear when user click on number option square', async function () {
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.VerifyPagingOptionDisplay();
    });
    it('39. Verify that the number records per page displayed correctly with chosen option', async function () {
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.ClickValueOfPagingOption(data.CONTINUITYPLAN.TC_39.NUMBER_10);
        await ACP.ClickValueOfPagingOption(data.CONTINUITYPLAN.TC_39.NUMBER_15);
        await ACP.ClickValueOfPagingOption(data.CONTINUITYPLAN.TC_39.NUMBER_5);
    });
    it('49. Verify that window pop-up will appear when user press delete continuity button.', async function () {
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.delete_icon.click();
        await expect(await ACP.delete_popup.isPresent()).toBe(true);
        await console.log('The Delete Confirm popup is displayed on UI');
        await ACP.no_button_popup.click()
    });
    it('50. Verify that user can successfully delete continuity plan by click save button.', async function () {

       // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.VerifyCPisDelete(data.CONTINUITYPLAN.TC_50.PLANNAME_KEY);
        await console.log(data.CONTINUITYPLAN.TC_50.PLANNAME_KEY, 'is deleted on UI');

    });
    it('51. Verify that user can cancel delete action by click NO button', async function () {
        await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.delete_icon.click();
        await ACP.no_button_popup.click();
        await expect(await ACP.delete_popup.isPresent()).toBe(false);
        await console.log('The Delete Confirm popup is not displayed on UI');

    });
    it('52. Verify that user can cancel delete action by click symbol "X" in the top right corner of the table', async function () {
        // await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        // await browser.refresh();
        await browser.waitForAngularEnabled(true);
        await ACP.delete_icon.click();
        await ACP.x_icon.click();
        await expect(await ACP.delete_popup.isPresent()).toBe(false);
        await console.log('The Delete Confirm popup is not displayed on UI');

    });
});
