import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageMaintenance } from '../PageObject/PageMaintenance.po';
import { browser, ExpectedConditions } from 'protractor';

// const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin Maintenance', () => {
    let pageNavigation: PageNavigation;
    let pageMaintenance: PageMaintenance;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageNavigation = new PageNavigation();
        pageMaintenance = new PageMaintenance();
    });


    // 1 - Verify that the confirm dialog was displayed when click on "Place VNFM in Maintenance mode"
    it('Verify that the confirm dialog was displayed when click on "Place VNFM in Maintenance mode" - 1 ', async function () {

        await pageNavigation.selectLeftNav(6);
        await pageNavigation.selectLeftNavSecond(6, 7);
        await browser.wait(ExpectedConditions.urlContains('vnfm/maintenance'), 3000);

        await pageMaintenance.setVNFMInMaintenanceMode();
        await pageMaintenance.verifyDisplayTheConfirmDialog();
    });

    // 2 - Verify that the user exit the confirm action when click on cancel button
    it('Verify that the user exit the confirm action when click on cancel button - 2 ', async function () {

        await pageMaintenance.verifyExitTheConfirmDialog();
    });

    // 3 - Verify that the VNFM can enter to the Maintenance mode when click on confirm button
    it('Verify that the VNFM can enter to the Maintenance mode when click on confirm button - 3', async function () {

        await pageMaintenance.setVNFMInMaintenanceMode();
        await pageMaintenance.verifyVNFMEnterToTheMantenanceMode();
    });

    // 4 - Verify that the confirm dialog was displayed when click on "Complete VNFM Maitenance"
    it('Verify that the confirm dialog was displayed when click on "Complete VNFM Maitenance" - 4 ', async function () {

        await pageMaintenance.removeVNFMFromMaintenanceMode();
        await pageMaintenance.verifyDisplayTheConfirmDialog();
    });

    // 5 - Verify that the user can exit the confirm action when click on cancel button
    it('Verify that the user can exit the confirm action when click on cancel button - 5', async function () {

        await pageMaintenance.verifyExitTheConfirmDialog();
    });

    // 6 - Verify that the VNFM can remove from the Maintenance mode when click on confirm button
    it('Verify that the VNFM can remove from the Maintenance mode when click on confirm button - 6 ', async function () {

        await pageMaintenance.removeVNFMFromMaintenanceMode();
        await browser.sleep(17000);
        await pageMaintenance.verifyVNFMIsRemovedFromMantenanceMode();

    });

 });



