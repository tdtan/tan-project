import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageDatabase } from '../PageObject/PageDatabase.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageError } from '../PageObject/PageError.po';


const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin Database', () => {
  let pageNavigation: PageNavigation;
  let pageDatabase: PageDatabase;
  let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageNavigation = new PageNavigation();
    pageDatabase = new PageDatabase();
    pageError = new PageError();
  });
  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
  });


  // 1 - Verify that the dropdown filter functions work properly with Hostname/IP column
  it('Verify that the dropdown filter functions work properly with Hostname/IP column - 1 ', async function () {

    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 5);
    browser.wait(ExpectedConditions.urlContains('vnfm/database'), 3000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if (result1 === true) {
      await pageError.cliclClearALL();
    }

    const numRecord = await pageDatabase.getRecord();
    const getIPInTable = await pageDatabase.GetTextInTd(1);
    await pageDatabase.clickShowHideFilter();
    await pageDatabase.SelectFilter(1, getIPInTable);
    await browser.waitForAngularEnabled(true);
    await pageDatabase.verifyRecord(data.ADMIN_DATABASE.TC_1.RECORD);
    await pageDatabase.SelectFilter(1, 'All');
    await browser.waitForAngularEnabled(true);
    const numRecord2 = await pageDatabase.getRecord();
    await expect(Boolean(numRecord === numRecord2)).toBe(true);
  });

  // 2 - Verify that the dropdown filter functions work properly with Status column
  it('Verify that the dropdown filter functions work properly with Status column - 2 ', async function () {

    const numRecord = await pageDatabase.getRecord();
    await pageDatabase.SelectFilter(5, data.ADMIN_DATABASE.TC_2.FILTER_STATUS);
    await pageDatabase.verifyRecord(data.ADMIN_DATABASE.TC_2.RECORD);
    await pageDatabase.SelectFilter(5, 'All');
    const numRecord2 = await pageDatabase.getRecord();
    await expect(Boolean(numRecord === numRecord2)).toBe(true);
  });

  // 3 - Verify that user can select the number of displayed columns
  it('Verify that user can select the number of displayed columns - 3', async function () {
    await pageDatabase.selectOptionNamefromShowHideColumnsList(data.ADMIN_DATABASE.TC_3.COLUMNNAME);
    await pageDatabase.verifyShowHidecolumns(data.ADMIN_DATABASE.TC_3.COLUMNNAME);
    await pageDatabase.selectOptionNamefromShowHideColumnsList(data.ADMIN_DATABASE.TC_3.COLUMNNAME);
  });

  // 4 - Verify that the Search function work properly
  it('Verify that search function for VNF works properly - 4', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 5);
    await browser.wait(ExpectedConditions.urlContains('vnfm/database'), 3000);
    await browser.waitForAngularEnabled(true);
    await pageDatabase.setSearch('wrong text');
    await pageDatabase.verifyRecord('No records found');
    await pageDatabase.setSearch(data.ADMIN_DATABASE.TC_4.INPUT_TEXT);
    await pageDatabase.verifyRecord(data.ADMIN_DATABASE.TC_4.RECORD);
  });

  // 5 - Verify that the Clear button search in table button work properly
  it('Verify that the Clear button search in table button work properly - 5 ', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 5);
    await browser.wait(ExpectedConditions.urlContains('vnfm/database'), 3000);
    await browser.waitForAngularEnabled(true);
    await pageDatabase.setSearch('wrong text');
    await pageDatabase.clickClearTextSearch();
    const result = await pageDatabase.getAttributeInSearch();
    await expect(Boolean(result === '')).toBe(true);
  });

  // 6 - Verify that the table show hide filter button work properly
  it('Verify that the table show hide filter button work properly - 6 ', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/database');
    await browser.waitForAngularEnabled(true);
    await expect(pageDatabase.verifyDropdownFilterDisplay()).toBeFalsy();
    await pageDatabase.clickShowHideFilter();
    await expect(pageDatabase.verifyDropdownFilterDisplay()).toBeTruthy();
  });

  // 7 - Verify that the clear function of filter work properly
  it('Verify that the clear function of filter work properly - 7 ', async function () {

    // const getIPInTable = await pageDatabase.GetTextInTd(1);
    // await pageDatabase.clickShowHideFilter();
    await browser.waitForAngularEnabled(true);
    await pageDatabase.SelectFilter(5, data.ADMIN_DATABASE.TC_2.FILTER_STATUS);
    await pageDatabase.clickClearButtonInFilter();
    const textFilter = await pageDatabase.getValueInFilter();
    await expect(Boolean(textFilter === 'All')).toBe(true);
  });

  // 8 - Verify that the tables display with maximum 5 entries in one page as default
  it('Verify that the tables display with maximum 5 entries in one page as default - 8 ', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/database');
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if (result1 === true) {
      await pageError.cliclClearALL();
    }
    await pageDatabase.verifyDisplayEntriesDefault();
  });

  // 9 - Verify that the user can choose the number of entries displayed in one page
  it('Verify that the user can choose the number of entries displayed in one page - 9 ', async function () {
    await browser.waitForAngularEnabled(true);
    await pageDatabase.verifyNumberRowDisplayOnTable(data.ADMIN_DATABASE.TC_9.NUMBER_ENTRIES);
  });


});
