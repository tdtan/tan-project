import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageEditSetting } from '../PageObject/PageEditSetting.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageAdminSetting } from '../PageObject/PageAdminSetting.po';
import { PageError } from '../PageObject/PageError.po';


const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin Edit Setting', () => {
    let pageNavigation: PageNavigation;
    let pageEditSetting: PageEditSetting;
    let pageAdminSetting: PageAdminSetting;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageNavigation = new PageNavigation();
        pageEditSetting = new PageEditSetting();
        pageAdminSetting = new PageAdminSetting();
        pageError = new PageError();
    });

    beforeEach(async () => {
      const result1 = await pageError.verifyMessegeError();
      const result2 = await pageError.verifyMessegeWarning();
      const result3 = await pageError.verifyMessegeInfo();
      if (result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
      }
    });

    // 1 - Verify that the user can display the Element when click on the Element
    it('Verify that the user can display the Element when click on the Element - 1 ', async function() {
      await pageNavigation.selectLeftNav(5);
      await pageNavigation.selectLeftNavSecond(5, 4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
      await browser.waitForAngularEnabled(true);
      const result3 = await pageAdminSetting.verifyShowHidecolumns('Element');
      if (result3 === false) {
        await pageAdminSetting.selectOptionNamefromShowHideColumnsList('Element');
      }
      await pageAdminSetting.setSearch(data.ADMIN_EDITSETTING.TC_1.INPUT_TEXT);
      await pageEditSetting.verifyRecord(data.ADMIN_EDITSETTING.TC_1.RECORD);
      await pageEditSetting.clickElement();
      await pageEditSetting.verifyDisplayElementAfterClickElement();
      await pageEditSetting.clickCancel();
      await pageEditSetting.verifyAfterClickCancel();
    });

    // 2 - Verify that user can edit the Element when click on Save button
    it('Verify that user can edit the Element when click on Save button - 2 ', async function () {
      await pageNavigation.selectLeftNav(5);
      await pageNavigation.selectLeftNavSecond(5, 4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
      await browser.waitForAngularEnabled(true);
      const result1 = await pageError.verifyMessegeError();
      if ( result1 === true ) {
          await pageError.cliclClearALL();
      }

      await pageAdminSetting.setSearch(data.ADMIN_EDITSETTING.TC_1.INPUT_TEXT);
      await pageEditSetting.clickElement();
      await pageEditSetting.verifyDisplayElementAfterClickElement();
      await pageEditSetting.setValue(data.ADMIN_EDITSETTING.TC_2.INPUT_EDIT);
      await pageEditSetting.clickSave();
      // await pageEditSetting.verifyAfterClickSave();
      await pageAdminSetting.setSearch(data.ADMIN_EDITSETTING.TC_1.INPUT_TEXT);
      const textValue = await pageEditSetting.verifyUpdatElementAfterEditElement();
      await console.log('Text: ' + textValue);
      await expect(Boolean(textValue === data.ADMIN_EDITSETTING.TC_2.INPUT_EDIT)).toBe(true);
    });

    // 3 - Verify that the Configuration table can update the value after edit the Element
    it('Verify that the Configuration table can update the value after edit the Element - 3 ', async function () {
      await pageNavigation.selectLeftNav(5);
      await pageNavigation.selectLeftNavSecond(5, 4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
      await browser.waitForAngularEnabled(true);
      const result1 = await pageError.verifyMessegeError();
      if ( result1 === true ) {
          await pageError.cliclClearALL();
      }
      await pageAdminSetting.setSearch(data.ADMIN_EDITSETTING.TC_1.INPUT_TEXT);
      await pageEditSetting.clickElement();
      await pageEditSetting.verifyDisplayElementAfterClickElement();
      await pageEditSetting.setValue(data.ADMIN_EDITSETTING.TC_2.INPUT_FORM);
      await pageEditSetting.clickSave();
      // await pageEditSetting.verifyAfterClickSave();
      await pageAdminSetting.setSearch(data.ADMIN_EDITSETTING.TC_1.INPUT_TEXT);
      const textValue = await pageEditSetting.verifyUpdatElementAfterEditElement();
      await console.log('Text: ' + textValue);
      await expect(Boolean(textValue === data.ADMIN_EDITSETTING.TC_2.INPUT_FORM)).toBe(true);

    });

    // 4 - Verify that user can close the edit Element table when click on Cancel button
    it('Verify that user can close the edit Element table when click on Cancel button - 4 ', async function () {

      await pageNavigation.selectLeftNav(2);
      await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'));
      await browser.waitForAngularEnabled(true);
      await pageNavigation.selectLeftNav(5);
      await pageNavigation.selectLeftNavSecond(5, 4);
      await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 5000);
      await browser.waitForAngularEnabled(true);
      const result1 = await pageError.verifyMessegeError();
      if ( result1 === true ) {
          await pageError.cliclClearALL();
      }
      await pageAdminSetting.setSearch(data.ADMIN_EDITSETTING.TC_4.INPUT_TEXT);
      await pageEditSetting.verifyRecord(data.ADMIN_EDITSETTING.TC_4.RECORD);
      await pageEditSetting.clickElement();
      await pageEditSetting.verifyDisplayElementAfterClickElement();
      await pageEditSetting.clickCancel();
      await pageEditSetting.verifyAfterClickCancel();
    });

  });
