import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageAdminVNFMs } from '../PageObject/PageAdminVNFMs.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageSecurityRealms } from '../PageObject/PageSecurityRealms.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin VNFMs', () => {
  let pageNavigation: PageNavigation;
  let pageAdminVNFMs: PageAdminVNFMs;
  let pageSecurityRealms: PageSecurityRealms;
  let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageNavigation = new PageNavigation();
    pageAdminVNFMs = new PageAdminVNFMs();
    pageSecurityRealms = new PageSecurityRealms();
    pageError = new PageError();
  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
      await pageError.cliclClearALL();
    }
  });

  // 0 - Verify that the VNFMs tab displayed in left navigation bar
  it('Verify that the VNFMs tab displayed in left navigation bar - 0', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
    await browser.waitForAngularEnabled(true);
    const txtHeader = await pageAdminVNFMs.getTextHeader();
    await expect(Boolean(txtHeader === data.ADMIN_VNFMs.TC_1.TEXT)).toBe(true);
  });

  // 1 - Verify that the Show Hide Filter button work properly
  it(' Verify that the Show Hide Filter button work properly - 1', async function () {
    await browser.waitForAngularEnabled(true);
    await pageAdminVNFMs.clickShowHideFilter();
    await pageAdminVNFMs.verifyFilterDisplayed();
    await pageAdminVNFMs.clickShowHideFilter();
    await pageAdminVNFMs.verifyFilterNotDisplayed();
  });

  // 2 - Verify that the dropdown filter and sort functions work properly with Alias column
  it('Verify that the dropdown filter and sort functions work properly with Alias column - 2', async function () {
    await pageAdminVNFMs.clickRefesh();
    await pageAdminVNFMs.clickShowHideFilter();
    const record = await pageAdminVNFMs.GetRecordCurrent();
    const txtAlias = await pageAdminVNFMs.GetTextInTd(2);
    await browser.waitForAngularEnabled(true);
    await pageAdminVNFMs.SelectFilterWithCheckbox(2, txtAlias);
    await pageAdminVNFMs.verifyRecord(data.ADMIN_VNFMs.TC_2.RECORD);
    await pageAdminVNFMs.SelectFilterWithCheckbox(2, txtAlias);
    await pageAdminVNFMs.verifyRecord(record);
    await pageAdminVNFMs.verifySortColumn(2);
  });

  // 3 - Verify that the dropdown filter and sort functions work properly with VNFM Name column
  it('Verify that the dropdown filter and sort functions work properly with VNFM Name column - 3', async function () {
    await pageAdminVNFMs.clickRefesh();
    const record = await pageAdminVNFMs.GetRecordCurrent();
    const txtVNFMName = await pageAdminVNFMs.GetTextInTd(1);
    await pageAdminVNFMs.SelectFilterWithCheckbox(1, txtVNFMName);
    await pageAdminVNFMs.verifyRecord(data.ADMIN_VNFMs.TC_3.RECORD);
    await pageAdminVNFMs.SelectFilterWithCheckbox(1, txtVNFMName);
    await pageAdminVNFMs.verifyRecord(record);
    await pageAdminVNFMs.verifySortColumn(1);
  });

  // 4 - Verify that the dropdown filter and sort functions work properly with Version column
  it('Verify that the dropdown filter and sort functions work properly with Version column - 4', async function () {
    await pageAdminVNFMs.clickRefesh();
    const txtVersion = await pageAdminVNFMs.GetTextInTd(4);
    await pageAdminVNFMs.SelectFilter(4, txtVersion);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === data.ADMIN_VNFMs.TC_4.RECORD)).toBe(true);
    // await pageAdminVNFMs.SelectFilter(4, 'All');
    await pageAdminVNFMs.verifySortColumn(4);
  });

  // 5 - Verify that the dropdown filter and sort functions work properly with Status column
  it('Verify that the dropdown filter and sort functions work properly with Status column- 5', async function () {
    await pageAdminVNFMs.clickRefesh();
    await pageAdminVNFMs.SelectFilter(5, data.ADMIN_VNFMs.TC_5.STATUS);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === data.ADMIN_VNFMs.TC_5.RECORD)).toBe(true);
    await pageAdminVNFMs.verifySortColumn(5);
  });

  // 6- Verify that user can select the number of displayed columns
  it('Verify that user can select the number of displayed columns- 6', async function () {
    await expect(pageAdminVNFMs.verifyColumnDisplayInTable('VNFM Name')).toBeTruthy();
    await expect(pageAdminVNFMs.verifyColumnDisplayInTable('Alias')).toBeTruthy();
    await expect(pageAdminVNFMs.verifyColumnDisplayInTable('Number of VNFs')).toBeTruthy();
    await expect(pageAdminVNFMs.verifyColumnDisplayInTable('Version')).toBeTruthy();
    await expect(pageAdminVNFMs.verifyColumnDisplayInTable('Status')).toBeTruthy();
    await expect(pageAdminVNFMs.verifyColumnDisplayInTable('Actions')).toBeTruthy();

    await pageAdminVNFMs.clickShowHideColumn();
    await pageAdminVNFMs.selectOptionNamefromShowHideColumnsList('IPV4');
    await expect(await pageAdminVNFMs.verifyColumnDisplayInTable('IPV4')).toBeTruthy();
    await pageAdminVNFMs.clickShowHideColumn();
    await pageAdminVNFMs.selectOptionNamefromShowHideColumnsList('IPV6');
    await expect(await pageAdminVNFMs.verifyColumnDisplayInTable('IPV6')).toBeTruthy();
    await browser.sleep(3000);
  });

  // 7 - Verify that the dropdown filter and sort functions work properly with IPv4 column
  it('Verify that the dropdown filter and sort functions work properly with IPv4 column - 7', async function () {
    const txtIPV4 = await pageAdminVNFMs.GetTextInTd(6);
    await pageAdminVNFMs.SelectFilter(6, txtIPV4);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === data.ADMIN_VNFMs.TC_7.RECORD)).toBe(true);
    await pageAdminVNFMs.verifySortColumn(6);
  });

  // 8 - Verify that the VNFMs table can update after click on Save button
  it('Verify that the VNFMs table can update after click on Save button - 8', async function () {
    await pageAdminVNFMs.ClearTxbSearch();
    const txtVNFMName = await pageAdminVNFMs.GetTextInTd(1);
    await pageAdminVNFMs.settextSearch(txtVNFMName);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === data.ADMIN_VNFMs.TC_8.RECORD)).toBe(true);
    await pageAdminVNFMs.clickRowTable();
    await pageAdminVNFMs.setIPv6(data.ADMIN_VNFMs.TC_8.IPV6);
    await pageAdminVNFMs.clickSaveButtonEditForm();
    await pageAdminVNFMs.ClearTxbSearch();
    await pageAdminVNFMs.settextSearch(data.ADMIN_VNFMs.TC_8.IPV6);
    await expect(pageAdminVNFMs.verifyRowTableDisplay()).toBeTruthy();
    const record2 = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record2 === data.ADMIN_VNFMs.TC_8.RECORD)).toBe(true);
  });

  // 9 - Verify that the dropdown filter and sort functions work properly with Ipv6 column
  it('Verify that the dropdown filter and sort functions work properly with Ipv6 column - 9', async function () {

    await pageAdminVNFMs.SelectFilter(7, data.ADMIN_VNFMs.TC_9.IPV6);
    const record1 = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record1 === data.ADMIN_VNFMs.TC_9.RECORD)).toBe(true);
    await pageAdminVNFMs.verifySortColumn(7);
  });

  // 10 - Verify that the Clear Filter button work properly
  it(' Verify that the Clear Filter button work properly - 10', async function () {

    const record = await pageAdminVNFMs.GetRecordCurrent();
    // await pageAdminVNFMs.clickShowHideFilter();
    await pageAdminVNFMs.SelectFilter(7, data.ADMIN_VNFMs.TC_9.IPV6);
    await pageAdminVNFMs.verifyRecord(data.ADMIN_VNFMs.TC_10.RECORD);
    await pageAdminVNFMs.ClickClearFilterButton();
    const record1 = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === record1)).toBe(true);
  });

  // 11 - Verify that the Search function work properly
  it('Verify that the Search function work properly- 11', async function () {
    await pageNavigation.selectLeftNav(2);
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 6);
    await browser.wait(ExpectedConditions.urlContains('vnfm/vnfms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if (result1 === true) {
      await pageError.cliclClearALL();
    }
    await pageAdminVNFMs.ClearTxbSearch();
    const txtVNFMName = await pageAdminVNFMs.GetTextInTd(1);
    await pageAdminVNFMs.settextSearch(txtVNFMName);
    const record2 = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record2 === data.ADMIN_VNFMs.TC_11.RECORD)).toBe(true);
    await pageAdminVNFMs.ClearTxbSearch();
    await pageAdminVNFMs.settextSearch('Text Wrong');
    const record = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === data.ADMIN_VNFMs.TC_11.NONRECORD)).toBe(true);
    await pageAdminVNFMs.ClearTxbSearch();
  });

  // 12 - Verify that the Clear Search button work properly
  it(' Verify that the Clear Search button work properly - 12', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
    // browser.driver.navigate().refresh();
    await browser.waitForAngularEnabled(true);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    const txtVNFMName = await pageAdminVNFMs.GetTextInTd(1);
    await pageAdminVNFMs.settextSearch(txtVNFMName);
    await pageAdminVNFMs.verifyRecord(data.ADMIN_VNFMs.TC_12.RECORD);
    await pageAdminVNFMs.ClickClearSearchButton();
    const record1 = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === record1)).toBe(true);
  });

  // 13 - Verify that the tables display with maximum 5 entries in one page as default
  it('Verify that the tables display with maximum 5 entries in one page as default- 13 ', async function () {
    await browser.waitForAngularEnabled(true);
    const numbRow = await pageAdminVNFMs.GetNumberRowInPage();
    const totalRow = await pageAdminVNFMs.CountNumberRowInpage();
    await expect(Boolean(numbRow === '5')).toBe(true);
    await expect(Boolean(totalRow <= 5)).toBe(true);
  });

  // 14 - Verify that the user can choose the number of entries displayed in one page
  it('Verify that the user can choose the number of entries displayed in one page - 14', async function () {
    await browser.waitForAngularEnabled(true);
    await pageAdminVNFMs.SelectNumberRowDisplay(15);
    const result2 = await pageAdminVNFMs.CountNumberRowInpage();
    await expect(Boolean(result2 <= 15)).toBe(true);
    await pageAdminVNFMs.SelectNumberRowDisplay(5);
    const result3 = await pageAdminVNFMs.CountNumberRowInpage();
    await expect(Boolean(result3 <= 5)).toBe(true);
  });

  // 15 - Verify that the user can move to specified page when click on the page number
  it('Verify that the user can move to specified page when click on the page number - 15', async function () {
    await browser.waitForAngularEnabled(true);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    const numRecord = Number(record.slice(0, 1));
    const numbRow = Number(await pageAdminVNFMs.GetNumberRowInPage());
    if (numRecord > numbRow) {
      await pageSecurityRealms.clickNumbertoMovePage();
      const numPage = await pageAdminVNFMs.getCurentPageActive();
      await expect(Boolean(numPage === '2')).toBe(true);
    }
  });

  // 16 -Verify that the value of current VNFM were displayed when user click on existing VNFM row
  it('Verify that the value of current VNFM were displayed when user click on existing VNFM row - 16', async function () {
    browser.driver.navigate().refresh();
    await browser.waitForAngularEnabled(true);
    const txtVNFMName = await pageAdminVNFMs.GetTextInTd(1);
    await pageAdminVNFMs.settextSearch(txtVNFMName);
    const record = await pageAdminVNFMs.GetRecordCurrent();
    await expect(Boolean(record === data.ADMIN_VNFMs.TC_16.RECORD)).toBe(true);
    // show form
    await pageAdminVNFMs.clickRowTable();
    await expect(await pageAdminVNFMs.verifyFormEditVnfmDisplay()).toBeTruthy();
    await pageAdminVNFMs.clickCancelButtonEditForm();
  });

  // 17 - Verify that the user can exit the edit VNFM form when click on Cancel button
  it('Verify that the user can exit the edit VNFM form when click on Cancel button - 17', async function () {
    await browser.waitForAngularEnabled(true);
    // show form
    await pageAdminVNFMs.clickRowTable();
    await expect(await pageAdminVNFMs.verifyFormEditVnfmPresent()).toBeTruthy();
    await pageAdminVNFMs.clickCancelButtonEditForm();
    await expect(await pageAdminVNFMs.verifyFormEditVnfmPresent()).toBeFalsy();
  });

  // 18 - Verify that the field Name, Number of VNFs, Version, Status are Read-only
  it('Verify that the field: Name, Number of VNFs, Version, Status are Read-only - 18', async function () {
    await browser.waitForAngularEnabled(true);
    // show form
    await pageAdminVNFMs.clickRowTable();
    // input name
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('name')).toBeTruthy();
    // input Number of VNFs
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('assignedVnfCount')).toBeTruthy();
    // input version
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('softwareVersion')).toBeTruthy();
    // input status
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('status')).toBeTruthy();
  });

  // 19 - Verify that the field Alias, IPv4, IPv6 are Editable
  it('Verify that the field: Alias, IPv4, IPv6 are Editable - 19', async function () {
    // input alias
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('alias')).toBeTruthy();
    // input ipv4
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('v4IpAddress')).toBeTruthy();
    // input ipv6
    await expect(await pageAdminVNFMs.verifyInputFormEditDisable('v6IpAddress')).toBeTruthy();
    await pageAdminVNFMs.clickCancelButtonEditForm();
  });

  // tslint:disable-next-line:max-line-length
  // 20 - Verify that the drop-down list Functions contain Start Maintenance Complete Maintenance, Reassign VNFs, Disable to DR, Enable from DR, Delete
  // tslint:disable-next-line:max-line-length
  it('Verify that the drop-down list Functions contain: Start Maintenance, Complete Maintenance, Reassign VNFs, Disable to DR, Enable from DR, Delete - 20', async function () {
    await browser.waitForAngularEnabled(true);
    await pageAdminVNFMs.clickbtnSelectActions();
    await expect(await pageAdminVNFMs.verifyEventDisplayInForm('Start Maintenance')).toBeTruthy();
    await expect(await pageAdminVNFMs.verifyEventDisplayInForm('Complete Maintenance')).toBeTruthy();
    await expect(await pageAdminVNFMs.verifyEventDisplayInForm('Reassign VNFs')).toBeTruthy();
    await expect(await pageAdminVNFMs.verifyEventDisplayInForm('Disable to DR')).toBeTruthy();
    await expect(await pageAdminVNFMs.verifyEventDisplayInForm('Enable from DR')).toBeTruthy();
    await expect(await pageAdminVNFMs.verifyEventDisplayInForm('Delete')).toBeTruthy();
  });

});
