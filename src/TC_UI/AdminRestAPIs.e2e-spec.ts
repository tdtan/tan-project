// import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageRESTAPIs } from '../PageObject/PageRESTAPIs.po';
import { browser } from 'protractor';
import { PageError } from '../PageObject/PageError.po';


// const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Admin REST APIs', () => {

  // let pageNavigation: PageNavigation;
  let pageRESTAPIs: PageRESTAPIs;
  let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    // pageNavigation = new PageNavigation();
    pageRESTAPIs = new PageRESTAPIs();
    pageError = new PageError();
  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await pageError.cliclClearALL();
    }
  });

  // 1 - Verify that user can Show/Hide the APIs of the group API
  it('Verify that user can Show/Hide the APIs of the group API - 1', async function () {

    // await pageNavigation.selectLeftNav(5);
    // await pageNavigation.selectLeftNavSecond(5, 7);
    // await browser.wait(ExpectedConditions.urlContains('vnfm/restapis'));
    await browser.get(browser.baseUrl + '/#/vnfm/restapis');
    await browser.waitForAngularEnabled(true);
    await browser.sleep(9000);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageRESTAPIs.verifyShowTextRESTAPIs('REST APIs');
    await pageRESTAPIs.clickShowHire();
    await expect(pageRESTAPIs.verifyTableAfterClickShowHire()).toBeTruthy();
    await pageRESTAPIs.clickShowHire();
    await expect(pageRESTAPIs.verifyTableAfterClickShowHire()).toBeFalsy();
  });

  // 2 - Verify that user can List Operations the APIs of the group API
  it('Verify that user can List Operations the APIs of the group API - 2', async function () {
    await browser.waitForAngularEnabled(true);
    await pageRESTAPIs.verifyTableAfterClickListOperation();
  });

  // 3 - Verify that user can Expand Operations the APIs of the group API
  it('Verify that user can Expand Operations the APIs of the group API - 3', async function () {

    await browser.waitForAngularEnabled(true);
    await pageRESTAPIs.verifyTableAfterClickExpandOperation();

  });

});
