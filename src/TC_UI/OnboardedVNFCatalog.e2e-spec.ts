import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { PageInstantiateVNF } from '../PageObject/PageInstantiateVNF.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project VNF Catalog Onboarded', () => {
    let pageLogin: any;
    let pageNavigation: any;
    let pageOnboardedVNFs: PageOnboardedVNFs;
    let pageFailedtoOnboard: PageFailedtoOnboard;
    let pageInstantiateVNF: PageInstantiateVNF;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 170000;
        pageLogin = new PageLogin();
        pageNavigation = new PageNavigation();
        pageOnboardedVNFs = new PageOnboardedVNFs();
        pageInstantiateVNF = new PageInstantiateVNF();
        pageFailedtoOnboard = new PageFailedtoOnboard();
        pageError = new PageError;
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
    });

    it('login VNFM ', async function () {

        await browser.restart();
        await browser.get(data.INFOLOGIN.URL);
        browser.driver.manage().window().maximize();
        await browser.waitForAngularEnabled(true);
        await pageLogin.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 4000);
        await browser.waitForAngularEnabled(true);
        const txtOnboard = await pageOnboardedVNFs.getTextOnboard();
        await expect(Boolean(txtOnboard === data.LEFTNAVIGATION.TC_2.TEXT)).toBe(true);
    });


    // 0- Verify that user can upload the CSAR from local computer to VNF Catalog table
    it('Verify that user can upload the CSAR from local computer to VNF Catalog table- 0', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);

        await pageOnboardedVNFs.settextSearch(data.VNF_ONBOARDED.TC_1.VNFVERSION);
        const afterSearch = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (afterSearch === true) {
            await pageOnboardedVNFs.clickDropdownActions();
            await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.VNF_ONBOARDED.TC_1.FUNCTION);
            await pageOnboardedVNFs.clickConfirmDelete();
        }
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.funcUploadCSAR(data.VNF_ONBOARDED.TC_1.PATH);
        await browser.sleep(3000);
        await pageFailedtoOnboard.clickCancelVNF();
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.VNF_ONBOARDED.TC_1.VNFVERSION);
        const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        await expect(Boolean(numrecord === 'Showing 1 of 1 record')).toBe(true);

    });

    // 1- Verify that the dropdown filter work properly with Name column
    it('Verify that the dropdown filter work properly with Name column- 1', async function () {

        // await pageNavigation.selectLeftNav(2);
        // await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
        await browser.waitForAngularEnabled(true);
        const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        await pageOnboardedVNFs.clickShowHideFilter();
        await pageOnboardedVNFs.SelectFilterWithCheckbox(1, data.VNF_ONBOARDED.TC_1.VNFNAME);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_1.RECORD);
        await pageOnboardedVNFs.SelectFilterWithCheckbox(1, data.VNF_ONBOARDED.TC_1.VNFNAME);
        await pageOnboardedVNFs.verifyRecord(numrecord);

    });

    // 2- Verify that the dropdown filter work properly with Type column
    it('Verify that the dropdown filter work properly with Type column- 2', async function () {

        const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        await pageOnboardedVNFs.SelectFilter(2, data.VNF_ONBOARDED.TC_2.VNFTYPE);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_2.RECORD);
        await pageOnboardedVNFs.SelectFilter(2, 'All');
        const numrecord2 = await pageOnboardedVNFs.GetRecordCurrent();
        await expect(Boolean(numrecord === numrecord2)).toBe(true);
    });

    // 3- Verify that the dropdown filter work properly with Size column
    it('Verify that the dropdown filter work properly with Size column- 3', async function () {

        const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        await pageOnboardedVNFs.SelectFilter(3, data.VNF_ONBOARDED.TC_3.VNFSIZE);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_3.RECORD);
        await pageOnboardedVNFs.SelectFilter(3, 'All');
        const numrecord2 = await pageOnboardedVNFs.GetRecordCurrent();
        await expect(Boolean(numrecord === numrecord2)).toBe(true);

    });

    // 4- Verify that the text filter work properly with Version column
    it('Verify that the dropdown filter work properly with Version column- 4', async function () {

        const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        await pageOnboardedVNFs.SelectFilter(4, data.VNF_ONBOARDED.TC_4.VNFVERSION);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_4.RECORD);
        await pageOnboardedVNFs.SelectFilter(4, 'All');
        const numrecord2 = await pageOnboardedVNFs.GetRecordCurrent();
        await expect(Boolean(numrecord === numrecord2)).toBe(true);

    });

    // 5- Verify that the text filter work properly with VNFD UID column
    it('Verify that the dropdown filter work properly with VNFD UID column- 5', async function () {

        const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        const txtUID = await pageOnboardedVNFs.GetTextInTd(5);
        await pageOnboardedVNFs.SelectFilterWithCheckbox(5, txtUID);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_5.RECORD);
        await pageOnboardedVNFs.SelectFilterWithCheckbox(5, txtUID);
        const numrecord2 = await pageOnboardedVNFs.GetRecordCurrent();
        await expect(Boolean(numrecord === numrecord2)).toBe(true);
        await console.log('Record current: ');

    });

    // // 6- Verify that the text filter work properly with Description column in Onboarded VNFs table
    // it('Verify that the text filter work properly with Description column in Onboarded VNFs table- 6', async function () {

    //     await browser.waitForAngularEnabled(true);
    //     const txtDescription = await pageOnboardedVNFs.GetTextInTd(6);
    //     await pageOnboardedVNFs.ClearTextDescription();
    //     await pageOnboardedVNFs.settextDescription('Text Correct Input');
    //     const result2 = await pageOnboardedVNFs.isTableHasValueAfterSearch();
    //     await expect(Boolean(result2 === false)).toBe(true);

    //     await pageOnboardedVNFs.ClearTextDescription();
    //     await pageOnboardedVNFs.settextDescription(txtDescription);
    //     await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_6.RECORD);

    // });

    // 7- Verify that table can Show or Hide filter form when click button Hide Filter
    it('Verify that table can Show or Hide filter form when click button Hide Filter- 7', async function () {

        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await expect(pageOnboardedVNFs.verifyDropdownFilterIsPresentInPage()).toBeTruthy();
        await pageOnboardedVNFs.clickShowHideFilter();
        await expect(pageOnboardedVNFs.verifyDropdownFilterIsPresentInPage()).toBeFalsy();
    });

    // 8- Verify that the Onboared VNFs table can clear text search when click clear button
    it('Verify that the Onboared VNFs table can clear text search when click clear button- 8', async function () {

        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.settextSearch('seach any text');
        await pageOnboardedVNFs.clickClearTextSearch();
        const result2 = await pageOnboardedVNFs.getAttributeSearch();
        await expect(Boolean(result2 === '')).toBe(true);
    });

    // 9 - Verify that the Clear Filter button work properly
    it(' Verify that the Clear Filter button work properly - 9', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 5000);
        await browser.waitForAngularEnabled(true);

        await pageOnboardedVNFs.clickShowHideFilter();
        await pageOnboardedVNFs.SelectFilter(4, data.VNF_ONBOARDED.TC_4.VNFVERSION);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_4.RECORD);
        await pageOnboardedVNFs.clickClearTextInFilter();
        const getTextInFilter = await pageOnboardedVNFs.GetTextInFilter(4);
        await expect(Boolean(getTextInFilter === 'All')).toBe(true);
    });

    // // 8- Verify that Refresh table function for Onboarded VNFs table work properly
    // it('Verify that Refresh table function for Onboarded VNFs table work properly- 8', async function () {

    //     await browser.waitForAngularEnabled(true);
    //     const result1 = await pageError.verifyMessegeError();
    //     if ( result1 === true ) {
    //         await pageError.cliclClearALL();
    //     }

    //     const txtrecord = await pageOnboardedVNFs.GetRecordCurrent();
    //     await pageOnboardedVNFs.settextSearch('wrong text');
    //     await pageOnboardedVNFs.SelectFilter(4, data.VNF_ONBOARDED.TC_4.VNFVERSION);
    //     await pageOnboardedVNFs.clickRefesh();
    //     const txtrecord2 = await pageOnboardedVNFs.GetRecordCurrent();
    //     await expect(Boolean(txtrecord === txtrecord2)).toBe(true);
    // });

    // 10 - Verify that the sort table function work properly in Onboarded VNFs table
    it('Verify that the sort table function work properly in Onboarded VNFs table- 10', async function () {

        await pageOnboardedVNFs.verifySortColumn(4);
    });

    // 11 - Verify that the global filter of the Onboarded VNFs table can work properly
    it('Verify that the global filter of the Onboarded VNFs table can work properly- 11', async function () {

        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.settextSearch('wrong text');
        await pageOnboardedVNFs.verifyRecord('No records found');
        await pageOnboardedVNFs.settextSearch(data.VNF_ONBOARDED.TC_10.VNFNAME);
        await pageOnboardedVNFs.verifyRecord(data.VNF_ONBOARDED.TC_10.RECORD);

    });

    // 12 - Verify that the Onboarded VNFs table display with 10 entries in one page as default
    it('Verify that the Onboarded VNFs table display with 10 entries in one page as default- 12', async function () {

        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        const numrow = await pageOnboardedVNFs.GetNumberRow();
        const numberRow = Number(numrow);
        await console.log('Default row display in one page: ' + numberRow);
        await expect(Boolean(numberRow === 10)).toBe(true);

    });

    // 13 - Verify that the user can choose the number of entries displayed in one page on table Onboarded VNFs
    it('Verify that the user can choose the number of entries displayed in one page on table Onboarded VNFs- 13', async function () {

        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.selectNumberRowOnTable(10);
        await pageOnboardedVNFs.verifyNumberRowDisplayOnTable(10);
        await pageOnboardedVNFs.clickRefesh();

    });

    // 14 - Verify that Instantiate VNF can work properly in Onboarded VNFs table
    it('Verify that Instantiate VNF can work properly in Onboarded VNFs table- 14', async function () {
        await pageNavigation.selectLeftNav(3);
        await browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.settextSearch(data.VNF_ONBOARDED.TC_14.VNFNAME);
        const result = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        // await pageOnboardedVNFs.clickShowHideFilter();
        if (result) {
            await pageOnboardedVNFs.clickDropdownActions();
            await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.VNF_ONBOARDED.TC_14.FUNCTION);
        }
        const txtInstantiate = await pageInstantiateVNF.getTextInstantiate();
        await expect(Boolean(txtInstantiate === data.VNF_ONBOARDED.TC_14.VERIFYTEXT)).toBe(true);
        await pageInstantiateVNF.clickCancel();
    });

    // 15 - Verify that user can delete the Onboarded VNFs in VNFs  Catalogtable
    it('Verify that user can delete the Onboarded VNFs in Catalog VNFs table- 15', async function () {
        await pageNavigation.selectLeftNav(3);
        await browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 5000);
        await browser.waitForAngularEnabled(true);
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 5000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.settextSearch(data.VNF_ONBOARDED.TC_15.VNFNAME);
        const result = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (result) {
            await pageOnboardedVNFs.clickDropdownActions();
            await pageOnboardedVNFs.clickFuctionInActionsDropdown(data.VNF_ONBOARDED.TC_15.FUNCTION);
            await pageOnboardedVNFs.clickConfirmDelete();
        }
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.VNF_ONBOARDED.TC_15.VNFNAME);
        const result2 = await pageOnboardedVNFs.GetRecordCurrent();
        await expect(Boolean(result2 === data.VNF_ONBOARDED.TC_15.RECORD)).toBe(true);

    });

});
