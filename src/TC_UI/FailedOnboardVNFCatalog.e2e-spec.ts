import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Failed to Onboard VNF Catalog', () => {
    let pageNavigation: PageNavigation;
    let pageFailedtoOnboard: PageFailedtoOnboard;
    let pageOnboardedVNFs: PageOnboardedVNFs;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageNavigation = new PageNavigation();
        pageOnboardedVNFs = new PageOnboardedVNFs();
        pageFailedtoOnboard = new PageFailedtoOnboard();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // 1- Verify that table can Show or Hide filter form when click button Hide Filter
    it('Verify that table can Show or Hide filter form when click button Hide Filter- 1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        await browser.waitForAngularEnabled(true);
        await expect(pageFailedtoOnboard.verifyDropdownFilterIsPresentInPage()).toBeFalsy();

        await pageFailedtoOnboard.clickShowHideFilter();
        await expect(pageFailedtoOnboard.verifyDropdownFilterIsPresentInPage()).toBeTruthy();
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 2- Verify that user can upload the CSAR from local computer to Failed to Onboard table
    it('Verify that user can upload the CSAR from local computer to Failed to Onboard VNF table- 2', async function () {
        await browser.waitForAngularEnabled(true);
        // await pageOnboardedVNFs.settextSearch(data.FAILEDONBOARDVNF.TC_2.TEXT_SEARCH);
        // const afterSearch1 = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        // if (afterSearch1 === false) {
        //     await pageOnboardedVNFs.clickOnboardbtn();
        //     await pageFailedtoOnboard.funcUploadCSAR(data.FAILEDONBOARDVNF.TC_2.PATH);
        //     // await pageFailedtoOnboard.clickCancelVNF();
        //     // await browser.waitForAngularEnabled(true);
        //     // await pageOnboardedVNFs.clickRefesh();
        //     // const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
        //     // await expect(Boolean(numrecord === 'Showing 1 of 1 record')).toBe(true);
        // }
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.setSearch(data.FAILEDONBOARDVNF.TC_2.TEXT_SEARCH);
        const afterSearch = await pageFailedtoOnboard.isTableHasValueAfterSearch();
        if (afterSearch) {
            await pageFailedtoOnboard.clickActionsDopdown();
            await pageFailedtoOnboard.clickFunctionActionsDopdown(data.FAILEDONBOARDVNF.TC_2.FUNCTION);
            await pageFailedtoOnboard.clickConfirmDelete();
        }
        await pageFailedtoOnboard.clickClearTextSearch();
        await pageFailedtoOnboard.funcUploadCSAR(data.FAILEDONBOARDVNF.TC_2.PATH);
        // await browser.sleep(4000);
        const result4 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result4 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
        await pageFailedtoOnboard.clickRefesh();
        await browser.waitForAngularEnabled(true);
        await pageFailedtoOnboard.setSearch(data.FAILEDONBOARDVNF.TC_2.TEXT_SEARCH);
        await pageFailedtoOnboard.verifyRecord('Showing 1 of 1 record');
        await pageFailedtoOnboard.clickCancelVNF();
    });

     // 3 -Verify that the dropdown filter work properly with ID column
     it('Verify that the dropdown filter work properly with ID column -3', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        const record = await pageFailedtoOnboard.GetRecordCurrent();
        const txtID = await pageFailedtoOnboard.GetTextInTd(2);
        // await pageFailedtoOnboard.clickShowHideFilter();
        await pageFailedtoOnboard.SelectFilterWithCheckbox(2, txtID);
        await pageFailedtoOnboard.verifyRecord(data.FAILEDONBOARDVNF.TC_3.RECORD);
        await pageFailedtoOnboard.clickClearFilterInPage();
        await pageFailedtoOnboard.verifyRecord(record);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 4 -Verify that the dropdown filter work properly with Type column
    it('Verify that the dropdown filter work properly with Type column -4', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.clickOnboardbtn();
        await browser.waitForAngularEnabled(true);
        const record = await pageFailedtoOnboard.GetRecordCurrent();
        // await pageFailedtoOnboard.clickShowHideFilter();
        await pageFailedtoOnboard.SelectFilter(3, data.FAILEDONBOARDVNF.TC_4.VNFTYPE);
        await pageFailedtoOnboard.verifyRecord(data.FAILEDONBOARDVNF.TC_4.RECORD);
        await pageFailedtoOnboard.SelectFilter(3, 'All');
        await pageFailedtoOnboard.verifyRecord(record);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 5 -Verify that the dropdown filter work properly with Name column
    it('Verify that the dropdown filter work properly with Name column -5', async function () {
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageOnboardedVNFs.clickOnboardbtn();
        await browser.waitForAngularEnabled(true);
        const record = await pageFailedtoOnboard.GetRecordCurrent();
        // await pageFailedtoOnboard.clickShowHideFilter();
        await pageFailedtoOnboard.SelectFilterWithCheckbox(1, data.FAILEDONBOARDVNF.TC_5.VNFNAME);
        await pageFailedtoOnboard.verifyRecord(data.FAILEDONBOARDVNF.TC_5.RECORD);
        await pageFailedtoOnboard.SelectFilterWithCheckbox(1, data.FAILEDONBOARDVNF.TC_5.VNFNAME);
        await pageFailedtoOnboard.verifyRecord(record);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 6 -Verify that the dropdown filter work properly with Version column
    it('Verify that the dropdown filter work properly with Version column -6', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        const record = await pageFailedtoOnboard.GetRecordCurrent();
        // await pageFailedtoOnboard.clickShowHideFilter();
        await pageFailedtoOnboard.SelectFilter(4, data.FAILEDONBOARDVNF.TC_6.VNFVERSION);
        await pageFailedtoOnboard.verifyRecord(data.FAILEDONBOARDVNF.TC_6.RECORD);
        await pageFailedtoOnboard.SelectFilter(4, 'All');
        await pageFailedtoOnboard.verifyRecord(record);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 7 -Verify that the global filter of the Failed to Onboard VNFs table can work properly
    it('Verify that the global filter of the Failed to Onboard VNFs table can work properly -7', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.setSearch('wrong text');
        await pageFailedtoOnboard.verifyRecord('No records found');
        await pageFailedtoOnboard.setSearch(data.FAILEDONBOARDVNF.TC_7.VNFTYPE);
        await pageFailedtoOnboard.verifyRecord(data.FAILEDONBOARDVNF.TC_7.RECORD);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 8- Verify that table can clear text search when click clear button
    it('Verify that table can clear text search when click clear button- 8', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.setSearch('seach any text');
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageFailedtoOnboard.clickClearTextSearch();
        const result2 = await pageFailedtoOnboard.VerifyAttributeInSear();
        await expect(Boolean(result2 === '')).toBe(true);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // // 9- Verify that all filter and search will be Refresh when click on refesh button
    // it('Verify that all filter and search will be Refresh when click on refesh button - 9', async function () {
    //     await pageNavigation.selectLeftNav(2);
    //     await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
    //     await browser.waitForAngularEnabled(true);
    //     const result1 = await pageError.verifyMessegeError();
    //     if (result1 === true) {
    //         await pageError.cliclClearALL();
    //     }
    //     await pageOnboardedVNFs.clickOnboardbtn();
    //     const record = await pageFailedtoOnboard.GetRecordCurrent();
    //     await pageFailedtoOnboard.setSearch('wrong text');
        // await pageFailedtoOnboard.clickShowHideFilter();
    //     await pageFailedtoOnboard.SelectFilter(4, data.FAILEDONBOARDVNF.TC_6.VNFVERSION);
    //     const result2 = await pageError.verifyMessegeError();
    //     if (result2 === true) {
    //         await pageError.cliclClearALL();
    //     }
    //     await pageFailedtoOnboard.clickRefesh();
    //     await pageFailedtoOnboard.VerifyAttributeInSearch();
    //     await pageFailedtoOnboard.verifyRecord(record);
    //     await pageFailedtoOnboard.clickCancelVNF();

    // });

    // 10 - Verify that the Clear Filter button work properly
    it(' Verify that the Clear Filter button work properly - 10', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        const record = await pageFailedtoOnboard.GetRecordCurrent();
        // await pageFailedtoOnboard.clickShowHideFilter();
        await pageFailedtoOnboard.SelectFilter(3, data.FAILEDONBOARDVNF.TC_4.VNFTYPE);
        await pageFailedtoOnboard.verifyRecord(data.FAILEDONBOARDVNF.TC_4.RECORD);
        await pageFailedtoOnboard.clickClearFilterInPage();
        await pageFailedtoOnboard.verifyRecord(record);
        // const getTextInFilter = await pageFailedtoOnboard.GetTextInFilter(4);
        // await expect(Boolean(getTextInFilter === 'All')).toBe(true);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 11- Verify that the sort table function work properly in Failed to Oboard VNFs table
    it('Verify that the sort table function work properly in Failed to Onboard VNFs table -11', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.verifySortColumn(1);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 12 - Verify that the Failed to Onboard VNFs table display with 10 entries in one page as default
    it('Verify that the Failed to Onboard VNFs table display with 10 entries in one page as default -12', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        const numrow = await pageFailedtoOnboard.GetNumberRow();
        const numberRow = Number(numrow);
        await console.log('Default row display in one page: ' + numberRow);
        await expect(Boolean(numberRow === 10)).toBe(true);
        await pageFailedtoOnboard.verifyNumberRowDisplayOnTable(10);
        await pageFailedtoOnboard.clickCancelVNF();

    });
    // 13 -  Verify that the user can choose the number of entries displayed in one page on table Failed to Onboard VNFs
    // tslint:disable-next-line:max-line-length
    it('Verify that the user can choose the number of entries displayed in one page on table Failed to Onboard VNFs -13', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.selectNumberRowOnTable(20);
        await pageFailedtoOnboard.verifyNumberRowDisplayOnTable(20);
        await pageFailedtoOnboard.selectNumberRowOnTable(10);
        await pageFailedtoOnboard.verifyNumberRowDisplayOnTable(10);
        await pageFailedtoOnboard.clickCancelVNF();
    });

    // 14 -Verify that user can delete the Failed to Onboard VNFs
    it('Verify that user can delete the Failed to Onboard VNFs-14', async function () {
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.clickOnboardbtn();
        await pageFailedtoOnboard.setSearch(data.FAILEDONBOARDVNF.TC_14.VNFNAME);
        await browser.waitForAngularEnabled(true);
        const result = await pageFailedtoOnboard.isTableHasValueAfterSearch();
        if (result === false) {
            await pageFailedtoOnboard.funcUploadCSAR(data.FAILEDONBOARDVNF.TC_14.PATH);
        }
        // await pageFailedtoOnboard.clickRefesh();
        await pageFailedtoOnboard.setSearch(data.FAILEDONBOARDVNF.TC_14.VNFNAME);
        await pageFailedtoOnboard.clickActionsDopdown();
        await pageFailedtoOnboard.clickFunctionActionsDopdown(data.FAILEDONBOARDVNF.TC_14.FUNCTION);
        await pageFailedtoOnboard.clickConfirmDelete();
        await browser.waitForAngularEnabled(true);
        await pageFailedtoOnboard.setSearch(data.FAILEDONBOARDVNF.TC_14.VNFNAME);
        const result4 = await pageFailedtoOnboard.isTableHasValueAfterSearch();
        expect(Boolean(result4)).toBe(false);
    });

});
