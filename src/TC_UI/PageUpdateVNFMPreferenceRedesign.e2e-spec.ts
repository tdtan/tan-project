// import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageUpdateVNFMPreferenceRedesign } from '../PageObject/PageUpdateVNFMPreferenceRedesign.po';
import { browser } from 'protractor';
import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
// import { PageReassignVNFs } from '../PageObject/PageReassignVNFs.po';
// const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project page PageUpdateVNFMPreferenceRedesign', () => {
    // let pageNavigation: PageNavigation;
    let pageUpdateVNFMPreferenceRedesign: PageUpdateVNFMPreferenceRedesign;
    let pageLiveVNFs: PageLiveVNFs;
    // let pageReassignVNFs: PageReassignVNFs;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        // pageNavigation = new PageNavigation();
        pageUpdateVNFMPreferenceRedesign = new PageUpdateVNFMPreferenceRedesign();
        pageLiveVNFs = new PageLiveVNFs();
        // pageReassignVNFs = new PageReassignVNFs();
    });

    // it('Verify that the the Reassign action was remove from Bulk action drop-list- 1', async function () {
    //     await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
    //     await expect(await pageUpdateVNFMPreferenceRedesign.isDisplayBulkAction()).toBeFalsy();
    // });

    // tslint:disable-next-line:max-line-length
    it('Verify that the empty form was displayed when click on "Update VNFM Preference" action in VNF actions drop-list- 2', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch('UI_AUTO');
        await pageLiveVNFs.verifyRecord('Showing 1 of 1 record');
        const textStatus = await pageLiveVNFs.GetTextInTd(7);
        await expect(Boolean(textStatus === 'Ready')).toBe(true);
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions('Update Vnfm Preference');
        await expect(pageUpdateVNFMPreferenceRedesign.VerifyPopupUpdateVNFPerferenceDisplay()).toBeTruthy();
        await expect(await pageUpdateVNFMPreferenceRedesign.getTextDropNewPreferredVNFM()).toEqual('Select VNFM');
    });

    it('Verify that the user can not complete the "Update VNFM Preference" form if not select the new Preferred VNFM-3', async function () {
        await browser.waitForAngularEnabled(true);
        await expect(await pageUpdateVNFMPreferenceRedesign.isYesButtonDialogConfirmEnabled()).toBeFalsy();
    });

    // it('Verify that the user can close the "Update VNFM Preference" form when click on No button- 4', async function () {
    //     await pageUpdateVNFMPreferenceRedesign.clickNoButtonDialogConfirm();
    //     await browser.sleep(2000);
    //     // await pageUpdateVNFMPreferenceRedesign.clickNoButtonDialogConfirm();
    //     await expect(await pageUpdateVNFMPreferenceRedesign.isShowDialogUpdateVNFMPreference()).toBeFalsy();
    // });

    it('Verify that the list of new Preferred VNFM was displayed correctly- 5', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
        await browser.waitForAngularEnabled(true);
        const listVnfm = await pageUpdateVNFMPreferenceRedesign.getListVNFMName();

        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch('UI_AUTO');
        await pageLiveVNFs.clickSelectActions();
        await pageLiveVNFs.clickOptionSelectActions('Update Vnfm Preference');
        await pageUpdateVNFMPreferenceRedesign.clickDropListNewVNFMPreference();
        const listDropNewPreferredVNFM = await pageUpdateVNFMPreferenceRedesign.getListDropNewPreferredVNFM();
        // await pageUpdateVNFMPreferenceRedesign.clickNoButtonDialogConfirm();
        await expect(listVnfm).toEqual(listDropNewPreferredVNFM);
    });

    // tslint:disable-next-line:max-line-length
    // it('Verify that the "Update VNFM Preference" form was closed when user select the new Preferred VNFM and click on Yes button', async function () {

    // });

    // tslint:disable-next-line:max-line-length
    // it('Verify that the Preferred VNFM was updated correctly in VNFM Reassignment table after completed the "Update VNFM Preference" form', async function () {

    // });

    // it('Verify that the VNF status in the VNFM Reassignment table is same as VNF Lifecycle table- 6', async function () {

        // await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        // await browser.waitForAngularEnabled(true);
        // await pageLiveVNFs.setSearch('Tessst');
        // const list1 = await pageUpdateVNFMPreferenceRedesign.getListStatusVNFLifeCycle();

        // await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
        // await browser.waitForAngularEnabled(true);
        // await pageUpdateVNFMPreferenceRedesign.showDialogReassignVNFs();
        // await pageReassignVNFs.settextSearch('Tessst');
        // const list2 = await pageUpdateVNFMPreferenceRedesign.getListStatusVNFMReassignment();
        // await expect(list1).toEqual(list2);
    // });
});
