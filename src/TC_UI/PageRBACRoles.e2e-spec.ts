import { PageRBACRoles } from '../PageObject/PageRBACRoles.po';
import { browser } from 'protractor';
import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageRole } from '../PageObject/PageRole.po';
import { PageError } from '../PageObject/PageError.po';
import { PageAdminRoles } from '../PageObject/PageAdminRole.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');
const rolename = 'user_test';

describe('workspace-project page RBAC', () => {
    let pageFailedtoOnboard: PageFailedtoOnboard;
    let pageRBACRoles: PageRBACRoles;
    let pageOnboardedVNFs: PageOnboardedVNFs;
    let pageNavigation: PageNavigation;
    let pageRole: PageRole;
    let pageAdminRoles: PageAdminRoles;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageRBACRoles = new PageRBACRoles();
        pageNavigation = new PageNavigation();
        pageRole = new PageRole();
        pageFailedtoOnboard = new PageFailedtoOnboard();
        pageOnboardedVNFs = new PageOnboardedVNFs();
        pageAdminRoles = new PageAdminRoles();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can not select the Permissions per Selected Node Type Section toggles when zero Access toggle selected in VNF Type section-1', async function () {
        await pageNavigation.selectLeftNav(2);
        await browser.waitForAngularEnabled(true);
        const rowVnfOnboard = await pageRBACRoles.countNumberVnfOnboard();
        if (rowVnfOnboard === 0) {
            // upload CSAR file
            await pageOnboardedVNFs.clickOnboardbtn();
            await pageFailedtoOnboard.funcUploadCSAR(data.VNF_ONBOARDED.TC_1.PATH);
            await browser.sleep(3000);
            const result1 = await pageError.verifyMessegeError();
            const result2 = await pageError.verifyMessegeWarning();
            const result3 = await pageError.verifyMessegeInfo();
            if (result1 === true || result2 === true || result3 === true) {
                await pageError.cliclClearALL();
            }
            await pageFailedtoOnboard.clickCancelVNF();
            await pageOnboardedVNFs.clickRefesh();
            const numrecord = await pageOnboardedVNFs.GetRecordCurrent();
            await expect(Boolean(numrecord === 'Showing 1 of 1 record')).toBe(true);
            await pageRBACRoles.countNumberVnfOnboard();
        }
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        await pageRBACRoles.clickBtnAddRole();
        await pageRBACRoles.disableAllSwitchVNFType();
        await expect(await pageRBACRoles.countSwitchVNFLifeCycleEnable()).toEqual(0);
    });

    it('Verify that the empty form was displayed when user click on add role button-2', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        await pageRBACRoles.clickBtnAddRole();
        await expect(await pageRBACRoles.isHaveInputSwitchOn()).toBeFalsy();
        await expect(pageRBACRoles.getValueInputRoleName()).toEqual('');
    });

    it('Verify that the user can not add role without input the Name of Role-3', async function () {
        await pageRBACRoles.clearValueInputRoleName();
        await pageRBACRoles.clickSaveButton();
        await expect(await pageRBACRoles.isRoleNameShowMessage('Rolename is required')).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can enable all toggles in form when click on All Permissions toggle (from disabled to enabled)-4', async function () {
        // click switch ALL Permissions
        await pageRBACRoles.clickSwitch(0);
        await expect(await pageRBACRoles.isHaveInputSwitchOff()).toBeFalsy();
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can disable all toggles in form when click on All Permissions toggle (from enabled to disabled)-5', async function () {
        // click switch ALL Permissions
        await pageRBACRoles.clickSwitch(0);
        await expect(await pageRBACRoles.isHaveInputSwitchOn()).toBeFalsy();
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can enable all toggles in Administration Permissions section when click on All Administration Permissions toggle (from disabled to enabled)-6', async function () {
        const numberSwitch = await pageRBACRoles.getNumberSwitchAdministrationPermissions();
        // click switch All Administration
        await pageRBACRoles.clickSwitch(0);
        const numberSwitchIsOn = await pageRBACRoles.getNumberSwitchAdministrationPermissionsIsOn();
        // await expect(numberSwitch).toEqual(19);
        await expect(numberSwitchIsOn).toEqual(numberSwitch);
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can disable all toggles in Administration Permissions section when click on All Administration Permissions toggle (from enabled to disabled)-7', async function () {
        // click switch All Administration
        await pageRBACRoles.clickSwitch(1);
        const numberSwitchIsOn = await pageRBACRoles.getNumberSwitchAdministrationPermissionsIsOn();
        await expect(numberSwitchIsOn).toEqual(0);
    });

    it('Verify that the user can enable all toggles in a row when click on "All" toggle (from disabled to enabled)-8', async function () {
        await pageRBACRoles.clickSwitch(1);
        await pageRBACRoles.verifyRowAdministrationPermissionsEnabled('Cloud');
        await pageRBACRoles.verifyRowAdministrationPermissionsEnabled('Tenant');
        await pageRBACRoles.verifyRowAdministrationPermissionsEnabled('Users');
        await pageRBACRoles.verifyRowAdministrationPermissionsEnabled('Settings');
        await pageRBACRoles.verifyRowAdministrationPermissionsEnabled('Snmp Trap Destination');
        await pageRBACRoles.verifyRowAdministrationPermissionsEnabled('Continuity Plan');

    });

    it('Verify that the user can disable all toggles in a row when click on "All" toggle (from enabled to disabled)-9', async function () {
        await pageRBACRoles.verifyRowAdministrationPermissionsDisabled('Cloud');
        await pageRBACRoles.verifyRowAdministrationPermissionsDisabled('Tenant');
        await pageRBACRoles.verifyRowAdministrationPermissionsDisabled('Users');
        await pageRBACRoles.verifyRowAdministrationPermissionsDisabled('Settings');
        await pageRBACRoles.verifyRowAdministrationPermissionsDisabled('Snmp Trap Destination');
        await pageRBACRoles.verifyRowAdministrationPermissionsDisabled('Continuity Plan');
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can enable a toggle except the "All" toggle when click on this toggle (from disabled to enabled)-10', async function () {
        await pageRBACRoles.clickSwitch(0);
        await expect(await pageRBACRoles.isHaveInputSwitchOff()).toBeFalsy();
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the user can disable a toggle except the "All" toggle when click on this toggle (from enabled to disabled)-11', async function () {
        await pageRBACRoles.clickSwitch(0);
        await expect(await pageRBACRoles.isHaveInputSwitchOn()).toBeFalsy();
    });

    // // Verify that user not select individual toggles in Administration Permissions when user VNFM Admin toggles disable
     // tslint:disable-next-line:max-line-length
    // it('Verify that user not select individual toggles in Administration Permissions when user VNFM Admin toggles disable -12', async function () {
    //     let getTextValue = await pageRBACRoles.getCssValueInToggle('');
    //     if (getTextValue) {

    //     }
    // });

    // it('Verify that the parent "All" toggle swtich to disable when user disable any toggle-13', async function () {
    //  // Verify that all individual toggles enable when user enable VNFM Admin
    //     await pageRBACRoles.verifyParentToggleSwitchToDisable();
    // });

    // it('Verify that the VNF type section display correctly with all Onboarded VNF type-14', async function () {
    //     await pageNavigation.selectLeftNav(2);
    //     const listOnboard = await pageRBACRoles.getListVNFOnboard();
    //     await pageNavigation.selectLeftNav(5);
    //     await pageNavigation.selectLeftNavSecond(5, 2);

    //     // Delete comment after JR fix
    //     // await pageRBACRoles.clickBackButtonToRolePage();
    //     await pageRBACRoles.clickBtnAddRole();
    //     const listVNFType = await pageRBACRoles.getListVNFType();
    //     await expect(listVNFType.includes(listOnboard)).toBeTruthy();
    // });

    it('Verify that the Add Role form was closed and table update with nothing when user click on Cancel button-15', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        await pageRBACRoles.clickBtnAddRole();

        await pageRBACRoles.setValueInputRoleName(rolename);
        await pageRBACRoles.clickCancelButton();
        await browser.sleep(1000);
        await expect(await pageRBACRoles.isPageAddRoleDisplay()).toBeFalsy();
        // await expect(await pageRBACRoles.verifyAppearInColumnRole(rolename)).toBeFalsy();
    });

    // tslint:disable-next-line:max-line-length
    it('Verify that the Add Role form was closed and the Role table was updated when with new role when user click on Save button-16', async function () {
        await pageRBACRoles.clickBtnAddRole();
        await pageRBACRoles.setValueInputRoleName(rolename);
        await pageRBACRoles.clickSwitch(1);
        await pageRBACRoles.clickSaveButton();
        await browser.sleep(1000);
        await expect(await pageRBACRoles.isPageAddRoleDisplay()).toBeFalsy();
        await pageAdminRoles.ClickRefreshButton();
        await expect(await pageRBACRoles.verifyAppearInColumnRole(rolename)).toBeTruthy();
        // await browser.sleep(2000);
    });

    it('Verify that the list permissions of selected Role was display when user click on a row of any Role-17', async function () {
        await pageRole.clickRowContainKeyWord(1, rolename);
        await expect(await pageRBACRoles.isPageEditRoleDisplay).toBeTruthy();
        const totalSwitch = await pageRBACRoles.getNumberSwitchAdministrationPermissions();
        const switchEnable = await pageRBACRoles.getNumberSwitchAdministrationPermissionsIsOn();
        await expect(totalSwitch).toEqual(switchEnable + 28);
    });

    it('Verify that the user can update the list permissions of old Role when complete the Edit Role function-18', async function () {
        await pageRBACRoles.clickSwitch(1);
        await pageRBACRoles.clickSaveButton();
        await expect(await pageRBACRoles.isPageEditRoleDisplay).toBeFalsy;
        await pageRole.clickRowContainKeyWord(1, rolename);
        const switchEnable = await pageRBACRoles.getNumberSwitchAdministrationPermissionsIsOn();
        await expect(switchEnable).toEqual(0);

        await pageRBACRoles.clickSwitch(1);
        await pageRBACRoles.clickSaveButton();
        await expect(await pageRBACRoles.isPageEditRoleDisplay).toBeFalsy;
        await pageRole.clickRowContainKeyWord(1, rolename);
        const switchEnable2 = await pageRBACRoles.getNumberSwitchAdministrationPermissionsIsOn();
        await expect(switchEnable2).toEqual(1);
    });

    it('Verify that the text note was displayed when zero node types onboarded-19', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfcatalog');
        await browser.waitForAngularEnabled(true);
        await pageRBACRoles.removeAllVnfOnboard();
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        // await pageRBACRoles.clickBackButtonToRolePage();
        await pageRBACRoles.clickBtnAddRole();
        await expect(await pageRBACRoles.isTextNoteWasDisplayedVnfType('There are no VNF Types Available.')).toBeTruthy();
    });

    //  tslint:disable-next-line:max-line-length
    it('Verify that the user can not select the Permissions per Selected Node Type Section toggles when zero node types onboarded-20', async function () {
        await expect(await pageRBACRoles.isPermissionsPerSelectedVNFTypeDisplay()).toBeFalsy();
    });

});
