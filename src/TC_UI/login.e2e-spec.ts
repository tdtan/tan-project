import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { browser} from 'protractor';
// import { PageError } from '../PageObject/PageError.po';
// import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project login', () => {
  let page: PageLogin;
  let page2: PageNavigation;
  // let pageLiveVNFs: PageLiveVNFs;
  // let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
    page = new PageLogin();
    page2 = new PageNavigation();
    // pageLiveVNFs = new PageLiveVNFs();
    // pageError = new PageError();
  });

  // Verify that user can not login to the VNFM UI without password
  // it('Verify that user can not login to the VNFM UI without password', async function () {
  //   await browser.restart();
  //   await browser.get(data.INFOLOGIN.URL);
  //   await browser.waitForAngularEnabled(true);
  //   browser.driver.manage().window().maximize();
  //   await page.funcLoginInfo('sysadmin', '');
  //   await page.verifyPasswordErrorMessageWhenLoginFail('Password is required.');
  // });

  // // Verify that user can't login to the VNFM UI without empty user
  // it('Verify that user can not login to the VNFM UI without empty user', async function () {
  //   await browser.restart();
  //   await browser.get(data.INFOLOGIN.URL);
  //   await browser.waitForAngularEnabled(true);
  //   browser.driver.manage().window().maximize();
  //   await page.funcLoginInfo('', 'sysadmin!!');
  //   await page.verifyUserNameErrorMessageWhenLoginFail('Username is required.');
  // });

  // // Verify that user can't login to the VNFM UI with empty user and password
  // it('Verify that user can not login to the VNFM UI with empty user and password', async function () {
  //   await browser.restart();
  //   await browser.get(data.INFOLOGIN.URL);
  //   await browser.waitForAngularEnabled(true);
  //   browser.driver.manage().window().maximize();
  //   await page.funcLoginInfo('', '');
  //   await page.verifyUserNameErrorMessageWhenLoginFail('Username is required.');
  // });

  // // Verify that user can't login to the VNFM UI with username or password is invalid
  // it('Verify that user can not login to the VNFM UI with username or password is invalid', async function () {
  //   await browser.restart();
  //   await browser.get(data.INFOLOGIN.URL);
  //   await browser.waitForAngularEnabled(true);
  //   browser.driver.manage().window().maximize();
  //   await page.funcLoginInfo('abc', 'abcftt');
  //   await page.verifyUserNameErrorMessageWhenLoginFail('Username or password is incorrect!');
  // });

  // Verify that user can login to the VNFM UI successfully
  it('check login successfully', async function () {
    await browser.restart();
    await browser.get(data.INFOLOGIN.URL);
    browser.driver.manage().window().maximize();
    await browser.waitForAngularEnabled(true);
    await page.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
    await browser.waitForAngularEnabled(false);
    await page2.verifyUserProfile(data.INFOLOGIN.USERNAME);
  });

  // // Verify that the VNFM UI returns to default display when refreshing the UI
  // it('Verify that the VNFM UI returns to default display when refreshing the UI', async function () {
  //   await page2.selectLeftNav(3);
  //   await browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
  //   await browser.waitForAngularEnabled(true);
  //   // const txtUrl1 = browser.getCurrentUrl();
  //   // await console.log('URL 1: ' + txtUrl1);
  //   const txtOnboard = await pageLiveVNFs.getTextHeaderPage();
  //   await console.log('Text 1: ' + txtOnboard);
  //   browser.driver.navigate().refresh();
  //   await browser.waitForAngularEnabled(true);
  //   // const txtUrl2 = browser.getCurrentUrl();
  //   // await console.log('URL 2: ' + txtUrl2);
  //   // await expect(Boolean(txtUrl1 === txtUrl2)).toBe(true);
  //   const txtLiveVNF = await pageLiveVNFs.getTextHeaderPage();
  //   await console.log('Text 2: ' + txtOnboard);
  //   await expect(Boolean(txtOnboard === txtLiveVNF)).toBe(true);

  // });
});
