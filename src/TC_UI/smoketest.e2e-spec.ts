import { PageLogin } from '../PageObject/PageLogin.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { browser} from 'protractor';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project login', () => {
  let page: PageLogin;
  let page2: PageNavigation;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    page = new PageLogin();
    page2 = new PageNavigation();
  });

  // Verify that user can login to the VNFM UI successfully
 it('check login successfully', async function () {
    await browser.restart();
    await browser.get(data.INFOLOGIN.URL);
    await browser.waitForAngularEnabled(true);
    browser.driver.manage().window().maximize();
    await page.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
    await browser.waitForAngularEnabled(false);
    await page2.verifyUserProfile(data.INFOLOGIN.USERNAME);
  });
});
