// import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageUsers } from '../PageObject/PageUsers.po';
import { browser } from 'protractor';
import { PageEditUser } from '../PageObject/PageEditUser.po';
import { PageError } from '../PageObject/PageError.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Users', () => {
  // let pageNavigation: PageNavigation;
  let pageUsers: PageUsers;
  let pageEditUser: PageEditUser;
  let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;
    // pageNavigation = new PageNavigation();
    pageEditUser = new PageEditUser();
    pageUsers = new PageUsers();
    pageError = new PageError();
  });
  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await pageError.cliclClearALL();
    }
  });

  // 1 - Verify that the dropdown filter and sort functions work properly with Username column
  it('Verify that the dropdown filter and sort functions work properly with Username column - 1 ', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/users');
    await browser.waitForAngularEnabled(true);
    const numRecord = await pageUsers.getRecord();
    await pageUsers.clickShowHideFilterButton();
    await pageUsers.SelectFilter(1, data.ADMIN_USERS.TC_1.FILTER_NAME);
    await pageUsers.verifyRecord(data.ADMIN_USERS.TC_1.RECORD);
    await pageUsers.SelectFilter(1, 'All');
    const numRecord2 = await pageUsers.getRecord();
    await expect(Boolean(numRecord === numRecord2)).toBe(true);
    await pageUsers.verifysort(1);
  });

  // 2 - Verify that the dropdown filter and sort functions work properly with Role column
  it('Verify that the dropdown filter and sort functions work properly with Role column - 2 ', async function () {
    const numRecord = await pageUsers.getRecord();
    await pageUsers.SelectFilter(2, data.ADMIN_USERS.TC_2.FILTER_ROLE);
    await pageUsers.verifyRecord(data.ADMIN_USERS.TC_2.RECORD);
    await pageUsers.SelectFilter(2, 'All');
    const numRecord2 = await pageUsers.getRecord();
    await expect(Boolean(numRecord === numRecord2)).toBe(true);
    await pageUsers.verifysort(2);
  });

  // 3 - Verify that the Clear Filter button work properly
  it('Verify that Clear Filter button work properly - 3 ', async function () {
    // await pageUsers.clickClearFilterButton();
    const numRecord = await pageUsers.getRecord();
    await pageUsers.SelectFilter(1, data.ADMIN_USERS.TC_3.FILTER_NAME);
    await pageUsers.verifyRecord(data.ADMIN_USERS.TC_3.RECORD);
    await pageUsers.clickClearFilterButton();
    const numRecord2 = await pageUsers.getRecord();
    await expect(Boolean(numRecord === numRecord2)).toBe(true);
  });

  // 4 - Verify that user can select the number of displayed columns
  it('Verify that user can select the number of displayed columns - 4', async function () {
    await browser.waitForAngularEnabled(true);
    await pageUsers.clickShowHideColumn();
    await pageUsers.selectOptionNamefromShowHideColumnsList(data.ADMIN_USERS.TC_4.COLUMNNAME);
    const verify1 = await pageUsers.verifyShowHideColumns(data.ADMIN_USERS.TC_4.COLUMNNAME);
    await expect(verify1).toBeFalsy();

    await pageUsers.clickShowHideColumn();
    await pageUsers.selectOptionNamefromShowHideColumnsList(data.ADMIN_USERS.TC_4.COLUMNNAME);
    // const verify2 = await pageUsers.verifyShowHideColumns(data.ADMIN_USERS.TC_4.COLUMNNAME);
    // await expect(verify2).toBeTruthy();
  });

  // 5 - Verify that the Search function work properly
  it('Verify that search function for VNF works properly - 5', async function () {
    await pageUsers.setSearch('wrong text');
    await pageUsers.verifyRecord('No records found');
    await pageUsers.setSearch(data.ADMIN_USERS.TC_5.INPUT_TEXT);
    await pageUsers.verifyRecord(data.ADMIN_USERS.TC_5.RECORD);
    await pageUsers.clickClearSearchButton();
  });

  // 6 - Verify that the Clear Search button work properly
  it('Verify that the Clear Search button work properly - 6 ', async function () {
    const Value = await pageUsers.getValue();
    await pageUsers.setSearch('wrong text');
    await pageUsers.verifyRecord('No records found');
    await pageUsers.clickClearSearchButton();
    const Value2 = await pageUsers.getValue();
    await expect(Boolean(Value === Value2)).toBe(true);
  });

  // 7 - Verify that the tables display with maximum 5 entries in one page as default
  it('Verify that the tables display with maximum 5 entries in one page as default - 7 ', async function () {
    const numbRow = await pageUsers.GetNumberRowInPage();
    const totalRow = await pageUsers.CountNumberRowInpage();
    await expect(Boolean(numbRow === '5')).toBe(true);
    await expect(Boolean(totalRow <= 5)).toBe(true);
  });

  // 8 - Verify that the user can choose the number of entries displayed in one page
  it('Verify that the user can choose the number of entries displayed in one page - 8 ', async function () {
    await pageUsers.SelectNumberRowDisplay(15);
    const result2 = await pageUsers.CountNumberRowInpage();
    await expect(Boolean(result2 <= 15)).toBe(true);
    await pageUsers.SelectNumberRowDisplay(5);
    const result3 = await pageUsers.CountNumberRowInpage();
    await expect(Boolean(result3 <= 5)).toBe(true);
  });


  // 9 - Verify that the user can move to specified page when click on the page number
  it('Verify that the user can move to specified page when click on the page number - 9', async function () {
    const record = await pageUsers.getRecord();
    const numRecord = record.slice(13);
    await console.log('number record: ' + numRecord);
    const numberRecord = Number(numRecord.replace(' records' , ''));
    await console.log('string: ' + numberRecord);
    if (numberRecord > 5) {
      await pageUsers.MoveToPageNumber(data.ADMIN_USERS.TC_9.PAGE_NUMBER);
    }
  });

  // 10 - Verify that the value of current user were displayed when user click on existing user row
  it('Verify that the value of current user were displayed when user click on existing user row - 10', async function () {
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageUsers.setSearch(data.ADMIN_USERS.TC_10.INPUT_TEXT);
    const record = await pageUsers.getRecord();
    await expect(Boolean(record === data.ADMIN_USERS.TC_10.RECORD)).toBe(true);
    // show form
    await pageUsers.clickRowTable();
    await expect(await pageEditUser.verifyDisplayInfoUserAfterClickUser());
    await pageEditUser.clickCancel();
    await pageUsers.clickClearSearchButton();
  });
});



