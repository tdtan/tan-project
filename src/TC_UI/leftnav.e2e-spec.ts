import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';
import { browser, ExpectedConditions} from 'protractor';
import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
import { PageVNFMConfTenants } from '../PageObject/PageVNFMConfTenants.po';
import { PageUsers } from '../PageObject/PageUsers.po';
import { PageAdminSetting } from '../PageObject/PageAdminSetting.po';
import { PageDatabase } from '../PageObject/PageDatabase.po';
import { PageRESTAPIs } from '../PageObject/PageRESTAPIs.po';
import { PageLogin } from '../PageObject/PageLogin.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Left Navigation', () => {
    let pageNavigation: PageNavigation;
    let pageVNFMConfCloudSystems: PageVNFMConfCloudSystems;
    let pageOnboardedVNFs: PageOnboardedVNFs;
    let pageLiveVNFs: PageLiveVNFs;
    let pageVNFMConfTenants: PageVNFMConfTenants;
    let pageUsers: PageUsers;
    let pageAdminSetting: PageAdminSetting;
    let pageDatabase: PageDatabase;
    let login: PageLogin;
    let pageRESTAPIs: PageRESTAPIs;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
        pageNavigation = new PageNavigation();
        pageVNFMConfCloudSystems = new PageVNFMConfCloudSystems();
        pageOnboardedVNFs = new PageOnboardedVNFs();
        pageLiveVNFs = new PageLiveVNFs();
        pageVNFMConfTenants = new PageVNFMConfTenants();
        pageUsers = new PageUsers();
        pageAdminSetting = new PageAdminSetting();
        pageDatabase = new PageDatabase();
        login = new PageLogin();
        pageRESTAPIs = new PageRESTAPIs();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    it('login VNFM', async function () {

        await browser.restart();
        await browser.get(data.INFOLOGIN.URL);
        browser.driver.manage().window().maximize();
        await browser.waitForAngularEnabled(true);
        await login.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
      });

    // 1- Verify that the Left Navigation can show the secondary level tabs when clicking on the caret of Parent tabs
    it('Verify that the Left Navigation can show the secondary level tabs when clicking on the caret of Parent tabs -1', async function () {

        await pageNavigation.selectLeftNav(4);
        await pageNavigation.selectLeftNavSecond(4, 1);
        await pageNavigation.verifyShowMenuSecond('tenants');
    });

    // 2-Verify that UI can navigate to the correct screen when clicking on VNF catalog tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on VNF catalog tab in Left Navigation- 2', async function () {

        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 4000);
        const txtOnboard = await pageOnboardedVNFs.getTextOnboard();
        await expect(Boolean(txtOnboard === data.LEFTNAVIGATION.TC_2.TEXT)).toBe(true);
    });

    // 3 - Verify that UI can navigate to the correct screen when clicking on Live VNF | VNFC tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on Live VNF | VNFC tab in Left Navigation- 3', async function () {

        await pageNavigation.selectLeftNav(3);
        await browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 4000);
        const txtOnboard = await pageLiveVNFs.getTextHeaderPage();
        await expect(Boolean(txtOnboard === data.LEFTNAVIGATION.TC_3.TEXT)).toBe(true);
    });

    // 4- Verify that UI can navigate to the correct screen when clicking on Cloud Systems tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on Cloud Systems tab in Left Navigation- 4', async function () {

        await pageNavigation.selectLeftNav(4);
        await pageNavigation.selectLeftNavSecond(4, 1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 4000);
        await pageVNFMConfCloudSystems.verifyShowTextCloud('Clouds');

    });

    // 5- Verify that UI can navigate to the correct screen when clicking on Tenants tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on Tenants tab in Left Navigation- 5', async function () {

        await pageNavigation.selectLeftNav(4);
        await pageNavigation.selectLeftNavSecond(4, 2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/tenant'), 4000);
        await pageVNFMConfTenants.verifyShowTextTenant('Tenants');

    });
    // 6- Verify that UI can navigate to the correct screen when clicking on Users tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on Users tab in Left Navigation- 6', async function () {

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/users'), 4000);
        await pageUsers.verifyShowTextUser('Users');

    });
    // 7- Verify that UI can navigate to the correct screen when clicking on Settings tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on Settings tab in Left Navigation- 7', async function () {

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 4);
        await browser.wait(ExpectedConditions.urlContains('vnfm/settings'), 4000);
        await pageAdminSetting.verifyShowTextConfiguration('Settings');

    });
    // 8 - Verify that UI can navigate to the correct screen when clicking on Database tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on Database tab in Left Navigation- 8', async function () {

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 5);
        await browser.wait(ExpectedConditions.urlContains('vnfm/database'), 4000);
        await pageDatabase.verifyShowTextDatabase('Database');

    });

    // 9 - Verify that UI can navigate to the correct screen when clicking on REST API tab in Left Navigation
    it('Verify that UI can navigate to the correct screen when clicking on REST API tab in Left Navigation- 9', async function () {

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 9);
        await browser.wait(ExpectedConditions.urlContains('vnfm/restapis'), 4000);
        await pageRESTAPIs.verifyShowTextRESTAPIs('REST APIs');

    });
});
