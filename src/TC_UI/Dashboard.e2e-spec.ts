import { PageDashboard } from '../PageObject/PageDashboard.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
import { PageAddSecurityRealms } from '../PageObject/PageAddSecurityRealms.po';
import { PageSecurityRealms } from '../PageObject/PageSecurityRealms.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Dashboard', () => {
    let pageDashboard: PageDashboard;
    let pageNavigation: PageNavigation;
    let pageSecurityRealms: PageSecurityRealms;
  let pageAddSecurityRealms: PageAddSecurityRealms;
    let pageError: PageError;

    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageDashboard = new PageDashboard();
        pageNavigation = new PageNavigation();
        pageSecurityRealms = new PageSecurityRealms();
        pageAddSecurityRealms = new PageAddSecurityRealms();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // 1- Verify that the Text filter and sort functions work properly with ID column
    it('Verify that the Text filter and sort functions work properly with ID column- 1', async function () {
        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        const record = await pageDashboard.getRecord();
        const UID1 = await pageDashboard.GetTextInTd(1);
        await pageDashboard.clickShowHidefilter();
        await pageDashboard.SelectFilterWithCheckbox(1, UID1);
        await pageDashboard.verifyRecord(data.DASHBOARD.TC_1.RECORD);
        await pageDashboard.SelectFilterWithCheckbox(1, UID1);
        await pageDashboard.verifyRecord(record);
        await pageDashboard.verifysort(1);
    });

    // 2- Verify that the date filter and sort functions work properly with Date column
    it('Verify that the date filter and sort functions work properly with Date column- 2', async function () {

        // const date = new Date();
        // const day = date.getDate();
        await pageDashboard.clickDate();
        await pageDashboard.clickLastDay();
        const result3 = await pageDashboard.isTableHasValueAfterSearch();
        await expect(Boolean(result3)).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
        // await pageDashboard.verifysort(2);
    });

    // 3- Verify that the dropdown filter and sort functions work properly with Type column
    it('Verify that the dropdown filter and sort functions work properly with Type column- 3', async function () {

        const record = await pageDashboard.getRecord();
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_3.TYPE);
        const result3 = await pageDashboard.isTableHasValueAfterSearch();
        await expect(Boolean(result3)).toBe(true);
        await pageDashboard.SelectFilter(3, 'All');
        await pageDashboard.verifyRecord(record);
        await pageDashboard.verifysort(3);
    });

    // 4- Verify that the dropdown filter and sort functions work properly with Severity column
    it('Verify that the dropdown filter and sort functions work properly with Severity column- 4', async function () {

        const record = await pageDashboard.getRecord();
        await pageDashboard.SelectFilter(4, data.DASHBOARD.TC_4.SEVERITY);
        const result3 = await pageDashboard.isTableHasValueAfterSearch();
        await expect(Boolean(result3)).toBe(true);
        await pageDashboard.SelectFilter(4, 'All');
        await pageDashboard.verifyRecord(record);
        await pageDashboard.verifysort(4);
    });

    // 5- Verify that the dropdown filter and sort functions work properly with User column
    it('Verify that the dropdown filter and sort functions work properly with User column- 5', async function () {

        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const record = await pageDashboard.getRecord();
        await pageDashboard.SelectFilter(5, data.DASHBOARD.TC_5.USER);
        const result3 = await pageDashboard.isTableHasValueAfterSearch();
        await expect(Boolean(result3)).toBe(true);
        await pageDashboard.SelectFilter(5, 'All');
        await pageDashboard.verifyRecord(record);
        await pageDashboard.verifysort(5);
    });

    // 6- Verify that the Text filter and sort functions work properly with Message column
    it('Verify that the Text filter and sort functions work properly with Message column- 6', async function () {
        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        await pageDashboard.clearDescription();
        await pageDashboard.settextDescription(data.DASHBOARD.TC_6.MESSAGE);
        const result3 = await pageDashboard.isTableHasValueAfterSearch();
        await expect(Boolean(result3)).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();

    });

    // 7- Verify that the Clear button in Filter work properly
    it('Verify that the Clear button in Filter work properly- 7', async function () {

        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageDashboard.SelectFilter(5, data.DASHBOARD.TC_5.USER);
        await pageDashboard.clickClearButtonToClearTextFilter();
        const txtText = await pageDashboard.getTextInDropdownFilter(5);
        await expect(Boolean(txtText === 'All')).toBe(true);
    });

    // 8- Verify that the Search function work properly
    it('Verify that the Search function work properly- 8', async function () {
        const UID1 = await pageDashboard.GetTextInTd(1);
        await pageDashboard.setSearch('wrong text');
        await pageDashboard.verifyRecord('No records found');
        await pageDashboard.setSearch(UID1);
        await pageDashboard.verifyRecord(data.DASHBOARD.TC_1.RECORD);
        await pageDashboard.clickRefesh();

    });

    // 9- Verify that the Clear table button work properly
    it('Verify that the Clear table button work properly- 9', async function () {
        await pageNavigation.selectLeftNav(1);
        browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageDashboard.setSearch('wrong text');
        await pageDashboard.clickClearSearch();
        const value = await pageDashboard.getAttributeSearch();
        await expect(Boolean(value === '')).toBe(true);
    });

    // 10- Verify that the tables display with maximum 10 entries in one page as default
    it('Verify that the tables display with maximum 10 entries in one page as default- 10', async function () {

        await pageNavigation.selectLeftNav(1);
        browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const numberR = await pageDashboard.GetNumberRow();
        await expect(Boolean(numberR === '10')).toBe(true);
        const numberTr = await pageDashboard.countNumberRowDisplayOnTable();
        await expect(Boolean(numberTr <= 10)).toBe(true);
    });

    // 11- Verify that the user can choose the number of entries diplayed in one page
    it('Verify that the user can choose the number of entries diplayed in one page- 11', async function () {

        await pageNavigation.selectLeftNav(1);
        browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageDashboard.selectNumberRowOnTable(data.DASHBOARD.TC_11.NUMBERPAGE);
        const numberR = await pageDashboard.GetNumberRow();
        await expect(Boolean(numberR === '20')).toBe(true);
        const count = await pageDashboard.countNumberRowDisplayOnTable();
        await expect(Boolean(count <= data.DASHBOARD.TC_11.NUMBERPAGE)).toBe(true);
    });

    // 12- Verify that the user can move to specified page when click on the page number
    it('Verify that the user can move to specified page when click on the page number- 12', async function () {

        await pageNavigation.selectLeftNav(1);
        browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        const record = await pageDashboard.getRecord();
        const SliceRecord = record.slice(0, 9);
        const ReplaceRecord = Number(SliceRecord.replace('Showing ', ''));
        const count = await pageDashboard.GetNumberRow();
        const numRow = Number(count);
        if (ReplaceRecord > numRow) {
            await pageDashboard.clickPage2();
            const numpage = await pageDashboard.GetPageCurrentdisplay();
            await expect(Boolean(numpage === '2')).toBe(true);
        }
    });

    // 13 - Verify that the table will update when an alerts was generated
    it('Verify that the table will update when an alerts was generated- 13', async function () {

        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const recordOld = await pageDashboard.getRecord();
        const SliceRecord = recordOld.slice(13);
        const numRecordOsld = Number(SliceRecord.replace(' records', ''));
        await console.log('number old record: ' + numRecordOsld);

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 3);
        await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
        await browser.waitForAngularEnabled(true);

        await pageSecurityRealms.clickbtnAddRealm();
        await pageAddSecurityRealms.setInputNameRealm(data.SECURITYREALMS.TC_6.REALMNAME);
        await pageAddSecurityRealms.selectRealmType(data.SECURITYREALMS.TC_6.REALMTYPE);
        await browser.waitForAngularEnabled(true);
        await pageAddSecurityRealms.clearURL();
        await pageAddSecurityRealms.setInputURL(data.SECURITYREALMS.TC_6.URL);
        await pageAddSecurityRealms.clearUserName();
        await pageAddSecurityRealms.setInputUserName(data.SECURITYREALMS.TC_6.USERNAME);
        await pageAddSecurityRealms.setInputTemlate(data.SECURITYREALMS.TC_6.TEMPATE);
        await pageAddSecurityRealms.setInputSearchBase(data.SECURITYREALMS.TC_6.SEARCHBASE);
        await pageAddSecurityRealms.setInputSearchFile(data.SECURITYREALMS.TC_6.SEARCHFILE);
        await pageAddSecurityRealms.clickbtnSave();
        await browser.waitForAngularEnabled(true);
        await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_25.TEXTSEARCH);
        await expect(pageSecurityRealms.verifyRowTableDisplay()).toBeTruthy();
        const record2 = await pageSecurityRealms.GetRecordCurrent();
        await expect(Boolean(record2 === data.SECURITYREALMS.TC_6.RECORD)).toBe(true);

        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const recordNew = await pageDashboard.getRecord();
        const SliceRecord2 = recordNew.slice(13);
        const numRecordNew = Number(SliceRecord2.replace(' records', ''));
        await console.log('number new record: ' + numRecordNew);
        if (numRecordNew < 1000) {
            await expect(Boolean(numRecordOsld < numRecordNew)).toBe(true);
        } else {
            await expect(Boolean(numRecordOsld === numRecordNew)).toBe(true);
            await expect(Boolean(numRecordOsld === 1000)).toBe(true);
        }

    });

    // 14 - Verify that the table display with all alerts and events within VNFM
    it('Verify that the table display with all alerts and events within VNFM- 14', async function () {

        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        const recordOld = await pageDashboard.getRecord();
        const SliceRecord = recordOld.slice(13);
        const numRecordOsld = Number(SliceRecord.replace(' records', ''));
        await console.log('number old record: ' + numRecordOsld);

        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 3);
        await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageSecurityRealms.ClearTxbSearch();
        await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_25.TEXTSEARCH);
        const result = await pageSecurityRealms.isTableHasValueAfterSearch();
        if (result) {
            await pageSecurityRealms.clickbtnSelectActions();
            await pageSecurityRealms.clickbtnOptionActions(data.SECURITYREALMS.TC_25.FUNCTION);
            await pageSecurityRealms.clickbtnConfirmYes();
        }

        await browser.waitForAngularEnabled(true);
        const result2 = await pageSecurityRealms.isTableHasValueAfterSearch();
        await expect(Boolean(result2 === false)).toBe(true);

        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);
        await browser.sleep(3000);
        const recordNew = await pageDashboard.getRecord();
        const SliceRecord2 = recordNew.slice(13);
        const numRecordNew = Number(SliceRecord2.replace(' records', ''));
        await console.log('number new record: ' + numRecordNew);
        if (numRecordNew < 1000) {
            await expect(Boolean(numRecordOsld < numRecordNew)).toBe(true);
        } else {
            await expect(Boolean(numRecordOsld === numRecordNew)).toBe(true);
            await expect(Boolean(numRecordOsld === 1000)).toBe(true);
        }

    });

    // 15 - Verify that the table display total number of events in the bottom
    it('Verify that the table display total number of events in the bottom- 15', async function () {

        await pageNavigation.selectLeftNav(1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        await browser.waitForAngularEnabled(true);

        const result1 = await pageError.verifyMessegeError();
        if (result1 === true) {
            await pageError.cliclClearALL();
        }
        await pageDashboard.selectNumberRowOnTable(data.DASHBOARD.TC_15.NUMBERPAGE);
        const recordOld = await pageDashboard.getRecord();
        const SliceRecord = recordOld.slice(14);
        const numRecord = Number(SliceRecord.replace(' records', ''));
        await console.log('number record: ' + numRecord);
        if (numRecord <= data.DASHBOARD.TC_15.NUMBERPAGE) {
            const numberRow = await pageDashboard.countNumberRowDisplayOnTable();
            await expect(Boolean(numberRow === numRecord)).toBe(true);
        } else {
            await pageDashboard.clickPageEnd();
            const numberRecordInPageEnd = await pageDashboard.GetPageCurrentdisplay2();
            const numberPage = Number(numberRecordInPageEnd);
            const numberRowInPageEnd = await pageDashboard.countNumberRowDisplayOnTable();
            const totalRowNoPageEnd = ((numberPage - 1) * data.DASHBOARD.TC_15.NUMBERPAGE);
            await console.log('total 1: ' + totalRowNoPageEnd);
            const totalRow = totalRowNoPageEnd + numberRowInPageEnd;
            await console.log('total 2: ' + totalRow);
            await expect(Boolean(totalRow === numRecord)).toBe(true);
            // await expect(recordOld.includes(totalRow)).toBeTruthy();
        }

    });

});
