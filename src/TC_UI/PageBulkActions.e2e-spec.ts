import { PageBulkActions } from '../PageObject/PageBulkActions.po';
import { browser } from 'protractor';

describe('workspace-project pageBulkActions', () => {
    let pageBulkActions: PageBulkActions;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageBulkActions = new PageBulkActions();
    });
    // Verify that the Bulk Action drop-list is disable by default
    it('Verify that the Bulk Action drop-list is disable by default-1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        await pageBulkActions.isBulkActionDropListDisable();
        await expect(await pageBulkActions.isBulkActionDropListDisable()).toBeTruthy();
    });

    // Verify that the Bulk Action drop-list only enable when two or more VNF checkboxes selected
    it('Verify that the Bulk Action drop-list only enable when two or more VNF checkboxes selected-2', async function () {
        await pageBulkActions.clickCheckBoxTableVnfs(1);
        await expect(await pageBulkActions.isBulkActionDropListDisable()).toBeTruthy();
        await pageBulkActions.selectAllCheckBoxTableVnf();
        await expect(await pageBulkActions.isBulkActionDropListDisable()).toBeFalsy();
        await pageBulkActions.unselectAllCheckBoxTableVnf();
        await expect(await pageBulkActions.isBulkActionDropListDisable()).toBeTruthy();
    });

    // Verify that the VNF select check box can work properly
    it('Verify that the VNF select check box can work properly-3', async function () {
        await pageBulkActions.selectAllCheckBoxTableVnf();
        const totalCheckBox = await pageBulkActions.getTotalCheckBoxInRowTable();
        const checkBoxSelected1 = await pageBulkActions.getNumberCheckBoxSelectedRowTable();
        await expect(totalCheckBox).toEqual(checkBoxSelected1);
        await pageBulkActions.unselectAllCheckBoxTableVnf();
        const checkBoxSelected2 = await pageBulkActions.getNumberCheckBoxSelectedRowTable();
        await expect(checkBoxSelected2).toBe(0);
    });

    // tslint:disable-next-line:max-line-length
    // Verify that the Bulk Action drop-down list displayed with options: Put into Maintenance, Reenable, Resync, Reassgin, VNFM Preference, Upgrade, Delete
    // tslint:disable-next-line:max-line-length
    it('Verify that the Bulk Action drop-down list displayed with options: Put into Maintenance, Reenable, Resync, Reassgin, VNFM Preference, Upgrade, Delete-4', async function () {
        await pageBulkActions.selectAllCheckBoxTableVnf();
        await pageBulkActions.showActionDropList();
        const listActionDrop = await pageBulkActions.getListActionDrop();
        await expect(listActionDrop).toContain('Put into Maintenance');
        await expect(listActionDrop).toContain('Reenable');
        await expect(listActionDrop).toContain('Resync');
        await expect(listActionDrop).toContain('Reassign');
        await expect(listActionDrop).toContain('Update VNFM Preference');
        await expect(listActionDrop).toContain('Upgrade');
        await expect(listActionDrop).toContain('Delete');
    });

    // Verify that the Bulk Action only apply to current displayed objects when Select All check box was selected
    it('Verify that the Bulk Action only apply to current displayed objects when Select All check box was selected-5', async function () {
        const listTextColumn = await pageBulkActions.getListTextRowSelectedByKeyWordColumn('VNF Name');
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Put into Maintenance');
        const ListVNFsAreSelected = await pageBulkActions.getListVNFsAreSelectedInPopups();
        await pageBulkActions.clickButtonClosePopups();
        await expect(listTextColumn).toEqual(ListVNFsAreSelected);
    });

    // Verify that the Actions Popups was displayed when select Re-enable action
    it('Verify that the Actions Popups was displayed when select Re-enable action-6', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Reenable');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'VNF Reenable')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that the Actions Popups was displayed when select Re-sync action
    it('Verify that the Actions Popups was displayed when select Re-sync action-7', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Resync');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'VNF Resync')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that the Actions Popups was displayed when select VNFM Preference action
    it('Verify that the Actions Popups was displayed when select VNFM Preference action-8', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('VNFM Preference');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopupVNFMPreference();
        await expect(Boolean(txtheader === 'Edit VNFM Preference')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that the Actions Popups was displayed when select Upgrade action
    it('Verify that the Actions Popups was displayed when select Upgrade action-9', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Upgrade');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'VNF Upgrade')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that the Actions Popups was displayed when select Delete action
    it('Verify that the Actions Popups was displayed when select Delete action-10', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Delete');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'VNF Delete')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that use can exit the Actions Popups when click on Cancel button
    it('Verify that use can exit the Actions Popups when click on Cancel button-11', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Maintenance');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
        await expect(await pageBulkActions.isShowActionsPopups()).toBeFalsy();
    });

    // Verify that the Bulk Action Name text box of Actions Popups can be input
    it('Verify that the Bulk Action Name text box of Actions Popups can be input-12', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Maintenance');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        await pageBulkActions.setTextInputBulkactionName('Verify that the Bulk Action Name text box of Actions Popups can be input');
        const inputText = await pageBulkActions.getTextInputBulkactionName();
        await pageBulkActions.clearTextInputBulkactionName();
        await expect(inputText).toEqual('Verify that the Bulk Action Name text box of Actions Popups can be input');
    });

    // Verify that the Comments text box of Actions Popups can be input
    it('Verify that the Comments text box of Actions Popups can be input-13', async function () {
        await pageBulkActions.setTextareaComments('Verify that the Comments text box of Actions Popups can be input');
        const areaText = await pageBulkActions.getTextareaComments();
        await pageBulkActions.clearTextareaComments();
        await pageBulkActions.clickButtonClosePopups();
        await expect(areaText).toEqual('Verify that the Comments text box of Actions Popups can be input');
    });

    // Verify that the Actions Popups was displayed when select Put into Maintenance action
    it('Verify that the Actions Popups was displayed when select Put into Maintenance action-14', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Put into Maintenance');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'VNF Put into Maintenance')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that the Actions Popups was displayed when select Put into  Reassign action
    it('Verify that the Actions Popups was displayed when select Put into  Reassign action-15', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Reassign');
        await browser.waitForAngularEnabled(true);
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'Reassign VNFs')).toBeTruthy();
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that user can select the VNFM Preference in VNFM Preference Actions Popups
    it('Verify that user can select the VNFM Preference in VNFM Preference Actions Popups-16', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('VNFM Preference');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        await pageBulkActions.clickCheckBoxTablePopups(1);
        const numberRowSelected = await pageBulkActions.getNumberRowSelectedTablePopups();
        await expect(numberRowSelected).toEqual(1);
        await pageBulkActions.clickButtonClosePopups();
    });

    // Verify that the Upgrade Actions Popups display all Upgrade version of all selected VNFs
    it('Verify that the Upgrade Actions Popups display all Upgrade version of all selected VNFs-17', async function () {
        await pageBulkActions.showActionDropList();
        await pageBulkActions.selectItemDropActionsByKeyWord('Upgrade');
        await expect(await pageBulkActions.isShowActionsPopups()).toBeTruthy();
        const txtheader = await pageBulkActions.getTextHeaderPopup();
        await expect(Boolean(txtheader === 'VNF Upgrade')).toBeTruthy();

    });

    // TODO: wait left nav 'Actions'
    // Verify that the Action table can update after click on Save button
    // it('Verify that the Action table can update after click on Save button-16', async function () {
    //     await pageNavigation.selectLeftNav(4);
    //     const numberRecord1 = await pageBulkActions.getTotalRecordsTableActions();
    //     await pageNavigation.selectLeftNav(3);
    //     await pageBulkActions.selectAllCheckBoxTableVnf();
    //     await pageBulkActions.showActionDropList();
    //     await pageBulkActions.selectItemDropActionsByKeyWord('Upgrade');
    //     await pageBulkActions.clickSaveButtonPopups();
    //     await pageNavigation.selectLeftNav(4);
    //     const numberRecord2 = await pageBulkActions.getTotalRecordsTableActions();
    //     await expect(numberRecord2).toBeGreaterThan(numberRecord1);
    // });
});
