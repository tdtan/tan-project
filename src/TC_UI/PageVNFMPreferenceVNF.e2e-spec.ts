import { browser} from 'protractor';
import { PageVNFMPreferenceVNF } from '../PageObject/PageVNFMPreferenceVNF.po';
import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
// import { PageLogin } from '../PageObject/PageLogin.po';
// import { PageNavigation } from '../PageObject/PageNavigation.po';
// const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project PageVNFMPreferenceVNF', () => {
    let pageVNFMPreferenceVNF: PageVNFMPreferenceVNF;
    let pageLiveVNFs: PageLiveVNFs;
    // let login: PageLogin;
    // let pageNavigation: PageNavigation;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageVNFMPreferenceVNF = new PageVNFMPreferenceVNF();
        pageLiveVNFs = new PageLiveVNFs();
        // login = new PageLogin();
        // pageNavigation = new PageNavigation();
    });
    // it('login VNFM', async function () {

    //     await browser.restart();
    //     await browser.get(data.INFOLOGIN.URL);
    //     browser.driver.manage().window().maximize();
    //     await browser.waitForAngularEnabled(true);
    //     await login.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
    //     await pageNavigation.selectLeftNav(3);
    //     browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
    //     await browser.waitForAngularEnabled(true);
    // });

    // Verify that the VNFM Preference was displayed correctly when click on View/Edit VNF in drop-down list function
    // tslint:disable-next-line:max-line-length
    it('Verify that the VNFM Preference was displayed correctly when click on View/Edit VNF in drop-down list function-1', async function () {
        // await pageNavigation.selectLeftNav(3);
        // browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        // await browser.waitForAngularEnabled(true);
        await browser.get(browser.baseUrl + '/#/vnfm/livevnf');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        await pageLiveVNFs.setSearch('UI_AUTO');
        await browser.waitForAngularEnabled(true);
        const indexColACtion = await pageVNFMPreferenceVNF.getIndexColumnContainKeyWord('Actions');
        await pageVNFMPreferenceVNF.selectItemFunctionByKeyWord(1, indexColACtion, 'Reassign + Update VNFM Preference');
        await expect(await pageVNFMPreferenceVNF.isVNFMPreferenceDisplay()).toBeTruthy();
    });

    // Verify that the user can Reassign VNF to different VNFM
    it('Verify that the user can Reassign VNF to different VNFM-2', async function () {
        const levelTable = 3;
        const numberCheckBox = await pageVNFMPreferenceVNF.countCheckBoxTableDialog(levelTable);
        await expect(await Boolean(numberCheckBox >= 1)).toBeTruthy();
    });

    // Verify that the user can Edit the VNFM Preference
    it('Verify that the user can Edit the VNFM Preference-3', async function () {
        const levelTable = 7;
        const numberCheckBox = await pageVNFMPreferenceVNF.countCheckBoxTableDialog(levelTable);
        await expect(await Boolean(numberCheckBox >= 1)).toBeTruthy();
    });

    // Verify that the VNFMs tab displayed in left navigation bar
    it('Verify that the VNFMs tab displayed in left navigation bar-4', async function () {
        await expect(await pageVNFMPreferenceVNF.isVNFMsTabDisplayed()).toBeTruthy();
        await pageVNFMPreferenceVNF.clickCancel();
    });
});
