import { browser } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageAddContinuityPlan } from '../PageObject/PageAddContinuityPlan.po';
import { PageAdminVNFMs } from '../PageObject/PageAdminVNFMs.po';
import { PageAdminContinuityPlan } from '../PageObject/PageAdminContinuityPlan.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');
describe('workspace-project Add Continuity Plan Page', () => {
    // let pageAdminContinuityPlan: PageAdminContinuityPlan;
    let pageAddContinuityPlan: PageAddContinuityPlan;
    let pageAdminContinuityPlan: PageAdminContinuityPlan;
    let pageError: PageError;
    let pageAdminVNFMs: PageAdminVNFMs;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
        pageAddContinuityPlan = new PageAddContinuityPlan();
        pageAdminContinuityPlan = new PageAdminContinuityPlan();
        pageError = new PageError();
        pageAdminVNFMs = new PageAdminVNFMs();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // Verify that the empty form was displayed when user click on add plan button
    it('Verify that the empty form was displayed when user click on add plan button- 1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await pageAdminContinuityPlan.ClickAddButton();
        await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeTruthy();
        const txt_Name = await pageAddContinuityPlan.getAttributeOfContinuityPlanName();
        const txt_Description = await pageAddContinuityPlan.getAttributeOfContinuityPlanDescription();
        await expect(txt_Name === '').toBe(true);
        await expect(txt_Description === '').toBe(true);
    });

    // Verify that user can fill in the blank of the empty form
    it('Verify that user can fill in the blank of the empty form- 2', async function () {
        await pageAddContinuityPlan.setContinuityPlanName('continuity 1');
        await pageAddContinuityPlan.setDescriptionContinuityPlan('description');
        const txt_Name = await pageAddContinuityPlan.getAttributeOfContinuityPlanName();
        const txt_Description = await pageAddContinuityPlan.getAttributeOfContinuityPlanDescription();
        await expect(txt_Name === 'continuity 1').toBe(true);
        await expect(txt_Description === 'description').toBe(true);
    });

    // Verify that all VNFMs in table Available VNFMs were list correctly
    it('3. Verify that all VNFMs in table Available VNFMs were list correctly- 3', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
        await browser.waitForAngularEnabled(true);
        const listVNFMName1 = await pageAdminVNFMs.getListVNFMNameInTable();
        await console.log('List VNFM Name In VNFms Page: ', listVNFMName1);

        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await pageAdminContinuityPlan.ClickAddButton();
        await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeTruthy();
        const listVNFMName2 = await pageAddContinuityPlan.getListVNFMNameInAvailableVNFMsTable();
        await console.log('List VNFM Name In Add Continuty plan: ', listVNFMName2);
        await expect(listVNFMName1).toEqual(listVNFMName2);
    });

    // Verify that user can select VNFM from table Available VNFMs to Continuity plan VNFM sequence-4
    it('Verify that user can select VNFM from table Available VNFMs to Continuity plan VNFM sequence- 4', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/continuityPlan');
        await browser.waitForAngularEnabled(true);
        await pageAdminContinuityPlan.ClickAddButton();
        const ltVNFM = await pageAddContinuityPlan.getListVNFMNameInAvailableVNFMsTable();
        await console.log('List VNFM Name In Available VNFMs Table: ', ltVNFM);
        for (let index = 0; index < ltVNFM.length; index++) {
            await pageAddContinuityPlan.chooseVNFMNameOnAvailableVNFMs(ltVNFM[index]);
        }
        const ltVNFM2 = await pageAddContinuityPlan.getListVNFMNameInContinuityPlanVNFMsSequenceTable();
        await console.log('List VNFM Name In Continuity Plan VNFMs Sequence Table: ', ltVNFM2);
        await expect(ltVNFM).toEqual(ltVNFM2);
    });

    // Verify that user can delete VNFM which already chosen in table Continuity plan VNFM sequence-5
    it('Verify that user can delete VNFM which already chosen in table Continuity plan VNFM sequence- 5', async function () {
        const ltVNFM1 = await pageAddContinuityPlan.getListVNFMNameInContinuityPlanVNFMsSequenceTable();
        await console.log('List VNFM Name In Continuity Plan VNFMs Sequence Table: ', ltVNFM1);
        for (let index = 0; index < ltVNFM1.length; index++) {
            await pageAddContinuityPlan.ClickremoveVNFMonContinuityPlanVNFMSequenceTable(ltVNFM1[index]);
        }
        const ltVNFM2 = await pageAddContinuityPlan.getListVNFMNameInAvailableVNFMsTable();
        await console.log('List VNFM Name In Available VNFMs Table: ', ltVNFM2);
        await expect(ltVNFM1).toEqual(ltVNFM2);
    });

    // Verify that user can close the form by click cancle button 6
    it('Verify that user can close the form by click cancle button- 6', async function () {
        await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeTruthy();
        await pageAddContinuityPlan.clickCancelButton();
        await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeFalsy();

    });

    // // Verify that user can orrange VNFM order on Continuity plan VNFM sequence table
    // it('Verify that user can orrange VNFM order on Continuity plan VNFM sequence table- 7', async function () {

    //     await pageAdminContinuityPlan.ClickAddButton();
    //     await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeTruthy();
    //     const ltVNFM = await pageAddContinuityPlan.getListVNFMNameInAvailableVNFMsTable();
    //     await console.log('List VNFM Name In Available VNFMs Table: ', ltVNFM);
    //     for (let index = 0; index < ltVNFM.length; index++) {
    //         await pageAddContinuityPlan.chooseVNFMNameOnAvailableVNFMs(ltVNFM[index]);
    //     }
    //     const ltVNFM2 = await pageAddContinuityPlan.getListVNFMNameInContinuityPlanVNFMsSequenceTable();
    //     await console.log('List VNFM Name In Continuity Plan VNFMs Sequence Table: ', ltVNFM2);
    //     await expect(ltVNFM).toEqual(ltVNFM2);
    //     let VNFMNAME_Row1 = await pageAddContinuityPlan.getTextVNFMName(1);
    //     let VNFMNAME_Row2 = await pageAddContinuityPlan.getTextVNFMName(3);
    //     await console.log('VNFM1: ', VNFMNAME_Row1, 'VNFM 2: ', VNFMNAME_Row2);
    //     await pageAddContinuityPlan.clickReOderButtonInRow(1, 3);
    //     await browser.sleep(6000);
    //     let VNFMName_ChRow1 = await pageAddContinuityPlan.getTextVNFMName(1);
    //     let VNFMName_ChRow2 = await pageAddContinuityPlan.getTextVNFMName(3);
    //     await console.log('VNFM1: ', VNFMName_ChRow1, 'VNFM 2: ', VNFMName_ChRow2);
    //     await pageAddContinuityPlan.clickCancelButton();

    // });

    //  Verify that user can add continuity plan by click save button-8
    it('Verify that user can add continuity plan by click save button- 8', async function () {
        await pageAdminContinuityPlan.ClickAddButton();
        await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeTruthy();
        await pageAddContinuityPlan.setContinuityPlanName(data.CONTINUITYPLAN.TC_8.CONTINUITYPLAN);
        await pageAddContinuityPlan.setDescriptionContinuityPlan(data.CONTINUITYPLAN.TC_8.DESCRIPTION);
        const ltVNFM = await pageAddContinuityPlan.getListVNFMNameInAvailableVNFMsTable();
        await console.log('List VNFM Name In Available VNFMs Table: ', ltVNFM);
        for (let index = 0; index < ltVNFM.length; index++) {
            await pageAddContinuityPlan.chooseVNFMNameOnAvailableVNFMs(ltVNFM[index]);
        }
        await browser.waitForAngularEnabled(true);
        await pageAddContinuityPlan.clickSaveButton();
        await expect(pageAddContinuityPlan.verifyAddContinuityPlanIsPresent()).toBeFalsy();
        await pageAdminContinuityPlan.search_global_txt.sendKeys(data.CONTINUITYPLAN.TC_8.CONTINUITYPLAN);
        let flag = await pageAdminContinuityPlan.CheckGlobalSearch(data.CONTINUITYPLAN.TC_8.CONTINUITYPLAN);
        await expect(flag).toBe(true);
        await console.log(data.CONTINUITYPLAN.TC_8.CONTINUITYPLAN, 'add to table');

    });

});
