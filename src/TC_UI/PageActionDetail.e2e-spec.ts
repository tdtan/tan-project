import { browser } from 'protractor';
import { PageActionDetail } from '../PageObject/PageActionDetail.po';

describe('workspace-project page PageActionDetail', () => {
    let pageActionDetail: PageActionDetail;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageActionDetail = new PageActionDetail();
    });

    // Verify that user can select one bulk action row to open a detail
    it('Verify that user can select one bulk action row to open a detail-1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/actions');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        await pageActionDetail.clickRowTableAction(1);
        await expect(await pageActionDetail.isBulkActionDetailTableDisplay()).toBeTruthy();
    });

    // Verify that the dropdown filter and sort functions work properly with VNF column of bulk action detail table
    it('Verify that the dropdown filter and sort functions work properly with VNF column of bulk action detail table-2', async function () {
        // column version
        const Column = 1;
        await pageActionDetail.verifySortColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with Status column of bulk action detail table
    // tslint:disable-next-line:max-line-length
    it('Verify that the dropdown filter and sort functions work properly with Status column of bulk action detail table-3', async function () {
        // column status
        const Column = 3;
        await pageActionDetail.verifySortColumn(Column);
    });

    // Verify that the sort functions work properly with Progress column of bulk action detail table
    it('Verify that the sort functions work properly with Progress column of bulk action detail table-4', async function () {
        // column status
        const Column = 5;
        await pageActionDetail.verifySortColumn(Column);
    });

    // Verify that the tables display with maximum 5 entries in one page as default
    it('Verify that the tables display with maximum 5 entries in one page as default-5', async function () {
        const currentNumberRow = await pageActionDetail.getCurrentNumberRow();
        await expect(await Boolean(currentNumberRow <= 5)).toBeTruthy();
        await expect(await pageActionDetail.getRowsPerPageOptions()).toEqual(5);
    });

    // Verify that the user can choose the number of entries displayed in one page
    it('Verify that the user can choose the number of entries displayed in one page-6', async function () {
        await pageActionDetail.selectRowsPerPageOptions('15');
        let numberRow = await pageActionDetail.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 15)).toBeTruthy();
        await pageActionDetail.selectRowsPerPageOptions('30');
        numberRow = await pageActionDetail.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 30)).toBeTruthy();
        await pageActionDetail.selectRowsPerPageOptions('5');
        numberRow = await pageActionDetail.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 5)).toBeTruthy();
    });
});
