import { PageRole } from '../PageObject/PageRole.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageLogin } from '../PageObject/PageLogin.po';
import { PageUsers } from '../PageObject/PageUsers.po';
import { PageAddUser } from '../PageObject/PageAddUser.po';
import { PageEditUser } from '../PageObject/PageEditUser.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');
describe('workspace-project page role', () => {
    let pageRole: PageRole;
    let pageLogin: PageLogin;
    let pageUsers: PageUsers;
    let pageAddUser: PageAddUser;
    let pageEditUser: PageEditUser;
    let pageNavigation: PageNavigation;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
        pageRole = new PageRole();
        pageLogin = new PageLogin();
        pageUsers = new PageUsers();
        pageAddUser = new PageAddUser();
        pageEditUser = new PageEditUser();
        pageNavigation = new PageNavigation();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    it('Verify that the dropdown filter and sort functions work properly with Role column-1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        // column version
        const Column = 1;
        await pageRole.verifySortColumn(Column);
        await pageRole.verifyDropdownFilterColumn(Column);
    });

    // Verify that user can select the number of displayed columns
    it('Verify that user can select the number of displayed columns-2', async function () {
        await pageRole.clickShowHideColumnButton();
        let currentNumberColumnDefault = await pageRole.getCurrentNumberColumn();
        let checkBoxChecked = await pageRole.getNumberCheckBoxDialogChecked();
        await expect(currentNumberColumnDefault).toEqual(checkBoxChecked);

        await pageRole.unSelectAllCheckBoxDialog();
        currentNumberColumnDefault = await pageRole.getCurrentNumberColumn();
        await expect(currentNumberColumnDefault).toEqual(0);

        await pageRole.selectAllCheckBoxDialog();
        currentNumberColumnDefault = await pageRole.getCurrentNumberColumn();
        checkBoxChecked = await pageRole.getNumberCheckBoxDialogChecked();
        await pageRole.clickCloseDialogButton();
        await expect(currentNumberColumnDefault).toEqual(checkBoxChecked);
    });

    // Verify that the Search function work properly
    it('Verify that the Search function work properly-3', async function () {
        const columnToGetText = 1;
        const numberRowTable = await pageRole.getCurrentNumberRow();
        const getTextForSearch = await pageRole.getTextCellOfTable(columnToGetText, 1);
        await pageRole.setTextOnSearchBox('Verify that the Search function work properly');
        await expect(await pageRole.getCurrentNumberRow()).toBe(0);
        await pageRole.setTextOnSearchBox(getTextForSearch);
        const isDropdownFilterNotAll = await pageRole.verifyFilterSearchBox(getTextForSearch, columnToGetText);
        await expect(isDropdownFilterNotAll).toBeTruthy();
        await pageRole.clearTextOnSearchBox();
        const isDropdownFilterAll = await pageRole.verifyDropdownFilterAll(numberRowTable);
        await expect(isDropdownFilterAll).toBeTruthy();
    });

    // Verify that the Clear table button work properly
    it('Verify that the Clear table button work properly- 4', async function () {
        const numberRowTable = await pageRole.getCurrentNumberRow();
        await pageRole.setTextOnSearchBox('Verify that the Clear table button work properly');
        await expect(await pageRole.verifyDropdownFilterAll(0)).toBeTruthy();
        await pageRole.clickClearButton();
        await expect(await pageRole.verifyDropdownFilterAll(numberRowTable));
    });

    // Verify that the Refresh button work properly
    it('Verify that the Refresh button work properly- 5', async function () {
        const numberRowTable = await pageRole.getCurrentNumberRow();
        await pageRole.setTextOnSearchBox('Verify that the Refresh button work properly');
        await expect(await pageRole.verifyDropdownFilterAll(0)).toBeTruthy();
        await pageRole.clickRefreshButton();
        await browser.waitForAngular();
        await expect(await pageRole.verifyDropdownFilterAll(numberRowTable));
    });

    // Verify that the tables display with maximum 5 entries in one page as default
    it('Verify that the tables display with maximum 5 entries in one page as default-7', async function () {
        const currentNumberRow = await pageRole.getCurrentNumberRow();
        await expect(await Boolean(currentNumberRow <= 5)).toBeTruthy();
        await expect(await pageRole.getRowsPerPageOptions()).toEqual(5);
    });

    // Verify that the user can choose the number of entries displayed in one page
    it('Verify that the user can choose the number of entries displayed in one page-8', async function () {
        await pageRole.selectRowsPerPageOptions('15');
        let numberRow = await pageRole.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 15)).toBeTruthy();
        await pageRole.selectRowsPerPageOptions('30');
        numberRow = await pageRole.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 30)).toBeTruthy();
        await pageRole.selectRowsPerPageOptions('5');
        numberRow = await pageRole.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 5)).toBeTruthy();
    });

    // Verify that the user can move to specified page when click on the page number
    it('Verify that the user can move to specified page when click on the page number-9', async function () {
        const listNumberPage = await pageRole.getListNumberPageInTable();
        const numberPageActive1 = await pageRole.getNumberPageActiveInTable();
        if (listNumberPage.length <= 1) {
            await expect(numberPageActive1).toEqual(listNumberPage[0]);
        } else {
            await pageRole.moveNumberPageInTable(listNumberPage[1]);
            const numberPageActive2 = await pageRole.getNumberPageActiveInTable();
            await expect(numberPageActive2).toEqual(listNumberPage[1]);

            await pageRole.moveNumberPageInTable(listNumberPage[0]);
            const numberPageActive3 = await pageRole.getNumberPageActiveInTable();
            await expect(numberPageActive3).toEqual(listNumberPage[0]);
        }
    });

    // Verify that the Role nav tab only visible with sysadmin user
    it('Verify that the Role nav tab only visible with sysadmin user-10', async function () {
        await pageNavigation.selectLeftNav(5);
        await pageNavigation.selectLeftNavSecond(5, 1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/users'), 3000);
        await pageUsers.setSearch(data.ADMIN_ROLE.TC_5.USERNAME);
        const result = await pageUsers.isTableHasValueAfterSearch();
        if (result) {
            await pageEditUser.clickDelete();
            await pageEditUser.clickConfirmDelete();
        }
        await pageAddUser.clickAddUser();
        // tslint:disable-next-line:max-line-length
        await pageAddUser.funcAddUser(data.ADMIN_ROLE.TC_5.USERNAME, data.ADMIN_ROLE.TC_5.PASSWORD);

        await pageRole.logOut();
        await pageLogin.funcLogin(data.ADMIN_ROLE.TC_5.USERNAME, data.ADMIN_ROLE.TC_5.PASSWORD);
        await pageNavigation.verifyUserProfile(data.ADMIN_ROLE.TC_5.USERNAME);
        await expect(await pageRole.verifyRoleNavTabVisible()).toBeFalsy();

        await pageRole.logOut();
        await pageLogin.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
        await pageNavigation.verifyUserProfile(data.INFOLOGIN.USERNAME);
        await expect(await pageRole.verifyRoleNavTabVisible()).toBeTruthy();
    });

});
