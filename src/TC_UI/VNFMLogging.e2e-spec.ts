// import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageAdminVNFMs } from '../PageObject/PageAdminVNFMs.po';
import { PageVNFMLogging } from '../PageObject/PageVNFMLogging.po';
import { browser } from 'protractor';
// import { PageError } from '../PageObject/PageError.po';

const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Access Logs', () => {
  let pageAdminVNFMs: PageAdminVNFMs;
  let pageVNFMLogging: PageVNFMLogging;
  // let pageNavigation: PageNavigation;
  // let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
    pageAdminVNFMs = new PageAdminVNFMs();
    pageVNFMLogging = new PageVNFMLogging();
    // pageNavigation = new PageNavigation();
    // pageError = new PageError();
  });

  // beforeEach(async () => {
  //   const result1 = await pageError.verifyMessegeError();
  //   const result2 = await pageError.verifyMessegeWarning();
  //   const result3 = await pageError.verifyMessegeInfo();
  //   if ( result1 === true || result2 === true || result3 === true) {
  //     await pageError.cliclClearALL();
  //   }
  // });

  // 1 - Verify that the empty form was displayed when user click on Access Logs button
  it('Verify that the empty form was displayed when user click on Access Logs button - 1 ', async function () {
    await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
    await browser.waitForAngularEnabled(true);
    const txt_Url = await browser.getCurrentUrl();
    console.log('Current URL: ' + txt_Url);
    const VNFM_Name = await pageAdminVNFMs.FunctionCompareCurentUrlWithIPv4InEditVNFMPage(txt_Url);
    await console.log('VNFM Name: ', VNFM_Name);

    await pageAdminVNFMs.settextSearch(VNFM_Name);
    const afterSearch = await pageAdminVNFMs.isTableHasValueAfterSearch();
    if (afterSearch) {
      // tslint:disable-next-line:max-line-length
      // await pageAdminVNFMs.clickActionOnRowHasValueofColumnNameEqualsTextValue(data.LOGGING.TC_1.STRTABLE, data.LOGGING.TC_1.COLUMNNAME, VNFM_Name, data.LOGGING.TC_1.FUNCTIONCOLUMNNAME, data.LOGGING.TC_1.FUNCTION);
      await pageAdminVNFMs.clickbtnSelectActions();
      await pageAdminVNFMs.clickbtnOptionActions(data.LOGGING.TC_1.FUNCTION);
    }
    const texTAccess = await pageVNFMLogging.getTextToverifyAfterClickAccessLogButton();
    await expect(Boolean(texTAccess === data.LOGGING.TC_1.FUNCTION)).toBe(true);
  });

  // 2 - Verify that the Download button was hidden as default when not have Logs already collected
  it('Verify that the Download button was hidden as default when not have Logs already collected - 2', async function () {
    await expect(pageVNFMLogging.verifyDownloadButtonIsEnabled()).toBeFalsy();
  });

  // 3 - Verify that the pop-up dialog was auto closed after click on Collect logs button
  it('Verify that the pop-up dialog was auto closed after click on Collect logs button - 3 ', async function () {
    // await browser.waitForAngularEnabled(true);
    await pageVNFMLogging.clickCollectLogsButton();
    await pageVNFMLogging.verifyFormAccessLogsNotDisplayed();
  });

  // // 4- Verify that an alarm was generated after performed collect logs
  // it('Verify that an alarm was generated after performed collect logs - 4', async function () {
  //   // await browser.waitForAngularEnabled(true);
  //         const txtMessage = await pageVNFMLogging.getTextInAlarmWhenClickCollectLog();
  //         await console.log('text: ', txtMessage);
  // // });

  // 5 - Verify that the Collect logs button was hidden as default when Logs are being collected.
  it('Verify that the Collect logs button was hidden as default when Logs are being collected. - 5', async function () {
    // await browser.sleep(10000);
    await browser.waitForAngularEnabled(true);
    const txt_Url = await browser.getCurrentUrl();
    console.log('Current URL: ' + txt_Url);
    const VNFM_Name = await pageAdminVNFMs.FunctionCompareCurentUrlWithIPv4InEditVNFMPage(txt_Url);
    await console.log('VNFM Name: ', VNFM_Name);

    await pageAdminVNFMs.settextSearch(VNFM_Name);
    const afterSearch = await pageAdminVNFMs.isTableHasValueAfterSearch();
    if (afterSearch) {
      // tslint:disable-next-line:max-line-length
      // await pageAdminVNFMs.clickActionOnRowHasValueofColumnNameEqualsTextValue(data.LOGGING.TC_1.STRTABLE, data.LOGGING.TC_1.COLUMNNAME, VNFM_Name, data.LOGGING.TC_1.FUNCTIONCOLUMNNAME, data.LOGGING.TC_1.FUNCTION);
      await pageAdminVNFMs.clickbtnSelectActions();
      await pageAdminVNFMs.clickbtnOptionActions(data.LOGGING.TC_1.FUNCTION);
    }
    await expect(pageVNFMLogging.verifyClollectLogsButtonIsEnabled()).toBeFalsy();
    await pageVNFMLogging.clickClose();
  });

  // // 6 - Verify that an alarm was generated after logs collect successfully
  // it('Verify that an alarm was generated after logs collect successfully - 6', async function () {
  //   await browser.waitForAngularEnabled(true);
  //   await browser.sleep(45000);
  //   await pageVNFMLogging.verifyMessageCollectLogsSuccessfully();
  // });

  // // 7 - Verify that the pop-up dialog was auto closed after click on Download button
  // it('Verify that the pop-up dialog was auto closed after click on Download button - 7', async function () {
  //   await browser.waitForAngularEnabled(true);
  //   await pageAdminVNFMs.clickAccessLogButton();
  //   await pageVNFMLogging.verifyAfterClickAccessLogButton();
  //   await expect(pageVNFMLogging.verifyDownloadButtonIsEnabled()).toBeTruthy();
  //   await pageVNFMLogging.clickDownloadButton();
  //   await browser.sleep(30000);
  //   await pageVNFMLogging.verifyFormAccessLogsNotDisplayed();
  // });


});



