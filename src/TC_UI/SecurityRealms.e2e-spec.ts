import { PageNavigation } from '../PageObject/PageNavigation.po';
import { browser, ExpectedConditions} from 'protractor';
import { PageSecurityRealms } from '../PageObject/PageSecurityRealms.po';
import { PageAddSecurityRealms } from '../PageObject/PageAddSecurityRealms.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Security Realms', () => {
  let pageNavigation: PageNavigation;
  let pageSecurityRealms: PageSecurityRealms;
  let pageAddSecurityRealms: PageAddSecurityRealms;
  let pageError: PageError;
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
    pageNavigation = new PageNavigation();
    pageSecurityRealms = new PageSecurityRealms();
    pageAddSecurityRealms = new PageAddSecurityRealms();
    pageError = new PageError();
  });

  beforeEach(async () => {
    const result1 = await pageError.verifyMessegeError();
    const result2 = await pageError.verifyMessegeWarning();
    const result3 = await pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await pageError.cliclClearALL();
    }
  });

  // 01 - Verify that the Security Realms tab displayed in left navigation bar
  it('Verify that the Security Realms tab displayed in left navigation bar - 0', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const txtHeader = await pageSecurityRealms.getTextHeader();
    await expect(Boolean(txtHeader === data.SECURITYREALMS.TC_1.TEXT)).toBe(true);
  });

  // 1 - Verify that the Empty form is displayed when user click on Add Realm button
  it('Verify that the Empty form is displayed when user click on Add Realm button- 1', async function () {
    await pageSecurityRealms.clickbtnAddRealm();
    const txtAfterAdd = await pageAddSecurityRealms.getTextAddRealm();
    await expect(Boolean(txtAfterAdd === data.SECURITYREALMS.TC_2.TEXTADDFORM)).toBe(true);
    const valueName = await pageAddSecurityRealms.getValueFormRealnmName();
    await expect(Boolean(valueName === '')).toBe(true);
    await browser.waitForAngularEnabled(true);
    const valueType = await pageAddSecurityRealms.getValueFormRealmType();
    await console.log('get text: ' + valueType);
    await browser.waitForAngularEnabled(true);
    await expect(Boolean(valueType === 'Select Realm Type')).toBe(true);
    await pageAddSecurityRealms.clickbtnCancel();
  });

  // 2-Verify that the "missing required info" message was displayed when user forgot to input the Security Realm Name and Realm type
  // tslint:disable-next-line:max-line-length
  it('Verify that the "missing required info" message was displayed when user forgot to input the Security Realm Name and Realm type- 2', async function () {
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.clickbtnAddRealm();
    await pageAddSecurityRealms.clickbtnSave();
    await expect(pageAddSecurityRealms.verifyMisingRequestInfo()).toBeTruthy();
    await expect(pageAddSecurityRealms.verifyMisingRequestInfotype()).toBeTruthy();
    await pageAddSecurityRealms.clickbtnCancel();
  });

  // 3-Verify that the Realm Properties section is displayed when user complete the Security Realm Name and Realm type selection
  // tslint:disable-next-line:max-line-length
  it('Verify that the Realm Properties section is displayed when user complete the Security Realm Name and Realm type selection- 3', async function () {
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.clickbtnAddRealm();
    await pageAddSecurityRealms.clickbtnSave();
    await expect(pageAddSecurityRealms.verifyRealmPropertiesDisplay()).toBeFalsy();
    await pageAddSecurityRealms.clickbtnCancel();

    await pageSecurityRealms.clickbtnAddRealm();
    await pageAddSecurityRealms.setInputNameRealm('RealmName');
    await pageAddSecurityRealms.selectRealmType('LDAP');
    await browser.waitForAngularEnabled(true);
    await expect(pageAddSecurityRealms.verifyRealmPropertiesDisplay()).toBeTruthy();
    await pageAddSecurityRealms.clickbtnCancel();
  });

  // 4- Verify that the user can exit the add Realm form when click on Cancel button
  it('Verify that the user can exit the add Realm form when click on Cancel button- 4', async function () {
    await pageSecurityRealms.clickbtnAddRealm();
    await expect(pageAddSecurityRealms.verifytxtAddDisplay()).toBeTruthy();
    await pageAddSecurityRealms.clickbtnCancel();
    await expect(pageAddSecurityRealms.verifytxtAddDisplay()).toBeFalsy();
  });

  // 5- Verify that table can Show or Hide filter form when click button Hide Filter
  it('Verify that table can Show or Hide filter form when click button Hide Filter- 5', async function () {
    await pageSecurityRealms.clickShowHideFilter();
    await expect(pageSecurityRealms.verifyDropdownFilterIsPresentInPage()).toBeTruthy();
    await pageSecurityRealms.clickShowHideFilter();
    await expect(pageSecurityRealms.verifyDropdownFilterIsPresentInPage()).toBeFalsy();
  });

  // 6- Verify that the Realm table can update with the new added Realm after click on Save button
  it('Verify that the Realm table can update with the new added Realm after click on Save button- 6', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_6.REALMNAME);
    const record1 = await pageSecurityRealms.isTableHasValueAfterSearch();
    if ( record1 === true) {
      await pageSecurityRealms.clicklinkInTableRealm();
      await pageSecurityRealms.clickbtnSelectActions();
      await pageSecurityRealms.clickbtnOptionActions(data.SECURITYREALMS.TC_25.FUNCTION);
      await pageSecurityRealms.clickbtnConfirmYes();
    }
    await pageSecurityRealms.clickbtnAddRealm();
    await pageAddSecurityRealms.setInputNameRealm(data.SECURITYREALMS.TC_6.REALMNAME);
    await pageAddSecurityRealms.selectRealmType(data.SECURITYREALMS.TC_6.REALMTYPE);
    await browser.waitForAngularEnabled(true);
    await pageAddSecurityRealms.clearURL();
    await pageAddSecurityRealms.setInputURL(data.SECURITYREALMS.TC_6.URL);
    await pageAddSecurityRealms.clearUserName();
    await pageAddSecurityRealms.setInputUserName(data.SECURITYREALMS.TC_6.USERNAME);
    await pageAddSecurityRealms.setInputTemlate(data.SECURITYREALMS.TC_6.TEMPATE);
    await pageAddSecurityRealms.setInputSearchBase(data.SECURITYREALMS.TC_6.SEARCHBASE);
    await pageAddSecurityRealms.setInputSearchFile(data.SECURITYREALMS.TC_6.SEARCHFILE);
    await pageAddSecurityRealms.clickbtnSave();
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_6.REALMNAME);
    await expect(pageSecurityRealms.verifyRowTableDisplay()).toBeTruthy();
    const record2 = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record2 === data.SECURITYREALMS.TC_6.RECORD)).toBe(true);
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 7- Verify that the value of current Realm were displayed when user click on existing Realm row
  it('Verify that the value of current Realm were displayed when user click on existing Realm row- 7', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_7.REALMNAME);
    const record2 = await pageSecurityRealms.isTableHasValueAfterSearch();
    await expect(Boolean(record2)).toBe(true);

    await pageSecurityRealms.clicklinkInTableRealm();
    await browser.waitForAngularEnabled(true);
    const txtEditRealm = await pageAddSecurityRealms.getTextAddRealm();
    await expect(Boolean(txtEditRealm === data.SECURITYREALMS.TC_7.TEXTEDIT)).toBe(true);
    const RealmType = await pageAddSecurityRealms.getValueFormRealmType();
    const RealmName = await pageAddSecurityRealms.getValueFormRealnmName();
    await expect(Boolean(RealmType === data.SECURITYREALMS.TC_7.REALMTYPE)).toBe(true);
    await expect(Boolean(RealmName === data.SECURITYREALMS.TC_7.REALMNAME)).toBe(true);
    await pageAddSecurityRealms.clickbtnCancel();
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 8- Verify that the Realm info and properties are editable
  it('Verify that the Realm info and properties are editable- 8', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_8.REALMNAME);
    const record2 = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record2 === data.SECURITYREALMS.TC_8.RECORD)).toBe(true);

    await pageSecurityRealms.clicklinkInTableRealm();
    await browser.waitForAngularEnabled(true);
    const txtEditRealm = await pageAddSecurityRealms.getTextAddRealm();
    await expect(Boolean(txtEditRealm === data.SECURITYREALMS.TC_8.TEXTEDIT)).toBe(true);
    await pageAddSecurityRealms.clickbtnCancel();
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 9- Verify that the sort function work properly with Sequence column
  it('Verify that the sort function work properly with Sequence column- 9', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    await pageSecurityRealms.verifySortColumn(2);
  });

  // 10- Verify that the dropdown filter and sort functions work properly with Realm column
  it('Verify that the dropdown filter and sort functions work properly with Realm column- 10', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 5000);
    await browser.waitForAngularEnabled(true);

    await pageSecurityRealms.clickShowHideFilter();
    await pageSecurityRealms.SelectFilter(3, data.SECURITYREALMS.TC_10.REALMNAME);
    const record = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record === data.SECURITYREALMS.TC_10.RECORD)).toBe(true);
    await pageSecurityRealms.SelectFilter(3, 'All');
    await pageSecurityRealms.verifySortColumn(3);
  });

  // 11- Verify that the dropdown filter and sort functions work properly with Type column
  it('Verify that the dropdown filter and sort functions work properly with Type column- 11', async function () {
    await pageSecurityRealms.SelectFilter(4, data.SECURITYREALMS.TC_11.REALMTYPE);
    const record = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record === data.SECURITYREALMS.TC_11.RECORD)).toBe(true);
    await pageSecurityRealms.SelectFilter(4, 'All');
    await pageSecurityRealms.verifySortColumn(4);
  });

  // 12- Verify that the dropdown filter and sort functions work properly with Status column
  it('Verify that the dropdown filter and sort functions work properly with Status column- 12', async function () {

    await pageSecurityRealms.SelectFilter(5, data.SECURITYREALMS.TC_12.STATUS);
    const record = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record === data.SECURITYREALMS.TC_12.RECORD)).toBe(true);
    await pageSecurityRealms.verifySortColumn(5);
    await browser.sleep(1000);
  });

  // 13 - Verify that the Search function work properly
  it('Verify that the Search function work properly- 13', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch('Text Wrong');
    const record = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record === data.SECURITYREALMS.TC_13.NONRECORD)).toBe(true);

    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_13.TEXT);
    const record2 = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record2 === data.SECURITYREALMS.TC_13.RECORD)).toBe(true);
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 14 - Verify that the Clear Search button work properly
  it(' Verify that the Clear Search button work properly - 14', async function () {
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    const record = await pageSecurityRealms.GetRecordCurrent();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_14.REALMNAME);
    await pageSecurityRealms.verifyRecord(data.SECURITYREALMS.TC_14.RECORD);
    await pageSecurityRealms.clickClearSearchButton();
    const record1 = await pageSecurityRealms.GetRecordCurrent();
    await expect(Boolean(record === record1)).toBe(true);
  });

  // // 15 - Verify that the Refresh button work properly
  // it('Verify that the Refresh button work properly- 15', async function () {
  //   await pageNavigation.selectLeftNav(5);
  //   await pageNavigation.selectLeftNavSecond(5, 3);
  //   await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
  //   await browser.waitForAngularEnabled(true);
  //   const result1 = await pageError.verifyMessegeError();
  //   if ( result1 === true ) {
  //     await pageError.cliclClearALL();
  //   }
  //   const record1 = await pageSecurityRealms.GetRecordCurrent();
  //   await pageSecurityRealms.clickShowHideFilter();
  //   await pageSecurityRealms.SelectFilter(3, data.SECURITYREALMS.TC_10.REALMNAME);
  //   await pageSecurityRealms.ClearTxbSearch();
  //   await pageSecurityRealms.settextSearch('Invalid value');
  //   await pageSecurityRealms.clickRefesh();
  //   const value = await pageSecurityRealms.getValueSearch();
  //   await expect(Boolean(value === '')).toBe(true);
  //   const record2 = await pageSecurityRealms.GetRecordCurrent();
  //   await expect(Boolean(record2 === record1)).toBe(true);
  // });

  // 16 - Verify that the Clear Filter button work properly
  it(' Verify that the Clear Filter button work properly - 16', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
     await pageError.cliclClearALL();
    }
    if (!await pageSecurityRealms.verifyDropdownFilterIsPresentInPage()) {
      await pageSecurityRealms.clickShowHideFilter();
    }
    await pageSecurityRealms.SelectFilter(4, data.SECURITYREALMS.TC_11.REALMTYPE);
    await pageSecurityRealms.verifyRecord(data.ADMIN_VNFMs.TC_16.RECORD);
    await pageSecurityRealms.clickClearFilterButton();
    const result = await pageSecurityRealms.GetTextInFilter(4);
    await expect(Boolean(result === 'All')).toBe(true);
  });

  // 17- Verify that the tables display with maximum 5 entries in one page as default
  it('Verify that the tables display with maximum 5 entries in one page as default- 17', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    const numbRow = await pageSecurityRealms.GetNumberRowInPage();
    const totalRow = await pageSecurityRealms.CountNumberRowInpage();
    await expect(Boolean(numbRow === '5')).toBe(true);
    await expect(Boolean(totalRow <= 5)).toBe(true);
  });

  // 18- Verify that the user can choose the number of entries displayed in one page
  it('Verify that the user can choose the number of entries displayed in one page- 18', async function () {
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.SelectNumberRowDisplay(15);
    const result2 = await pageSecurityRealms.CountNumberRowInpage();
    await expect(Boolean(result2 <= 15)).toBe(true);
    await pageSecurityRealms.SelectNumberRowDisplay(5);
    const result3 = await pageSecurityRealms.CountNumberRowInpage();
    await expect(Boolean(result3 <= 5)).toBe(true);
  });

  // 19- Verify that the user can move to specified page when click on the page number
  it('Verify that the user can move to specified page when click on the page number- 19', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    const record = await pageSecurityRealms.GetRecordCurrent();
    const numRecord = Number(record.slice(0, 1));
    const numbRow = Number(await pageSecurityRealms.GetNumberRowInPage());
    if (numRecord > numbRow) {
      await pageSecurityRealms.clickNumbertoMovePage();
      const numPage = await pageSecurityRealms.getCurentPageActive();
      await expect(Boolean(numPage === '2')).toBe(true);
    }
  });

  // 20- Verify that user can select the number of displayed columns
  it('Verify that user can select the number of displayed columns- 20', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await expect(pageSecurityRealms.verifyColumnDisplayInTable('Reorder')).toBeTruthy();
    await expect(pageSecurityRealms.verifyColumnDisplayInTable('Sequence')).toBeTruthy();
    await expect(pageSecurityRealms.verifyColumnDisplayInTable('Realm')).toBeTruthy();
    await expect(pageSecurityRealms.verifyColumnDisplayInTable('Type')).toBeTruthy();
    await expect(pageSecurityRealms.verifyColumnDisplayInTable('Status')).toBeTruthy();
    await expect(pageSecurityRealms.verifyColumnDisplayInTable('Actions')).toBeTruthy();

    await pageSecurityRealms.selectOptionNamefromShowHideColumnsList('Type');
    await browser.waitForAngularEnabled(true);
    // await expect(await pageSecurityRealms.verifyColumnDisplayInTable('Type')).toBeFalsy();
    await pageSecurityRealms.selectOptionNamefromShowHideColumnsList('Type');
    await browser.waitForAngularEnabled(true);
    await expect(await pageSecurityRealms.verifyColumnDisplayInTable('Type')).toBeTruthy();
  });

  // 21 - Verify that the drop-down list of Function column contain the Enable and Delete option when the Realm in status Disable
  // tslint:disable-next-line:max-line-length
  it('Verify that the drop-down list of Function column contain the Enable and Delete option when the Realm in status Disable- 21', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_21.TEXTSEARCH);
    const strStatus = await pageSecurityRealms.GetTextInTd(5);
    if (strStatus === data.SECURITYREALMS.TC_21.STATUS) {
      await pageSecurityRealms.clickbtnSelectActions();
      await expect(await pageSecurityRealms.verifyEventDisplayInForm('Enable')).toBeTruthy();
      await expect(await pageSecurityRealms.verifyEventDisplayInForm('Delete')).toBeTruthy();
    }
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 22 -Verify that the confirm pop-up was display when perform Enable action
  it('Verify that the confirm pop-up was display when perform Enable action- 22', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_22.TEXTSEARCH);
    const result = await pageSecurityRealms.isTableHasValueAfterSearch();
    if ( result) {
      const strStatus = await pageSecurityRealms.GetTextInTd(5);
      if (strStatus === data.SECURITYREALMS.TC_22.STATUS) {
        await pageSecurityRealms.clickbtnSelectActions();
        await pageSecurityRealms.clickbtnOptionActions(data.SECURITYREALMS.TC_22.FUNCTION);
        await expect(await pageSecurityRealms.verifyPopupConfirmDisplay()).toBeTruthy();
        await pageSecurityRealms.clickbtnConfirmYes();
      }
    }
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 23 - Verify that the drop-down list of Function column contain the Disable and Delete option when the Realm in status Enable
  // tslint:disable-next-line:max-line-length
  it('Verify that the drop-down list of Function column contain the Disable and Delete option when the Realm in status Enable- 23', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_23.TEXTSEARCH);
    const result = await pageSecurityRealms.isTableHasValueAfterSearch();
    if ( result) {
      const strStatus = await pageSecurityRealms.GetTextInTd(5);
      if (strStatus === data.SECURITYREALMS.TC_23.STATUS) {
        await pageSecurityRealms.clickbtnSelectActions();
        await expect(await pageSecurityRealms.verifyEventDisplayInForm('Disable')).toBeTruthy();
      }
    }
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 24 - Verify that the confirm pop-up was display when perform Disable action
  it('Verify that the confirm pop-up was display when perform Disable action- 24', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_24.TEXTSEARCH);
    const result = await pageSecurityRealms.isTableHasValueAfterSearch();
    if ( result) {
      const strStatus = await pageSecurityRealms.GetTextInTd(5);
      if (strStatus === data.SECURITYREALMS.TC_24.STATUS) {
        await pageSecurityRealms.clickbtnSelectActions();
        await pageSecurityRealms.clickbtnOptionActions(data.SECURITYREALMS.TC_24.FUNCTION);
        await expect(pageSecurityRealms.verifyPopupConfirmDisplay()).toBeTruthy();
        await pageSecurityRealms.clickbtnConfirmNo();
      }
    }
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 25 - Verify that the confirm pop-up was display when perform Delete action
  it('Verify that the confirm pop-up was display when perform Delete action- 25', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_25.TEXTSEARCH);
    const result = await pageSecurityRealms.isTableHasValueAfterSearch();
    if ( result) {
        // tslint:disable-next-line:max-line-length
        // await pageSecurityRealms.clickActionOnRowHasValueofColumnNameEqualsTextValue(data.SECURITYREALMS.TC_25.STRTABLE, data.SECURITYREALMS.TC_25.COLUMNNAME, data.SECURITYREALMS.TC_25.TEXTVALUE, data.SECURITYREALMS.TC_25.FUNCTIONCOLUMNNAME , data.SECURITYREALMS.TC_25.FUNCTION);
        await pageSecurityRealms.clickbtnSelectActions();
        await pageSecurityRealms.clickbtnOptionActions(data.SECURITYREALMS.TC_25.FUNCTION);
        await browser.waitForAngularEnabled(true);
        await expect(pageSecurityRealms.verifyPopupConfirmDisplay()).toBeTruthy();
        await pageSecurityRealms.clickbtnConfirmNo();
    }
    await pageSecurityRealms.clickClearSearchButton();
  });

  // 26 - Verify that the table was update after perform Delete action
  it('Verify that the table was update after perform Delete action- 26', async function () {
    await pageNavigation.selectLeftNav(5);
    await pageNavigation.selectLeftNavSecond(5, 3);
    await browser.wait(ExpectedConditions.urlContains('vnfm/securityRealms'), 4000);
    await browser.waitForAngularEnabled(true);
    const result1 = await pageError.verifyMessegeError();
    if ( result1 === true ) {
      await pageError.cliclClearALL();
    }
    await pageSecurityRealms.ClearTxbSearch();
    await pageSecurityRealms.settextSearch(data.SECURITYREALMS.TC_25.TEXTSEARCH);
    const result = await pageSecurityRealms.isTableHasValueAfterSearch();
    if ( result) {
        // tslint:disable-next-line:max-line-length
        // await pageSecurityRealms.clickActionOnRowHasValueofColumnNameEqualsTextValue(data.SECURITYREALMS.TC_25.STRTABLE, data.SECURITYREALMS.TC_25.COLUMNNAME, data.SECURITYREALMS.TC_25.TEXTVALUE, data.SECURITYREALMS.TC_25.FUNCTIONCOLUMNNAME, data.SECURITYREALMS.TC_25.FUNCTION);
        await pageSecurityRealms.clickbtnSelectActions();
        await pageSecurityRealms.clickbtnOptionActions(data.SECURITYREALMS.TC_25.FUNCTION);
        await pageSecurityRealms.clickbtnConfirmYes();
    }
    await browser.waitForAngularEnabled(true);
    const result2 = await pageSecurityRealms.isTableHasValueAfterSearch();
    await expect(Boolean(result2 === false)).toBe(true);
    await pageSecurityRealms.clickClearSearchButton();
  });

});
