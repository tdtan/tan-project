import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageLogin } from '../PageObject/PageLogin.po';
import { browser, ExpectedConditions } from 'protractor';
import { PageLiveVNFCs } from '../PageObject/PageLiveVNFCs.po';
import { PageError } from '../PageObject/PageError.po';
import { PageLiveVNFs } from '../PageObject/PageLiveVNFs.po';
import { PageHeal } from '../PageObject/PageHeal.po';
import { PageMigrate } from '../PageObject/PageMigrate.po';
// import { PageOnboardedVNFs } from '../PageObject/PageOnboardedVNFs.po';
// import { PageFailedtoOnboard } from '../PageObject/PageFailedtoOnboard.po';
// import { PageUpgradeVNFC } from '../PageObject/PageUpgradeVNFC.po';
// import { PageRollback } from '../PageObject/PageRollback.po';
import { PageTerminate } from '../PageObject/PageTerminate.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Live VNFC', () => {
    let login: PageLogin;
    let pageNavigation: PageNavigation;
    let pageLiveVNFCs: PageLiveVNFCs;
    let pageLiveVNFs: PageLiveVNFs;
    let pageHeal: PageHeal;
    let pageMigrate: PageMigrate;
    // let pageOnboardedVNFs: PageOnboardedVNFs;
    // let pageFailedtoOnboard: PageFailedtoOnboard;
    // let pageUpgradeVNFC: PageUpgradeVNFC;
    // let pageRollback: PageRollback;
    let pageTerminate: PageTerminate;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 180000;
        login = new PageLogin();
        pageNavigation = new PageNavigation();
        pageLiveVNFCs = new PageLiveVNFCs();
        pageLiveVNFs = new PageLiveVNFs();
        pageHeal = new PageHeal();
        pageMigrate = new PageMigrate();
        // pageOnboardedVNFs = new PageOnboardedVNFs();
        // pageFailedtoOnboard = new PageFailedtoOnboard();
        // pageUpgradeVNFC = new PageUpgradeVNFC();
        // pageRollback = new PageRollback();
        pageTerminate = new PageTerminate();
        pageError = new PageError();
    });
    beforeEach( async() => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
    });

    it('login VNFM', async function () {

        await browser.restart();
        await browser.get(data.INFOLOGIN.URL);
        browser.driver.manage().window().maximize();
        await browser.waitForAngularEnabled(true);
        await login.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
      });

    // 1 - Verify that search function for VNFC works properly
    it('Verify that search function for VNFC works properly - 1', async function () {
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch('wrong text');
        await pageLiveVNFCs.verifyRecord('No records found');
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_1.TEXT_SEARCH);
        await pageLiveVNFCs.verifyRecord(data.LIVEVNFCs.TC_1.RECORD);
        await pageLiveVNFCs.clickClearSearch();
    });

    // 2 - Verify that clear search function for VNFC work properly
    it('Verify that clear search function for VNFC work properly - 2', async function () {
        await pageLiveVNFCs.setSearch('wrong text');
        await pageLiveVNFCs.clickClearSearch();
        const value =  await pageLiveVNFCs.getAttributeInSearch();
        await expect(Boolean(value === '')).toBe(true);
    });

    // 3 - Verify that user can sort or filter the VNFC Name work properly
    it('Verify that user can sort or filter the VNFC Name work properly - 3', async function () {
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_1.TEXT_SEARCH);
        await pageLiveVNFCs.verifysort(2);
        const record = await pageLiveVNFCs.getRecord();
        await pageLiveVNFCs.clickShowHideFilter();
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.clickLableFilter(2);
        await pageLiveVNFCs.SelectOptionFilter(data.LIVEVNFCs.TC_3.VNFCNAME);
        await pageLiveVNFCs.clickCloseFilter();
        await pageLiveVNFCs.clickClearFilterButton();
        const recordFilter = await pageLiveVNFCs.getRecord();
        await expect(Boolean(recordFilter === record)).toBe(true);
    });

    // 4 - Verify that user can sort or filter the Status work properly
    it('Verify that user can sort or filter the Status work properly - 4', async function () {
        await browser.waitForAngularEnabled(true);
        const record = await pageLiveVNFCs.getRecord();
        await pageLiveVNFCs.SelectFilter(3, data.LIVEVNFCs.TC_4.STATUS);
        await pageLiveVNFCs.verifyRecord(data.LIVEVNFCs.TC_4.RECORD);
        await pageLiveVNFCs.SelectFilter(3, 'All');
        const numRecord = await pageLiveVNFCs.getRecord();
        await expect(Boolean(numRecord === record)).toBe(true);
        await pageLiveVNFCs.verifysort(3);
    });

    // 5 - Verify that user can sort or filter the VNF work properly
    it('Verify that user can sort or filter the VNF work properly - 5', async function () {
        await pageLiveVNFCs.verifysort(4);
        const record = await pageLiveVNFCs.getRecord();
        await pageLiveVNFCs.SelectFilter(4, data.LIVEVNFCs.TC_5.VNFNAME);
        await pageLiveVNFCs.verifyRecord(data.LIVEVNFCs.TC_5.RECORD);
        await pageLiveVNFCs.SelectFilter(4, 'All');
        const numRecord = await pageLiveVNFCs.getRecord();
        await expect(Boolean(numRecord === record)).toBe(true);
    });

    // 6 - Verify that the VNFC details for each VNF
    it('Verify that the VNFC details for each VNF - 6', async function () {
        browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_6.TEXT_SEARCH);
        await pageLiveVNFCs.clickDetailVNFC();
        const textDetail = await pageLiveVNFCs.gettextdetailVNFC();
        await expect(Boolean(textDetail === data.LIVEVNFCs.TC_6.DETAILVNFC)).toBe(true);
        await pageLiveVNFCs.clickDetailVNFC();
    });

    // 7 - Verify that the VNFC can be displayed after clicking to each VNF
    it('Verify that the VNFC can be displayed after clicking to each VNF - 7', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_7.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result === false) {
            await pageLiveVNFs.clickRowInTable();
        }
        await browser.waitForAngularEnabled(true);
        const textVNFName = await pageLiveVNFCs.GetTextInTd(1, 4);
        const textVNFName2 = await pageLiveVNFCs.GetTextInTd(2, 4);
        await expect(Boolean(textVNFName === data.LIVEVNFCs.TC_7.VNFNAME)).toBe(true);
        await expect(Boolean(textVNFName2 === data.LIVEVNFCs.TC_7.VNFNAME)).toBe(true);
        await pageLiveVNFs.clickClearSearchVNFTable();
    });

    // 8 - Verify that the live VNFC displays correct records
    it('Verify that the live VNFC displays correct records - 8', async function () {
        await browser.waitForAngularEnabled(true);
        const record = await pageLiveVNFCs.getRecord();
        const numRecord = record.slice(13);
        await console.log('number record: ' + numRecord);
        const numberRecordTotal = Number(numRecord.replace(' records' , ''));
        await console.log('number record: ' + numberRecordTotal);
        const StrnumberRow =  await pageLiveVNFCs.getNumberRowDisplayInpage();
        const numberRow = Number(StrnumberRow);
        if (numberRecordTotal <= numberRow) {
            const numrow = await pageLiveVNFCs.countNumberRowDisplayOnTable();
            await expect(Boolean(numberRecordTotal === numrow)).toBe(true);
        } else {
            await pageLiveVNFCs.selectNumberRowOnTable(15);
            const numrow = await pageLiveVNFCs.countNumberRowDisplayOnTable();
            await expect(Boolean(numberRecordTotal === numrow)).toBe(true);
        }
    });

    // 9 - Verify form fields show/hide properly when selecting Heal - Rebuild action
    it('Verify form fields show/hide properly when selecting Heal - Rebuild action - 9', async function () {
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_9.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_9.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        const getText = await pageHeal.GetTextdescription();
        await expect(getText === data.LIVEVNFCs.TC_9.DESCRIPTION).toBe(true);
    });

    // 10 - Verify that the image and flavor are properly populated and pre-selected in Heal - Rebuild action
    it('Verify that the image and flavor are properly populated and pre-selected in Heal - Rebuild action - 10', async function () {
        await browser.waitForAngularEnabled(true);
        const ImageText = await pageHeal.GetTextImage();
        await expect(Boolean(ImageText === data.LIVEVNFCs.TC_10.IMAGE)).toBe(true);
        // await pageHeal.clickbtnCancel();
    });

    // 11 - Verify form fields show/hide properly when selecting Heal - Recreate_Destroy action
    it('Verify form fields show/hide properly when selecting Heal - Recreate_Destroy action - 11', async function () {
        await browser.waitForAngularEnabled(true);
        await pageHeal.clickbtnSelectheal();
        await pageHeal.clickoptionHeal('Recreate (destroy)');
        const getText = await pageHeal.GetTextdescription();
        await expect(getText === data.LIVEVNFCs.TC_11.DESCRIPTION).toBe(true);
    });
    // 12 - Verify that the image and flavor are properly populated and pre-selected in Heal - Recreate_Destroy action
    // tslint:disable-next-line:max-line-length
    it('Verify that the image and flavor are properly populated and pre-selected in Heal - Recreate_Destroy action - 12', async function () {
        await browser.waitForAngularEnabled(true);
        const ImageText = await pageHeal.GetTextImage();
        const FlavorText = await pageHeal.GetTextFlavor();
        await expect(Boolean(ImageText === data.LIVEVNFCs.TC_12.IMAGE)).toBe(true);
        await expect(Boolean(FlavorText === data.LIVEVNFCs.TC_12.FLAVOR)).toBe(true);
        // await pageHeal.clickbtnCancel();
    });

    // 13 - Verify form fields show/hide properly when selecting Heal - Recreate_reuse action
    it('Verify form fields show/hide properly when selecting Heal - Recreate_reuse action - 13', async function () {
        await browser.waitForAngularEnabled(true);
        await pageHeal.clickbtnSelectheal();
        await pageHeal.clickoptionHeal('Recreate (reuse)');
        const getText = await pageHeal.GetTextdescription();
        await expect(getText === data.LIVEVNFCs.TC_13.DESCRIPTION).toBe(true);
        // await pageHeal.clickbtnCancel();
    });

    // 14 - Verify that the image and flavor are properly populated and pre-selected in Heal - Recreate_reuse action
    it('Verify that the image and flavor are properly populated and pre-selected in Heal - Recreate_reuse action - 14', async function () {
        await browser.waitForAngularEnabled(true);
        const ImageText = await pageHeal.GetTextImage();
        const FlavorText = await pageHeal.GetTextFlavor();
        await expect(Boolean(ImageText === data.LIVEVNFCs.TC_14.IMAGE)).toBe(true);
        await expect(Boolean(FlavorText === data.LIVEVNFCs.TC_14.FLAVOR)).toBe(true);
        await pageHeal.clickbtnCancel();
    });

    // 15 - Verify that the image and flavor are properly populated and pre-selected in Migrate action
    it('Verify that the image and flavor are properly populated and pre-selected in Migrate action- 15', async function () {
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_15.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_15.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        const txtMigrade = await pageMigrate.GetTextMigrade();
        await console.log(' Text: ' + txtMigrade);
        await expect(Boolean(txtMigrade === data.LIVEVNFCs.TC_15.TEXTVERIFY)).toBe(true);
        await pageMigrate.clickCancel();
    });

    // 16 - Verify that user can migrate the VNF on the UI
    it('Verify that user can migrate the VNF on the UI - 16', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_15.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_15.FUNCTION);
        }
        const FlavorText = await pageMigrate.GetTextFlavor();
        await expect(Boolean(FlavorText === data.LIVEVNFCs.TC_16.FLAVOR)).toBe(true);
        await pageMigrate.clickMigrate();
        await pageLiveVNFCs.verifyFormNotDisplayed();
        // const status1 = await pageLiveVNFCs.GetTextInTd(1, 3);
        // await expect(Boolean(status1 === 'Disabled')).toBe(true);
    });

    // 17 - Verify that the UI works properly with healing via rebuild option
    /*it('Verify that the UI works properly with healing via rebuild option - 17', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_17.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_17.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        const ImageText = await pageHeal.GetTextImage();
        await expect(Boolean(ImageText === data.LIVEVNFCs.TC_17.IMAGE)).toBe(true);
        await pageHeal.clickbtnHeal();
        await pageLiveVNFCs.clickRefreshButton();
        const checkStatus = await pageLiveVNFCs.waitVNFCHealAvailable(1, 3, 'Available', 2700000, 'TC failed due to Available time out');
        await expect(checkStatus).toBeTruthy();
    }, 2710000);


    // 18 - Verify that the UI works properly with healing via recreate_destroy option
    it('Verify that the UI works properly with healing via recreate_destroy option - 18', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_18.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_18.FUNCTION);
        }
        await pageHeal.clickbtnSelectheal();
        await pageHeal.clickoptionHeal('Recreate (destroy)');
        const getText = await pageHeal.GetTextdescription();
        await expect(getText === data.LIVEVNFCs.TC_11.DESCRIPTION).toBe(true);
        const ImageText = await pageHeal.GetTextImage();
        const FlavorText = await pageHeal.GetTextFlavor();
        await expect(Boolean(ImageText === data.LIVEVNFCs.TC_18.IMAGE)).toBe(true);
        await expect(Boolean(FlavorText === data.LIVEVNFCs.TC_18.FLAVOR)).toBe(true);
        await pageHeal.clickbtnHeal();
        await pageLiveVNFCs.clickRefreshButton();
        // await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_18.VNFNAME);
        const checkStatus = await pageLiveVNFCs.waitVNFCHealAvailable(1, 3, 'Available', 2700000, 'TC failed due to Available time out');
        await expect(checkStatus).toBeTruthy();
    }, 2710000);


    // 19 - Verify that the UI works properly with healing via recreate_reuse option
    it('Verify that the UI works properly with healing via recreate_reuse option- 19', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_19.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_19.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        await pageHeal.clickbtnSelectheal();
        await pageHeal.clickoptionHeal('Recreate (reuse)');
        const getText = await pageHeal.GetTextdescription();
        await expect(getText === data.LIVEVNFCs.TC_13.DESCRIPTION).toBe(true);
        const ImageText = await pageHeal.GetTextImage();
        const FlavorText = await pageHeal.GetTextFlavor();
        await expect(Boolean(ImageText === data.LIVEVNFCs.TC_19.IMAGE)).toBe(true);
        await expect(Boolean(FlavorText === data.LIVEVNFCs.TC_19.FLAVOR)).toBe(true);
        await pageHeal.clickbtnHeal();
        await pageLiveVNFCs.clickRefreshButton();
        // await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_19.VNFNAME);
        const checkStatus = await pageLiveVNFCs.waitVNFCHealAvailable(1, 3, 'Available', 2700000, 'TC failed due to Available time out');
        await expect(checkStatus).toBeTruthy();
    }, 2710000);

    // 20 - Verify that the UI works properly with healing via reboot_soft option
    it('Verify that the UI works properly with healing via reboot_soft option- 20', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_20.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_20.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        await pageHeal.clickbtnSelectheal();
        await pageHeal.clickoptionHeal('Reboot (soft)');
        await pageHeal.clickbtnHeal();
        await pageLiveVNFCs.clickRefreshButton();
        // await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_20.VNFNAME);
        const checkStatus = await pageLiveVNFCs.waitVNFCHealAvailable(1, 3, 'Available', 2700000, 'TC failed due to Available time out');
        await expect(checkStatus).toBeTruthy();
    }, 2710000);

    // 21 - Verify that the UI works properly with healing via recreate_hard option
    it('Verify that the UI works properly with healing via recreate_hard option - 21', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_21.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFCs.clickSelectActions(1);
            await pageLiveVNFCs.clickOptionSelectActions(data.LIVEVNFCs.TC_21.FUNCTION);
        }
        await browser.waitForAngularEnabled(true);
        await pageHeal.clickbtnSelectheal();
        await pageHeal.clickoptionHeal('Reboot (hard)');
        await pageHeal.clickbtnHeal();
        await pageLiveVNFCs.clickRefreshButton();
        // await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_21.VNFNAME);
        const checkStatus = await pageLiveVNFCs.waitVNFCHealAvailable(1, 3, 'Available', 2700000, 'TC failed due to Available time out');
        await expect(checkStatus).toBeTruthy();
     }, 2710000);

    // 22 - Verify that user can choose upgrade function from UI
    /*it('Verify that user can choose upgrade function from UI - 22', async function() {
        await pageNavigation.selectLeftNav(2);
        await browser.wait(ExpectedConditions.urlContains('vnfm/vnfcatalog'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageOnboardedVNFs.settextSearch(data.LIVEVNFCs.TC_22.VERSIONCSAR);
        const afterSearch = await pageOnboardedVNFs.isTableHasValueAfterSearch();
        if (afterSearch === false) {
            await pageOnboardedVNFs.clickOnboardbtn();
            await pageFailedtoOnboard.setSearch(data.LIVEVNFCs.TC_22.VERSIONCSAR);
            const result1 = await pageFailedtoOnboard.isTableHasValueAfterSearch();
            if (result1 === true) {
                await pageFailedtoOnboard.clickActionsDopdown();
                await pageFailedtoOnboard.clickFunctionActionsDopdown('Delete');
                await pageFailedtoOnboard.clickConfirmDelete();
            }
            await browser.sleep(10000);
            await pageFailedtoOnboard.funcUploadCSAR(data.LIVEVNFCs.TC_22.PATH);
            await pageFailedtoOnboard.clickBackToPageVNFCatalog();
        }
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_22.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            const textStatus = await pageLiveVNFs.GetTextInTd(7);
            await expect(Boolean(textStatus === 'Ready')).toBe(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFCs.TC_22.FUNCTION);
            const txtVNFMPreference = await pageLiveVNFs.getTextUpgrade();
            expect(Boolean(txtVNFMPreference === 'Upgrade VNF')).toBe(true);
            await pageLiveVNFs.clickOptionSelectVersionUpgrade(data.LIVEVNFCs.TC_22.VERSIONCSAR);
            await pageLiveVNFs.clickConfirm();
            await browser.sleep(10000);
            const textStt = await pageLiveVNFs.GetTextInTd(7);
            await expect(Boolean(textStt === 'Upgrading')).toBe(true);
        }
    });

    // 23 - Verify that the UI works properly with upgrade via rebuild option
    it('Verify that the UI works properly with upgrade via rebuild option - 23', async function () {
        browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_23.VNFNAME);
        const result5 = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result5) {
            const status1 = await pageLiveVNFCs.GetTextInTd(1, 3);
            if (status1 === 'Upgrade Required') {
                await pageLiveVNFCs.clickSelectActions(1);
                await pageLiveVNFCs.clickOptionSelectActions('Upgrade');
                const txtUpgrade = await pageUpgradeVNFC.GetTextUpgrade();
                await expect(Boolean(txtUpgrade === 'UI-ssbc-1 | Upgrade')).toBe(true);
                await pageUpgradeVNFC.clickbtnSelectTypeUpgrade();
                await pageUpgradeVNFC.clickoptionUpgrade('Rebuild');
                await pageUpgradeVNFC.clickbtnUpgrade();
            }
            await browser.sleep(5000);
            // tslint:disable-next-line:max-line-length
            const checkStatus = await pageLiveVNFCs.waitVNFCUpgraded(1, 3, 'Upgraded', 1800000, 'TC failed due to Upgraded time out');
            await expect(checkStatus).toBeTruthy();
        }
    }, 1810000);

    // 24 - Verify that the UI works properly with rollback via rebuild option
    it('Verify that the UI works properly with rollback via rebuild option - 24', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_23.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            const status1 = await pageLiveVNFCs.GetTextInTd(1, 3);
            await expect(Boolean(status1 === 'Upgraded')).toBeTruthy();
            if (status1 === 'Upgraded') {
                await pageLiveVNFCs.clickSelectActions(1);
                await pageLiveVNFCs.clickOptionSelectActions('Rollback');
                const txtRollback = await pageRollback.GetTextRollback();
                await expect(Boolean(txtRollback === 'UI-ssbc-1 | Rollback')).toBe(true);
                // await pageRollback.clickbtnSelectTypeRollback();
                // await pageRollback.clickoptionRollback(1);
                await pageRollback.clickRollbackButton();
            }
            await browser.sleep(5000);
            // tslint:disable-next-line:max-line-length
            const checkStatus = await pageLiveVNFCs.waitVNFCRollBack(1, 3, 'Upgrade Required', 1800000, 'TC failed due to Upgraded time out');
            await expect(checkStatus).toBeTruthy();
        }
    }, 1810000);

    // 25 - Verify that the UI works properly with upgrade via re_create option
    it('Verify that the UI works properly with upgrade via re_create option - 25', async function () {
        await pageNavigation.selectLeftNav(2);
        await pageNavigation.selectLeftNav(3);
        browser.wait(ExpectedConditions.urlContains('vnfm/livevnf'), 3000);
        await browser.waitForAngularEnabled(true);
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_23.VNFNAME);
        const result5 = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result5) {
            const status1 = await pageLiveVNFCs.GetTextInTd(2, 3);
            if (status1 === 'Upgrade Required') {
                await pageLiveVNFCs.clickSelectActions(2);
                await pageLiveVNFCs.clickOptionSelectActions('Upgrade');
                const txtUpgrade = await pageUpgradeVNFC.GetTextUpgrade();
                await expect(Boolean(txtUpgrade === 'UI-ssbc-2 | Upgrade')).toBe(true);
                await pageUpgradeVNFC.clickbtnSelectTypeUpgrade();
                await pageUpgradeVNFC.clickoptionUpgrade('Recreate (reuse)');
                await pageUpgradeVNFC.clickbtnUpgrade();
            }
            await browser.sleep(5000);
            // tslint:disable-next-line:max-line-length
            const checkStatus = await pageLiveVNFCs.waitVNFCUpgraded(2, 3, 'Upgraded', 1800000, 'TC failed due to Upgraded time out');
            await expect(checkStatus).toBeTruthy();
        }
    }, 1810000);

    // 26 - Verify that the UI works properly with rollback via re_create option
    it('Verify that the UI works properly with rollback via re_create option - 26', async function () {
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFCs.setSearch(data.LIVEVNFCs.TC_23.VNFNAME);
        const result = await pageLiveVNFCs.isTableHasValueAfterSearch();
        if (result) {
            const status1 = await pageLiveVNFCs.GetTextInTd(2, 3);
            await expect(Boolean(status1 === 'Upgraded')).toBeTruthy();
            if (status1 === 'Upgraded') {
                await pageLiveVNFCs.clickSelectActions(2);
                await pageLiveVNFCs.clickOptionSelectActions('Rollback');
                const txtRollback = await pageRollback.GetTextRollback();
                await expect(Boolean(txtRollback === 'UI-ssbc-2 | Rollback')).toBe(true);
                // await pageRollback.clickbtnSelectTypeRollback();
                // await pageRollback.clickoptionRollback(2);
                await pageRollback.clickRollbackButton();
            }
            await browser.sleep(5000);
            // tslint:disable-next-line:max-line-length
            const checkStatus = await pageLiveVNFCs.waitVNFCRollBack(2, 3, 'Upgrade Required', 1800000, 'TC failed due to Upgraded time out');
            await expect(checkStatus).toBeTruthy();
        }
    }, 1810000);*/

    // 27 - Verify that user can terminate the VNF with Graceful Destroy option
    it('Verify that user can terminate the VNF with Graceful Destroy option - 27', async function() {
        browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_27.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFCs.TC_27.FUNCTION);
            await pageTerminate.clickGraceful();
            await pageTerminate.clickConfirmYes();
        }
        await browser.sleep(150000);
        await pageLiveVNFs.clickRefesh();
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_27.VNFNAME);
        const result1 = await pageLiveVNFs.isTableHasValueAfterSearch();
        await expect(result1).toBeFalsy();
    });

    // 28 - Verify that user can terminate the VNF with Retain preallocated Floating IPs option
    it('Verify that user can terminate the VNF with Retain preallocated Floating IPs option- 28', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_28.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await browser.waitForAngularEnabled(true);
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFCs.TC_28.FUNCTION);
            await pageTerminate.clickRetainIp();
            await pageTerminate.clickConfirmYes();
        }
        await browser.sleep(150000);
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
        await pageLiveVNFs.clickRefesh();
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_28.VNFNAME);
        const result4 = await pageLiveVNFs.isTableHasValueAfterSearch();
        await expect(result4).toBeFalsy();
        });

    // 29 - Verify that user can terminate the VNF
    it('Verify that user can terminate the VNF - 29', async function() {
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_29.VNFNAME);
        const result = await pageLiveVNFs.isTableHasValueAfterSearch();
        if (result) {
            await pageLiveVNFs.clickSelectActions();
            await pageLiveVNFs.clickOptionSelectActions(data.LIVEVNFCs.TC_29.FUNCTION);
            await pageTerminate.clickConfirmYes();
        }
        await browser.sleep(150000);
        await pageLiveVNFs.clickRefesh();
        await pageLiveVNFs.setSearch(data.LIVEVNFCs.TC_29.VNFNAME);
        const result1 = await pageLiveVNFs.isTableHasValueAfterSearch();
        await expect(result1).toBeFalsy();
    });



});
