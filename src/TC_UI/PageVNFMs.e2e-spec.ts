import { PageVNFMs } from '../PageObject/PageVNFMs.po';
import { browser } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';

describe('workspace-project page vnfms', () => {
    let pageVNFMs: PageVNFMs;
    let pageError: PageError;
    let pageNavigation: PageNavigation;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageVNFMs = new PageVNFMs();
        pageError = new PageError();
        pageNavigation = new PageNavigation();
    });


    // Verify that the VNFMs tab displayed in left navigation bar
    it('Verify that the VNFMs tab displayed in left navigation bar-1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        await expect(await pageVNFMs.verifyVNFMsTabDisplayed()).toBeTruthy();
    });

    // Verify that user can select the number of displayed columns
    it('Verify that user can select the number of displayed columns-2', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        await pageVNFMs.clickShowHideColumnButton();
        let currentNumberColumnDefault = await pageVNFMs.getCurrentNumberColumn();
        let checkBoxChecked = await pageVNFMs.getNumberCheckBoxDialogChecked();
        await expect(currentNumberColumnDefault).toEqual(checkBoxChecked);

        await pageVNFMs.unSelectAllCheckBoxDialog();
        currentNumberColumnDefault = await pageVNFMs.getCurrentNumberColumn();
        await expect(currentNumberColumnDefault).toEqual(0);

        await pageVNFMs.selectAllCheckBoxDialog();
        currentNumberColumnDefault = await pageVNFMs.getCurrentNumberColumn();
        checkBoxChecked = await pageVNFMs.getNumberCheckBoxDialogChecked();
        await pageVNFMs.clickCloseDialogButton();
        await expect(currentNumberColumnDefault).toEqual(checkBoxChecked);
    });

    // Verify that the dropdown filter and sort functions work properly with VNFM column
    it('Verify that the dropdown filter and sort functions work properly with VNFM column-3', async function () {
        // column vnfm
        const Column = 1;
        await pageVNFMs.clickIconShowMultiSelectItemByColumn(Column);
        const listItem = await pageVNFMs.getListMultiSelectFilter();
        if (listItem.length <= 2) {
            const numberSelect = 1;
            await pageVNFMs.selectItemMultiSelect(numberSelect);
            const listItemSelected = await pageVNFMs.getListMultiSelectFilterIsSelected();
            const listColumn = await pageVNFMs.getListColumnCurrentPage(Column);
            for (let i = 0; i < listColumn.length; i++) {
                await expect(listItemSelected).toContain(listColumn[i]);
            }
            await pageVNFMs.unselectItemMultiSelect(numberSelect);
        } else {
            const numberSelect = 2;
            await pageVNFMs.selectItemMultiSelect(numberSelect);
            const listItemSelected = await pageVNFMs.getListMultiSelectFilterIsSelected();
            const listColumn = await pageVNFMs.getListColumnCurrentPage(Column);
            for (let i = 0; i < listColumn.length; i++) {
                await expect(listItemSelected).toContain(listColumn[i]);
            }
            await pageVNFMs.unselectItemMultiSelect(numberSelect);
        }
        await pageVNFMs.verifySortColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with Alias column
    it('Verify that the dropdown filter and sort functions work properly with Alias column-4', async function () {
        // Alias vnfm
        const Column = 2;
        await browser.waitForAngularEnabled(true);
        await pageVNFMs.clickIconShowMultiSelectItemByColumn(Column);
        const listItem = await pageVNFMs.getListMultiSelectFilter();
        if (listItem.length <= 2) {
            const numberSelect = 1;
            await pageVNFMs.selectItemMultiSelect(numberSelect);
            const listItemSelected = await pageVNFMs.getListMultiSelectFilterIsSelected();
            const listColumn = await pageVNFMs.getListColumnCurrentPage(Column);
            for (let i = 0; i < listColumn.length; i++) {
                await expect(listItemSelected).toContain(listColumn[i]);
            }
            await pageVNFMs.unselectItemMultiSelect(numberSelect);
        } else {
            const numberSelect = 2;
            await pageVNFMs.selectItemMultiSelect(numberSelect);
            const listItemSelected = await pageVNFMs.getListMultiSelectFilterIsSelected();
            const listColumn = await pageVNFMs.getListColumnCurrentPage(Column);
            for (let i = 0; i < listColumn.length; i++) {
                await expect(listItemSelected).toContain(listColumn[i]);
            }
            await pageVNFMs.unselectItemMultiSelect(numberSelect);
        }
        await pageVNFMs.verifySortColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with Number of VNFs column
    it('Verify that the dropdown filter and sort functions work properly with Number of VNFs column-5', async function () {
        // column Number of VNFs
        const Column = 3;
        await pageVNFMs.verifySortColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with version column
    it('Verify that the dropdown filter and sort functions work properly with version column-6', async function () {
        // column version
        const Column = 4;
        await pageVNFMs.verifySortColumn(Column);
        await pageVNFMs.verifyDropdownFilterColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with status column
    it('Verify that the dropdown filter and sort functions work properly with status column-7', async function () {
        // column status
        const Column = 5;
        await pageVNFMs.verifySortColumn(Column);
        await pageVNFMs.verifyDropdownFilterColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with IPV4 column
    it('Verify that the dropdown filter and sort functions work properly with IPV4 column-8', async function () {
        // column IPV4
        const Column = 6;
        await pageVNFMs.verifySortColumn(Column);
        await pageVNFMs.verifyDropdownFilterColumn(Column);
    });

    // Verify that the dropdown filter and sort functions work properly with IPV6 column
    it('Verify that the dropdown filter and sort functions work properly with IPV6 column-9', async function () {
        // column IPV6
        const Column = 7;
        await pageVNFMs.verifySortColumn(Column);
        await pageVNFMs.verifyDropdownFilterColumn(Column);
    });

    // Verify that the Search function work properly
    fit('Verify that the Search function work properly-10', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const column = 1;
        const numberRowTable = await pageVNFMs.getCurrentNumberRow();
        const getTextForSearch = await pageVNFMs.getTextCellOfTable(column, 1);
        await pageVNFMs.setTextOnSearchBox('Verify that the Search function work properly');
        await expect(await pageVNFMs.getCurrentNumberRow()).toBe(0);
        await pageVNFMs.setTextOnSearchBox(getTextForSearch);
        const isDropdownFilterNotAll = await pageVNFMs.verifyFilterSearchBox(getTextForSearch, column);
        await expect(isDropdownFilterNotAll).toBeTruthy();
        await pageVNFMs.clearTextOnSearchBox();
        const isDropdownFilterAll = await pageVNFMs.verifyDropdownFilterAll(numberRowTable);
        await expect(isDropdownFilterAll).toBeTruthy();
    });

    // Verify that the Clear table button work properly
    it('Verify that the Clear table button work properly- 11', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const numberRowTable = await pageVNFMs.getCurrentNumberRow();
        await pageVNFMs.setTextOnSearchBox('Verify that the Clear table button work properly');
        await expect(await pageVNFMs.verifyDropdownFilterAll(0)).toBeTruthy();
        await pageVNFMs.clickClearButton();
        await expect(await pageVNFMs.verifyDropdownFilterAll(numberRowTable));
    });

    // Verify that the Refresh button work properly
    it('Verify that the Refresh button work properly- 12', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const numberRowTable = await pageVNFMs.getCurrentNumberRow();
        await pageVNFMs.setTextOnSearchBox('Verify that the Refresh button work properly');
        await expect(await pageVNFMs.verifyDropdownFilterAll(0)).toBeTruthy();
        await pageVNFMs.clickRefreshButton();
        await browser.waitForAngular();
        await expect(await pageVNFMs.verifyDropdownFilterAll(numberRowTable));
    });

    // // Verify that the tables can be expanded or minimized when click on right-top button
    // it('Verify that the tables can be expanded or minimized when click on right-top button-13', async function () {
    //     const result1 = await pageError.verifyMessegeError();
    //     if ( result1 === true ) {
    //         await pageError.cliclClearALL();
    //     }
    //     await browser.waitForAngularEnabled(true);
    //     await pageVNFMs.clickMinimizeTable();
    //     await expect(await pageVNFMs.verifyTableMinimized()).toBeTruthy();
    //     await pageVNFMs.clickExpandedTable();
    //     await expect(await pageVNFMs.verifyTableExpanded()).toBeTruthy();
    // });

    // Verify that the tables display with maximum 5 entries in one page as default
    it('Verify that the tables display with maximum 5 entries in one page as default-14', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const currentNumberRow = await pageVNFMs.getCurrentNumberRow();
        await expect(await Boolean(currentNumberRow <= 5)).toBeTruthy();
        await expect(await pageVNFMs.getRowsPerPageOptions()).toEqual(5);
    });

    // Verify that the user can choose the number of entries displayed in one page
    it('Verify that the user can choose the number of entries displayed in one page-15', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        await pageVNFMs.selectRowsPerPageOptions('15');
        let numberRow = await pageVNFMs.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 15)).toBeTruthy();
        await pageVNFMs.selectRowsPerPageOptions('30');
        numberRow = await pageVNFMs.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 30)).toBeTruthy();
        await pageVNFMs.selectRowsPerPageOptions('5');
        numberRow = await pageVNFMs.getCurrentNumberRow();
        await expect(await Boolean(numberRow <= 5)).toBeTruthy();
    });

    // Verify that the user can move to specified page when click on the page number
    it('Verify that the user can move to specified page when click on the page number-16', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const listNumberPage = await pageVNFMs.getListNumberPageInTable();
        const numberPageActive1 = await pageVNFMs.getNumberPageActiveInTable();
        if (listNumberPage.length <= 1) {
            await expect(numberPageActive1).toEqual(listNumberPage[0]);
        } else {
            await pageVNFMs.moveNumberPageInTable(listNumberPage[1]);
            const numberPageActive2 = await pageVNFMs.getNumberPageActiveInTable();
            await expect(numberPageActive2).toEqual(listNumberPage[1]);

            await pageVNFMs.moveNumberPageInTable(listNumberPage[0]);
            const numberPageActive3 = await pageVNFMs.getNumberPageActiveInTable();
            await expect(numberPageActive3).toEqual(listNumberPage[0]);
        }
    });
    // tslint:disable-next-line:max-line-length
    // Verify that the drop-down list Functions contain: Start Maintenance, Complete Maintenance, Reassign VNFs, Disable to DR, Enable from DR, Delete
    // tslint:disable-next-line:max-line-length
    it('Verify that the drop-down list Functions contain: Start Maintenance, Complete Maintenance, Reassign VNFs, Disable to DR, Enable from DR, Delete-17', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const indexColumn = await pageVNFMs.getIndexColumnContainsKeyWord('Actions');
        const listFunction = await pageVNFMs.getListDropdownActions(1, indexColumn);
        await expect(listFunction).toContain('Start Maintenance');
        await expect(listFunction).toContain('Complete Maintenance');
        await expect(listFunction).toContain('Reassign VNFs');
        await expect(listFunction).toContain('Disable to DR');
        await expect(listFunction).toContain('Enable from DR');
        await expect(listFunction).toContain('Delete');
    });

    // 18- Verify that the value of current VNFM were displayed when user click on existing VNFM row
    it('Verify that the value of current VNFM were displayed when user click on existing VNFM row-18', async function () {
        // show form
        await pageVNFMs.clickRowTable(1);
        await expect(await pageVNFMs.verifyFormEditVnfmDisplay()).toBeTruthy();
        await pageVNFMs.clickRowTable(1);
        // hide form
        await expect(await pageVNFMs.verifyFormEditVnfmDisplay()).toBeFalsy();
    });

    // 19- Verify that the field: Name, Number of VNFs, Version, Status are Read-only
    it('Verify that the field: Name, Number of VNFs, Version, Status are Read-only-19', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        // show form
        await pageVNFMs.clickRowTable(1);
        // input name
        await expect(await pageVNFMs.verifyInputFormEditDisable('name')).toBeTruthy();
        // input Number of VNFs
        await expect(await pageVNFMs.verifyInputFormEditDisable('assignedVnfCount')).toBeTruthy();
        // input version
        await expect(await pageVNFMs.verifyInputFormEditDisable('softwareVersion')).toBeTruthy();
        // input status
        await expect(await pageVNFMs.verifyInputFormEditDisable('status')).toBeTruthy();
    });

    // 20- Verify that the field: Alias, IPv4, IPv6 are Editable
    it('Verify that the field: Alias, IPv4, IPv6 are Editable-20', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        // input alias
        await expect(await pageVNFMs.verifyInputFormEditDisable('alias')).toBeFalsy();
        // input ipv4
        await expect(await pageVNFMs.verifyInputFormEditDisable('v4IpAddress')).toBeFalsy();
        // input ipv6
        await expect(await pageVNFMs.verifyInputFormEditDisable('v6IpAddress')).toBeFalsy();
    });

    // Verify that the VNFMs table can update after click on Save button
    it('Verify that the VNFMs table can update after click on Save button- 21', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        const textInput = await pageVNFMs.getTextInput('alias');
        const textInputSub = 'test' + textInput;
        await pageVNFMs.setTextInput('alias', textInputSub);
        await pageVNFMs.clickSaveButtonEditForm();
        await pageVNFMs.verifyDropdownFilterNotAll(textInputSub, 2);

        // show form
        await pageVNFMs.clickRowTable(1);
        const textInput1 = await pageVNFMs.getTextInput('alias');
        const textInputSub1 = textInput1.slice(4);
        await pageVNFMs.setTextInput('alias', textInputSub1);
        await pageVNFMs.clickSaveButtonEditForm();
        await pageVNFMs.verifyDropdownFilterNotAll(textInputSub1, 2);
    });

    // Verify that the user can exit the edit VNFM form when click on Cancel button
    it('Verify that the user can exit the edit VNFM form when click on Cancel button- 22', async function () {
        const result1 = await pageError.verifyMessegeError();
        if ( result1 === true ) {
            await pageError.cliclClearALL();
        }
        await browser.waitForAngularEnabled(true);
        // show form
         await pageVNFMs.clickRowTable(1);
         await expect(await pageVNFMs.verifyFormEditVnfmDisplay()).toBeTruthy();
         await pageVNFMs.clickCancelButtonEditForm();
         await expect(await pageVNFMs.verifyFormEditVnfmDisplay()).toBeFalsy();

        await pageNavigation.clickUserProfile();
        await pageNavigation.VerifyLogout();
    });
});
