import { browser, ExpectedConditions } from 'protractor';
import { PageAddCloud } from '../PageObject/PageAddCloud.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project page add cloud', () => {
    let pageAddCloud: PageAddCloud;
    let confCloud: PageVNFMConfCloudSystems;
    let navigation: PageNavigation;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageAddCloud = new PageAddCloud();
        confCloud = new PageVNFMConfCloudSystems();
        navigation = new PageNavigation();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    // 1. Verify that the Test Version button can identify if the cloud to be created is configured correctly when add cloud
    // tslint:disable-next-line:max-line-length
    it('Verify that the Test Version button can identify if the cloud to be created is configured correctly when add cloud-1', async function () {
       // await browser.get(browser.baseUrl + '/#/vnfm/cloud');
        await navigation.selectLeftNav(4);
        await navigation.selectLeftNavSecond(4, 1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();

        await confCloud.clickAddCloudbtn();
        await expect(await pageAddCloud.countInputVersionDisable()).toBe(5);
        await pageAddCloud.inputCloudConfig(5, await pageAddCloud.getIpFromText(data.CLOUD_SYSTEM.TC_0.KEYSTONE));
        await pageAddCloud.inputCloudConfig(7, await pageAddCloud.getIpFromText(data.CLOUD_SYSTEM.TC_0.NOVA));
        await pageAddCloud.inputCloudConfig(9, await pageAddCloud.getIpFromText(data.CLOUD_SYSTEM.TC_0.NEUTRON));
        await pageAddCloud.inputCloudConfig(11, await pageAddCloud.getIpFromText(data.CLOUD_SYSTEM.TC_0.CINDER));
        await pageAddCloud.inputCloudConfig(13, await pageAddCloud.getIpFromText(data.CLOUD_SYSTEM.TC_0.IMAGE));
        await pageAddCloud.clickGetVersion();
        await expect(await pageAddCloud.countInputVersionDisable()).toBe(0);
        await pageAddCloud.clickCancel();
    });

    // 2. Verify that the Test Version button can identify if the cloud to be created is not configured correctly when add cloud-1
    // tslint:disable-next-line:max-line-length
    it('Verify that the Test Version button can identify if the cloud to be created is not configured correctly when add cloud-2', async function () {
        await confCloud.clickAddCloudbtn();
        await expect(await pageAddCloud.countInputVersionDisable()).toBe(5);
        await pageAddCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_DataError.KEYSTONE);
        await pageAddCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_DataError.NOVA);
        await pageAddCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_DataError.NEUTRON);
        await pageAddCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_DataError.CINDER);
        await pageAddCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_DataError.IMAGE);
        await expect(await pageAddCloud.countInputVersionDisable()).toBe(5);
        await pageAddCloud.clickCancel();
    });
});
