import { browser } from 'protractor';
import { PageLogin } from '../PageObject/PageLogin.po';
import { PageUsers } from '../PageObject/PageUsers.po';
import { PageAddUser } from '../PageObject/PageAddUser.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
import { PageAdminRoles } from '../PageObject/PageAdminRole.po';
// import { PageRBACRoles } from '../PageObject/PageRBACRoles.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');
describe('workspace-project page role', () => {
    let pageLogin: PageLogin;
    let pageUsers: PageUsers;
    let pageAddUser: PageAddUser;
    let pageNavigation: PageNavigation;
    let pageAdminRoles: PageAdminRoles;
    // let pageRBACRoles: PageRBACRoles;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 150000;
        pageLogin = new PageLogin();
        pageUsers = new PageUsers();
        pageAddUser = new PageAddUser();
        pageNavigation = new PageNavigation();
        pageAdminRoles = new PageAdminRoles();
        // pageRBACRoles = new PageRBACRoles();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });

    it('Verify that the dropdown filter and sort functions work properly with Role column-1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/roles');
        await browser.waitForAngularEnabled(true);
        // await browser.waitForAngular();
        await pageAdminRoles.ClickFilterButton();
        await pageAdminRoles.ClickFilterRoleColumn();
        await pageAdminRoles.ClickOptionInSelectFilter(data.ADMIN_ROLE.TC_1.ROLENAME);
        const txtRoleName = await pageAdminRoles.getTextRoleName();
        await expect(Boolean(txtRoleName === data.ADMIN_ROLE.TC_1.ROLENAME)).toBe(true);
        await pageAdminRoles.ClickClearFilterButton();
        await pageAdminRoles.verifysort(1);

    });

    // Verify that user can select the number of displayed columns
    it('Verify that user can select the number of displayed columns-2', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminRoles.ClickShowCloumnButton();
        await pageAdminRoles.selectOptionNamefromShowHideColumnsList('Role');
        await pageAdminRoles.ClickCloseButtonInShowColumn();
        await expect(pageAdminRoles.verifyColumnDisplay('Role')).toBeFalsy();

        await pageAdminRoles.ClickShowCloumnButton();
        await pageAdminRoles.selectOptionNamefromShowHideColumnsList('Role');
        await pageAdminRoles.ClickCloseButtonInShowColumn();
        await expect(pageAdminRoles.verifyColumnDisplay('Role')).toBeTruthy();

    });

    // Verify that the Search function work properly
    it('Verify that the Search function work properly-3', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminRoles.ClearTextSearch();
        await pageAdminRoles.settextSearch(data.ADMIN_ROLE.TC_1.ROLENAME);
        await expect(pageAdminRoles.verifyDisplay1RecordInPage()).toBeTruthy();
        await pageAdminRoles.ClearTextSearch();
        await pageAdminRoles.settextSearch('Search incorect text');
        await expect(pageAdminRoles.verifyDisplay1RecordInPage()).toBeFalsy();
    });

    // Verify that the Clear table button work properly
    it('Verify that the Clear table button work properly- 4', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminRoles.ClearTextSearch();
        await pageAdminRoles.settextSearch('Search incorect text');
        await expect(pageAdminRoles.verifyDisplay1RecordInPage()).toBeFalsy();
        await pageAdminRoles.ClickClearSearchButton();
        const txtSearch = await pageAdminRoles.getAttributeOfTextboxSearch();
        await expect(Boolean(txtSearch === '')).toBe(true);
    });

    // Verify that the Clear Filter button work properly
    it('Verify that the Clear Filter button work properly- 5', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminRoles.ClickFilterRoleColumn();
        await pageAdminRoles.ClickOptionInSelectFilter(data.ADMIN_ROLE.TC_1.ROLENAME);
        await expect(pageAdminRoles.verifyDisplay1RecordInPage()).toBeTruthy();
        await pageAdminRoles.ClickClearFilterButton();
        const txtTextFilter = await pageAdminRoles.getTextLabelFilter();
        await expect(Boolean(txtTextFilter === 'All')).toBe(true);
    });

     // Verify that the Filter button work properly
     it('Verify that the Filter button work properly- 6', async function () {
        await browser.waitForAngularEnabled(true);
        await expect(pageAdminRoles.verifyDisplayDropdownFilterInPage()).toBeTruthy();
        await pageAdminRoles.ClickFilterButton();
        await expect(pageAdminRoles.verifyDisplayDropdownFilterInPage()).toBeFalsy();
    });

    // Verify that the tables display with maximum 5 entries in one page as default
    it('Verify that the tables display with maximum 5 entries in one page as default-7', async function () {
        await browser.waitForAngularEnabled(true);
        const txtRowNumber = await pageAdminRoles.getTextNumberRowDisplayInPage();
        const numberRow = await pageAdminRoles.countNumberRowDisplayOnTable();
        await expect(Boolean(txtRowNumber === '5')).toBe(true);
        await expect(Boolean(numberRow <= 5)).toBe(true);
    });

    // Verify that the user can choose the number of entries displayed in one page
    it('Verify that the user can choose the number of entries displayed in one page-8', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminRoles.selectNumberRowOnTable(10);
        const txtRowNumber = await pageAdminRoles.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtRowNumber === '10')).toBe(true);
        await pageAdminRoles.selectNumberRowOnTable(15);
        const txtRowNumber2 = await pageAdminRoles.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtRowNumber2 === '15')).toBe(true);
        await pageAdminRoles.selectNumberRowOnTable(5);
        const txtRowNumber3 = await pageAdminRoles.getTextNumberRowDisplayInPage();
        await expect(Boolean(txtRowNumber3 === '5')).toBe(true);
    });

    // Verify that the user can move to specified page when click on the page number
    it('Verify that the user can move to specified page when click on the page number-9', async function () {
        await browser.waitForAngularEnabled(true);
        const RowNumber = await pageAdminRoles.countNumberRowDisplayOnTable();
        if (RowNumber > 5) {
            await expect(pageAdminRoles.verifySwitchButtonDisplay()).toBeTruthy();
            await pageAdminRoles.ClickPage2InTable();
            const txtNumberPage = await pageAdminRoles.getTextNumberPage();
            await expect(Boolean(txtNumberPage === '2')).toBe(true);
        } else {
            const txtNumberPage = await pageAdminRoles.getTextNumberPage1();
            await expect(Boolean(txtNumberPage === '1')).toBe(true);
        }
    });

    // Verify that the Role nav tab only visible with sysadmin user
    it('Verify that the Role nav tab only visible with sysadmin user-10', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/users');
        await browser.waitForAngularEnabled(true);
        await browser.waitForAngular();
        await pageUsers.setSearch(data.ADMIN_ROLE.TC_5.USERNAME);
        if (!pageUsers.isTableHasNoValueAfterSearch()) {
            await pageUsers.clickDelete();
            await pageUsers.clickConfirmDelete();
        }
        await pageUsers.clickAddUser();
        await pageAddUser.funcAddUser(data.ADMIN_ROLE.TC_5.USERNAME, data.ADMIN_ROLE.TC_5.PASSWORD);

        await pageNavigation.clickUserProfile();
        await pageNavigation.VerifyLogout();
        await pageLogin.funcLogin(data.ADMIN_ROLE.TC_5.USERNAME, data.ADMIN_ROLE.TC_5.PASSWORD);
        await browser.waitForAngularEnabled(true);
        await pageNavigation.verifyUserProfile(data.ADMIN_ROLE.TC_5.USERNAME);
        await pageNavigation.selectLeftNav(4);
        await expect(pageAdminRoles.verifyMenuRoleDisplayWithUser()).toBeFalsy();

        await pageNavigation.clickUserProfile();
        await pageNavigation.VerifyLogout();
        await pageLogin.funcLogin(data.INFOLOGIN.USERNAME, data.INFOLOGIN.PASSWORD);
        await browser.waitForAngularEnabled(true);
        await pageNavigation.verifyUserProfile(data.INFOLOGIN.USERNAME);
        await pageNavigation.selectLeftNav(5);
        await expect(pageAdminRoles.verifyMenuRoleDisplayWithUser()).toBeTruthy();

    });

});
