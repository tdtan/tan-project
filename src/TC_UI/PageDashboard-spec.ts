import { PageDashboard } from '../PageObject/PageDashboard.po';
import { browser } from 'protractor';
// import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project Dashboard', () => {
    let pageDashboard: PageDashboard;
    // let pageNavigation: PageNavigation;
    let pageError: PageError;

    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageDashboard = new PageDashboard();
        // pageNavigation = new PageNavigation();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
    });

    // 1- Verify that the Dashboard was displayed with "All" option as default when user open the Dashboard screen
    it('Verify that the Dashboard was displayed with "All" option as default when user open the Dashboard screen- 1', async function () {

        await browser.get(browser.baseUrl + '/#/vnfm/dashboard');
        await browser.waitForAngularEnabled(true);
        const txtDefault = await pageDashboard.GetDefaultDisplayAlert();
        await expect(Boolean(txtDefault === data.DASHBOARD.TC_16.TYPE)).toBe(true);
    });

    // 2- Verify that the Dashboard now has 3 chart section: "All", "System Alerts", "VNF Lifecycle Events"
    it('Verify that the Dashboard now has 3 chart section: "All", "System Alerts", "VNF Lifecycle Events"- 2', async function () {

        await browser.waitForAngularEnabled(true);
        await expect(pageDashboard.verifylblAlertDisplay(data.DASHBOARD.TC_16.TYPE)).toBeTruthy();
        await expect(pageDashboard.verifylblAlertDisplay('System Alerts')).toBeTruthy();
        await expect(pageDashboard.verifylblAlertDisplay('VNF Lifecycle Events')).toBeTruthy();
    });

    // 3- Verify that the Table display with both of types of events when user select the "All" section
    it('Verify that the Table display with both of types of events when user select the "All" section"- 3', async function () {

        await browser.waitForAngularEnabled(true);
        await pageDashboard.clickShowHidefilter();
        const txtrecord = await pageDashboard.getRecord();
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE2);
        await browser.waitForAngularEnabled(true);
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE);
        const txtrecord2 = await pageDashboard.getRecord();
        await expect(Boolean(txtrecord === txtrecord2)).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
    });

    // tslint:disable-next-line:max-line-length
    // 4- Verify that the Table display with only System Alerts when user select the "System Alerts" section - the filter should reflect also
    // tslint:disable-next-line:max-line-length
    it('Verify that the Table display with only System Alerts when user select the "System Alerts" section - the filter should reflect also- 4', async function () {

        await pageDashboard.clickRefesh();
        await browser.waitForAngularEnabled(true);
        const txtrecord = await pageDashboard.getRecord();
        const SliceRecord = txtrecord.slice(14);
        const numRecordFilter = Number(SliceRecord.replace(' records', ''));
        await console.log('record 1: ' + numRecordFilter);
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE2);
        const txtType = await pageDashboard.GetTextInTd(3);
        const txtrecord2 = await pageDashboard.getRecord();
        const SliceRecord3 = txtrecord2.slice(14);
        const numRecord = Number(SliceRecord3.replace(' records', ''));
        await console.log('record 2: ' + numRecord);
        await expect(Boolean(txtType === data.DASHBOARD.TC_16.TYPE2)).toBe(true);
        await expect(Boolean(numRecord < numRecordFilter)).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
    });

    // tslint:disable-next-line:max-line-length
    // 5- Verify that the Table display with only VNF Lifecycle Events when user select the "VNF Lifecycle Events" section - the filter should reflect also
    // tslint:disable-next-line:max-line-length
    it('Verify that the Table display with only VNF Lifecycle Events when user select the "VNF Lifecycle Events" section - the filter should reflect also- 5', async function () {

        await pageDashboard.clickRefesh();
        await browser.waitForAngularEnabled(true);
        const txtrecord = await pageDashboard.getRecord();
        const SliceRecord = txtrecord.slice(14);
        const numRecordFilter = Number(SliceRecord.replace(' records', ''));
        await console.log('record 1: ' + numRecordFilter);
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE3);
        const txtType = await pageDashboard.GetTextInTd(3);
        const txtrecord2 = await pageDashboard.getRecord();
        const SliceRecord3 = txtrecord2.slice(14);
        const numRecord = Number(SliceRecord3.replace(' records', ''));
        await console.log('record 2: ' + numRecord);
        await expect(Boolean(txtType === data.DASHBOARD.TC_16.TYPE3)).toBe(true);
        await expect(Boolean(numRecord < numRecordFilter)).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
    });

    // 6- Verify that the Chart was changed to "All" section when user perform "Type" filter in Table with "All" value
    // tslint:disable-next-line:max-line-length
    it('Verify that the Chart was changed to "All" section when user perform "Type" filter in Table with "All" value- 6', async function () {

        // await pageNavigation.selectLeftNav(1);
        // await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 3000);
        // await browser.waitForAngularEnabled(true);
        // browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE);
        const txtEvent = await pageDashboard.GetDefaultDisplayAlert();
        await expect(Boolean(txtEvent === data.DASHBOARD.TC_16.TYPE)).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
    });

    // 7- Verify that the Chart was changed to "System Alerts" section when user perform "Type" filter in Table with "System Alerts" value
    // tslint:disable-next-line:max-line-length
    it('Verify that the Chart was changed to "System Alerts" section when user perform "Type" filter in Table with "System Alerts" value- 7', async function () {

        // browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE2);
        const txtEvent = await pageDashboard.GetDefaultDisplayAlert();
        await expect(Boolean(txtEvent === 'System Alerts')).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
    });

    // tslint:disable-next-line:max-line-length
    // 8- Verify that the Chart was changed to "VNF Lifecycle Events" section when user perform "Type" filter in Table with "VNF Lifecycle Events" value
    // tslint:disable-next-line:max-line-length
    it('Verify that the Chart was changed to "VNF Lifecycle Events" section when user perform "Type" filter in Table with "VNF Lifecycle Events" value- 8', async function () {

        // browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageDashboard.SelectFilter(3, data.DASHBOARD.TC_16.TYPE3);
        const txtEvent = await pageDashboard.GetDefaultDisplayAlert();
        await expect(Boolean(txtEvent === 'VNF Lifecycle Events')).toBe(true);
        await pageDashboard.clickClearButtonToClearTextFilter();
    });

    // 9-Verify that the Chart back to "All" Section when user click on Refresh table button
    it('Verify that the Chart back to "All" Section when user click on Refresh table button- 9', async function () {

        await pageDashboard.clickRefesh();
        const txtEvent = await pageDashboard.GetDefaultDisplayAlert();
        await expect(Boolean(txtEvent === 'All')).toBe(true);
    });
});
