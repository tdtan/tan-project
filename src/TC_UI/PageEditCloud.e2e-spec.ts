import { browser, ExpectedConditions } from 'protractor';
import { PageAddCloud } from '../PageObject/PageAddCloud.po';
import { PageNavigation } from '../PageObject/PageNavigation.po';
import { PageError } from '../PageObject/PageError.po';
import { PageVNFMConfCloudSystems } from '../PageObject/PageVNFMConfCloudSystems.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project page edit cloud', () => {
    let pageAddCloud: PageAddCloud;
    let confCloud: PageVNFMConfCloudSystems;
    let navigation: PageNavigation;
    let pageError: PageError;
    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 80000;
        pageAddCloud = new PageAddCloud();
        confCloud = new PageVNFMConfCloudSystems();
        navigation = new PageNavigation();
        pageError = new PageError();
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await pageError.cliclClearALL();
        }
    });
    // 1. Verify that the Test Version button can identify if the cloud to be created is configured correctly when edit cloud-1
    // tslint:disable-next-line:max-line-length
    it('Verify that the Test Version button can identify if the cloud to be created is configured correctly when edit cloud-1', async function () {
        // await browser.get(browser.baseUrl + '/#/vnfm/cloud');
        await navigation.selectLeftNav(4);
        await navigation.selectLeftNavSecond(4, 1);
        await browser.wait(ExpectedConditions.urlContains('vnfm/cloud'), 3000);
        await browser.waitForAngularEnabled(true);
        await confCloud.clickEditCloud(1);
        await expect(await pageAddCloud.countInputVersionDisable()).toBe(0);
    });

    // 2. Verify that the Test Version button can identify if the cloud to be created is not configured correctly when edit cloud-2
    // tslint:disable-next-line:max-line-length
    it('Verify that the Test Version button can identify if the cloud to be created is not configured correctly when edit cloud-2', async function () {
        await pageAddCloud.inputCloudConfig(5, data.CLOUD_SYSTEM.TC_DataError.KEYSTONE);
        await pageAddCloud.inputCloudConfig(7, data.CLOUD_SYSTEM.TC_DataError.NOVA);
        await pageAddCloud.inputCloudConfig(9, data.CLOUD_SYSTEM.TC_DataError.NEUTRON);
        await pageAddCloud.inputCloudConfig(11, data.CLOUD_SYSTEM.TC_DataError.CINDER);
        await pageAddCloud.inputCloudConfig(13, data.CLOUD_SYSTEM.TC_DataError.IMAGE);
        await expect(await pageAddCloud.countInputVersionDisable()).toBe(0);
        await pageAddCloud.clickCancel();
    });
});
