import { PageReassignVNFs } from '../PageObject/PageReassignVNFs.po';
import { browser } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageAdminVNFMs } from '../PageObject/PageAdminVNFMs.po';
const data = require('../../../e2e/VNFM_DB.e2e.json');

describe('workspace-project PageReassignVNFs', () => {
    let pageReassignVNFs: PageReassignVNFs;
    let pageAdminVNFMs: PageAdminVNFMs;
    let pageError: PageError;
    beforeEach(() => {
        pageReassignVNFs = new PageReassignVNFs();
        pageAdminVNFMs = new PageAdminVNFMs();
        pageError = new PageError();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 170000;
    });

    beforeEach(async () => {
        const result1 = await pageError.verifyMessegeError();
        const result2 = await pageError.verifyMessegeWarning();
        const result3 = await pageError.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await pageError.cliclClearALL();
        }
    });

    // 1 -Verify that user can select the number of displayed columns
    it('Verify that user can select the number of displayed columns-1', async function () {
        await browser.get(browser.baseUrl + '/#/vnfm/vnfms');
        await browser.waitForAngularEnabled(true);
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.ClickShowColumnButton();
        await pageReassignVNFs.selectOptionNamefromShowHideColumnsList(data.VNFM_REASSIGN.TC_1.COLUMNNAME);
        await expect(pageReassignVNFs.verifyThDisplayOnTable(data.VNFM_REASSIGN.TC_1.COLUMNNAME)).toBeFalsy();
        await pageReassignVNFs.ClickShowColumnButton();
        await pageReassignVNFs.selectOptionNamefromShowHideColumnsList(data.VNFM_REASSIGN.TC_1.COLUMNNAME);
        await expect(pageReassignVNFs.verifyThDisplayOnTable(data.VNFM_REASSIGN.TC_1.COLUMNNAME)).toBeTruthy();
        await pageReassignVNFs.ClickCancelButton();
    });
    // 2- Verify that the filter button work properly
    it('Verify that the filter button work properly-2', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.ClickFilterButton();
        await expect(pageReassignVNFs.verifyDrodownFilterDisplayOnTable()).toBeTruthy();
        await pageReassignVNFs.ClickFilterButton();
        await expect(pageReassignVNFs.verifyDrodownFilterDisplayOnTable()).toBeFalsy();
        await pageReassignVNFs.ClickCancelButton();
    });

    // 3- Verify that the dropdown filter and sort functions work properly with VNFM column
    it('Verify that the dropdown filter and sort functions work properly with VNFM column - 3', async function () {
        await browser.waitForAngularEnabled(true);
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        const txtVNFName = await pageReassignVNFs.GetTextInTd(2);
        await pageReassignVNFs.ClickFilterButton();
        await pageReassignVNFs.SelectFilterWithCheckbox(2, txtVNFName);
        const txtRecord = await pageReassignVNFs.GetRecordInTable();
        await expect(Boolean(txtRecord === 'Showing 1 of 1 record')).toBe(true);
        await pageReassignVNFs.verifySortColumn(2);
        await pageReassignVNFs.ClickClearFilterButton();
        await pageReassignVNFs.ClickCancelButton();
    });

    // Verify that the dropdown filter and sort functions work properly with Type column
    it('Verify that the dropdown filter and sort functions work properly with Type column-4', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        const txtVNFType = await pageReassignVNFs.GetTextInTd(3);
        await pageReassignVNFs.SelectFilterWithCheckbox(3, txtVNFType);
        const result = await pageReassignVNFs.isTableHasValueAfterSearch();
        await expect(Boolean(result === true)).toBe(true);
        await pageReassignVNFs.verifySortColumn(3);
        await pageReassignVNFs.ClickClearFilterButton();
        await pageReassignVNFs.ClickCancelButton();
    });

    // Verify that the dropdown filter and sort functions work properly with Version column
    // it('Verify that the dropdown filter and sort functions work properly with Version column-5', async function () {
    // });

    // Verify that the dropdown filter and sort functions work properly with status column
    it('Verify that the dropdown filter and sort functions work properly with status column-6', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        const txtStatus = await pageReassignVNFs.GetTextInTd(5);
        await pageReassignVNFs.SelectFilterWithCheckbox(5, txtStatus);
        const result = await pageReassignVNFs.isTableHasValueAfterSearch();
        await expect(Boolean(result === true)).toBe(true);
        await pageReassignVNFs.verifySortColumn(5);
        await pageReassignVNFs.ClickClearFilterButton();
        await pageReassignVNFs.ClickCancelButton();
    });

    // // Verify that the dropdown filter and sort functions work properly with Preferred vnfm column
    // it('Verify that the dropdown filter and sort functions work properly with Preferred vnfm column-7', async function () {
    // });

    // Verify that the dropdown filter and sort functions work properly with current vnfm column
    it('Verify that the dropdown filter and sort functions work properly with current VNFM vnfm column-8', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        const txtVNFMName = await pageReassignVNFs.GetTextInTd(7);
        await pageReassignVNFs.SelectFilterWithCheckbox(7, txtVNFMName);
        const result = await pageReassignVNFs.isTableHasValueAfterSearch();
        await expect(Boolean(result === true)).toBe(true);
        await pageReassignVNFs.verifySortColumn(6);
        await pageReassignVNFs.ClickClearFilterButton();
        await pageReassignVNFs.ClickCancelButton();
    });

    // Verify that the tables display with maximum 5 entries in one page as default
    it('Verify that the tables display with maximum 5 entries in one page as default-10', async function () {
        browser.driver.navigate().refresh();
        await browser.waitForAngularEnabled(true);
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        const txtNumberRow = await pageReassignVNFs.GetNumberRowDisplayInTable();
        await expect(Boolean(txtNumberRow === '5')).toBe(true);
        const txtRow = await pageReassignVNFs.CountNumberRowInpage();
        await expect(Boolean(txtRow <= 5)).toBe(true);
        await pageReassignVNFs.ClickCancelButton();
    });

    // Verify that the Search function work properly
    it('Verify that the Search function work properly-11', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.ClearTxbSearch();
        await pageReassignVNFs.settextSearch('wrong text');
        const result = await pageReassignVNFs.isTableHasValueAfterSearch();
        await expect(Boolean(result === false)).toBe(true);
        await pageReassignVNFs.ClearTxbSearch();
        await pageReassignVNFs.settextSearch('ssbc');
        const result1 = await pageReassignVNFs.isTableHasValueAfterSearch();
        await expect(Boolean(result1 === true)).toBe(true);
        await pageReassignVNFs.ClickCancelButton();
    });

    // Verify that the Clear table button work properly
    it('Verify that the Clear table button work properly-12', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.ClearTxbSearch();
        await pageReassignVNFs.settextSearch('wrong text');
        await pageReassignVNFs.ClickClearButtonSearch();
        const txtSearch = await pageReassignVNFs.getAttributeSearch();
        await expect(Boolean(txtSearch === '')).toBe(true);
        await pageReassignVNFs.ClickCancelButton();
    });

    // // Verify that the user can choose the number of entries displayed in one page
    it('Verify that the user can choose the number of entries displayed in one page-11', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.SelectNumberRowDisplay(10);
        const numberRow1 = await pageReassignVNFs.CountNumberRowInpage();
        await expect(Boolean(numberRow1 <= 10)).toBe(true);
        await pageReassignVNFs.SelectNumberRowDisplay(15);
        const numberRow2 = await pageReassignVNFs.CountNumberRowInpage();
        await expect(Boolean(numberRow2 <= 15)).toBe(true);
        await pageReassignVNFs.ClickCancelButton();
    });

    // // Verify that the user can move to specified page when click on the page number
    // it('Verify that the user can move to specified page when click on the page number-12', async function () {

    // });

    // Verify that the Select check box can work properly
    it('Verify that the Select check box can work properly-14', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.ClickAllCheckboxVNF();
        const txt_Number = await pageReassignVNFs.CountCheckboxChecked();
        await expect(Boolean(txt_Number >= 2)).toBe(true);
        await pageReassignVNFs.ClickCancelButton();
    });

    // Verify that the user can exit the Reassign VNFs form when click on Cancel button
    it('Verify that the user can exit the Redistribute VNFs form when click on Cancel button-17', async function () {
        await pageAdminVNFMs.clickbtnSelectActions();
        await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
        await pageReassignVNFs.ClickCancelButton();
        await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeFalsy();
    });

    // // Verify that the Clear Filter button work properly
    // it('Verify that the Clear Filter button work properly-13', async function () {
    //     await pageAdminVNFMs.clickbtnSelectActions();
    //     await pageAdminVNFMs.clickbtnOptionActions('Reassign VNFs');
    //     await expect(pageReassignVNFs.verifyPopupReassingPresent()).toBeTruthy();
    //     const txtVNFName = await pageReassignVNFs.GetTextInTd(2);
    //     await pageReassignVNFs.ClickFilterButton();
    //     await pageReassignVNFs.SelectFilterWithCheckbox(2, txtVNFName);
    //     const txtRecord = await pageReassignVNFs.GetRecordInTable();
    //     await expect(Boolean(txtRecord === 'Showing 1 of 1 record')).toBe(true);
    //     await pageReassignVNFs.ClickClearFilterButton();
    //     const txtInFilter = await pageReassignVNFs.GetTextFilter(2);
    //     await expect(Boolean(txtInFilter === 'All')).toBe(true);
    //     await pageReassignVNFs.ClickCancelButton();
    // });

});
