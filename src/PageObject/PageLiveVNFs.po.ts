import { element, by, ExpectedConditions, browser } from 'protractor';

const txbsearch = by.xpath('//app-vnfs-list/rbn-common-table//div[2]/div/div/input');
const btnCancel = by.xpath('//*[@id="relocate"]//button[1]');
const btnminmax = by.xpath('//*[@id="panel-vnf"]/div/div[1]/a');
const btnClearnSearch = by.xpath('//app-vnfs-list/rbn-common-table//div/div/p-button[@icon="fa fa-times"]');
const btnClearFilter = by.xpath('//app-vnfs-list//table//p-button[@icon="fa fa-times"]');
const btnRefesh = by.xpath('//app-vnfs-list/rbn-common-table//div/div/p-button[@icon="fa fa-refresh"]');
const btnShowHideColunm = by.xpath('//app-vnfs-list/rbn-common-table//p-button[@icon="fa fa-cog"]');
const btnShowHideFilter = by.xpath('//app-vnfs-list/rbn-common-table//p-button[@icon="fa fa-sliders"]');
const rowTable = by.xpath('//app-vnfs-list/rbn-common-table//table/tbody/tr');
const lblNumberPage = by.xpath('//*[@id="vnfTable"]//p-paginator//p-dropdown//div[2]/span');

const txtRecord = by.xpath('//app-vnfs-list/rbn-common-table//p-paginator/div/div//span');
const txtNoRecord = by.xpath('//app-vnfs-list/rbn-common-table//*[contains(text(),"No records found")]');
const btnCloseShowEvent = by.xpath('//app-vnf-events//p-accordiontab/div[1]//p-button[@icon="fa fa-times"]');
const btnDate = by.xpath('//table/thead/tr[2]/th[9]//button[@icon="fa fa-calendar"]');
const btnClearDate = by.xpath('//button[contains(span,"Cancel")]');
const btnReset = by.xpath('//button[contains(span,"Reset")]');
const txtpageheader = by.css('#container-common-table p-accordiontab p-header span');

const btnCloseGetKey = by.xpath('//*[@id="getkey"]/div/div[3]/p-footer/button');
const btnGetKey = by.xpath('//*[@id="getkey"]/div/div[2]/div/a');
const btnConfirmReenable = by.xpath('//*[@id="reEnable"]/div/div[3]/p-footer/div/button[2]');
const btnConfirmResync = by.xpath('//*[@id="resync"]/div/div[3]/p-footer/div/button[2]');
const btnConfirmMaintenance = by.xpath('//*[@id="maintenance"]/div/div[3]/p-footer/div/button[2]');
const btnConfirm = by.xpath('//button[@icon="pi pi-pw pi-check"]');

const btnClearAll = by.xpath('//button[@label="Clear All"]');
const lcb_divError = by.xpath('/html/body/app-root/rbn-message/p-toast/div/p-toastitem[1]/div/div');
const lcb_divWarning = by.xpath('/html/body/app-root/rbn-message/p-toast/div/p-toastitem/div/div');
const lcb_divInfo = by.xpath('/html/body/app-root/rbn-message/p-toast/div/p-toastitem/div');

const btnSelectActions = by.xpath('//app-vnfs-list/rbn-common-table//td[13]/p-dropdown');
const txtAction = by.xpath('//app-vnfs-list//tr[1]/td[13]/p-dropdown//label');
const txtpreference = by.xpath('//*[@id="relocate"]/div/div[1]/span');
const txtpreferredVNFM = by.xpath('//*[@id="redistContainer"]/div/div[1]/div[2]');
const txtupgrade = by.xpath('//*[@id="upgrade"]/div/div[1]/span');
const txtCancelUpgrade = by.xpath('//*[@id="upgradeCancel"]/div/div[1]/span');
const btnOKError = by.xpath('//*[@id="#dialogError"]//button');

export class PageLiveVNFs {
    // get text Onboard in page header
    async getTextHeaderPage() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtpageheader)), 3000);
        const textOnb = await element(txtpageheader).getText();
        return textOnb;
    }
    // get text PreferredVNFM
    async getTextPreferredVNFM() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtpreferredVNFM)), 3000);
        const textOnb = await element(txtpreferredVNFM).getText();
        return textOnb;
    }

    // get text Upgrade
    async getTextUpgrade() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtupgrade)), 3000);
        const textOnb = await element(txtupgrade).getText();
        return textOnb;
    }

    // get text Upgrade Cancel
    async getTextCancelUpgrade() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtCancelUpgrade)), 3000);
        const textOnb = await element(txtCancelUpgrade).getText();
        return textOnb;
    }

    // Click button select version upgrade
    async clickOptionSelectVersionUpgrade(version: string) {
        await console.log('Start click select version.');
        const optVersion = by.xpath('//*[@id="upgrade"]/div/div[2]/div[3]/p-dropdown');
        const sel_version = by.xpath('//li[@aria-label="' + version + '"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(optVersion)), 3000);
        await element(optVersion).click();
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sel_version)), 3000);
        await element(sel_version).click();

        await console.log('Done.');
    }

    // get text Preference
    async getTextPreference() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtpreference)), 3000);
        const textOnb = await element(txtpreference).getText();
        return textOnb;
    }

    // Click button select Preference VNFM
    async clickOptionSelectPerference(num: number) {
        await console.log('Start click preference.');
        const optpreference = by.xpath('//*[@id="redistContainer"]/div/div[2]/div[2]/p-dropdown');
        const selperference = by.xpath('//*[@id="redistContainer"]//p-dropdownitem[' + num + ']');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(optpreference)), 3000);
        await element(optpreference).click();
        await browser.wait(ExpectedConditions.elementToBeClickable(element(selperference)), 3000);
        await element(selperference).click();

        await console.log('Done click preference');
    }

    // Click button Cancel
    async clickCancel() {
        await console.log('Start click Cancel.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 10000);
        await element(btnCancel).click();
        await console.log('Done click Cancel');
    }

    // Set text Search in Available Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbsearch)), 4000);
        await element(txbsearch).clear();
        await element(txbsearch).sendKeys(user);
        console.log('Done Input search.');
    }

    // Click row in table
    async clickRowInTable() {
        await console.log('Start click row in table.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        await element(rowTable).click();
        await console.log('Done click row in table');
    }

    // Click button minimized
    async clickMinimiz() {
        await console.log('Start click Minimined.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnminmax)), 3000);
        await element(btnminmax).click();
        await console.log('Done click Minimined');
    }

    // Click button close Get Key
    async clickCloseGetKey() {
        await console.log('Start click close Get Key.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCloseGetKey)), 3000);
        await element(btnCloseGetKey).click();
        await console.log('Done click close Get Key');
    }

    // Click button confirm option Re-enable
    async clickConfirmReEnable() {
        await console.log('Start click confirm option Re-enable.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmReenable)), 3000);
        await element(btnConfirmReenable).click();
        await browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(element(btnConfirmReenable))), 5000);
        await console.log('Done click confirm option Re-enable');
    }

    // Click button confirm option Re-sync
    async clickConfirmReSync() {
        await console.log('Start click confirm option Re-sync.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmResync)), 3000);
        await element(btnConfirmResync).click();
        await browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(element(btnConfirmResync))), 5000);
        await console.log('Done click confirm option Re-sync');
    }
    // Click button confirm option Maintenance
    async clickConfirmMaintenance() {
        await console.log('Start click confirm option Maintenance.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmMaintenance)), 5000);
        await element(btnConfirmMaintenance).click();
        await console.log('Done click confirm option Maintenance');
    }

    // Click button confirm upgrade
    async clickConfirm() {
        await console.log('Start click confirm Upgrade.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirm)), 3000);
        await element(btnConfirm).click();
        await console.log('Done click confirm Upgrade');
    }

    // Click button date
    async clickbuttonDate() {
        await console.log('Start click Date.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnDate)), 3000);
        await element(btnDate).click();
        await console.log('Done click Date');
    }

    // Click reset button
    async clickResetButton() {
        await console.log('Start click reset button.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnReset)), 3000);
        await element(btnReset).click();
        await console.log('Done click reset button');
    }

    // Click cancel date button
    async clickCancelDateButton() {
        await console.log('Start click cancel date button.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearDate)), 3000);
        await element(btnClearDate).click();
        await console.log('Done click cancel date button');
    }


    // Click button Refesh
    async clickRefesh() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefesh)), 3000);
        await element(btnRefesh).click();
        await console.log('Done click Refesh');
    }

    // Click button Clear input Search
    async clickClearSearchVNFTable() {
        await console.log('Start click lear Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearnSearch)), 3000);
        await element(btnClearnSearch).click();
        await console.log('Done.');
    }

     // Click button Clear Filter
     async clickClearFilterButton() {
        await console.log('Start click Clear Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearFilter)), 5000);
        await element(btnClearFilter).click();
        await console.log('Done click Clear Filter');
    }

    // Click button Show Hide Filter
    async clickShowHideFilterButton() {
        await console.log('Start click Show Hide Filter');
        await element(btnShowHideFilter).click();
        await console.log('Done click Show Hide Filter');
    }

    // Click button Show Hide Column
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await element(btnShowHideColunm).click();
        await console.log('Done click Show Hide Column');
    }

    // Click button Get Key
    async clickGetKey() {
        await console.log('Start click Get key');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnGetKey)), 4000);
        await element(btnGetKey).click();
        await console.log('Done click Get key');
    }

    // Click button cancel show event
    async clickCloseShowEvent() {
        await console.log('Start click Close Show Event');
        await element(btnCloseShowEvent).click();
        await console.log('Done');
    }

    // Click Close show hide column
    async clickCloseShowHideCloumn() {
        const btnClose = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await console.log('Start click close');
        await element(btnClose).click();
        await console.log('Done');
    }

    // Click Select Actions
    async clickSelectActions() {
        await console.log('Start click select actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectActions)), 3000);
        await element(btnSelectActions).click();
        await console.log('Done');
    }

    // Click option Select Actions
    async clickOptionSelectActions(optActions: string) {
        const btnOptActions = by.xpath('//li[contains(option,"' + optActions + '")]');
        await console.log('Start click select actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptActions)), 10000);
        await element(btnOptActions).click();
        await console.log('Done');
    }


    // Get text record in table Live VNF
    async getRecord() {
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 8000);
        const _record = await element(txtRecord).getText();
        await console.log('Text record: ' + _record);
        await console.log('Done get record');
        return _record;
    }

    // Get text in Td display
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//app-vnfs-list/rbn-common-table//tr[1]/td[' + numTd + ']');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;
    }

    // Get text Destroy
    async verifyTextDestroy(text: string) {
        const txttd = by.xpath('//app-vnfs-list/rbn-common-table//tr[1]/td[7]//*[contains(span,"Destroy")]');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 5000);
        await expect(element(txttd).isPresent()).toBeTruthy();
    }

    // Select date in event table
    async selectDateInTable(strday: string) {
        await console.log('Start select date ');
        const sel3 = element(by.xpath('//button[contains(span,"' + strday + '")]'));
        sel3.click();
    }

    // Select number page on table in Live VNF Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }

    // Select filter in table Live VNF
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {
        const filtercolumn = by.xpath('//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 10000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();
        await browser.waitForAngularEnabled(true);

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 5000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 5000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Live VNF page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select Option from Show/ Hide columns List in Live VNF Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btnClose = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await element(btnClose).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Select Option cloumn show or hide in table live VNF
    async selectOptionCloumnShowOrhide(strOptionName: string) {
        await console.log('Start Select Option cloumn show or hide in table live VNF');
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        await console.log('Done selecting Option ' + strOptionName + ' from Show Hide Columns List');
    }

    // Verify element display in form
    async verifyEventDisplayInForm(strEventItem: string) {
        await console.log('Start verify event item is option in Function: ' + strEventItem);
        const elmDisplay = by.xpath('//li[contains(option,"' + strEventItem + '")]');
        // await expect(browser.wait(ExpectedConditions.visibilityOf(element(elmDisplay)), 5000)).toBeTruthy();
        await expect(element(elmDisplay).isPresent()).toBeTruthy();
    }

    // Verify element display in form
    async verifyFormDisplayed() {
        await console.log('Start verify event form is displayed');
        const elmDisplay = by.xpath('/html/body/div/div/ul');
        await browser.sleep(6000);
        await expect(element(elmDisplay).isPresent()).toBeTruthy();
    }

    // Verify element disable
    async verifyGetKeyInForm() {
        await console.log('Start verify lick get key in form enable');
        const elmDisplay = by.xpath('//*[@id="getkey"]/div/div[2]/div/a');
        if (element(elmDisplay).isEnabled()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify element disable
    async verifyEventItemDisable(strEventItem: string) {
        await console.log('Start verify event item ' + strEventItem + ' disable on list option');
        const elmDisplay = by.xpath('//li[contains(option,"' + strEventItem + '")]');
        if (element(elmDisplay).isEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    // Verify Attribute in Search
    async VerifyAttributeInSearch() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txbsearch)), 3000);
        const value = await element(txbsearch).getAttribute('value');
        expect(value).toEqual('');
    }

    // Click button Ok when get key error
    async clickOkWhenGetKeyError() {
        await console.log('Start click button Ok when Get key Error');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOKError)), 4000);
        await element(btnOKError).click();
        await console.log('Done click button Ok when Get key Error');
    }

    // Verify element display in form
    async verifyDisplayPupopErrorGetKey() {
        await console.log('Start verify Popun display when Get key Error');
        return await element(btnOKError).isPresent();
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Verify display or undisplay table when click minimized
    async verifyTableAfterClickMinimized() {
        await console.log('Start verify table');
        await this.clickMinimiz();
        expect(await element(rowTable).isDisplayed()).toBeFalsy();

        await this.clickMinimiz();
        expect(await element(rowTable).isDisplayed()).toBeTruthy();
        await console.log('Done verify table');
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//table/thead/tr[1]/th[text()=" ' + strOptionName + ' "]');
        if (await element(tableTH).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//table/thead/tr[1]/th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//table/thead/tr[1]/th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//table/thead/tr[1]/th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click().then(function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click().then(function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');

    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Get number of columns in current search result table
    async getNumberOfColumnsInSearchTable(strTable: string) {
        let _numberOfColumn: number;
        _numberOfColumn = 0;
        const _numOfMaximumColumns = 11;
        await console.log('Start checking if we have any table name equals Text value?');
        for (let i = 1; i <= _numOfMaximumColumns; i++) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            if (await element(_strColumnLocator).isPresent()) {
                _numberOfColumn = i;
            }
        }
        return _numberOfColumn;

    }

    // Get column number based on column name in VFM Settings Page
    async getColumnNumberBasedOnColumnName(strTable: string, columnname: string) {
        let _numCurrentColumns: any;
        let _columnnum: number;
        _columnnum = 0;
        _numCurrentColumns = await this.getNumberOfColumnsInSearchTable(strTable);
        await console.log('Start getting column number based on column Name');
        for (let i = 1; i <= _numCurrentColumns; i++) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            await element(_strColumnLocator).getText().then(function (text) {
                if (text === columnname) {
                    _columnnum = i;
                }
            });
        }
        if (_columnnum === 0) {
            await console.log('No found ' + columnname + ' in search table');
        } else {
            await console.log('Column ' + _columnnum + ' has ' + columnname + ' as name');
        }
        return _columnnum;
    }

    // Get row number has Column Name Equals Text Value in Onboarded VNFs Page
    async getNumberofRowHasValueofColumnNameEqualsTextValue(strTable: string, intColumnNumber: number, strTextValue: string) {
        let _rownum: number;
        _rownum = 0;
        const record = by.xpath('//*[@id="' + strTable + '"]//p-paginator//span');
        const numrecord = await element(record).getText();
        const _intSearchSheetRowNum = Number(numrecord.slice(0, 1));
        await console.log('Current Search table has ' + _intSearchSheetRowNum + ' row(s)');
        for (let i = 1; i <= _intSearchSheetRowNum; i++) {
            const TextValueLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + i + ']/td[' + intColumnNumber + ']');
            await element(TextValueLocator).getText().then(function (text) {
                if (text === strTextValue) {
                    _rownum = i;
                }
            });

        }
        if (_rownum === 0) {
            await console.log('No found Row has Column Name Equals ' + strTextValue + ' in search table');
        } else {
            await console.log('Row ' + _rownum + ' has value of Column Name equals ' + strTextValue);
        }
        return _rownum;
    }

    // Click Function On Row Has Value of ColumnName Equals TextValue
    // tslint:disable-next-line:max-line-length
    async clickActionOnRowHasValueofColumnNameEqualsTextValue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, functionchoose: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        const result1 = await this.verifyMessegeError();
        const result2 = await this.verifyMessegeWarning();
        const result3 = await this.verifyMessegeInfo();
        if ( result1 === true || result2 === true || result3 === true) {
          await this.cliclClearALL();
        }
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        const _function = by.xpath('//li[contains(option,"' + functionchoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_function)), 4000);
        await element(_function).click();
        await console.log('Done');
    }

    // Click Functions On Row
    // tslint:disable-next-line:max-line-length
    async clickFunctionOnRowHasValueofColumnName(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        await console.log('Done');
    }

    // Click Link VNF On Row
    // tslint:disable-next-line:max-line-length
    async clickLinkOnRowHasValueofColumnName(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']');
        await element(btn_FunctionLocator).click();
        await console.log('Done');
    }

    // get Text On Table Have Text value
    // tslint:disable-next-line:max-line-length
    async getTextOnTableHaveTextvalue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']');
        const gettxtFunction = await element(btn_FunctionLocator).getText();
        await console.log('Done');
        return gettxtFunction;
    }

    async getTextActionDefault() {
        await console.log('get text Actions');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtAction)), 3000);
        const getTextTd = await element(txtAction).getText();
        return getTextTd;
    }

    // tslint:disable-next-line:max-line-length
    // async waitVNFDeployReady(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, strTimeOutType: string) {
    //     let TimeOut: number;
    //     let TimeOutMax: number;
    //     let _cellTextLocalter: string;
    //     let b: boolean;

    //     _cellTextLocalter = await this.getTextOnTableHaveTextvalue(strTable, columnname, strTextValue, strFunctionColumnName);
    //     b = true;
    //     TimeOut = 0;
    //     TimeOutMax = 1200000;
    //     do {
    //         if (_cellTextLocalter === 'Failed') {
    //             await console.log('Current ' + strTextValue + ' VNF state is: ' + _cellTextLocalter + '. We will break loop.');
    //             b = false;
    //             return b;
    //         } else {
    //             if (_cellTextLocalter === 'Ready') {
    //                 await console.log('Current ' + strTextValue + ' VNF state is: ' + _cellTextLocalter + '.');
    //                 b = true;
    //                 return b;
    //             } else {
    //                 await console.log('Current ' + strTextValue + ' VNF state is: ' + _cellTextLocalter + '.');
    //                 await browser.sleep(10000);
    //                 await this.setSearch(strTextValue);
    //                 TimeOut = TimeOut+20000;
    //                 await console.log('Wait for ready ' + TimeOut + ' s.');
    //                 if ( TimeOut === TimeOutMax) {
    //                     await console.log('break loop due to max time out');
    //                     b = false;
    //                     return b;
    //                 }
    //             }
    //         }

    //     } while ((_cellTextLocalter === 'Ready') && (TimeOut <= TimeOutMax));
    //     await console.log('Done');
    //     return b;

    // }
    async waitVNFReady(row: number, column: number, statusName: string, timeWait: number, messageTimeOut: string) {
        let result = true;
        await console.log('start await status: ', statusName);
        await browser.waitForAngularEnabled(true);
        await browser.wait(async () => {
            console.log('working');
            const statusText = await this.getTextCellTable(row, column);
            await console.log('status table: ', statusText);
            if (statusText === 'Failed') {
                result = false;
                await console.log('Current VNF status is ' + statusText);
                return true;
            } else {
                if (statusText === statusName) { // If status = statusName then give the pass test case
                    return true;
                } else {
                    await browser.sleep(10000); // distance of check time is 1s.
                    return false; // If status !== statusName then continue checking
                }
            }
        }, timeWait, messageTimeOut);
        return result;
    }

    async getTextCellTable(row: number, column: number) {
        // tslint:disable-next-line:max-line-length
        const statusText = await element(by.xpath('//app-vnfs-list/rbn-common-table//tr[' + row + ']/td[' + column + ']')).getText();
        return statusText;
    }

    // Verify Messege Error
    async verifyMessegeError() {
        await console.log('Start checking if display messege error');
        if (await element(lcb_divError).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Messege Warning
    async verifyMessegeWarning() {
        await console.log('Start checking if display messege warning');
        if (await element(lcb_divWarning).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Messege Info
    async verifyMessegeInfo() {
        await console.log('Start checking if display messege Info');
        if (await element(lcb_divInfo).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Click clear All
    async cliclClearALL() {
        await console.log('Start click Clear All');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearAll)), 5000);
        await element(btnClearAll).click();
        await console.log('Done');
    }
}
