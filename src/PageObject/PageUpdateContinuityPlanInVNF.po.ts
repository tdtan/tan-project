import { element, by } from 'protractor';

export class PageUpdateContinuityPlanInVNF {

    txt_ContinuityPlanText = element(by.xpath('//*[@id="upgrade"]//span'));
    btnNo                  = element(by.xpath('//button[@icon="fa fa-times"]'));
    btnYes                 = element(by.xpath('//button[@icon="pi pi-pw pi-check"]'));
    drp_Continuity         = element(by.xpath('//*[text()="Continuity Plan"]/following-sibling::div/p-dropdown'));
    txt_ContinuityOption   = element(by.xpath('//*[text()="Continuity Plan"]/following-sibling::div/p-dropdown//label'));
    txt_OptionInDropdown   = element.all(by.xpath('//p-dropdownitem//span'));

    // get text Continuity Plan In Pupop
    async getTextContinuityPlanPupop() {
        await console.log('Start get text Continuity Plan In Pupop');
        const textContinuity = await this.txt_ContinuityPlanText.getText();
        return textContinuity;
    }

    // get text Continuity Plan Option
    async getTextContinuityPlanOption() {
        await console.log('Start get text Continuity Option');
        const textContinuity = await this.txt_ContinuityOption.getText();
        return textContinuity;
    }

    // Click button Upgrade
    async clickbtnYes() {
        await console.log('Start click Yes Button');
        await this.btnYes.click();
        await console.log('Done click Yes Button');
    }

    // Click button Cancel
    async clickbtnNo() {
        await console.log('Start click No Button');
        await this.btnNo.click();
        await console.log('Done click No Button');
    }

    // Click Continuity plan dropdown
    async clickContinuityPlanDropdown() {
        await console.log('Start click Continuity plan dropdown');
        await this.drp_Continuity.click();
        await console.log('Done click Continuity plan dropdown');
    }

    // Click Choose Option Continuity plan
    async clickChooseOptionContinuityPlan(StrOption: string) {
        await console.log('Start click Choose Option Continuity plan');
        const txb_Option = by.xpath('//li[contains(span,"' + StrOption + '")]');
        await element(txb_Option).click();
        await console.log('Done click Choose Option Continuity plan');
    }

    // Get text option in dropdown on form
    async getTextOptionInDrodown() {
        await console.log('Start get text option in dropdown on form');
        let lsName: string [] = [];
        const sizeName  = await this.txt_OptionInDropdown.count();
        await console.log('size: ', sizeName);
        for (let index = 1; index <= sizeName; index++) {
            const list1 = await element(by.xpath('//p-dropdownitem[' + index + ']//span')).getText();
            await console.log('Name continuity plan: ', list1);
            if (!lsName.includes(list1)) {
                lsName.push(list1);
            }
        }
        await lsName.sort();
        return lsName;
    }


}
