
import { element, by, browser, ExpectedConditions } from 'protractor';

// tslint:disable-next-line:max-line-length
const linksys = by.xpath('//span[@class="ui-menuitem-text"]//following-sibling::span');
const txtlogout = '//p-menubarsub/ul/li[2]/a';
const txtUserprofile = '//p-menubar/div/p-menubarsub/ul/li/a/span[2]';

const txtVersion = by.xpath('/html/body//app-topheader//rbn-headeruser/p-dialog[2]/div/div[1]/span');
const headerVersion = by.css('p-menubarsub ul li p-menubarsub ul li:nth-child(1) a');
// const btnCloseVersion = by.css('p-dialog a[role="button"]');
const headerHelp = by.xpath('//p-menubarsub/ul/li/a/span[text()="Help"]');
// const leftnav = 'div.ui-panelmenu-panel:nth-child(4) p-panelmenusub ul li:nth-child(1)';

export class PageNavigation {

  // Click User Profile in Header
  async clickUserProfile() {
    console.log('Start click User Profile');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(linksys)), 7000);
    await element(linksys).click();
    console.log('Done');
  }



  // Select Left Navigation
  /**
   * paramater: lever
   */
  async selectLeftNav(level: number) {
    console.log('Start click menu have number: ' + level);
    const sel = 'div.ui-panelmenu-panel:nth-child(' + level + ')';
    // const sel = 'rbn-leftnav p-panelmenu div:nth-child(' + level + ') a';
    await browser.wait(ExpectedConditions.elementToBeClickable(element(by.css(sel))), 7000);

    await element(by.css(sel)).click();
    const menuName = await element(by.css(sel)).getText();
    await console.log('Current menu: ' + menuName);
    await console.log('Done');
  }

  // Select Left Navigation Second
  /**
   * paramater: lever
   */
  async selectLeftNavSecond(levelParent: number, level: number) {
    console.log('Start click menu second have number: ' + level);
    const sel = 'div.ui-panelmenu-panel:nth-child(' + levelParent + ') p-panelmenusub ul li:nth-child(' + level + ')';
    await browser.wait(ExpectedConditions.elementToBeClickable(element(by.css(sel))), 10000);
    await element(by.css(sel)).click();
    console.log('Done');
  }

  // Verify Logout function in header
  async VerifyLogout() {
    console.log('Start click Logout');
    await element(by.xpath(txtlogout)).click();
    const btnYes = 'button[icon="pi pi-pw pi-check"]';
    await element(by.css(btnYes)).click();
    await expect(browser.wait(ExpectedConditions.urlContains('/login'), 9000)).toBeTruthy();
    console.log('Done');
  }

  // Verify display version in header
  async VerifyVersion(_Version: any) {

    console.log('Start Version');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(headerVersion)), 5000);
    await element(headerVersion).click();
    await console.log('get text in version form');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(txtVersion)), 5000);
    await expect(element(txtVersion).getText()).toEqual(_Version);
    const btnClose = 'p-dialog a[role="button"]';
    await element(by.css(btnClose)).click();
    await console.log('Done');
  }

  // Verify display help in headerr

  async VerifyHelp() {
    console.log('Start click Help ');
    await element(headerHelp).click();

    browser.switchTo().alert().then(function (alert) {

      alert.accept();
    });
    await console.log('Done');
  }

  // Verify User current log

  async verifyUserProfile(user: any) {
    console.log('Start check User Profile');
    // await browser.sleep(2000);
    await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath(txtUserprofile))), 5000);
    const txtUser = await element(by.xpath(txtUserprofile)).getText();
    await expect(txtUser).toContain(user);
    await console.log('Current User: ' + txtUser);
    await console.log('Done');
  }

  // Verify show menu second
  async verifyShowMenuSecond(idmenu: any) {

    await console.log('Verify show menu second after click menu');
    const txtmenuSecond = await by.xpath('//*[@id="' + idmenu + '"]/span[2]');
    expect(await element(txtmenuSecond).isDisplayed()).toBeTruthy();
    const menuSecond = await element(txtmenuSecond).getText();

    await console.log('Menu Current is: ' + menuSecond);

    await console.log('Done');
  }

}

