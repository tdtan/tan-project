import { element, by, ExpectedConditions, browser, protractor } from 'protractor';

const btnSaveTenant = by.xpath('//*[@id="panel-tenant-action"] //button[@icon="pi pi-pw pi-check"]');
const btnCancel = by.xpath('//*[@id="panel-tenant-action"] //button[@icon="fa fa-times"]');
const txtEditTenant = by.xpath('//*[@id="panel-tenant-action"]//h4[text()="Tenant Usage"]');

export class PageEditTenant {


    // Verify show text edit tenant
  async verifyShowTextEditTenant() {
    console.log('Start verify show text add tenant.');
    await expect(element(txtEditTenant).isPresent()).toBeTruthy();
    await console.log('Done.');
  }

  // Verify text edit tenant not display
  async verifyEditCloudNoPresent() {
    console.log('Start verify text add tenant not present.');
    await expect(element(txtEditTenant).isPresent()).toBeFalsy();
    await console.log('Done.');
  }



  // Verify previous information of cloud is displayed
  async verifyInfoDisplayed(txtConfig: string) {
    const InfoDisplayed = by.css('#panel-tenant-action input[formcontrolname="' + txtConfig + '"].ng-valid');
    await console.log(InfoDisplayed);
    await console.log('Start verify previous information of cloud is displayed');
    await expect(element(InfoDisplayed).isPresent()).toBeTruthy();
    await console.log('Done verify previous information of cloud is displayed');
  }



  // Set input tenant config in edit tenant Page
  async inputTenantInfo(level: number, info: string) {
    const tenant = by.xpath('//*[@id="panel-tenant-action"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    await console.log('Start input Tenant Info');
    await browser.wait(ExpectedConditions.visibilityOf(element(tenant)), 3000);
    await element(tenant).clear();
    await element(tenant).sendKeys(info);
    await console.log('Done');
  }

  // Set input tenant config in edit tenant Page
  async ClearInfoTenantEdit(level: number) {
    const tenant = by.xpath('//*[@id="panel-tenant-action"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    await console.log('Start Clear Tenant Info');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(tenant)), 3000);
    await element(tenant).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    await element(tenant).sendKeys(protractor.Key.BACK_SPACE);
    await console.log('Done');
  }


  // Click associated Cloud in edit tenant Page
  async clickAssociatedCloud() {
    const Associated  = by.css('#panel-tenant-action div.ui-grid-row p-dropdown');
    console.log('Start click associated Cloud');
    await browser.wait(ExpectedConditions.visibilityOf(element(Associated)), 2000);
    await element(Associated).click();
    console.log('Done');
  }


  // Select associated Cloud in edit tenant Page
  async SelectCloud(cloud: string) {
    const AssociatedCloud = by.xpath('//p-dropdown//span[text()="' + cloud + '"]');
    console.log('Start Select associated Cloud');
    await browser.wait(ExpectedConditions.visibilityOf(element(AssociatedCloud)), 2000);
    await element(AssociatedCloud).click();
    console.log('Done');
  }



    // Function to convert RGB/RGBA to hex:
  async convertRgb2hex(rgb: any) {
    await console.log('Start convert RGB/RGBA to hex');
    rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
    return (rgb && rgb.length === 4) ? '#' +
      ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
      ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
      ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
  }


  // Get color
  async getColor(num: number) {
    const color = by.css('div.content-rows:nth-child(' + num + ') p-progressbar div.ui-progressbar-value');
    await console.log('Start get color');
    const valueColor = await element(color).getCssValue('background-color');
    console.log('mã màu RGB/RGBA: ', valueColor);
    return valueColor;
  }


    // Get Usage Number
  async getUsageNumber(num: number) {
    const percent = by.css('div.content-rows:nth-child(' + num + ') p-progressbar div.ui-progressbar-label');
    await console.log('Start get Usage Number');
    await browser.wait(ExpectedConditions.visibilityOf(element(percent)), 3000);
    const getnum = await element(percent).getText();
    const number = Number(getnum.slice(0, 1));
    console.log('number value: ', number);
    await console.log('Done');
    return number;
  }


    // Get Usage Number
  async getUsageNumber2(num: number) {
    const percent = by.css('div.content-rows:nth-child(' + num + ') p-progressbar div.ui-progressbar-label');
    await console.log('Start get Usage Number 2');
    await browser.wait(ExpectedConditions.visibilityOf(element(percent)), 3000);
    const getnum = await element(percent).getText();
    const number = Number(getnum.slice(0, 2));
    console.log('number2 value: ', number);
    await console.log('Done');
    return number;
  }


    // Click tenant in edit tenant Page
  async clickTenant(level: number) {
    const tenant = by.xpath('//*[@id="panel-tenant-action"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    console.log('Start click tenant');
    await browser.wait(ExpectedConditions.visibilityOf(element(tenant)), 2000);
    await element(tenant).click();
    console.log('Done');
  }


    // Verify "missing required info" message display
  async verifyMissingRequiredInfo(message: string) {
    const txtMessage = by.xpath('//div[text()=" ' + message + ' is required"]');
    await console.log('Start verify "missing required info" message display');
    // console.log(missingMessage);
    return await element(txtMessage).isPresent();
  }

    // Verify usage info" message display
  async verifyUsageInfo(message: string) {
    const UsageInfo = by.xpath('//div[text()="' + message + '"]');
    await console.log('Start verify "missing required info" message display');
    await console.log(UsageInfo);
    await expect(element(UsageInfo).isPresent()).toBeTruthy();
    await console.log('Done verify "missing required info" message display');
  }


    // Click button Cancel in edit tenant Page
  async clickCancel() {
    console.log('Start click Cancel');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnCancel)), 2000);
    await element(btnCancel).click();
    console.log('Done');
  }


  // Click button Save tenant in edit tenant Page
  async clickSaveTenant() {
    console.log('Start click Save tenant');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnSaveTenant)), 2000);
    await element(btnSaveTenant).click();
    console.log('Done');
  }



  // Verify Empty form is displayed when user click on edit tenant
  async verifyEmptyDisplayed(txtConfig: string) {
    const emptyDisplayed = by.css('#panel-tenant-action input[formcontrolname="' + txtConfig + '"].ng-invalid');
    await console.log('Start verify Empty form is displayed when user click on edit tenant');
    await expect(element(emptyDisplayed).isPresent()).toBeTruthy();
    await console.log('Done verify empty form is displayed when user click on Add tenant');
}



  // Count cloud Number
  async countRowNumber() {
    const RowNumber = by.css('#panel-tenant-action ul.ui-dropdown-items li');
    await console.log('Start get Record Number');
    await browser.wait(ExpectedConditions.visibilityOf(element(RowNumber)), 3000);
    const number = await element.all(RowNumber).count();
    await console.log('Done');
    return number;

  }


}
