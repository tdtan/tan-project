import { element, by, ExpectedConditions, browser } from 'protractor';
const btn_AccessLog = by.xpath('//p-button[@class="button-add-action ng-star-inserted"]');
const txtVNFMs = by.xpath('//*[@id="vnfmsContainer"]//p-accordiontab/div[1]/a/p-header/span');
const txbIPV6 = by.xpath('//*[@id="vnfmsTablePanel"]//div[2]/div[2]/div[2]/input');
const txb_IPV4 = by.xpath('//*[@id="vnfmsTablePanel"]//input[@formcontrolname="v4IpAddress"]');
const txb_VNFMName = by.xpath('//*[@id="vnfmsTablePanel"]//input[@formcontrolname="name"]');
const txbSearch = by.xpath('//*[@id="vnfmsContainer"]//p-table/div/div[1]//input[@type="text"]');
const btnSave = by.xpath('//*[@id="vnfmsTablePanel"]//button[@icon="pi pi-pw pi-check"]');
const btnCancelEditFrom = by.xpath('//*[@id="vnfmsTablePanel"]//button[@icon="fa fa-times"]');
const btnClearFilter = by.xpath('//*[@id="vnfmsContainer"]//tr[2]//p-button[@icon="fa fa-times"]');
const btnClearSearch = by.xpath('//div[1]/div/div[2]//p-button[@icon="fa fa-times"]');
const btnRefesh = by.xpath('//*[@id="vnfmsContainer"]//p-button[@title="Refresh Table"]/button');
const btnShowFilter = by.xpath('//p-button[@icon="fa fa-sliders"]');
const btnShowColumn = by.xpath('//p-button[@title="Show/Hide Columns"]');
const elEditVnfm = by.xpath('//*[@id="vnfmsTablePanel"]/div/div[1]');
const frmEditVNFM = by.css('#vnfmFormContainer form');
const txtRecord = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator/div/div/div/span');
const txtNoRecord = by.xpath('//*[text()=" No records found "]');
const rowTable = by.xpath('//*[@id="vnfmsContainer"]//table/tbody/tr');
const rowFilter = by.xpath('//p-accordion//p-table//table/thead/tr[2]');
const lblNumPage = by.xpath('//*[@id="vnfmsContainer"]//p-paginator//p-dropdown//label');
const btnPage2 = by.xpath('//*[@id="vnfmsContainer"]//p-paginator//span/a[2]');
// tslint:disable-next-line:max-line-length
const pageActive = by.xpath('//*[@id="vnfmsContainer"]//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
const btnConfirmYes = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const btnConfirmNo = by.xpath('//button[@icon="fa fa-times"]');
const pupConfirm = by.xpath('//div[@role="dialog"]');

const btnSelectActions = by.xpath('//*[@id="vnfmsContainer"]//table/tbody/tr[1]/td[9]/p-dropdown');

export class PageAdminVNFMs {
    // Clear textbox Search in page VNFMs
    /**
     * parameter:
     * author: tthiminh
     */
    async ClearTxbSearch() {
        await console.log('Start clear textbox Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).clear();
        await console.log('Done');
    }
    //
    async ClickClearFilterButton() {
        await console.log('Start click clear ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearFilter)), 3000);
        await element(btnClearFilter).click();
        await console.log('Done');
    }

    async clickAccessLogButton() {
        await console.log('Start click Access Log Button ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_AccessLog)), 3000);
        await element(btn_AccessLog).click();
        await console.log('Done');
    }
    //
    async ClickClearSearchButton() {
        await console.log('Start click clear ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done');
    }
    // Verify form edit vnfm display
    async verifyFormEditVnfmDisplay() {
        await console.log('Verify form edit VNFM display');
        return await element(elEditVnfm).isPresent();
    }

    // Verify form edit vnfm present
    async verifyFormEditVnfmPresent() {
        await console.log('Verify form edit VNFM Present');
        return await element(frmEditVNFM).isPresent();
    }

    // Verify form input disable
    async verifyInputFormEditDisable(formcontrolname: string) {
        const inputDisable = by.css('#vnfmFormContainer > form > div > p-panel  input[formcontrolname="' + formcontrolname + '"]:disabled');
        await element(inputDisable).isPresent();
        return inputDisable;
    }
    // Verify form input disable
    async verifyInputFormEditEnable(formcontrolname: string) {
        const inputEnable = by.xpath('//*[@id="vnfmsTablePanel"]//input[@formcontrolname="' + formcontrolname + '"]');
        await element(inputEnable).isEnabled();
        return inputEnable;
    }

    // Set text Search in page VNFMs
    /**
     * parameter: search
     * author: tthiminh
     */
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).clear();
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // Set IPv6 in page VNFMs
    /**
     * parameter: value
     * author: tthiminh
     */
    async setIPv6(value: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbIPV6)), 3000);
        await element(txbIPV6).clear();
        await element(txbIPV6).sendKeys(value);
        await console.log('Done');
    }

    // Click link in table to edit VNFM
    /**
     * parameter:
     * author: tthiminh
     */
    async clickRowTable() {
        console.log('Start link in table VNFMs');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 5000);
        await element(rowTable).click();
        console.log('Done');
    }
    // Click button Show|Hide Filter in VNFMs Page
    async clickShowHideFilter() {
        await console.log('Start click Show| Hide Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done click Show| Hide Filter');
    }

    // Verify Filter display
    async verifyFilterDisplayed() {
        await console.log('Start verify row Filter display');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowFilter)), 3000);
        await expect(element(rowFilter).isPresent()).toBeTruthy();
        await console.log('Done');
    }
    // Verify Filter not display
    async verifyFilterNotDisplayed() {
        await console.log('Start verify row Filter display');
        await expect(element(rowFilter).isPresent()).toBeFalsy();
        await console.log('Done');
    }

    // Click button Refesh in VNFMs Page
    async clickRefesh() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefesh)), 3000);
        await element(btnRefesh).click();
        await console.log('Done click refesh');
    }
    // Click button Save in Edit VNFMs Page
    async clickSaveButtonEditForm() {
        await console.log('Start click Save');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSave)), 3000);
        await element(btnSave).click();
        await console.log('Done click Save');
    }
    // click cancel button in Edit VNFMs page
    async clickCancelButtonEditForm() {
        await element(btnCancelEditFrom).click();
        await console.log('Done click cancel button');
    }
    // Click button Refesh in VNFMs Page
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done click Show Hide Column');
    }

    // // Click button minimized in VNFMs Page
    // async clickMinimaxTable() {
    //     await console.log('Start click minimized table');
    //     await browser.wait(ExpectedConditions.elementToBeClickable(element(btnMinimix)), 3000);
    //     await element(btnMinimix).click();
    //     await console.log('Done click minimized table');
    // }

    // Click to move page 2
    /**
     * parameter:
     * author: tthiminh
     */
    async clickNumbertoMovePage() {
        console.log('Start click to move page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPage2)), 5000);
        await element(btnPage2).click();
        console.log('Done');
    }

    // Click Confirm Yes
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnConfirmYes() {
        console.log('Start click button confonfirm Yes');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmYes)), 5000);
        await element(btnConfirmYes).click();
        console.log('Done');
    }

    // Click Confirm No
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnConfirmNo() {
        console.log('Start click button confonfirm No');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmNo)), 5000);
        await element(btnConfirmNo).click();
        console.log('Done');
    }

    // Click button Select Actions
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnSelectActions() {
        console.log('Start click button Select Actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectActions)), 5000);
        await element(btnSelectActions).click();
        console.log('Done');
    }

    // Click Option select Actions
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnOptionActions(txtOption: string) {
        const btnOptActions = by.xpath('//li[contains(option,"' + txtOption + '")]');
        console.log('Start click button Option Actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptActions)), 5000);
        await element(btnOptActions).click();
        console.log('Done');
    }

    // get curent page active
    /**
     * parameter:
     * author: tthiminh
     */
    async getCurentPageActive() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(pageActive)), 3000);
        const textPage = await element(pageActive).getText();
        return textPage;
    }

    // get text at header page
    /**
     * parameter:
     * author: tthiminh
     */
    async getTextHeader() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtVNFMs)), 3000);
        const textSecurity = await element(txtVNFMs).getText();
        return textSecurity;
    }

    // get value txb search
    /**
     * parameter:
     * author: tthiminh
     */
    async getValueSearch() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        const value = await element(txbSearch).getAttribute('value');
        return value;
    }

    // Get Record  Current display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetRecordCurrent() {
        // await browser.sleep(3000);
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        const record = await element(txtRecord).getText();
        await console.log('Record current: ' + record);
        return record;

    }

    // Get Number Row page default
    /**
     * parameter:
     * author: tthiminh
     */
    async GetNumberRowInPage() {
        await console.log('Start get text row number');
        await browser.wait(ExpectedConditions.visibilityOf(element(lblNumPage)), 3000);
        const getTextNumRow = await element(lblNumPage).getText();
        await console.log('Number Row Display in Page ' + getTextNumRow);
        return getTextNumRow;

    }

    // Get text in Action display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="container-common-table"]//table/tbody/tr/td[' + numTd + ']//span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 5000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Get IPv4 On Edit VNFM Page
    /**
     * parameter:
     * author: tthiminh
     */
    async GetIPv4OnEditVNFMPage() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_IPV4)), 5000);
        const text_IPv4 = await element(txb_IPV4).getAttribute('value');
        return text_IPv4;

    }

    // Get VNFM Name On Edit VNFM Page
    /**
     * parameter:
     * author: tthiminh
     */
    async GetVNFMNameOnEditVNFMPage() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_VNFMName)), 5000);
        const text_VNFMName = await element(txb_VNFMName).getAttribute('value');
        return text_VNFMName;

    }

    // Count number row display in page
    /**
     * parameter:
     * author: tthiminh
     */
    async CountNumberRowInpage() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }


    // Select Option from Show/ Hide columns List in Onboarded Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name: ' + strOptionName);
        // await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btnClose = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await console.log('Start click close show hide column');
        await element(btnClose).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="vnfmsContainer"]//th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="vnfmsContainer"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 3000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Select number row display in page
    async SelectNumberRowDisplay(optionNumber: number) {

        const btnOptionNum = by.xpath('//*[@id="vnfmsContainer"]//p-paginator//p-dropdown//span');
        const filChoose = by.xpath('//li[contains(span,"' + optionNumber + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptionNum)), 3000);
        await console.log('Click lable filter: ' + btnOptionNum);
        await element(btnOptionNum).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Verify sort column
    async verifySortColumn(numTh: number, ) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="vnfmsContainer"]//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="vnfmsContainer"]//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="vnfmsContainer"]//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click();
        await expect(element(sortup).isPresent()).toBeTruthy();
        await element(sortup).click();
        await expect(element(sortdown).isPresent()).toBeTruthy();

    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Current record: ' + txtVerify);
        await console.log('Done');
    }

    // // Verify display or undisplay table when click minimized
    // async verifyTableAfterClickMinimized() {
    //     await console.log('Start verify table');
    //     await this.clickMinimaxTable();
    //     expect(await element(rowTable).isDisplayed()).toBeFalsy();
    //     await this.clickMinimaxTable();
    //     expect(await element(rowTable).isDisplayed()).toBeTruthy();
    //     await console.log('Done verify table');
    // }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
        });
    }

    // Verify row display on table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyRowTableDisplay() {
        return await element(rowTable).isDisplayed();
    }

    // Verify column name display
    /**
     * parameter:strTHName: TH Name in table
     * author: tthiminh
     */
    async verifyColumnDisplayInTable(strTHName: string) {
        const tableTH = by.xpath('//*[@id="vnfmsContainer"]//tr[1]/th[text()=" ' + strTHName + ' "]');
        return await element(tableTH).isPresent();
    }

    // Verify element display in form
    /**
     * parameter:strEventItem: Item in Function on table
     * author: tthiminh
     */
    async verifyEventDisplayInForm(strEventItem: string) {
        await console.log('Start verify event item is option in Function: ' + strEventItem);
        const elmDisplay = by.xpath('//li[contains(option,"' + strEventItem + ' ")]');
        return element(elmDisplay).isDisplayed();
    }

    // Verify row display on table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyPopupConfirmDisplay() {
        return await element(pupConfirm).isPresent();
    }


    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Get number of columns in current search result table
    async getNumberOfColumnsInSearchTable(strTable: string) {
        let _numberOfColumn: number;
        _numberOfColumn = 0;
        const _numOfMaximumColumns = 11;
        await console.log('Start checking if we have any table name equals Text value?');
        for (let i = 1; i <= _numOfMaximumColumns; i++) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            if (await element(_strColumnLocator).isPresent()) {
                _numberOfColumn = i;
            }
        }
        await console.log('Number of column: ', _numberOfColumn);
        return _numberOfColumn;

    }

    // Get column number based on column name in VFM Settings Page
    async getColumnNumberBasedOnColumnName(strTable: string, columnname: string) {
        let _numCurrentColumns: any;
        let _columnnum: number;
        _columnnum = 0;
        _numCurrentColumns = await this.getNumberOfColumnsInSearchTable(strTable);
        await console.log('Start getting column number based on column Name');
        for (let i = 1; i <= _numCurrentColumns; i++) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            await element(_strColumnLocator).getText().then(async function (text) {
                await console.log('Text TH in ', i , 'is: ', text);
                if (text === columnname) {
                    _columnnum = i;
                }
            });
        }
        if (_columnnum === 0) {
            await console.log('No found ' + columnname + ' in search table');
        } else {
            await console.log('Column ' + _columnnum + ' has ' + columnname + ' as name');
        }
        return _columnnum;
    }

    // Get row number has Column Name Equals Text Value in Onboarded VNFs Page
    async getNumberofRowHasValueofColumnNameEqualsTextValue(strTable: string, intColumnNumber: number, strTextValue: string) {
        let _rownum: number;
        _rownum = 0;
        const record = by.xpath('//*[@id="' + strTable + '"]//p-paginator/div/div/div/span'); // Showing 3 of 3 records
        await browser.wait(ExpectedConditions.elementToBeClickable(element(record)), 3000);
        const numrecord = await element(record).getText();
        const _intRowNum = numrecord.slice(0, 13);
        const _intSearchSheetRowNum = Number(_intRowNum.replace(' records', ''));
        await console.log('Current Search table has ' + _intSearchSheetRowNum + ' row(s)');
        for (let i = 1; i <= _intSearchSheetRowNum; i++) {
            const TextValueLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + i + ']/td[' + intColumnNumber + ']//span');
            await element(TextValueLocator).getText().then(function (text) {
                if (text === strTextValue) {
                    _rownum = i;
                }
            });
        }
        if (_rownum === 0) {
            await console.log('No found Row has Column Name Equals ' + strTextValue + ' in search table');
        } else {
            await console.log('Row ' + _rownum + ' has value of Column Name equals ' + strTextValue);
        }
        return _rownum;
    }

    // Click Function On Row Has Value of ColumnName Equals TextValue
    // tslint:disable-next-line:max-line-length
    async clickActionOnRowHasValueofColumnNameEqualsTextValue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, functionchoose: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        const _function = by.xpath('//li[contains(span,"' + functionchoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_function)), 4000);
        await element(_function).click();
        await console.log('Done');
    }

    // Click Functions On Row
    // tslint:disable-next-line:max-line-length
    async clickFunctionOnRowHasValueofColumnName(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        await console.log('Done');
    }

    // Compare curent Url with IPv4 in Edit VNFM page
    // tslint:disable-next-line:max-line-length
    async ClickVNFMNameIntoEditVNFM(Num_Row: number) {
        await console.log('Start click VNFMs ', Num_Row, ' switch edit page');
        const num_tableRow = by.xpath('//*[@id="vnfmsContainer"]//table/tbody/tr[' + Num_Row + ']');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(num_tableRow)), 6000);
        await element(num_tableRow).click();
        await console.log('Done');
    }


    // Compare curent Url with IPv4 in Edit VNFM page
    // tslint:disable-next-line:max-line-length
    async FunctionCompareCurentUrlWithIPv4InEditVNFMPage(txt_URl: any): Promise<string> {
        let VNFMName = '';
        const NumRow = await this.CountNumberRowInpage();
        for (let i = 1; i <= NumRow; i++) {
            await this.ClickVNFMNameIntoEditVNFM(i);
            await expect(this.verifyFormEditVnfmPresent()).toBeTruthy();
            const txt_IPv4 = await this.GetIPv4OnEditVNFMPage();
            if (txt_URl.includes(txt_IPv4)) {
                VNFMName = await this.GetVNFMNameOnEditVNFMPage();
            }
            await this.clickCancelButtonEditForm();
        }
        return VNFMName;
    }


    async getListVNFMNameInTable() {
        await console.log('begin get VNFM Name in Table');
        const lsName: any = [];
        const elColumnVNFName = by.css('p-table table tbody tr td:nth-child(1) span');
        const list1 = await element.all(elColumnVNFName).getText();
        for (let index = 0; index < list1.length; index++) {
            if (!lsName.includes(list1[index])) {
                lsName.push(list1[index]);
            }
        }
        return lsName;
    }
}
