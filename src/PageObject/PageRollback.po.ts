import { element, by, ExpectedConditions, browser } from 'protractor';

const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
const btnRollback = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txtRollback = by.xpath('//*[@id="vnfc-function"]/div/div[1]/span');
const btnSelectTypeUpgrade = by.xpath('//*[@id="vnfc-function"]/div/div[2]/div/div/ul/p-dropdown');
export class PageRollback {

    // Click button Rollback
    async clickRollbackButton() {
        await console.log('Start click Rollback');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRollback)), 3000);
        await element(btnRollback).click();
        await console.log('Done click Rollback');
    }

    // Click button Cancel
    async clickbtnCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click Cancel');
    }

    // Click lable type Rollback
    async clickbtnSelectTypeRollback() {
        await console.log('Start click lable select Version');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectTypeUpgrade)), 3000);
        await element(btnSelectTypeUpgrade).click();
        await console.log('Done click lable select Version');
    }

    // Click choose option Rollback
    async clickoptionRollback(numOption: number) {
        const btnoptionVersion = by.xpath('//*[@id="vnfc-function"]//p-dropdownitem[' + numOption + ']');
        await console.log('Start click option heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnoptionVersion)), 3000);
        await element(btnoptionVersion).click();
        await console.log('Done click option heal');
    }

    // Get text in description of Rollback
    async GetTextRollback() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRollback)), 3000);
        const getTextDes = await element(txtRollback).getText();
        await console.log('Text Descrition: ' + getTextDes);
        return getTextDes;

    }

}
