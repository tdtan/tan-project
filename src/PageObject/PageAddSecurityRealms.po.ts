import { element, by, ExpectedConditions, browser } from 'protractor';

    const txtTextHeader = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[1]/span');
    const tbxNameRealm = by.xpath('//input[@formcontrolname="SecurityRealmName"]');
    const drpRealmType = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div/div[3]/div[2]/p-dropdown/div/label');
    const btnSave = by.xpath('//*[@id="panel-securityRealm-actions"]//button[@icon="pi pi-pw pi-check"]');
    const btnCancel = by.xpath('//*[@id="panel-securityRealm-actions"]//button[@icon="fa fa-times"]');
    const txtMessName = by.xpath('//*[@id="panel-securityRealm-actions"]//div[text()=" Security Realm Name is Required "]');
    const txtMessType = by.xpath('//*[@id="panel-securityRealm-actions"]//div[text()=" Realm Type Required "]');
    const txtRealmProperties = by.xpath('//*[@id="panel-securityRealm-actions"]//h4[text()=" Realm Properties "]');
    const txbURL = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div[2]/div/div[2]/div/div[2]/input');
    const txbUserName = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div[2]/div/div[3]/div/div[2]/input');
    const txbPassword = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div[2]/div/div[4]/div/div[2]/input');
    const txbTemplate = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div[2]/div/div[5]/div/div[2]/input');
    const txbserchbase = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div[2]/div/div[6]/div/div[2]/input');
    const txbsearchfile = by.xpath('//*[@id="panel-securityRealm-actions"]/div/div[2]/div/div/form/div[2]/div/div[7]/div/div[2]/input');
    const txbSearch = by.css('//app-security-realms/rbn-common-table//div[2]/div/div/input');

export class PageAddSecurityRealms {

    // Click button Save
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnSave() {
        await console.log('Start click save');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSave)), 3000);
        await element(btnSave).click();
        await console.log('Done');
    }
    // Click button Cancel
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done');
    }

    // Set text Search in Security Realm Page
    async settextSearch(search: string) {
        await browser.sleep(2000);
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).clear();
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }


    // get text header after click Add Realm
    /**
     * parameter:
     * author: tthiminh
     */
    async getTextAddRealm() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtTextHeader)), 3000);
        const textOnb = await element(txtTextHeader).getText();
        return textOnb;
    }

    // get value of Realm Name
    /**
     * parameter:
     * author: tthiminh
     */
    async getValueFormRealnmName() {
        await console.log('Start get value of form Realm Name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(tbxNameRealm)), 3000);
        const textName = await element(tbxNameRealm).getAttribute('value');
        await console.log('Done');
        return textName;
    }

    // get value of Realm Type
    /**
     * parameter:
     * author: tthiminh
     */
    async getValueFormRealmType() {
        await console.log('Start get value of form Realm Type');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpRealmType)), 3000);
        const textType = await element(drpRealmType).getText();
        await console.log('Done');
        return textType;
    }

    // Clear Name Security Realm
    /**
     * parameter:
     * author: tthiminh
     */
    async clearNameRealm() {
        await console.log('Start clear Realm name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(tbxNameRealm)), 3000);
        await element(tbxNameRealm).clear();
        await console.log('Done');
    }

    // set Name Security Realm
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputNameRealm(strName: string) {
        await console.log('Start input text Realm name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(tbxNameRealm)), 3000);
        await element(tbxNameRealm).sendKeys(strName);
        await console.log('Done');
    }

    // Select Realm Type in page Add Realm
    /**
     * parameter:
     * author: tthiminh
     */
    async selectRealmType(RealmType: string) {

        await console.log('Start select Realm Type in page Add Realm');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpRealmType)), 4000);
        await element(drpRealmType).click();
        const optionType = by.xpath('//li[contains(span,"' + RealmType + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(optionType)), 5000);
        await element(optionType).click();
        await console.log('Done select Realm Type in page Add Realm');
    }
    // Clear URL
    /**
     * parameter:
     * author: tthiminh
     */
    async clearURL() {
        await console.log('Start Clear URL');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbURL)), 3000);
        await element(txbURL).clear();
        await console.log('Done');
    }

    // set URL
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputURL(strURL: string) {
        await console.log('Start input text URL');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbURL)), 3000);
        await element(txbURL).sendKeys(strURL);
        await console.log('Done');
    }
    // Clear UserName
    /**
     * parameter:
     * author: tthiminh
     */
    async clearUserName() {
        await console.log('Start clear User Name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbUserName)), 3000);
        await element(txbUserName).clear();
        await console.log('Done');
    }

    // set UserName
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputUserName(strName: string) {
        await console.log('Start input text User Name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbUserName)), 3000);
        await element(txbUserName).sendKeys(strName);
        await console.log('Done');
    }

    // set Password
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputPassword(strName: string) {
        await console.log('Start input text Password');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbPassword)), 3000);
        await element(txbPassword).clear();
        await element(txbPassword).sendKeys(strName);
        await console.log('Done');
    }

    // set Input Temlate
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputTemlate(strName: string) {
        await console.log('Start Input Temlate');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbTemplate)), 3000);
        await element(txbTemplate).clear();
        await element(txbTemplate).sendKeys(strName);
        await console.log('Done');
    }

    // set Search Base
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputSearchBase(strName: string) {
        await console.log('Start input text Search Base');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbserchbase)), 3000);
        await element(txbserchbase).clear();
        await element(txbserchbase).sendKeys(strName);
        await console.log('Done');
    }

    // set Seach File
    /**
     * parameter: strName
     * author: tthiminh
     */
    async setInputSearchFile(strName: string) {
        await console.log('Start input text Search File');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbsearchfile)), 3000);
        await element(txbsearchfile).clear();
        await element(txbsearchfile).sendKeys(strName);
        await console.log('Done');
    }

    // Verify that the mess missing request info Realm Name
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyMisingRequestInfo() {
        return await element(txtMessName).isPresent();
    }

    // Verify that the mess missing request info Realm Type
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyMisingRequestInfotype() {
        return await element(txtMessType).isPresent();
    }

    // Verify that the mess missing request info Realm Type
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyRealmPropertiesDisplay() {
        return await element(txtRealmProperties).isPresent();
    }

    // Verify that the mess missing request info Realm Type
    /**
     * parameter:
     * author: tthiminh
     */
    async verifytxtAddDisplay() {
        return await element(txtTextHeader).isPresent();
    }


}
