import { element, by, ExpectedConditions, browser} from 'protractor';
const btnClearAll = by.xpath('//button[@label="Clear All"]');
const txtError = by.xpath('//div[text()="Error"]');
const lcb_divError = by.xpath('//*[text()="Error"]');
const lcb_divWarning = by.xpath('//*[text()="Warning"]');
const lcb_divInfo = by.xpath('//*[text()="Info"]');

export class PageError {

    // Verify text Messege Error
    async verifyTextMessegeError() {
        await console.log('Start checking if display messege error');
        if (await element(txtError).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Messege Error
    async verifyMessegeError() {
        await console.log('Start checking if display messege error');
        if (await element(lcb_divError).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Messege Warning
    async verifyMessegeWarning() {
        await console.log('Start checking if display messege warning');
        if (await element(lcb_divWarning).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Messege Info
    async verifyMessegeInfo() {
        await console.log('Start checking if display messege Info');
        if (await element(lcb_divInfo).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Click clear All
    async cliclClearALL() {
        await console.log('Start click Clear All');
        await browser.wait(await ExpectedConditions.elementToBeClickable(element(btnClearAll)), 7000);
        await element(btnClearAll).click();
        await console.log('Done');
    }
}
