import { element, by, ExpectedConditions, browser} from 'protractor';

const txtAddTrap = by.css('#trapDestination p-panel span');
const txbTrapName = by.xpath('//*[@id="destinationName"]');
const txbTrapAddress = by.xpath('//*[@id="ipAddress"]');
const txbPortNumber = by.xpath('//*[@id="portNumber"]');
const txbCommunity = by.xpath('//*[@id="trapCommunity"]');
const drpTrapVersion = by.xpath('//*[@id="trapDestination"]//p-dropdown[@formcontrolname="version"]');
const lbl_TrapVersion = by.xpath('//*[@id="trapDestination"]//p-dropdown[@formcontrolname="version"]//label');
const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
const btnSave = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txtErrorTrapName = by.xpath('//*[text()=" Destination Name is required "]');
const txtErrorTrapIPAddress = by.xpath('//*[text()=" IP Address (v4/v6) is required "]');
const txtErrorTrapPortNumber = by.xpath('//*[text()=" Port Number is required "]');
const txtErrorTrapCommunity = by.xpath('//*[text()=" SNMP Trap Community is required "]');
const txtSecurity = by.xpath('//*[text()=" Security "]');
const btnBackPage = by.xpath('//*[@id="trapDestination"]//li[@role="menuitem"]/a');
const cbxAuthentication = by.xpath('//*[@formcontrolname="authEnabled"]/div');
const tbxAuthPassphrase = by.xpath('//*[@id="authPassphrase"]');
const drpAuthProtocol = by.xpath('//p-dropdown[@formcontrolname="snmpV3AuthProtocol"]');
const txtAuthPassphrase = by.xpath('//*[text()="SNMP V3 Authentication Passphrase"]');
const txtAuthProtocol = by.xpath('//*[text()="SNMP V3 Authentication Protocol"]');
const cbxPrivacy = by.xpath('//*[@formcontrolname="privacyEnabled"]/div');
const txbPrivPassphrase = by.xpath('//*[@id="privPassphrase"]');
const drpPrivProtocol = by.xpath('//*[@formcontrolname="snmpV3PrivProtocol"]');
const txtPrivacyPassphrase = by.xpath('//*[text()="SNMP V3 Privacy Passphrase"]');
const txtPrivacyProtocol = by.xpath('//*[text()="SNMP V3 Privacy Protocol"]');
export class PageAddAdminTrapDestinations {

    // Verify text Add Trap Destination Display
    async verifySwitchButtonDisplay() {
        await console.log('Start Verify text Add Trap Destination Display');
        return element(txtAddTrap).isPresent();
    }

    // Set text Trap Name
    async setTrapName(txt_TrapName: string) {
        console.log('Start Input Trap Name.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbTrapName)), 3000);
        await element(txbTrapName).clear();
        await element(txbTrapName).sendKeys(txt_TrapName);
        console.log('Done Input Trap Name.');
    }

    // Set text Trap Address
    async setTrapAddress(txt_TrapAddress: string) {
        console.log('Start Input Trap Address.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbTrapAddress)), 3000);
        await element(txbTrapAddress).clear();
        await element(txbTrapAddress).sendKeys(txt_TrapAddress);
        console.log('Done Input Trap Address.');
    }

    // Set text Trap Port Number
    async setTrapPortNumber(txt_TrapPortNumber: string) {
        console.log('Start Input Trap Port Number.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbPortNumber)), 3000);
        await element(txbPortNumber).clear();
        await element(txbPortNumber).sendKeys(txt_TrapPortNumber);
        console.log('Done Input Trap Port Number.');
    }

    // Set text Trap Community
    async setTrapCommunity(txt_TrapCommunity: string) {
        console.log('Start Input Trap Community.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbCommunity)), 3000);
        await element(txbCommunity).clear();
        await element(txbCommunity).sendKeys(txt_TrapCommunity);
        console.log('Done Input Trap Community.');
    }

    // Click dropdown Trap Version
    async clickDropdownTrapVersion() {
        await console.log('Start click dropdown Trap Version');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpTrapVersion)), 3000);
        await element(drpTrapVersion).click();
        await console.log('Done click dropdown Trap Version');
    }

    // Click Cancel Button
    async clickCancelButton() {
        await console.log('Start click Cancel Button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click Cancel Button');
    }

    // Click Save Button
    async clickSaveButton() {
        await console.log('Start click save button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSave)), 3000);
        await element(btnSave).click();
        await console.log('Done click save button');
    }

    // Choose option Trap Version in page
    async ClickChooseOptionTrapVersion(txtOption: String) {
        await console.log('Start Click Choose option Trap Version in page');
        const btnOption = by.xpath('//li[contains(span,"' + txtOption + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOption)), 3000);
        await element(btnOption).click();
        await console.log('Done');
    }

    // Click Back Page Trap button
    async clickBackPageTrapButton() {
        await console.log('Start click Back Page Trap button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnBackPage)), 3000);
        await element(btnBackPage).click();
        await console.log('Done click Back Page Trap button');
    }

    // Click Authentication Checkbox
    async clickAuthenticationCheckbox() {
        await console.log('Start click Authentication Checkbox');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxAuthentication)), 3000);
        await element(cbxAuthentication).click();
        await console.log('Done click Authentication Checkbox');
    }

    // Click Privacy checkbox
    async clickPrivacyCheckbox() {
        await console.log('Start click Privacy checkbox');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxPrivacy)), 3000);
        await element(cbxPrivacy).click();
        await console.log('Done click Privacy checkbox');
    }

    // Get Attribute In Trap Name
    async getAttributeInTrapName() {
        await console.log('Start Get Attribute In Trap Name');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbTrapName)), 4000);
        return await element(txbTrapName).getAttribute('value');
    }

    // Get Attribute In Trap Ip Address
    async getAttributeInTrapAddress() {
        await console.log('Start Get Attribute In Trap Address');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbTrapAddress)), 4000);
        return await element(txbTrapAddress).getAttribute('value');
    }

    // Get Attribute In Port Number
    async getAttributeInPortNumber() {
        await console.log('Start Get Attribute In Port Number');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbPortNumber)), 4000);
        return await element(txbPortNumber).getAttribute('value');
    }

    // Get Attribute In Trap Community
    async getAttributeInTrapCommunity() {
        await console.log('Start Get Attribute In Trap Community');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbCommunity)), 4000);
        return await element(txbCommunity).getAttribute('value');
    }

    // Get text In Trap Version
    async getTextInTrapVersion() {
        await console.log('Start Get text In Trap Version');
        await browser.wait(ExpectedConditions.visibilityOf(element(lbl_TrapVersion)), 4000);
        return await element(lbl_TrapVersion).getText();
    }

    // Verify display Message error Trap Name
    async verifyDisplayMessageErrorTrapName() {
        await console.log('Start verify display Message error Trap Name');
        return element(txtErrorTrapName).isPresent();
    }

    // Verify display Message Error Trap Ip Address
    async verifyDisplayMessageErrorTrapIPAddress() {
        await console.log('Start verify display Message Error Trap Address ');
        return element(txtErrorTrapIPAddress).isPresent();
    }

    // Verify display Message Error Trap Port Number
    async verifyDisplayMessageErrorTrapPortNumber() {
        await console.log('Start verify display Message Error Trap Port Number ');
        return element(txtErrorTrapPortNumber).isPresent();
    }

    // Verify display Message Error Trap Community
    async verifyDisplayMessageErrorTrapCommunity() {
        await console.log('Start verify display Message Error Trap Community');
        return element(txtErrorTrapCommunity).isPresent();
    }

    // Verify text Auth Passphrase Display
    async verifyTextAuthPassphraseDisplay() {
        await console.log('Start verify text Auth Passphrase Display');
        return element(txtAuthPassphrase).isPresent();
    }

    // Verify text AuthProtocol is present
    async verifyTextAuthProtocolIsPresent() {
        await console.log('Start verify text Auth Protocol is present in page');
        return element(txtAuthProtocol).isPresent();
    }

    // Verify Auth Passphrase textbox is present in page
    async verifyAuthPassphraseTextboxIsPresent() {
        await console.log('Start verify Auth Passphrase textbox is present in page');
        return element(tbxAuthPassphrase).isPresent();
    }

    // Verify Auth Protocol dropdown is present in page
    async verifyAuthProtocolDropdownIsPresent() {
        await console.log('Start verify Auth Protocol dropdown is present in page');
        return element(drpAuthProtocol).isPresent();
    }

    // Verify text Security is present in page
    async verifyTextSecurityIsPresent() {
        await console.log('Start verify text Security is present in page');
        return element(txtSecurity).isPresent();
    }

    // Verify text Privacy Passphrase Display
    async verifyTextPrivacyPassphraseDisplay() {
        await console.log('Start verify text Privacy Passphrase Display');
        return element(txtPrivacyPassphrase).isPresent();
    }

    // Verify text Privacy Protocol is present
    async verifyTextPrivacyProtocolIsPresent() {
        await console.log('Start verify text Privacy Protocol is present in page');
        return element(txtPrivacyProtocol).isPresent();
    }

    // Verify Privacy Passphrase textbox is present in page
    async verifyPrivacyPassphraseTextboxIsPresent() {
        await console.log('Start verify Privacy Passphrase textbox is present in page');
        return element(txbPrivPassphrase).isPresent();
    }

    // Verify Privacy Protocol dropdown is present in page
    async verifyPrivacyProtocolDropdownIsPresent() {
        await console.log('Start verify Privacy Protocol dropdown is present in page');
        return element(drpPrivProtocol).isPresent();
    }


}
