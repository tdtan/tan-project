import { element, by, ExpectedConditions, browser, protractor } from 'protractor';
import { PageError } from './PageError.po';

const txtTenant = by.xpath('//*[@id="container-common-table"]//p-header/span');
const txbSearch = by.xpath('//app-tenant//p-table/div/div[1]//input[@type="text"]');
const btnAddTenant = by.css('#container-common-table p-button.button-add-action button');
const btnRefesh = by.css('#panel-tenant p-button[title="Refresh Table"]');
const btnClearSearch = by.xpath('// p-button[@icon="fa fa-times"]');
const btnClear = by.xpath('//app-tenant//p-table//table/thead//p-button');
const btnShowColumn = by.xpath('//p-button[@title="Show/Hide Columns"]');
const btnShowFilter = by.xpath('//p-button[@title="Show/Hide Filter Row"]');
const btnMinimix = by.css('#panel-tenant a.ui-panel-titlebar-icon');
const lblNumberPage = by.css('#panel-tenant p-paginator div.ui-dropdown');
const txtRecord = by.xpath('//p-table//p-paginator/div/div//span');
const rowTable = by.css('#tb-vnf tbody.ui-table-tbody tr');
const txtNoRecord = by.xpath('//span[text()=" No records found "]');
const rowFilter = by.xpath('//app-tenant//p-table//table/thead/tr[2]');
const lblNumPage = by.xpath('//app-tenant//p-table//p-paginator//p-dropdown//label');

// tslint:disable-next-line:max-line-length
const pageActive = by.xpath('//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');

export class PageVNFMConfTenants {

  private pageError = new PageError();

  // Verify show text tenants
  async verifyShowTextTenant(_textCloud: any) {
    console.log('Start get text.');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtTenant)), 5000);
    const cloud = await element(txtTenant).getText();
    expect(cloud).toEqual(_textCloud);
    await console.log('text: ' + cloud);
    await console.log('Done.');
  }

  // Clear text search in Configuration Tenant Page
  async cleartextSearch() {
    await console.log('Start clear text Search');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
    await element(txbSearch).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    await element(txbSearch).sendKeys(protractor.Key.BACK_SPACE);
    await console.log('Done');
  }

  // Set text Search in Configuration Tenant Page
  async settextSearch(search: string) {
    await console.log('Start input text Search');
    await browser.wait(ExpectedConditions.visibilityOf(element(txbSearch)), 3000);
    await element(txbSearch).clear();
    await element(txbSearch).sendKeys(search);
    await console.log('Done');
  }

  // Click button Clear Text Search
  async clickCleartTextSearch() {
    await console.log('Start click Clear Search');
    browser.actions().mouseMove(element(btnClearSearch)).perform();
    await browser.wait(ExpectedConditions.visibilityOf(element(btnClearSearch)), 5000);
    await element(btnClearSearch).click();
    await console.log('Done click Clear Search');
  }
  // Input text search
  async inputSearch(search: string) {
    await console.log('Start input text Search');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
    await element(txbSearch).sendKeys(search);
    await console.log('Done');
  }


  // Click button Add Cloud in Configuration Tenant Page
  async clickAddTenantbtn() {
    console.log('Start click Add Tenant');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btnAddTenant)), 4000);
    await element(btnAddTenant).click();
    console.log('Done');
  }


  // Click button Edit Cloud in Configuration Tenant Page
  async clickEditTenant() {
    const editTenant = by.css('table tbody tr');
    console.log('Start click Edit Tenant');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(editTenant)), 3000);
    await element(editTenant).click();
    console.log('Done');
  }

  // Click button Refesh in Configuration Tenant Page
  async clickRefesh() {
    await console.log('Start click Refesh');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnRefesh)), 3000);
    await element(btnRefesh).click();
    await console.log('Done click refesh');
  }

  // Click button Clear in Configuration Tenant Page
  async ClickClearFilterButton() {
    await console.log('Start click Clear Filter');
    browser.actions().mouseMove(element(btnClear)).perform();
    await browser.wait(ExpectedConditions.visibilityOf(element(btnClear)), 5000);
    await element(btnClear).click();
    await console.log('Done click Clear Filter');
  }

  // Click button Show/Hide Column in Configuration Tenant Page
  async clickShowHideColumn() {
    await console.log('Start click Show Hide Column');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnShowColumn)), 5000);
    await element(btnShowColumn).click();
    await console.log('Done click Show Hide Column');
  }

   // Click button Show/Hide Filter in Configuration Tenant Page
   async clickShowHideFilter() {
    await console.log('Start click Show Hide Filter');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnShowFilter)), 5000);
    await element(btnShowFilter).click();
    await console.log('Done click Show Hide Filter');
  }

  // Click button minimaxed in Configuration Tenant Page
  async clickMinimaxTable() {
    await console.log('Start click minimized table');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnMinimix)), 3000);
    await element(btnMinimix).click();
    await console.log('Done click minimized table');
  }

  // Get Record  Current display
  async GetRecordCurrent() {
    await console.log('Start verify record');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
    const record = await element(txtRecord).getText();
    await console.log('Record current: ' + record);
    return record;

  }

  // Select Option from Show/ Hide columns List in Configuration Tenant Page
  async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
    await console.log('Start selecting Option Name from Show Hide Columns List');
    await this.clickShowHideColumn();
    const btnSelectCol = by.xpath('//p-checkbox[@binary="true"]//label[text()="' + strOptionName + '"]');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnSelectCol)), 3000);
    await element(btnSelectCol).click();
    const btnClose = by.xpath('//button[contains(span,"Close")]');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnClose)), 3000);
    await element(btnClose).click();
    await console.log('Done selecting Option Name from Show Hide Columns List');
  }

  // Select Option from Show/ Hide columns List in Configuration Tenant Page
  async selectMultipleOptionNamefromShowHideColumnsList(strOptionName: string) {
    await console.log('Start selecting Multiple Option Name from Show Hide Columns List');
    const btnSelectCol = by.xpath('//p-checkbox[@binary="true"]//label[text()="' + strOptionName + '"]');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnSelectCol)), 3000);
    await element(btnSelectCol).click();
    await console.log('Done selecting Multiple Option Name from Show Hide Columns List');
  }

  // Click clole hide/show column
  async clickCloseShowHide() {
    const btnClose = by.xpath('//button[contains(span,"Close")]');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnClose)), 3000);
    await element(btnClose).click();
  }



  // Verify sort column
  async verifysort(columnnumber: number) {
    const sort = by.css('tr.ng-star-inserted th:nth-child(' + columnnumber + ') p-sorticon');
    const sortup = by.xpath('//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
    const sortdown = by.xpath('//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
    await console.log('Start verify sort on column number: ' + columnnumber);
    browser.actions().mouseMove(element(sort)).perform();
    await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 5000);
    await element(sort).click().then(function () {
      expect(element(sortup).isPresent()).toBeTruthy();
    });
    await browser.wait(ExpectedConditions.elementToBeClickable(element(sortup)), 5000);
    await element(sortup).click().then(function () {
      expect(element(sortdown).isPresent()).toBeTruthy();
    });
    await console.log('Done verify sort');

  }

  // Select filter from talbe in Configuration Tenant page
  async SelectFilter(columnnumber: number, txtChoose: string) {
    const filtercolumn = by.css('tr:nth-child(2) th:nth-child(' + columnnumber + ') p-dropdown');
    const filChoose = by.xpath('//p-dropdownitem//span[text()="' + txtChoose + '"]');
    await browser.wait(ExpectedConditions.visibilityOf(element(filtercolumn)), 3000);
    await console.log('Click lable filter');
    await element(filtercolumn).click();
    await browser.wait(ExpectedConditions.visibilityOf(element(filChoose)), 3000);
    await console.log('Click choose element filter');
    await element(filChoose).click();
    await console.log('Done');
  }

  // Clear text search in Fliter with checkbox
  async cleartextSearchInfilter() {
    const txbSearchID = by.xpath('/html/body/div/div[1]/div[2]/input');
    await console.log('Start clear text Search');
    await browser.wait(ExpectedConditions.visibilityOf(element(txbSearchID)), 3000);
    await element(txbSearchID).clear();
    await console.log('Done');
  }

  // Set text Search in Configuration Tenant Page
  async settextSearchInFilter(search: string) {
    const txbSearchID = by.xpath('/html/body/div/div[1]/div[2]/input');
    await console.log('Start input text Search');
    await browser.wait(ExpectedConditions.visibilityOf(element(txbSearchID)), 3000);
    await element(txbSearchID).sendKeys(search);
    await console.log('Done');
  }
  // Click select All in Filter
  async clickCheckBoxSelectAll() {
    const cbxSelectAll = by.xpath('/html/body/div/div[1]/div[1]/div[2]');
    await console.log('Start Click select All in Filter');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxSelectAll)), 3000);
    await element(cbxSelectAll).click();
    await console.log('Done');
  }

  // Click select All in Filter
  async clickLableFilter(columnnumber: number) {
    // tslint:disable-next-line:max-line-length
    const filtercolumn = by.xpath('//app-tenant//p-table//table/thead/tr[' + columnnumber + ']/th[1]/p-multiselect');
    await console.log('Start Click lable Filter');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
    await element(filtercolumn).click();
    await console.log('Done');
  }

  // Select filter from talbe in Onboarded page
  async SelectOptionFilter(txtChoose: string) {

    const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
    await console.log('Click choose element filter');
    await element(filChoose).click();
  }

  // Click select All in Filter
  async clickCloseFilter() {
    const txtClose = by.xpath('/html/body/div/div[1]/a');
    await console.log('Start Click Close Filter');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 3000);
    await element(txtClose).click();
    await console.log('Done');
  }

  // Click Date Added in Configuration Tenant Page
  async clickDateAdded() {
    const DateAdded = by.xpath('//*[@icon="fa fa-calendar"]');
    await console.log('Start click Date Added');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(DateAdded)), 3000);
    await element(DateAdded).click();
    await console.log('Done click Date Added');
  }

  // Click Date Added in Configuration Tenant Page
  async clickDateClear() {
    const DateClear = by.xpath('//*[contains(span,"Cancel")]');
    await console.log('Start click Clear Date Added');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(DateClear)), 3000);
    await element(DateClear).click();
    await console.log('Done click Clear Date Added');
  }

  //
  async GetNumberRowInPage() {
    await console.log('Start get text row number');
    await browser.wait(ExpectedConditions.visibilityOf(element(lblNumPage)), 3000);
    const getTextNumRow = await element(lblNumPage).getText();
    await console.log('Number Row Display in Page ' + getTextNumRow);
    return getTextNumRow;
  }

  //
  async clickNumbertoMovePage(number: number) {
    console.log('Start click to move page');
    const btnPage2 = by.xpath('//app-tenant/rbn-common-table//p-paginator//span/a[' + number + ']');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPage2)), 5000);
    await element(btnPage2).click();
    console.log('Done');
  }

  //
  async getCurentPageActive() {
    await browser.wait(ExpectedConditions.elementToBeClickable(element(pageActive)), 3000);
    const textPage = await element(pageActive).getText();
    return textPage;
  }

  // Click from day in calender filter in Configuration Tenant Page
  async clickDay1Filter(day: number) {
    await console.log('Start click choose from day');
    const sel3 = by.xpath('//div[1]/p-calendar//a[text()="' + day + '"]');
    element(sel3).click();
    await console.log('Done');
  }

  // Click to day in calender filter in Configuration Tenant Page
  async clickDay2Filter(day: number) {
    await console.log('Start click choose from day');
    const sel3 = by.xpath('//div[2]/p-calendar//a[text()="' + day + '"]');
    element(sel3).click();
    await console.log('Done');
  }

  // Click click Last 5 Hour Button In Form Configuration Tenant Page
  async clickLast5HourButtonInForm() {
    await console.log('Start click Last 5 Hour Button In Form ');
    const sel3 = by.xpath('//*[contains(span,"Last 5 hours")]');
    element(sel3).click();
    await console.log('Done Last 5 Hour Button In Form ');
  }

  // Click Date Added in Configuration Tenant Page
  async clickApplyButton() {
    const btnApply = by.xpath('//*[contains(span,"Apply")]');
    await console.log('Start click Apply button in Date Added');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btnApply)), 3000);
    await element(btnApply).click();
    await console.log('Done click Apply button in Date Added');
  }

  // Verify date display
  async verifyDate() {
    const sel = by.css('#tb-vnf tbody.ui-table-tbody tr');
    await console.log('Start verify date display');
    expect(element(sel).isPresent()).toBeTruthy();
    await console.log('Done');
  }

  // Get text in Td display
  async GetTextInTd(numtr: number, numTd: number) {
    const txttd = by.xpath(' //app-tenant//p-accordion//p-table//table/tbody/tr[' + numtr + ']/td[' + numTd + ']');
    await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
    const getTextTd = await element(txttd).getText();
    await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
    return getTextTd;
  }

  // Verify text display
  async verifyText(filtercolumn: number, txtVerify: string) {
    const column = by.css('tbody.ui-table-tbody tr:nth-child(1) td:nth-child(' + filtercolumn + ')');
    await console.log('Start verify Text');
    await browser.wait(ExpectedConditions.visibilityOf(element(column)), 3000);
    expect(await element(column).getText()).toContain(txtVerify);
    await console.log('Done');
  }

  // Verify Filter display
  async verifyFilterDisplayed() {
    await console.log('Start verify row Filter display');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(rowFilter)), 3000);
    await expect(element(rowFilter).isPresent()).toBeTruthy();
    await console.log('Done');
  }

  // Verify Filter not display
  async verifyFilterNotDisplayed() {
    await console.log('Start verify row Filter display');
    await expect(element(rowFilter).isPresent()).toBeFalsy();
    await console.log('Done');
  }


  // Verify Record display
  async verifyRecord(txtVerify: string) {
    await console.log('Start verify record');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
    expect(await element(txtRecord).getText()).toContain(txtVerify);
    await console.log('Done');
  }

  // Check if table has Search value?
  async isTableNoValueAfterSearch() {
    await console.log('Start checking if table has not value after searching?');
    if (await element(txtNoRecord).isPresent()) {
      return false;
    } else {
      return true;
    }
  }

  // Verify display or undisplay table when click minimized
  async verifyTableAfterClickMinimized() {
    await console.log('Start verify table');
    await this.clickMinimaxTable();
    expect(await element(rowTable).isDisplayed()).toBeFalsy();
    await this.clickMinimaxTable();
    expect(await element(rowTable).isDisplayed()).toBeTruthy();
    await console.log('Done verify table');
  }

  // Verify Show/ Hide columns when select column show
  async verifyShowHidecolumns(strOptionName: string) {
    await console.log('Start verify column show or hide in table');
    const tableTH = by.xpath('//th[text()=" ' + strOptionName + ' "]');
    if (await element(tableTH).isDisplayed()) {
      return true;
    } else {
      return false;
    }
  }



  // Select number page on table in Configuration Tenant Page
  async selectNumberRowOnTable(num_row: number) {
    await console.log('Start select number row display on table');
    await browser.wait(ExpectedConditions.visibilityOf(element(lblNumberPage)), 3000);
    await element(lblNumberPage).click();
    const rowid = by.xpath('//span[text()="' + num_row + '"]');
    await element(rowid).click();
    await console.log('Done select number row display on table');
  }

  // Verify row display on 1 page in table
  async verifyNumberRowDisplayOnTable(numberRow: number) {
    await this.selectNumberRowOnTable(numberRow);
    await element.all(rowTable).count().then(async function (count) {
      await console.log('Number Row Display On Table: ' + count);
      expect(Boolean(count <= numberRow)).toBe(true);
    });
  }


  // Get Record Number
  async getRecordNumber() {
    await console.log('Start get Record Number');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
    const getnum = await element(txtRecord).getText();
    await console.log('Done');
    return getnum;

  }

  // Verify user can move to specified page
  async verifyMovePage() {
    const sel = by.css('#tb-vnf  span.pi-caret-right');
    await console.log('Start Verify Move Page');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
    const getnum = await element(txtRecord).getText();
    const number = Number(getnum.slice(0, 1));
    if (number > 5) {
      await browser.wait(ExpectedConditions.elementToBeClickable(element(sel)), 3000);
      await element(sel).click();
    }
    await console.log('Done');
  }

  // Count row Number
  async countRowNumber() {
    const RowNumber = by.xpath('//app-tenant//p-table//div[2]//div[2]/table/tbody/tr');
    await console.log('Start get Record Number');
    await browser.wait(ExpectedConditions.visibilityOf(element(RowNumber)), 3000);
    const number = await element.all(RowNumber).count();
    await console.log('Done');
    return number;
  }

  // Get Number Row display
  async getNumberRow() {
    const txtNumberRow = by.css('#tb-vnf p-paginator div.ui-dropdown');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRow)), 4000);
    const getNumRow = await element(txtNumberRow).getText();
    await console.log(' Current Row Number Display: ' + getNumRow);
    return getNumRow;
  }

  // Select number row display in page
  async SelectNumberRowDisplay(optionNumber: number) {

    const btnOptionNum = by.xpath('//app-tenant//rbn-common-table//p-paginator//p-dropdown//span');
    const filChoose = by.xpath('//li[contains(span,"' + optionNumber + '")]');

    await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptionNum)), 3000);
    await console.log('Click lable filter: ' + btnOptionNum);
    await element(btnOptionNum).click();

    await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
    await console.log('Click choose element filter: ' + filChoose);
    await element(filChoose).click();
    await console.log('Done');
}

  // Click Select in Configuration Tenant Systems Page
  async clickSelect() {
    const select = by.css('tr:nth-child(1) td.ng-star-inserted p-dropdown');
    console.log('Start click Select');
    // await browser.wait(ExpectedConditions.visibilityOf(element(select)), 2000);
    await element(select).click();
    console.log('Done');
  }

  // Click Delete in Configuration Tenant Systems Page
  async clickDelete() {
    const Delete = by.xpath('//li[contains(option,"Delete")]');
    console.log('Start click Delete');
    // await browser.wait(ExpectedConditions.visibilityOf(element(Delete)), 2000);
    await element(Delete).click();
    console.log('Done');
  }

  // Click click Confirm Delete  in Configuration Tenant Systems Page
  async clickConfirmDelete() {
    const confirmDelete = by.xpath('//button[@icon="pi pi-pw pi-check"]');
    console.log('Start click Confirm Delete');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(confirmDelete)), 2000);
    await element(confirmDelete).click();
    console.log('Done');
  }

  // Click click Confirm Cancel Delete  in Configuration Tenant Systems Page
  async clickConfirmCancelDelete() {
    const cancelDelete = by.xpath('//button[@icon="fa fa-times"]');
    console.log('Start click Cancel Delete');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(cancelDelete)), 2000);
    await element(cancelDelete).click();
    console.log('Done');
  }

  // Click sysadmin
  async clicksysadmin() {
    const Account = by.xpath('//span[text()="sysadmin"]');
    await console.log('Start click sysadmin');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(Account)), 3000);
    await element(Account).click();
    await console.log('Done click sysadmin');
  }

  async countNumberTenant() {
    await console.log('begin count Number Tenant');
    const el = by.css('p-table table > tbody > tr');
    const rowNumber = await element.all(el).count();
    await console.log('number tenant: ', rowNumber);
    return rowNumber;
  }

  async showDropActionDropDown(row: number) {
    await console.log('begin showDropActionDropDown');
    const el = by.css('tr:nth-child(' + row + ') td.ng-star-inserted p-dropdown');
    const isShow = await this.isDropListActionsDisplay();
    if (!isShow) {
        await element(el).click();
        await console.log('displayed drop list action');
    }
  }

  async isDropListActionsDisplay() {
    await console.log('begin isDropListActionsDisplay');
    const el = by.css('body > div > div.ui-dropdown-items-wrapper');
    const check = await element(el).isPresent();
    return check;
  }

  async removeAllTenant() {
    await console.log('begin remove all tenant');
    while (await this.countNumberTenant() > 0) {
        await this.showDropActionDropDown(1);
        await this.clickDelete();
        await this.clickConfirmDelete();
        await console.log('deleted row 1');
        this.clearMessage();
    }
    this.clearMessage();
    await console.log('deleted all tenant');
  }

  async clearMessage() {
    await console.log('begin clearMessage');
    const result1 = await this.pageError.verifyMessegeError();
    const result2 = await this.pageError.verifyMessegeWarning();
    const result3 = await this.pageError.verifyMessegeInfo();
    if (result1 === true || result2 === true || result3 === true) {
        await this.pageError.cliclClearALL();
    }
    await console.log('done');
  }

}

