import { element, by, ExpectedConditions, browser } from 'protractor';
import { PageCommon } from './PageCommon.po';

const txb_search = by.xpath('//*[@id="tb-vnf"]//input[@placeholder="Search"]');
const DrMn = by.xpath('//*[@id="tb-vnf"]/div/div[2]//td[8]/p-dropdown//div[3]/span');
const dialog = by.xpath('//*[@id="dialog-maintenance"]//span[text()="VNFM Maintenance"]');
const btn_Confirm = by.xpath('//*[@id="dialog-maintenance"]//span[text()="Confirm"]');
const btn_Cancel = by.xpath('//*[@id="dialog-maintenance"]//span[text()="Cancel"]');
const txtMaintenanceMode = by.xpath('//span[text()="VNFM is in Maintenance mode"]');
const txt_Record = by.xpath('//*[@id="tb-vnf"]//p-paginator/div/div//span');
const txtVNFMName = by.xpath('//*[@id="vnfmsContainer"]//table/tbody/tr/td[1]');
export class VariableElementPageVnfms {
    static tabNav = by.css('#vnfms');
    static rowNumber = by.xpath('//*[@id="vnfmsContainer"]//p-table//table/tbody/tr');
    static listDropdown = by.css('body > div > div > ul > p-dropdownitem > li');
    static searchBox = by.xpath('//*[@id="vnfmsContainer"]//p-table//div[2]/div/div/input');
    // tslint:disable-next-line:max-line-length
    static clearButton = by.css('#tb-vnf > div > div.ui-table-caption > span > div > div:nth-child(2) > div > p-button:nth-child(3) > button');
    // tslint:disable-next-line:max-line-length
    static refreshButton = by.css('#tb-vnf > div > div.ui-table-caption> span > div > div:nth-child(2) > div > p-button:nth-child(4) > button');
    // tslint:disable-next-line:max-line-length
    static iconMinus = by.css('#vnfmsTablePanel > div > div.ui-panel-titlebar > a > span.pi-minus');
    // tslint:disable-next-line:max-line-length
    static iconPlus = by.css('#vnfmsTablePanel > div > div.ui-panel-titlebar > a > span.pi-plus');
    static elMinimize = by.css('#vnfmsTablePanel > div > div.ui-panel-content-wrapper.ui-panel-content-wrapper-overflown');
    static elExpanded = by.css('#vnfmsTablePanel > div > div.ui-panel-content-wrapper');
    static entriesInOnePage = by.css('#tb-vnf p-paginator > div > p-dropdown > div > label');
    // tslint:disable-next-line:max-line-length
    static iconPerPageOptions = by.css('#tb-vnf p-paginator > div > p-dropdown > div > div.ui-dropdown-trigger.ui-state-default.ui-corner-right');
    static PerPageOptions = '#tb-vnf p-paginator > div > p-dropdown > div > div.ui-dropdown-panel > div > ul > p-dropdownitem > li';
    // tslint:disable-next-line:max-line-length
    static columnNumber = by.css('#tb-vnf table > thead > tr:nth-child(1) > th');
    // tslint:disable-next-line:max-line-length
    static showHideColumnButton = by.xpath('//*[@id="vnfmsContainer"]//p-button[@title="Show/Hide Columns"]');
    // tslint:disable-next-line:max-line-length
    static checkboxDialog = by.css('body p-dialog > div > div.ui-dialog-content.ui-widget-content > div > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default');
    // tslint:disable-next-line:max-line-length
    static checkBoxDialogChecked = by.css('body p-dialog > div > div.ui-dialog-content.ui-widget-content > div > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default.ui-state-active');
    static closeDialogButton = by.css('body app-vnfms-table > p-dialog > div > div.ui-dialog-footer p-footer > p-button > button');
    static pagesPaginator = by.css('#tb-vnf > div > p-paginator > div > span > a');
    static pagePaginatorActive = by.css('#tb-vnf > div > p-paginator > div > span > a.ui-state-active');
    static listItemDropdown = by.css('body > div > div > ul > p-dropdownitem > li');
    static multiSelectFilter = by.css('body div.ui-multiselect-items-wrapper > ul > p-multiselectitem > li');
    static multiSelectFilterIsSelected = by.css('body div.ui-multiselect-items-wrapper > ul > p-multiselectitem > li.ui-state-highlight');
    static getDynamicCellTable(column: number, row: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + column + ')');
        return el;
    }
    static getDynamicIConSortColumnTable(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf table > thead > tr:nth-child(1) > th:nth-child(' + column + ') > p-sorticon > i.pi-sort');
        return el;
    }
    static getDynamicIConSortDownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf table > thead > tr:nth-child(1) > th:nth-child(' + column + ') > p-sorticon > i.pi-sort-down');
        return el;
    }
    static getDynamicIConSortUpColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf table > thead > tr:nth-child(1) > th:nth-child(' + column + ') > p-sorticon > i.pi-sort-up');
        return el;
    }
    static getDynamicIConDropdownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf  table > thead > tr:nth-child(2) > th:nth-child(' + column + ') > p-dropdown > div > div.ui-dropdown-trigger > span');
        return el;
    }
    static getDynamicItemDropdownColumn(level: number) {
        const el = by.css('body > div > div > ul > p-dropdownitem:nth-child(' + level + ') > li');
        return el;
    }
    static getDynamicTextDropdownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf table > thead > tr:nth-child(2) > th:nth-child(' + column + ') > p-dropdown > div > label');
        return el;
    }
    static getDynamicCheckboxDialogChecked(level: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('body p-dialog > div > div.ui-dialog-content > div:nth-child(' + level + ') > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default.ui-state-active');
        return el;
    }
    static getDynamicCheckboxDialogNotChecked(level: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('body p-dialog > div > div.ui-dialog-content > div:nth-child(' + level + ') > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default');
        return el;
    }
    static getPageNumberPaginator(page: string) {
        const regExPage = new RegExp('^' + page + '$');
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('#tb-vnf p-paginator > div > span > a', regExPage);
        return el;
    }
    static getIconDropdownActions(row: number, column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-vnf table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + column + ') > p-dropdown > div > div.ui-dropdown-trigger.ui-state-default.ui-corner-right > span');
        return el;
    }
    static getDynamicItemDropdownActionsByKeyWord(nameItem: string) {
        const regExNameItem = new RegExp('^' + nameItem + '$');
        const el = by.cssContainingText('body > div > div > ul > p-dropdownitem > li', regExNameItem);
        return el;
    }
    static getDynamicRowTable(level: number) {
        const el = by.css('#tb-vnf table tbody tr:nth-child(' + level + ')');
        return el;
    }
    static getDynamicColumnTable(column: number) {
        const el = by.css('#tb-vnf table tbody tr td:nth-child(' + column + ')');
        return el;
    }
    static getIconShowMultiSelectItemByColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const iconShowMultiSelectItem = by.css('#tb-vnf table thead tr:nth-child(2) th:nth-child(' + column + ') p-multiselect div');
        return iconShowMultiSelectItem;
    }
    static getItemMultiSelect(level: number) {
        // tslint:disable-next-line:max-line-length
        const item = by.css('body div.ui-multiselect-items-wrapper > ul > p-multiselectitem:nth-child(' + level + ') > li');
        return item;
    }
    static getItemMultiSelectIsSelected(level: number) {
        // tslint:disable-next-line:max-line-length
        const item = by.css('body div.ui-multiselect-items-wrapper > ul > p-multiselectitem:nth-child(' + level + ') > li.ui-state-highlight');
        return item;
    }
}
const elEditVnfm = by.css('#vnfmFormContainer');
const btnSaveEditFrom = by.css('button[icon="pi pi-pw pi-check"]');
const btnCancelEditFrom = by.css('button[icon="fa fa-times"]');

function getDynamicInputDisable(formcontrolname: string) {
    // tslint:disable-next-line:max-line-length
    const inputDisable = by.css('#vnfmFormContainer > form > div > p-panel  input[formcontrolname="' + formcontrolname + '"]:disabled');
    return inputDisable;
}

function getDynamicInput(formcontrolname: string) {
    // tslint:disable-next-line:max-line-length
    const input = by.css('#vnfmFormContainer > form > div > p-panel  input[formcontrolname="' + formcontrolname + '"]');
    return input;
}
export class PageVNFMs extends PageCommon {
    constructor() {
        super(VariableElementPageVnfms);
    }
    // Set text Search in VNFMs Page
    async setSearch(user: string) {
        // await browser.sleep(3000);
        console.log('Start Input search.');
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        // await browser.sleep(3000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        expect(await element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Set VNFM in Maintenance Mode
    async setVNFMInMaintenanceMode() {
        // await browser.sleep(3000);
        await console.log('Start Set VNFM in Maintenance Mode');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn)), 3000);
        await element(DrMn).click();
        const Maintenance = by.xpath('//li[contains(span,"Maintenance")]');
        await element(Maintenance).click();
        await console.log('Done Set VNFM in Maintenance Mode');
        // await browser.sleep(3000);
    }

    // Verify the dialog is displayed after set VNFM in Maintenance Mode
    async verifyDisplayTheConfirmDialog() {
        // await browser.sleep(3000);
        console.log('Verify the dialog is displayed after set VNFM in Maintenance Mode');
        await browser.wait(ExpectedConditions.visibilityOf(element(dialog)), 3000);
        await expect(element(dialog).getText()).toEqual('VNFM Maintenance');
        await console.log('Done.');
    }

    // Click button confirm to place VNFM in Maintenance Mode
    async clickConfirm() {
        await console.log('Start click confirm');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Confirm)), 3000);
        await element(btn_Confirm).click();
        await console.log('Done click confirm');
    }

    // Verify VNFM is in Maintenance Mode
    async verifyVNFMEnterToTheMantenanceMode() {
        await console.log('Verify VNFM is in Maintenance Mode');
        await this.clickConfirm();
        // await browser.sleep(20000);
        await browser.wait(ExpectedConditions.visibilityOf(element(txtMaintenanceMode)), 20000);
        await expect(element(txtMaintenanceMode).isPresent()).toBeTruthy();
        await console.log('Done.');
    }

    // Click button Cancel to place VNFM in Maintenance Mode
    async clickCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
        await element(btn_Cancel).click();
        await console.log('Done click Cancel');
    }

    // click save button
    async clickSaveButtonEditForm() {
        await element(btnSaveEditFrom).click();
        await console.log('clicked save button');
    }

    // click cancel button
    async clickCancelButtonEditForm() {
        await element(btnCancelEditFrom).click();
        await console.log('clicked cancel button');
    }

    // get text input
    async getTextInput(formcontrolname: string) {
        const textInput = await element(await getDynamicInput(formcontrolname)).getAttribute('value');
        await console.log('text input: ', textInput);
        return textInput;
    }
    // set text input
    async setTextInput(formcontrolname: string, sendText: string) {
        await element(await getDynamicInput(formcontrolname)).clear();
        await element(await getDynamicInput(formcontrolname)).sendKeys(sendText);
        await console.log('sent text: ', sendText);
    }
    // Verify exit the confirm dialog after click button cancel
    async verifyExitTheConfirmDialog() {
        await console.log('Verify exit the confirm dialog after click button cancel');
        await this.clickCancel();
        // await browser.sleep(3000);
        await expect(element(dialog).isPresent()).toBeFalsy();
    }

    // Remove VNFM from Maintenance Mode
    async removeVNFMFromMaintenanceMode() {
        await console.log('Remove VNFM from Maintenance Mode');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn)), 3000);
        await element(DrMn).click();
        const Activate = by.xpath('//li[contains(span,"Activate")]');
        await element(Activate).click();
        await console.log('Remove VNFM from Maintenance Mode');
    }

    // Verify VNFM Is Removed From Mantenance Mode
    async verifyVNFMIsRemovedFromMantenanceMode() {
        await console.log('Verify VNFM is removed from the Maintenance Mode');
        await this.clickConfirm();
        // await browser.sleep(20000);
        await expect(element(txtMaintenanceMode).isPresent()).toBeFalsy();
        await console.log('Done.');
    }

    /*
        ncdinh
     */
    // Verify form edit vnfm display
    async verifyFormEditVnfmDisplay() {
        return await element(elEditVnfm).isPresent();
    }

    // Verify form edit vnfm display
    async verifyInputFormEditDisable(formcontrolname: string) {
        return await element(await getDynamicInputDisable(formcontrolname)).isPresent();
    }

    // Get VNFM Name in table
    async getVNFMNameinTable() {
        await console.log('Start Get VNFM Name in table');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtVNFMName)), 5000);
        const VNFMName = await element(txtVNFMName).getText();
        await console.log('Done');
        return VNFMName;
    }
}
