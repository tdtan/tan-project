import { element, by, ExpectedConditions, browser, protractor } from 'protractor';

const applyToAll = by.xpath('//*[@id="panel-actions"]//label[text()="Apply To All"]');
const btnSaveCloud = by.xpath('//*[@id="panel-actions"]//span[text()="Save"]');
const btnCancel = by.xpath('//*[@id="panel-actions"] //span[text()="Cancel"]');
const btnGetVersion = by.xpath('//*[@id="panel-actions"] //span[text()="Get Version"]');
const txtEditCloud = by.xpath('//*[@id="panel-actions"]//span[text()="Edit Cloud"]');

export class PageEditCloud {


  // Verify text Edit Cloud
  async verifyShowTextEditCloud() {
    console.log('Start verify text Edit Cloud.');
    await expect(element(txtEditCloud).isPresent()).toBeTruthy();
    await console.log('Done.');
  }

  // Verify text Edit Cloud not display
  async verifyEditCloudNoPresent() {
    console.log('Start verify text Edit Cloud not display.');
    await expect(element(txtEditCloud).isPresent()).toBeFalsy();
    await console.log('Done.');
  }



  // Verify previous information of cloud is displayed
  async verifyInfoDisplayed(txtConfig: string) {
    const InfoDisplayed = by.css('#panel-actions input[formcontrolname="' + txtConfig + '"].ng-valid');
    await browser.wait(ExpectedConditions.visibilityOf(element(InfoDisplayed)), 5000);
    await console.log('Start verify previous information of cloud is displayed with element ' + txtConfig);
    await expect(element(InfoDisplayed).isPresent()).toBeTruthy();
    await console.log('Done verify previous information of cloud is displayed with element ' + txtConfig);
  }


  // Set input cloud config in edit cloud Page
  async inputCloudConfig(level: number, info: string) {
    const config = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    await console.log('Start input Cloud Config');
    await browser.wait(ExpectedConditions.visibilityOf(element(config)), 3000);
    await element(config).clear();
    await element(config).sendKeys(info);
    await console.log('Done');
  }


  // Set input location in edit cloud Page
  async inputLoca(level: number, info: string) {
    const location = by.xpath('//*[@id="panel-actions"]//div/form//div[3]/div[' + level + ']/div[2]/input');
    await console.log('Start input location');
    await browser.wait(ExpectedConditions.visibilityOf(element(location)), 3000);
    await element(location).clear();
    await element(location).sendKeys(info);
    await console.log('Done');
  }


  // Click cloud config in edit cloud Page
  async clickCloudConfig(level: number) {
    const config = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    console.log('Start click cloud config');
    await browser.wait(ExpectedConditions.visibilityOf(element(config)), 2000);
    await element(config).click();
    console.log('Done');
  }

  // Click cloud config in edit cloud Page
  async clearCloudConfig(level: number) {
    const config = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    console.log('Start clear cloud config');
    await browser.wait(ExpectedConditions.visibilityOf(element(config)), 2000);
    await element(config).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    await element(config).sendKeys(protractor.Key.BACK_SPACE);
    console.log('Done');
  }


  // Verify "missing required info" message display
  async verifyMissingRequiredInfo(message: string) {
    const missingMessage = by.xpath('//div[text()=" ' + message + ' is required"]');
    await console.log('Start verify "missing required info" message display');
    await expect(element(missingMessage).isPresent()).toBeTruthy();
    await console.log('Done verify "missing required info" message display');
  }



  // Verify "invalid info" message display
  async verifyInvalidInfo(message: string) {
    const invalidMessage = by.xpath('//div[text()="' + message + '"]');
    await console.log('Start verify "invalid info" message display');
    await expect(element(invalidMessage).isPresent()).toBeTruthy();
    await console.log('Done verify "invalid info" message display');
  }


  // Verify "invalid info" message not display
  async verifyInvalidInfoNoPresent(message: string) {
    const invalidMessage = by.xpath('//div[text()="' + message + '"]');
    await console.log('Start verify "invalid info" message not display');
    await expect(element(invalidMessage).isPresent()).toBeFalsy();
    await console.log('Done verify "invalid info" message not display');
  }



  // Click location in add cloud Page
  async clickLocation(level: number) {
    const location = by.xpath('//*[@id="panel-actions"]//div/form//div[3]/div[' + level + ']/div[2]/input');
    console.log('Start click location');
    await browser.wait(ExpectedConditions.visibilityOf(element(location)), 2000);
    await element(location).click();
    console.log('Done');
  }

  // get Attribute of element in form
  async getAttributeOfElementInForm(txtConfig: string) {
    const emptyDisplayed = by.css('#panel-actions input[formcontrolname="' + txtConfig + '"].ng-valid');
    await console.log('Start get Attribute of element ' + txtConfig + ' in form');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(emptyDisplayed)), 3000);
    const value = await element(emptyDisplayed).getAttribute('value');
    await console.log('Done get Attribute of element ' + txtConfig + ' in form');
    return value;
  }

  // Select version cloud
  async selectVersionCloud(level: number, version: string) {
    // tslint:disable-next-line:max-line-length
    const selectVersion = by.xpath('//*[@id="panel-actions"]/div/div[2]/div/form/div/div[1]/div[' + level + ']/div[3]/p-dropdown/div/label');
    await element(selectVersion).click();
    const se = by.xpath('//*[@id="panel-actions"]//li[contains(span,"' + version + '")]');
    browser.driver.sleep(500);
    await element(se).click();
  }


  // Click Apply To All in  edit cloud Page
  async clickApplyToAll() {
    console.log('Start click Apply To All');
    await browser.wait(ExpectedConditions.visibilityOf(element(applyToAll)), 2000);
    await element(applyToAll).click();
    console.log('Done');
  }

  // Select Protocol in edit cloud Page
  async selectProtocol(txtChoose: string) {
    const selectProtocol = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[15]/div[2]/p-dropdown');
    const Protocol = by.xpath('//span[text()="' + txtChoose + '"]');
    await browser.wait(ExpectedConditions.visibilityOf(element(selectProtocol)), 3000);
    await console.log('click Select Protocol');
    await element(selectProtocol).click();
    await browser.wait(ExpectedConditions.visibilityOf(element(Protocol)), 3000);
    await console.log('Select Protocol');
    await element(Protocol).click();
    await console.log('Done');
  }


  // Click button Get Version in add cloud Page
  async clickGetVersion() {
    console.log('Start click button Get Version');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnGetVersion)), 2000);
    await element(btnGetVersion).click();
    console.log('Done');
  }



  // Verify version
  async verifyVersion(message: string) {
    const version = by.css('#panel-actions [aria-label="' + message + '"][placeholder="Version"]');
    await console.log('Start verify version');
    await expect(element(version).isPresent()).toBeTruthy();
    await console.log('Done verify version');
  }




  // Click button Cancel in edit cloud Page
  async clickCancel() {
    console.log('Start click Cancel');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnCancel)), 2000);
    await element(btnCancel).click();
    console.log('Done');
  }



  // Click button Save Cloud in edit cloud Page
  async clickSaveCloud() {
    console.log('Start click Save Cloud');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnSaveCloud)), 2000);
    await element(btnSaveCloud).click();
    console.log('Done');
  }


  // Verify "Cloud Instance Name" can not edit
  async verifyInstanceCanNotEdit() {
    const InstanceName = by.css('div.ui-row div.ui-g-6 :disabled');
    await console.log('Start verify element Instatiate');
    await expect(element(InstanceName).isPresent()).toBeTruthy();
    await console.log('Done verify element Instatiate');
  }




}
