import { element, by, ExpectedConditions, browser, protractor } from 'protractor';

const pupReassign = by.xpath('//*[@id="isShowRedis"]/div[@role="dialog"]');
const txbSearch = by.xpath('//*[@id="isShowRedis"]//p-accordiontab//p-table/div/div[1]//div[2]//input');
const btnShowFilter = by.xpath('//*[@id="isShowRedis"]//p-button[@icon="fa fa-sliders"]/button');
// const btnFefresh = by.xpath('//*[@id="isShowRedis"]//p-button[@icon="fa fa-refresh"]/button');
const btnShowColumn = by.xpath('//*[@id="isShowRedis"]//p-button[@title="Show/Hide Columns"]/button');
const btnClearSearch = by.xpath('//*[@id="isShowRedis"]//div[@class="container-search-input"]/p-button[@icon="fa fa-times"]/button ');
const btnClearFilter = by.xpath('//*[@id="isShowRedis"]//table//p-button[@icon="fa fa-times"]/button');
const drpMutilFilter = by.css('table thead tr th p-multiselect');
const txtRecord = by.xpath('//*[@id="isShowRedis"]//p-paginator/div/div/div/span');
const txtNumberRow = by.xpath('//*[@id="isShowRedis"]//p-table//p-paginator//p-dropdown//label');
const rowTable = by.css('#isShowRedis table tbody tr');
const txtNoRecord = by.xpath('//*[text()=" No records found "]');
const btnCancel = by.xpath('//*[@id="isShowRedis"]//p-footer//button[1]');
const btnSave = by.xpath('//*[@id="isShowRedis"]//p-footer//button[2]');
const cbxChooseVNF = by.xpath('//*[@class="ui-chkbox-box ui-widget ui-state-default ui-state-active"]');
const cbxChooseAll = by.xpath('//*[@id="isShowRedis"]//p-tableheadercheckbox');

export class PageReassignVNFs {
    // Clear textbox Search in page Reassign
    /**
     * parameter:
     * author: tthiminh
     */
    async ClearTxbSearch() {
        await console.log('Start clear textbox Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(txbSearch).sendKeys(protractor.Key.BACK_SPACE);
        await console.log('Done');
    }

    // Set text Search in page Reassign
    /**
     * parameter: search
     * author: tthiminh
     */
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // Verify row display on table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyPopupReassingPresent() {
        await console.log('Start verify popup Present');
        return await element(pupReassign).isPresent();
    }

    // verify TH Display On Table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyThDisplayOnTable(THName: String) {
        await console.log('Start verify TH Display On Table');
        const txtTH = by.xpath('//*[text()=" ' + THName + ' "]');
        return await element(txtTH).isDisplayed();
    }

    // verify dropdown filter display on table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyDrodownFilterDisplayOnTable() {
        await console.log('Start verify dropdown filter display on table');
        return await element(drpMutilFilter).isPresent();
    }

    // Click clear filter button
    async ClickClearFilterButton() {
        await console.log('Start click clear ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearFilter)), 3000);
        await element(btnClearFilter).click();
        await console.log('Done');
    }

    // Click clear button search
    async ClickClearButtonSearch() {
        await console.log('Start click clear ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done');
    }

    // click filter button
    async ClickFilterButton() {
        await console.log('Start click filter button ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done');
    }

    // Click show column button
    async ClickShowColumnButton() {
        await console.log('Start click show column button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done');
    }

    // Click Cancel button
    async ClickCancelButton() {
        await console.log('Start click cancel button ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done');
    }

    // Click Save Button
    async ClickSaveButton() {
        await console.log('Start click Save Button ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSave)), 3000);
        await element(btnSave).click();
        await console.log('Done');
    }

    // Click checkbox choose all VNF
    async ClickAllCheckboxVNF() {
        await console.log('Start click checkbox choose all VNF ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxChooseAll)), 3000);
        await element(cbxChooseAll).click();
        await console.log('Done');
    }

    // Select Option from Show/ Hide columns List in Onboarded Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name: ' + strOptionName);
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btnClose = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await console.log('Start click close show hide column');
        await element(btnClose).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="isShowRedis"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 3000);
        await element(txtClose).click();
        await console.log('Done');
    }

     // Select number row display in page
     async SelectNumberRowDisplay(optionNumber: number) {

        const btnOptionNum = by.xpath('//*[@id="isShowRedis"]//p-paginator/div/p-dropdown');
        const filChoose = by.xpath('//*[@id="isShowRedis"]//li[contains(span,"' + optionNumber + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptionNum)), 3000);
        await console.log('Click lable filter: ' + btnOptionNum);
        await element(btnOptionNum).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Get text in Action display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="isShowRedis"]/div/div[2]//table/tbody/tr[1]/td[' + numTd + ']/div/div/span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Get text in Action display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetTextFilter(numTd: number) {
        const txttd = by.xpath('//*[@id="isShowRedis"]//table/thead/tr[2]/th[' + numTd + ']/p-multiselect/div/div[2]/span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        return element(txttd).getText();

    }

    // Get text in Action display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetRecordInTable() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        return await element(txtRecord).getText();

    }

    // Get number row display in table
    /**
     * parameter:
     * author: tthiminh
     */
    async GetNumberRowDisplayInTable() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRow)), 3000);
        return await element(txtNumberRow).getText();

    }

    // get Attribute in Search
    async getAttributeSearch() {
        await console.log('Start get Attribute in Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        return await element(txbSearch).getAttribute('value');
    }

    // Count number row display in page
    /**
     * parameter:
     * author: tthiminh
     */
    async CountNumberRowInpage() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }

    // Count Checkbox Checked in form
    /**
     * parameter:
     * author: tthiminh
     */
    async CountCheckboxChecked() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxChooseVNF)), 3000);
        const count = await element.all(cbxChooseVNF).count();
        await console.log('Number Checkbox Checked in form: ' + count);
        return count;
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Verify sort column
    async verifySortColumn(numTh: number, ) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="isShowRedis"]//th[' + numTh + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="isShowRedis"]//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="isShowRedis"]//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click();
        await expect(element(sortup).isPresent()).toBeTruthy();
        await element(sortup).click();
        await expect(element(sortdown).isPresent()).toBeTruthy();

    }
}
