import { element, by, ExpectedConditions, browser } from 'protractor';
const btn_AddUser = by.css('#container-common-table p-button.button-add-action button');
const txtUser = by.xpath('//*[@id="container-common-table"]//p-header/span');
const txb_search = by.xpath('//p-accordion//p-table/div/div[1]//input[@type="text"]');
const btn_minus = by.xpath('//*[@id="panel-user"]/div/div[1]/a/span');
const btn_ClearSearch = by.xpath('//p-table/div/div[1]//p-button[@icon="fa fa-times"]');
const btn_ClearFilter = by.xpath('//p-table//thead//p-button[@icon="fa fa-times"]');
const btn_ShowFilter = by.xpath('//p-button[@icon="fa fa-sliders"]');
const btn_Refesh = by.xpath('//*[@id="tb-vnf"]//p-button[@icon="fa fa-refresh"]');
const btn_ShowHide = by.xpath('//p-button[@icon="fa fa-cog"]');
const rowTable = by.xpath('//p-table//div[2]//div[2]/table/tbody/tr');
const lblNumberRow = by.xpath('//p-paginator//p-dropdown//div[2]/span');
const NumberRow = by.xpath('//p-paginator//p-dropdown//label');

const txt_Record = by.xpath('//p-table//p-paginator/div/div//span');
const txt_NoRecord = by.xpath('//*[contains(text(),"No records found")]');



export class PageUsers {

    // Verify show text User
    async verifyShowTextUser(_textUser: any) {
        console.log('Start get text.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtUser)), 5000);
        const User = await element(txtUser).getText();
        expect(User).toEqual(_textUser);
        await console.log('text: ' + User);
        await console.log('Done.');
    }

    // Click button Add User in Add User Page
    async clickAddUser() {
        console.log('Start Click Button Add User');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_AddUser)), 3000);
        await element(btn_AddUser).click();
        console.log('Done Click Button Add User');
    }

    // Set text Search in Users Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 5000);
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }

    // Get value search in tab Users
    async getValue() {
        await console.log('Start get value');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 4000);
        const value = await element(txb_search).getAttribute('value');
        await expect(value).toEqual('');
        await console.log('Done get value');
    }

    // Click button minimized
    async clickMinimize() {
        await console.log('Start click Minimized.');
        await element(btn_minus).click();
        await console.log('Done click Minimized');
    }

    // Verify display or undisplay table when click minimized
    async verifyTableAfterClickMinimized() {
        await console.log('Start verify table');
        await this.clickMinimize();
        await expect(element(rowTable).isDisplayed()).toBeFalsy();

        await this.clickMinimize();
        await expect(element(rowTable).isDisplayed()).toBeTruthy();
        await console.log('Done verify table');
    }

    // Click button Refesh
    async clickRefesh() {
        await console.log('Start click Refesh');
        await element(btn_Refesh).click();
        await console.log('Done click Refesh');
    }

    // Click button Clear Search table
    async clickClearSearchButton() {
        await console.log('Start click Clear Search Button');
        await element(btn_ClearSearch).click();
        await console.log('Done click Clear Search Button');
    }

    // Click button ShowHide Filter table
    async clickClearFilterButton() {
        await console.log('Start click Clear Filter Button');
        await element(btn_ClearFilter).click();
        await console.log('Done click Clear Filter Button');
    }

    // Click button ShowHide Filter table
    async clickShowHideFilterButton() {
        await console.log('Start click Show Hide Filter Button');
        await element(btn_ShowFilter).click();
        await console.log('Done click Show Hide Filter Button');
    }

    // Click button Show Hide Column
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHide)), 3000);
        await element(btn_ShowHide).click();
        await console.log('Done click Show Hide Column');
    }

    // Click rowTable
    async clickRowTable() {
        await console.log('Start click row Table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        await element(rowTable).click();
        await console.log('Done click row Table');
    }

    // Select Option from Show/ Hide columns List in Users Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btn_Close = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Close)), 3000);
        await element(btn_Close).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHideColumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//table/thead//th[text()=" ' + strOptionName + ' "]');
        if (await element(tableTH).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    // Click button Delete
    async clickDelete() {
        await console.log('Start click button delete');
        const btn_delete = by.xpath('//p-accordion//p-table//table/tbody/tr/td[3]/p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_delete)), 3000);
        await element(btn_delete).click();
        await console.log('stat choose option delete');
        const opt_delete = by.xpath('//li[contains(option,"Delete ")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_delete)), 3000);
        await element(opt_delete).click();
        await console.log('Done click button delete');
    }
    // Click button cancel in Delete tab
    async clickConfirmCancel() {
        await console.log('Start click cancel');
        const btn_confirmcancel = by.xpath('//button[@icon="fa fa-times"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_confirmcancel)), 3000);
        await element(btn_confirmcancel).click();
        await console.log('Done click confirm delete');
    }

    // Click button confirm delete in Delete tab
    async clickConfirmDelete() {
        await console.log('Start click delete');
        const btn_confirmDelete = by.xpath('//button[@icon="pi pi-pw pi-check"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_confirmDelete)), 3000);
        await element(btn_confirmDelete).click();
        await console.log('Done click delete');
    }

    //
    async GetNumberRowInPage() {
        await console.log('Start get text row number');
        await browser.wait(ExpectedConditions.visibilityOf(element(NumberRow)), 3000);
        const getTextNumRow = await element(NumberRow).getText();
        await console.log('Number Row Display in Page ' + getTextNumRow);
        return getTextNumRow;
    }

    // Set table display 5 entries as default
    async setEntriesDefault() {
        await console.log('Start set 5 rows on table');
        await browser.wait(ExpectedConditions.visibilityOf(element(NumberRow)), 3000);
        await expect(element(NumberRow).getText()).toEqual('5');
        await console.log('Done set 5 rows on table');
    }

    // Count number row display in page
    async CountNumberRowInpage() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }

    // Select number page on table in User Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberRow)), 3000);
        await element(lblNumberRow).click();
        const numberrow = by.xpath('//p-dropdown//li[contains(span,"' + num_row + '")]');
        await element(numberrow).click();
        await console.log('Done select number row display on table');
    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await this.selectNumberRowOnTable(numberRow);
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            await expect(Boolean(count <= numberRow)).toBe(true);
        });
    }

    // Select number page in Users Page
    async MoveToPageNumber(page_num: number) {
        await console.log('Start select number page');
        const PageNumber = by.xpath('//a[text()="' + page_num + '"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(PageNumber)), 3000);
        await element(PageNumber).click();
        // tslint:disable-next-line:max-line-length
        const page = by.xpath('//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(page)), 3000);
        await expect(element(page).getText()).toEqual(page_num);
        await console.log('Done select number page');
    }

    // Get text record in tab Users
    async getRecord() {
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 4000);
        const _record = await element(txt_Record).getText();
        await console.log('Done get record');
        return _record;
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        await expect(await element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Select filter from table in Users page
    async SelectFilter(columnumber: number, txtChoose: string) {
        const filtercolumn = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[' + columnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click drodown filter');
        await element(filtercolumn).click();
        await console.log('Done click drodown filter');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select number row display in page
    async SelectNumberRowDisplay(optionNumber: number) {

        const filChoose = by.xpath('//li[contains(span,"' + optionNumber + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberRow)), 3000);
        await console.log('Click lable filter: ' + lblNumberRow);
        await element(lblNumberRow).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click().then(async function () {
            await browser.wait(ExpectedConditions.visibilityOf(element(sortup)), 3000);
            await expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click().then(async function () {
            await browser.wait(ExpectedConditions.visibilityOf(element(sortdown)), 3000);
            await expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');
    }

    // Check if table has Search value?
    async isTableHasNoValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txt_NoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // tslint:disable-next-line:eofline
}