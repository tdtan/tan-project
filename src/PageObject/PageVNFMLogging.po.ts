import { element, by, ExpectedConditions, browser } from 'protractor';

const txb_AccessLog = by.xpath('//*[@id="dialog-log"]//span');
const btn_download = by.xpath('//*[@id="accesslogs"]/div/button[2]');
const btn_CollectLogs = by.xpath('//*[@id="accesslogs"]/div/button[1]');
const btn_Close = by.xpath('//*[@id="dialog-log"]/div/div[1]/a');
const lcb_divInfo = by.xpath('//div[text()="Vnfm log collection job has started."]');
const lcb_divError = by.xpath('//div[text()="Vnfm log collection job has finished successfully"]');
// tslint:disable-next-line:max-line-length
const lcb_dicAlarm = by.xpath('//div[text()="Logs are being collected. You can close this window and return to download logs at any time."]');
const el_Alarm = by.xpath('//div[@class="ui-toast-summary"][text()="Info"]/following-sibling::div');


export class PageVNFMLogging {

    // Verify Empty form is displayed when user click on Access Log button
    async getTextToverifyAfterClickAccessLogButton() {
        await console.log('Start verify display Empty form');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_AccessLog)), 3000);
        return element(txb_AccessLog).getText();
    }

    // Verify button Download is hidden
    async verifyDownloadButtonIsEnabled() {
        await console.log('Verify button Download is hidden');
        await browser.wait(ExpectedConditions.visibilityOf(element(btn_download)), 3000);
        return element(btn_download).isEnabled();
    }

    // Verify button Collect Logs is hidden
    async verifyClollectLogsButtonIsEnabled() {
        await console.log('Verify button Download is hidden');
        await browser.wait(ExpectedConditions.visibilityOf(element(btn_CollectLogs)), 3000);
        return element(btn_CollectLogs).isEnabled();
    }

    // Click button Collect Logs
    async clickCollectLogsButton() {
        await console.log('Start click collect Logs');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_CollectLogs)), 3000);
        await element(btn_CollectLogs).click();
        await console.log('Done click collect logs');
    }

    // Click button Download
    async clickDownloadButton() {
        await console.log('Start click Download');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_download)), 3000);
        await element(btn_download).click();
        await console.log('Done click Download');
    }

     // Click button Close
     async clickClose() {
        await console.log('Start click close');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Close)), 3000);
        await element(btn_Close).click();
        await console.log('Done.');
    }

    //
    async verifyFormAccessLogsNotDisplayed() {
        await console.log('Start verify ');
        await expect(element(txb_AccessLog).isPresent()).toBeFalsy();
        console.log('Done.');
    }

    // get Text alarm when click collect log
    async getTextInAlarmWhenClickCollectLog() {
        await console.log('Start get Text alarm when click collect log');
        await browser.wait(ExpectedConditions.visibilityOf(element(el_Alarm)), 20000);
        return element(el_Alarm).getText();
    }

    // Verify Messege Collect Logs
    async verifyMessageCollectLogs() {
        await console.log('Start checking if display messege Collect Logs');
        // await expect(browser.wait(ExpectedConditions.visibilityOf(element(lcb_divInfo)), 10000)).toBeTruthy();
        if (await element(lcb_divInfo).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Alarm Collect Logs Display
    async verifyAlarmCollectLogsDisplay() {
        await console.log('Verify Alarm Collect Logs Display');
        // await expect(browser.wait(ExpectedConditions.visibilityOf(element(lcb_divInfo)), 10000)).toBeTruthy();
        if (await element(lcb_dicAlarm).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Messege Collect Logs Successfully
    async verifyMessageCollectLogsSuccessfully() {
        await console.log('Start checking if display messege Collect Logs Successfully');
        // await expect(browser.wait(ExpectedConditions.visibilityOf(element(lcb_divError)), 50000)).toBeTruthy();
        if (await element(lcb_divError).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }


}
