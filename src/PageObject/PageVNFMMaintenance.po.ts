import { element, by, ExpectedConditions, browser } from 'protractor';

const txb_search = by.xpath('//*[@id="tb-vnf"]//input[@placeholder="Search"]');
const DrMn = by.xpath('//*[@id="tb-vnf"]/div/div[2]//td[8]/p-dropdown//div[3]/span');
const dialog = by.xpath('//*[@id="dialog-maintenance"]//span[text()="VNFM Maintenance"]');
const btn_Confirm = by.xpath('//*[@id="dialog-maintenance"]//span[text()="Confirm"]');
const btn_Cancel = by.xpath('//*[@id="dialog-maintenance"]//span[text()="Cancel"]');
const txtMaintenanceMode = by.xpath('//span[text()="VNFM is in Maintenance mode"]');
const txt_Record = by.xpath('//*[@id="tb-vnf"]//p-paginator/div/div//span');


export class PageVNFMMaintenance {

    // Set text Search in VNFMs Page
    async setSearch(user: string) {
        await browser.sleep(3000);
        console.log('Start Input search.');
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await browser.sleep(3000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        expect(await element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Set VNFM in Maintenance Mode
    async setVNFMInMaintenanceMode() {
        await browser.sleep(3000);
        await console.log('Start Set VNFM in Maintenance Mode');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn)), 3000);
        await element(DrMn).click();
        const Maintenance = by.xpath('//li[contains(span,"Maintenance")]');
        await element(Maintenance).click();
        await console.log('Done Set VNFM in Maintenance Mode');
        await browser.sleep(3000);
    }

    // Verify the dialog is displayed after set VNFM in Maintenance Mode
    async verifyDisplayTheConfirmDialog() {
        await browser.sleep(3000);
        console.log('Verify the dialog is displayed after set VNFM in Maintenance Mode');
        await browser.wait(ExpectedConditions.visibilityOf(element(dialog)), 3000);
        await expect(element(dialog).getText()).toEqual('VNFM Maintenance');
        await console.log('Done.');
     }

    // Click button confirm to place VNFM in Maintenance Mode
    async clickConfirm() {
        await console.log('Start click confirm');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Confirm)), 3000);
        await element(btn_Confirm).click();
        await console.log('Done click confirm');
    }

    // Verify VNFM is in Maintenance Mode
    async verifyVNFMEnterToTheMantenanceMode() {
        await console.log('Verify VNFM is in Maintenance Mode');
        await this.clickConfirm();
        await browser.sleep(20000);
        await browser.wait(ExpectedConditions.visibilityOf(element(txtMaintenanceMode)), 20000);
        await expect( element(txtMaintenanceMode).isPresent()).toBeTruthy();
        await console.log('Done.');
    }

    // Click button Cancel to place VNFM in Maintenance Mode
    async clickCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
        await element(btn_Cancel).click();
        await console.log('Done click Cancel');
    }

    // Verify exit the confirm dialog after click button cancel
    async verifyExitTheConfirmDialog() {
        await console.log('Verify exit the confirm dialog after click button cancel');
        await this.clickCancel();
        await browser.sleep(3000);
        await expect( element(dialog).isPresent()).toBeFalsy();
    }

    // Remove VNFM from Maintenance Mode
    async removeVNFMFromMaintenanceMode() {
        await console.log('Remove VNFM from Maintenance Mode');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn)), 3000);
        await element(DrMn).click();
        const Activate = by.xpath('//li[contains(span,"Activate")]');
        await element(Activate).click();
        await console.log('Remove VNFM from Maintenance Mode');
    }

    // Verify VNFM Is Removed From Mantenance Mode
    async verifyVNFMIsRemovedFromMantenanceMode() {
        await console.log('Verify VNFM is removed from the Maintenance Mode');
        await this.clickConfirm();
        await browser.sleep(20000);
        await expect( element(txtMaintenanceMode).isPresent()).toBeFalsy();
        await console.log('Done.');
     }






}
