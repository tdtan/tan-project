import { element, by, ExpectedConditions, browser } from 'protractor';

const txb_search = by.xpath('//p-accordion//p-table/div/div[1]//input[@type="text"]');
const rowTable = by.xpath('//p-table//div[2]//div[2]/table/tbody/tr');
const panel = by.xpath('//p-panel//span[text()="Edit User"]');
const txb_password = by.xpath('//*[@id="panel-user-actions"]//div[3]/div[2]/input');
const txb_confirm = by.xpath('//*[@id="panel-user-actions"]//div[4]/div[2]/input');
const btn_Save = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const btn_Cancel = by.xpath('//button[@icon="fa fa-times"]');
const mis_password = by.xpath('//*[@id="panel-user-actions"]//div[text()=" Password is required. "]');
const value = by.xpath('//*[@id="tb-vnf"]//div[2]//td[2]');
const username = by.xpath('//*[@formcontrolname="Username"]');
const btn_delete = by.xpath('//p-accordion//p-table//table/tbody/tr/td[3]/p-dropdown');
const opt_delete = by.xpath('//li[contains(option,"Delete ")]');
const btn_confirmcancel = by.xpath('//*[@id="user-delete"]//button[@icon="fa fa-times"]');
const btn_confirmDelete = by.xpath('//*[@id="user-delete"]//button[@icon="pi pi-pw pi-check"]');

const txt_Record = by.xpath('//*[@id="tb-vnf"]//p-paginator/div/div//span');
const DrMn_role = by.xpath('//*[@id="panel-user-actions"]//form//p-dropdown[@name="key"]/div/label');
const DrMn_tenant = by.xpath('//*[@id="panel-user-actions"]//form//p-dropdown[@name="value"]/div/label');



export class PageEditUser {

    // Set text Search in Configuration Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 5000);
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }

    // Click button Delete
    async clickDelete() {
        await console.log('Start click button delete');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_delete)), 3000);
        await element(btn_delete).click();
        await console.log('stat choose option delete');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_delete)), 3000);
        await element(opt_delete).click();
        await console.log('Done click button delete');
    }

    // Click button cancel in Delete tab
    async clickConfirmCancel() {
        await console.log('Start click cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_confirmcancel)), 3000);
        await element(btn_confirmcancel).click();
        await console.log('Done click confirm delete');
    }

    // Click button confirm delete in Delete tab
    async clickConfirmDelete() {
        await console.log('Start click delete');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_confirmDelete)), 3000);
        await element(btn_confirmDelete).click();
        await console.log('Done click delete');
    }

    // Click UserName
    async clickUserName() {
        await console.log('Start click username');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        await element(rowTable).click();
        await console.log('Done click username');
    }

    // Verify display Edit user when click username
    async verifyAfterClickUserName() {
        await console.log('Start verify display edit user');
        await browser.wait(ExpectedConditions.visibilityOf(element(panel)), 5000);
        expect(await element(panel).isPresent()).toBeTruthy();
    }

    // Set value in password form
    async setPassword(password: string) {
        console.log('Start input password.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_password)), 5000);
        await element(txb_password).clear();
        await element(txb_password).sendKeys(password);
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_confirm)), 2000);
        await element(txb_confirm).clear();
        await element(txb_confirm).sendKeys(password);
        console.log('Done input password');
    }

    // Click button Save
    async clickSave() {
        await console.log('Start click button save');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Save)), 3000);
        await element(btn_Save).click();
        await console.log('Done click button save');
    }

    // Verify undisplay Edit User tab after click button save
    async verifyAfterClickSave() {
        await console.log('Verify undisplay Edit User tab after click button save');
        await this.clickSave();
        // await browser.sleep(5000);
        await expect(element(panel).isPresent()).toBeFalsy();
        await console.log('Done.');
    }

    // Verify display Edit User tab after missing info
    async verifyMissingInfo() {
        await console.log('Verify display Edit User tab after missing info');
        return element(panel).isPresent();
    }

    // Verify display Edit verifyUserNameDisplay
    async verifyUserNameIsEnabled() {
        await console.log('Verify display Edit verifyUserNameDisplay');
        await browser.wait(ExpectedConditions.visibilityOf(element(username)), 3000);
        return element(username).isEnabled();
    }

    // Verify display missing info massage after missing info
    async verifyDisplayMissingInfoMessage() {
        await console.log('Verify display missing info massage');
        await browser.wait(ExpectedConditions.visibilityOf(element(mis_password)), 3000);
        expect(await element(mis_password).isPresent()).toBeTruthy();
    }

    // Verify update value after edit user
    async verifyUpdatValueAfterEditElement(txtVerify: string) {
        await console.log('Verify Value is updated after edit element');
        await browser.wait(ExpectedConditions.visibilityOf(element(value)), 3000);
        expect(await element(value).getText()).toEqual(txtVerify);
    }


    // Click button Cancel
    async clickCancel() {
        await console.log('Start click button Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
        await element(btn_Cancel).click();
        await console.log('Done click button Cancel');
    }

    // Verify undisplay Edit User tab after click button cancel
    async verifyAfterClickCancel() {
        await console.log('Verify undisplay Edit User tab after click button cancel');
        // await browser.sleep(2000);
        expect(await element(panel).isPresent()).toBeFalsy();
    }

    // Verify not edit username of a user
    async verifyNotEditUserName() {
        await console.log('Start verify edit username of a user');
        // await browser.sleep(2000);
        await browser.wait(ExpectedConditions.visibilityOf(element(username)), 3000);
        expect(await element(username).isEnabled()).toBe(false);
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        // await browser.sleep(3000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        expect(await element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }
    // async getText User Name
    async getTextUser() {
        await console.log('Start get Text User');
        const rowNumber = by.css('#tb-vnf table > tbody > tr:nth-child(1) > td:nth-child(1)');
        await browser.wait(ExpectedConditions.visibilityOf(element(rowNumber)), 5000);
        const userName = element(rowNumber).getText();
        await console.log('done get Text User');
        return userName;
    }
    // verify value username
    async verifyUserName() {
        await console.log('Start  Verify Username');
        await this.clickUserName();
        const txb_username = by.css('#panel-user-actions .form-add-user form .rows-edit-form:nth-child(2) .ui-grid-col-8 input');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_username)), 5000);
        const userName = await element(txb_username).getAttribute('value');
        await console.log(userName);
        await expect(userName !== '').toBeTruthy();
        await console.log('Done  Verify Username');
    }
      // Click seclect Tenant
      async clickSelectTenant() {
        await console.log('Start click Select Tenan');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn_tenant)), 3000);
        await element(DrMn_tenant).click();
        await console.log('Done click Select Tenant');
    }
    // Click seclect Role
    async clickSelectRole() {
        await console.log('Start click Select Role');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn_role)), 3000);
        await element(DrMn_role).click();
        await console.log('Done click Select Role');
    }
    // verify Role Value
    async verifyRole() {
        await console.log('Start verify Select Role');
         await browser.wait(ExpectedConditions.visibilityOf(element(DrMn_role)), 5000);
         const itemRole = await element(DrMn_role).getText();
         await console.log(itemRole);
         await expect(itemRole !== null).toBeTruthy();
         await console.log('Done verify Select Role');
    }
    // Verify UserName Id
    async verifyUserNameId() {
        await console.log('Start  Verify Username');
        const txb_username = by.css('#panel-user-actions .form-add-user form .rows-edit-form:nth-child(1) .ui-grid-col-8');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_username)), 5000);
        const userName = await element(txb_username).getAttribute('value');
        await console.log(userName);
        await expect(userName !== '').toBeTruthy();
        await console.log('Done  Verify Username');
    }
     // verify Role Value
     async verifyTenant() {
        await console.log('Start verify Select Tenant');
         // tslint:disable-next-line:max-line-length
         await browser.wait(ExpectedConditions.visibilityOf(element(DrMn_tenant)), 5000);
         const itemTenant = await element(DrMn_tenant).getText();
         await console.log(itemTenant);
         await expect(itemTenant !== null).toBeTruthy();
         await console.log('Done verify Select Tenant');
    }


       // Click User
       async clickUser() {
        await console.log('Start click user');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        await element(rowTable).click();
        await console.log('Done click user');
    }

       // Verify display user when click user
       async verifyDisplayInfoUserAfterClickUser() {
       // await browser.sleep(3000);
        console.log('Start verify display user');
        // await browser.sleep(3000);
        await browser.wait(ExpectedConditions.visibilityOf(element(panel)), 3000);
        await expect(element(panel).isDisplayed()).toBeTruthy();
        console.log('Done verify display user');
    }

}
