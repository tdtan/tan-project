import { element, by, ExpectedConditions, browser, protractor } from 'protractor';
import { PageError } from './PageError.po';
const btnSelectActions = by.xpath('//*[@id="container-common-table"]//tr/td[2]/p-dropdown');
const txbSearch = by.css('#container-common-table div.container-search-input input');
const txtNoRecord = by.xpath('//*[text()=" No records found "]');
const btnConfirmYes = by.xpath('//*[@icon="pi pi-pw pi-check"]');
const btnShowFilter = by.css('p-button[title="Show/Hide Filter Row"] button');
const btnRefresh = by.css('p-button[title="Refresh Table"] button');
const btnShowColumn = by.css('p-button[title="Show/Hide Columns"] button');
const btnClearSearch = by.css('div.container-search-input p-button[icon="fa fa-times"] button');
const btnClearTextFilter = by.xpath('//table/thead/tr[2]/th[3]/p-button[@icon="fa fa-times"]/button');
const drpRoleFilter = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[1]/p-dropdown');
const txtRoleName = by.xpath('//*[@id="container-common-table"]//table/tbody/tr/td[1]//span');
const txtShow1Record = by.xpath('//*[text()=" Showing 1 of 1 record "]');
const txtLabelInFilter = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[1]/p-dropdown/div/label');
const txtNumberRowDisplay = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown/div/label');
const rowTable = by.css('#container-common-table table tbody tr');
const txtMenuRole = by.xpath('//*[@id="roles"]');
const lblNumberPage = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown');
const btnPage2Number = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator/div/span/a[2]');
const btnSwitchPage = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator//a[3]');
// tslint:disable-next-line:max-line-length
const txtNumberCurrentPage = by.xpath('//*[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
// tslint:disable-next-line:max-line-length
const txtNumberCurrentPage1 = by.xpath('//*[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ui-state-active ng-star-inserted"]');

export class PageAdminRoles {
    private pageError = new PageError();
    // Click Select Action in page
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickSelectActionInPage() {
        await console.log('Start Click Select Action in page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectActions)), 3000);
        await element(btnSelectActions).click();
        await console.log('Done');
    }

    // Click Option in Action
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickOptionInSelectAction(txtOption: string) {
        await console.log('Start click Option in Action');
        const btnOption = by.xpath('//li[contains(option,"' + txtOption + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOption)), 3000);
        await element(btnOption).click();
        await console.log('Done');
    }

     // Click Option in Filter
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickOptionInSelectFilter(txtOption: string) {
        await console.log('Start click Option in Filter');
        const btnOption = by.xpath('//li[contains(span,"' + txtOption + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOption)), 3000);
        await element(btnOption).click();
        await console.log('Done');
    }

    // Click Confirm Yes button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickConfirmYesButton() {
        await console.log('Start Click Confirm Yes button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmYes)), 3000);
        await element(btnConfirmYes).click();
        await console.log('Done');
    }

    // Click Refresh button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickRefreshButton() {
        await console.log('Start Click Refresh button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefresh)), 3000);
        await element(btnRefresh).click();
        await console.log('Done');
    }
    // Click Filter button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickShowCloumnButton() {
        await console.log('Start Click Show Cloumn button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done');
    }

    // Click Filter button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickFilterButton() {
        await console.log('Start Click Filter button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done');
    }

    // Click Filter Role Column
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickFilterRoleColumn() {
        await console.log('Start Click Filter Role Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpRoleFilter)), 3000);
        await element(drpRoleFilter).click();
        await console.log('Done');
    }

    // Click Choose Option Filter In Role Column
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickChooseOptionFilterInRoleColumn() {
        await console.log('Start Click Choose Option Filter In Role Column');
        const btnOption = by.xpath('//li[contains(span,"user_test")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOption)), 3000);
        await element(btnOption).click();
        await console.log('Done');
    }

    // Click Clear search button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickClearSearchButton() {
        await console.log('Start Click Clear search button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done');
    }

    // Click Clear Filter button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickClearFilterButton() {
        await console.log('Start Click Clear Filter button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearTextFilter)), 3000);
        await element(btnClearTextFilter).click();
        await console.log('Done');
    }

    // Click Close Button In Show Column
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickCloseButtonInShowColumn() {
        await console.log('Start Click Close Button In Show Column');
        const btnClose = by.xpath('//*[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await element(btnClose).click();
        await console.log('Done');
    }

    // Click Page 2 In Table
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickPage2InTable() {
        await console.log('Start Click Page 2 In Table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPage2Number)), 3000);
        await element(btnPage2Number).click();
        await console.log('Done');
    }

    // Clear text in textbox search
    /**
     * parameter:
     * author: tthiminh
     */
    async ClearTextSearch() {
        await console.log('Start Clear text in textbox search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(txbSearch).sendKeys(protractor.Key.BACK_SPACE);
        await console.log('Done');
    }

    // Count row display one page
    async countNumberRowDisplayOnTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }

    async countNumberRoleInPage() {
        await console.log('begin count Number Role In Page');
        const el = by.css('p-table table > tbody > tr');
        const rowNumber = await element.all(el).count();
        await console.log('number role in page: ', rowNumber);
        return rowNumber;
    }

    // Set text Search in Role Page
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // Select Option from Show/ Hide columns List in Configuration Tenant Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Multiple Option Name from Show Hide Columns List');
        const btnSelectCol = by.xpath('//*[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        await console.log('Done selecting Multiple Option Name from Show Hide Columns List');
    }

    // Select number page on table in Onboarded Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        // tslint:disable-next-line:max-line-length
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Get text Role Name
    async getTextRoleName() {
        await console.log('Start get text Role Name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtRoleName)), 3000);
        return element(txtRoleName).getText();

    }

    // Get text label filter
    async getTextLabelFilter() {
        await console.log('Start get text label filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtLabelInFilter)), 3000);
        return element(txtLabelInFilter).getText();

    }

    // Get Text Number Row Display In Page
    async getTextNumberRowDisplayInPage() {
        await console.log('Start get Text Number Row Display In Page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtNumberRowDisplay)), 3000);
        return element(txtNumberRowDisplay).getText();

    }

    // Get Attribute of textbox search
    async getAttributeOfTextboxSearch() {
        await console.log('Start get Attribute of textbox search');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbSearch)), 4000);
        return element(txbSearch).getAttribute('value');
    }

    // Get Text Number Page
    async getTextNumberPage() {
        await console.log('Start Get Text Number Page');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberCurrentPage)), 4000);
        return element(txtNumberCurrentPage).getText();
    }

    // Get Text Number Page
    async getTextNumberPage1() {
        await console.log('Start Get Text Number Page 1');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberCurrentPage1)), 4000);
        return element(txtNumberCurrentPage1).getText();
    }

    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 5000);
        await element(sort).click().then(function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click().then(function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');
    }

    // Verify column display in page
    async verifyColumnDisplay(columnName: string) {

        const txtTH = by.xpath('//th[text()=" ' + columnName + ' "]');
        await console.log('Start verify column display in Pages ');
        return element(txtTH).isDisplayed();
    }

    // Verify display 1 record in page
    async verifyDisplay1RecordInPage() {
        await console.log('Start verify display 1 record in page ');
        return element(txtShow1Record).isPresent();
    }

    // Verify display dropdown filter in page
    async verifyDisplayDropdownFilterInPage() {
        await console.log('Start verify display display dropdown filter in page ');
        return element(drpRoleFilter).isPresent();
    }

    // Verify Menu Role Display With User
    async verifyMenuRoleDisplayWithUser() {
        await console.log('Start verify Menu Role Display With User ');
        return element(txtMenuRole).isPresent();
    }

    // Verify Switch Button Display
    async verifySwitchButtonDisplay() {
        await console.log('Start Verify Switch Button Display');
        return element(btnSwitchPage).isEnabled();
    }

    async removeAllRole() {
        await console.log('begin removeAllRole');
        while (await this.countNumberRole() > 0) {
            await this.showDropActionDropDown(1);
            await this.selectItemDropActions(2);
            await this.clickYesConfirmDialog();
            await console.log('deleted row 1');
            this.clearMessage();
        }
        this.clearMessage();
        await console.log('deleted all vnf onboard');
    }

    async countNumberRole() {
        await console.log('begin countNumberRole');
        const el = by.css('p-table table > tbody > tr');
        const rowNumber = await element.all(el).count();
        await console.log('number Role: ', rowNumber);
        return rowNumber;
    }

    async showDropActionDropDown(row: number) {
        await console.log('begin showDropActionDropDown');
        const el = by.css('p-table table > tbody > tr:nth-child(' + row + ') > td:nth-child(2) > p-dropdown');
        const isShow = await this.isDropListActionsDisplay();
        if (!isShow) {
            await element(el).click();
            await console.log('displayed drop list action');
        }
    }

    async isDropListActionsDisplay() {
        await console.log('begin isDropListActionsDisplay');
        const el = by.css('body > div > div.ui-dropdown-items-wrapper');
        const check = await element(el).isPresent();
        return check;
    }

    async selectItemDropActions(level: number) {
        await console.log('begin selectItemDropActions');
        const el = by.css('body ul p-dropdownitem:nth-child(' + level + ') li');
        await element(el).click();
        await console.log('done select item: ', level);
    }

    async clickYesConfirmDialog() {
        await console.log('begin clickYesConfirmDialog');
        const el = by.css('p-dialog button[icon="pi pi-pw pi-check"]');
        await element(el).click();
        await console.log('done click yes confirm');
    }

    async clearMessage() {
        await console.log('begin clearMessage');
        const result1 = await this.pageError.verifyMessegeError();
        const result2 = await this.pageError.verifyMessegeWarning();
        const result3 = await this.pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await this.pageError.cliclClearALL();
        }
        await console.log('done');
    }

}
