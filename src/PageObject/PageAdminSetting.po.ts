import { element, by, ExpectedConditions, browser } from 'protractor';

const txb_search = by.xpath('//*[@id="container-common-table"]//div[@class="container-search-input"]/input');
const btn_Clearn = by.xpath('//*[@id="container-common-table"]//p-button[@icon="fa fa-times"]/button');
const btn_Refesh = by.xpath('//*[@id="container-common-table"]//p-button[@title="Refresh Table"]/button');
const btn_ShowHide = by.xpath('//*[@id="container-common-table"]//p-button[@title="Show/Hide Columns"]/button');
const btn_ShowHideFilter = by.xpath('//*[@id="container-common-table"]//p-button[@title="Show/Hide Filter Row"]/button');
const ipt_InFilter = by.css('tr th input');
const txt_Value = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[2]/input');
const btn_ClearTextfilter = by.xpath('//*[@id="container-common-table"]//tr[2]/th[3]//p-button[@icon="fa fa-times"]/button');
const rowTable = by.css('#container-common-table table tbody tr');
const lblNumberRow = by.xpath('//*[@id="container-common-table"]//p-dropdown');
const NumberRow = by.xpath('//*[@id="table-config"]//p-paginator//p-dropdown//label');

const txt_Record = by.xpath('//*[@id="container-common-table"]//p-paginator/div/div/div/span');
const txtConfiguration = by.xpath('//*[@id="container-common-table"]//p-header/span');



export class PageAdminSetting {

    // Verify show text Configuration
    async verifyShowTextConfiguration(_textConfiguration: any) {
        console.log('Start get text.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtConfiguration)), 5000);
        const Configuration = await element(txtConfiguration).getText();
        expect(Configuration).toEqual(_textConfiguration);
        await console.log('text: ' + Configuration);
        await console.log('Done.');
    }

    // Set text Search in Configuration Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 4000);
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }
    // Set text Search in Configuration Page
    async setTextIntoElementInput(textvalue: string) {
        console.log('Start set Text Into Element Input.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Value)), 4000);
        await element(txt_Value).clear();
        await element(txt_Value).sendKeys(textvalue);
        console.log('Done set Text Into Element Input.');
    }

    // Get text search in tab Configuration
    async getTextSearch() {
        await console.log('Start get text search');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 4000);
        return element(txb_search).getAttribute('value');
    }

    // Get Attribute of text filter
    async getAttributeOfTextFilter() {
        await console.log('Start get Attribute of text filter');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Value)), 4000);
        return element(txt_Value).getAttribute('value');
    }

    // Click button minimized
    async clickMinimize() {
        await console.log('Start click Show Hide Filter.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHideFilter)), 4000);
        await element(btn_ShowHideFilter).click();
        await console.log('Done');
    }

    // verify Input In Filter Display
    async verifyInputInFilterDisplay() {
        await console.log('Start verify Input In Filter Display');
        return element(ipt_InFilter).isPresent();
    }


    // Click button Refesh
    async clickRefesh() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Refesh)), 5000);
        await element(btn_Refesh).click();
        await console.log('Done click Refesh');
    }

    // Click button Clear table
    async clickClear() {
        await console.log('Start click Clear text search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Clearn)), 5000);
        await element(btn_Clearn).click();
        await console.log('Done click Clear text search');
    }

    // Click Clear button to clear text into filter table
    async clickClearTextOfFilter() {
        await console.log('Start Clear button to clear text into filter table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ClearTextfilter)), 3000);
        await element(btn_ClearTextfilter).click();
        await console.log('Done Clear button to clear text into filter table');
    }

    // Click Show Hide Filter Button In Table
    async clickShowHideFilterButtonInTable() {
        await console.log('Start click Show Hide Filter Button In Table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHideFilter)), 3000);
        await element(btn_ShowHideFilter).click();
        await console.log('Done click Show Hide Filter Button In Table');
    }

    // Click button Show Hide Column
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHide)), 3000);
        await element(btn_ShowHide).click();
        await console.log('Done click Show Hide Column');
    }

    // Select Option from Show/ Hide columns List in Configuration Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btn_Close = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Close)), 3000);
        await element(btn_Close).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//*[@id="container-common-table"]//table/thead/tr/th[text()=" ' + strOptionName + ' "]');
        if (await element(tableTH).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Set table display 5 entries as default
    async setEntriesDefault() {
        await console.log('Start set 5 rows on table');
        await browser.wait(ExpectedConditions.visibilityOf(element(NumberRow)), 3000);
        await expect(element(NumberRow).getText()).toEqual('5');
        await console.log('Done set 5 rows on table');
    }

    // Verify table display maximum 5 entries as default
    async verifyDisplayEntriesDefault() {
        await console.log('Start verify display rows on table');
        await browser.wait(ExpectedConditions.visibilityOf(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Done verify display rows on table');
        return count;
    }

    // Select number page on table in Configuration Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberRow)), 3000);
        await element(lblNumberRow).click();
        const numberrow = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(numberrow).click();
        await console.log('Done select number row display on table');
    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await this.selectNumberRowOnTable(numberRow);
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
          });
    }

    // Select number page in Configuration Page
    async MoveToPageNumber(page_num: number) {
        await console.log('Start select number page');
        const PageNumber = by.xpath('//*[@id="container-common-table"]//a[text()="' + page_num + '"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(PageNumber)), 3000);
        await element(PageNumber).click();
        // tslint:disable-next-line:max-line-length
        const page = by.xpath('//*[@id="container-common-table"]//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(page)), 3000);
        await expect(element(page).getText()).toEqual(page_num);
        await console.log('Done select number page');
    }

    // Get text record in tab Configuration
    async getRecord() {
       // await browser.sleep(3000);
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 4000);
        const _record = await element(txt_Record).getText();
        await console.log('Done get record');
        return _record;
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
       // await browser.sleep(2000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        await expect(element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }


    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 5000);
        await element(sort).click(). then (function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click(). then (function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');
    }


}
