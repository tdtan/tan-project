import { element, by, ExpectedConditions, browser } from 'protractor';
const txtTerminate = by.xpath('//*[@id="terminate"]/div/div[1]/span');
const cbx_Graceful = by.xpath('//p-checkbox[contains(label,"Graceful Destroy")]');
const cbx_Retain = by.xpath('//p-checkbox[contains(label,"Retain preallocated Floating IPs")]');
const btn_Yes = by.xpath('//*[@id="terminate"]/div/div[3]/p-footer/div/button[2]');
const btn_Cacel = by.xpath('//*[@id="terminate"]/div/div[3]/p-footer/div/button[1]');
export class PageTerminate {

    // Click Gracaful Destroy
    async clickGraceful() {
        await console.log('Start click graceful');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbx_Graceful)), 3000);
        await element(cbx_Graceful).click();
        await console.log('Done click graceful');
    }

    // Click Gracaful Destroy
    async clickRetainIp() {
        await console.log('Start click retain IPs');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbx_Retain)), 3000);
        await element(cbx_Retain).click();
        await console.log('Done click retains IPs');
    }

    // Click Confirm terminate Yes
    async clickConfirmYes() {
        await console.log('Start click Confirm teminate Yes');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Yes)), 3000);
        await element(btn_Yes).click();
        await console.log('Done click Confirm teminate Yes');
    }

    // Click Confirm terminate Cancel
    async clickConfirmCancel() {
        await console.log('Start click Confirm teminate Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cacel)), 3000);
        await element(btn_Cacel).click();
        await console.log('Done click Confirm teminate Cancel');
    }

    // Verify text in terminate page
    async verifyText() {
        await console.log('Start verify text in terminate page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtTerminate)), 3000);
        const txt_termina = await element(txtTerminate).getText();
        return txt_termina;
    }


}
