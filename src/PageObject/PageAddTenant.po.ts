import { element, by, ExpectedConditions, browser } from 'protractor';

const btnSaveTenant = by.xpath('//*[@id="panel-tenant-action"]//button[@icon="pi pi-pw pi-check"]');
const btnCancel = by.xpath('//*[@id="panel-tenant-action"]//button[@icon="fa fa-times"]');
const txtAddTenant = by.xpath('//*[@id="panel-tenant-action"]//span[text()="Add Tenant"]');

export class PageAddTenant {


    // Verify show text add tenant
  async verifyShowTextAddTenant() {
    console.log('Start verify show text add tenant.');
    await expect(element(txtAddTenant).isPresent()).toBeTruthy();
    await console.log('Done.');
  }

  // Verify text add tenant not display
  async verifyAddCloudNotPresent() {
    console.log('Start verify text add tenant not present.');
    await expect(element(txtAddTenant).isPresent()).toBeFalsy();
    await console.log('Done.');
  }


  // Verify Empty form is displayed when user click on Add tenant
  async verifyEmptyDisplayed(txtConfig: string) {
    const emptyDisplayed = by.xpath('//*[@id="panel-tenant-action"]//input[@formcontrolname="cloudTenantName"]');
    await console.log('Start verify Empty form is displayed when user click on Add tenant');
    await browser.sleep(500);
    await expect(element(emptyDisplayed).isPresent()).toBeTruthy();
    await console.log('Done verify empty form is displayed when user click on Add tenant');
}


// Click button Cancel in add tenant Page
  async clickCancel() {
    console.log('Start click Cancel');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnCancel)), 2000);
    await element(btnCancel).click();
    console.log('Done');
  }


  // Set input tenant config in add tenant Page
  async inputTenantInfo(level: number, info: string) {
    const config = by.xpath('//*[@id="panel-tenant-action"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    await console.log('Start input Tenant Info');
    await browser.wait(ExpectedConditions.visibilityOf(element(config)), 3000);
    await element(config).clear();
    await element(config).sendKeys(info);
    await console.log('Done');
  }


// Click associated cloud in add tenant Page
  async clickAssociatedCloud() {
    const Associated  = by.css('#panel-tenant-action div.ui-grid-row p-dropdown');
    console.log('Start click associated cloud ');
    await browser.wait(ExpectedConditions.visibilityOf(element(Associated)), 2000);
    await element(Associated).click();
    console.log('Done');
  }


  // Select associated Cloud in add tenant Page
  async SelectCloud(cloud: string) {
    const AssociatedCloud = by.xpath('//p-dropdown//span[text()="' + cloud + '"]');
    console.log('Start Select associated Cloud');
    await browser.wait(ExpectedConditions.visibilityOf(element(AssociatedCloud)), 2000);
    await element(AssociatedCloud).click();
    console.log('Done');
  }


  // Click button Save tenant in add tenant Page
  async clickSaveTenant() {
    console.log('Start click Save tenant');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnSaveTenant)), 2000);
    await element(btnSaveTenant).click();
   // await browser.sleep(3000);
    console.log('Done');
  }



// Verify "missing required info" message display
  async verifyMissingRequiredInfo(message: string) {
    const missingMessage = by.xpath('//div[text()=" ' + message + ' is required"]');
    await console.log('Start verify "missing required info" message display');
    // await browser.sleep(500);
    await expect(element(missingMessage).isPresent()).toBeTruthy();
    await console.log('Done verify "missing required info" message display');
  }


  // Count cloud Number
  async countRowNumber() {
    const RowNumber = by.css('#panel-tenant-action ul.ui-dropdown-items li');
   // await browser.sleep(2000);
    await console.log('Start get Record Number');
    await browser.wait(ExpectedConditions.visibilityOf(element(RowNumber)), 3000);
    const number = await element.all(RowNumber).count();
    await console.log('Done');
    return number;

  }


}
