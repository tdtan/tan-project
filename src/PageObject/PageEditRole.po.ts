/*
author ncdinh
*/
import { PageRole } from './PageRole.po';
import { by, element, browser, ExpectedConditions } from 'protractor';

const pageEditRole = by.css('#panel-role-actions');
// tslint:disable-next-line:max-line-length
const elPermissionsList = by.css('#panel-role-actions > div > div.ui-panel-content-wrapper > div > div > div.right-form > div > div > div.rows-edit-form');
// tslint:disable-next-line: max-line-length
const elSaveBtn = by.css('#panel-role-actions > div > div.ui-panel-content-wrapper > div > footer > p-toolbar > div > div > button:nth-child(2)');
const elInputRoleName = by.css('#panel-role-actions form > div > div > div.ui-grid-col-8 > input');
// tslint:disable-next-line:max-line-length
const btnDialogDeleteRole = by.css('#role-delete > div > div.ui-dialog-footer> p-footer > div > button:nth-child(2)');
const elOnAllPermissions = by.css('#allPermissionsToggle > div[aria-checked=true]');
const elOffAllPermissions = by.css('#allPermissionsToggle > div[aria-checked=false]');

export class PageEditRole {
    private pageRole = new PageRole();

    // check find key word in table
    async checkFindKeyword(textCompare: string) {
        let rs = 0;
        const numberCurrentRow = await this.pageRole.getCurrentNumberRow();
        for (let i = 1; i <= numberCurrentRow; i++) {
            const textCell = await this.pageRole.getTextCellOfTable(1, i);
            if (textCell === textCompare) {
                rs++;
            }
        }
        console.log('had found ', rs);
        return rs;
    }

    // select item in dropdown of column function
    async selectItemInDropdown(row: number, textContain: string) {
        const regTextContain = new RegExp('^' + textContain + '$');
        // tslint:disable-next-line:max-line-length
        const iconDropdown = by.css('#tb-roles table > tbody > tr:nth-child(' + row + ') > td:nth-child(2) > p-dropdown');
        // tslint:disable-next-line:max-line-length
        const item = by.cssContainingText('table tbody tr:nth-child(1) td:nth-child(2)  p-dropdown ul p-dropdownitem:nth-child(2) li', regTextContain);
        await element(iconDropdown).click();
        await browser.wait(ExpectedConditions.elementToBeClickable(element(item)), 3000);
        await element(item).click();
        await console.log('selected item');
    }

    // click save form edit role
    async clickSaveFormRole() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(elSaveBtn)), 5000);
        await element(elSaveBtn).click();
        console.log('clicked save');
    }

    // click button delete dialog
    async clickBtnDeleteRole() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnDialogDeleteRole)), 5000);
        await element(btnDialogDeleteRole).click();
        console.log('clicked');
    }
    // get permissions list
    async getPermissionsList() {
        const permissionsList = await element.all(elPermissionsList).getText();
        console.log('permissions list: ', permissionsList);
        return permissionsList;
    }
    // get text input role name
    async getTextInputRoleName() {
        const roleName = await element(elInputRoleName).getAttribute('value');
        console.log('Text in input role name: ', roleName);
        return roleName;
    }
    // get index row contain keyWork
    async getIndexRowContainKeyWork(keyWork: string) {
        let rowIndex = 0;
        const numberCurrentRow = await this.pageRole.getCurrentNumberRow();
        for (let i = 1; i <= numberCurrentRow; i++) {
            const textCell = await this.pageRole.getTextCellOfTable(1, i);
            if (textCell === keyWork) {
                rowIndex = i;
                break;
            }
        }
        await console.log('row index contain "' + keyWork + '" is: ' + rowIndex + '');
        return rowIndex;
    }
    // on all permissions
    async onAllPermissions() {
        const isOn = await element(elOnAllPermissions).isPresent();
        if (!isOn) {
            await element(elOffAllPermissions).click();
            await browser.wait(ExpectedConditions.visibilityOf(element(elOnAllPermissions)), 5000);
            console.log('Enabled');
        } else {
            console.log('Enabled');
        }
    }
    // off all permissions
    async offAllPermissions() {
        const isOff = await element(elOffAllPermissions).isPresent();
        if (!isOff) {
            await element(elOnAllPermissions).click();
            await browser.wait(ExpectedConditions.visibilityOf(element(elOffAllPermissions)), 5000);
            console.log('Disabled');
        } else {
            console.log('Disabled');
        }
    }
    // verify page edit role display
    async verifyPageEditRoleDisplay() {
        return await element(pageEditRole).isPresent();
    }
}
