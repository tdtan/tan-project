import { element, by, ExpectedConditions, browser } from 'protractor';

const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
const btnUpgrade = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txtUpgrade = by.xpath('//*[@id="vnfc-function"]/div/div[1]/span');
const txtRebuild = by.xpath('//*[@id="vnfc-function"]//label');
const btnSelectTypeUpgrade = by.xpath('//*[@id="vnfc-function"]//p-dropdown');
export class PageUpgradeVNFC {

    // Click button Upgrade
    async clickbtnUpgrade() {
        await console.log('Start click Upgrade');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnUpgrade)), 3000);
        await element(btnUpgrade).click();
        await console.log('Done click Upgrade');
    }

    // Click button Cancel
    async clickbtnCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click Cancel');
    }

    // Click lable select Version
    async clickbtnSelectTypeUpgrade() {
        await console.log('Start click lable select Version');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectTypeUpgrade)), 3000);
        await element(btnSelectTypeUpgrade).click();
        await console.log('Done click lable select Version');
    }

    // Click choose option Version
    async clickoptionUpgrade(optionVersion: string) {
        const btnoptionVersion = by.xpath('//li[contains(span,"' + optionVersion + '")]');
        await console.log('Start click option Upgrade');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnoptionVersion)), 3000);
        await element(btnoptionVersion).click();
        await console.log('Done click option Upgrade');
    }

    // Get text in description of option Heal
    async GetTextUpgrade() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtUpgrade)), 3000);
        const getTextDes = await element(txtUpgrade).getText();
        await console.log('Text Descrition: ' + getTextDes);
        return getTextDes;

    }

    // Get text in description of option Heal
    async GetTextUpgradeRebuild() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRebuild)), 3000);
        const getTextRebuild = await element(txtRebuild).getText();
        await console.log('Text Descrition: ' + getTextRebuild);
        return getTextRebuild;

    }

}
