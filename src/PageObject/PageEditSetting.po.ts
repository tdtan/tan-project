import { element, by, ExpectedConditions, browser } from 'protractor';

const rowTable = by.css('#container-common-table table tbody tr:nth-child(1)');
const panel = by.xpath('//*[text()="Edit Element"]');
const txb_form = by.xpath('//*[@id="admin-config-edit"]//input[@type="text"]');
const btn_Save = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const btn_Cancel = by.xpath('//button[@icon="fa fa-times"]');
const value = by.xpath('//*[@id="container-common-table"]//table/tbody/tr/td[2]//span');
const txt_Record = by.xpath('//*[@id="container-common-table"]//p-paginator/div/div/div/span');

export class PageEditSetting {

    // Click Element
    async clickElement() {
        await console.log('Start click element');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        await element(rowTable).click();
        await console.log('Done click element');
    }

    // Verify display Element when click element
        async verifyDisplayElementAfterClickElement() {
        console.log('Start verify display element');
        await browser.wait(ExpectedConditions.visibilityOf(element(panel)), 3000);
        await expect(element(panel).isDisplayed()).toBeTruthy();
        console.log('Done verify display element');
    }

    // Set value in Edit Element form
    async setValue(value123: string) {
        console.log('Start Input Value.');
        await element(txb_form).clear();
        await element(txb_form).sendKeys(value123);
        console.log('Done Input Value.');
    }

    // Click button Save
    async clickSave() {
        await console.log('Start click button save');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Save)), 3000);
        await element(btn_Save).click();
        await console.log('Done click button save');
    }

    // Verify undisplay Edit Element tab after click button save
    async verifyAfterClickSave() {
        await console.log('Verify undisplay Edit Element tab after click button save');
        await this.clickSave();
        expect(await element(panel).isPresent()).toBeFalsy();
    }

    // Verify update Element after edit element
    async verifyUpdatElementAfterEditElement() {
        await console.log('Verify Element is updated after edit element');
        await browser.wait(ExpectedConditions.visibilityOf(element(value)), 5000);
        const txt_elementValue = await element(value).getText();
        return txt_elementValue;
    }


    // Click button Cancel
    async clickCancel() {
        await console.log('Start click button Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
        await element(btn_Cancel).click();
        await console.log('Done click button Cancel');
    }

    // Verify undisplay Edit Element tab after click button cancel
    async verifyAfterClickCancel() {
        await console.log('Verify display Edit Element tab after click button cancel');
        expect(await element(panel).isPresent()).toBeFalsy();
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        expect(await element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }


}
