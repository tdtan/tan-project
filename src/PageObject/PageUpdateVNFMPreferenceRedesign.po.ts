import { by, element } from 'protractor';

const pupUpdateVNFPerference = by.css('#relocate div[role="dialog"]');
export class PageUpdateVNFMPreferenceRedesign {

    // check is display BulkAction
    async isDisplayBulkAction() {
        await console.log('start isDisplayBulkAction');
        const el = by.cssContainingText('#tb-vnf div.ui-table-caption p-dropdown > div > label', 'Bulk Actions');
        return await element(el).isPresent();
    }

    // get index column
    async getIndexColumn(columnName: string) {
        await console.log('start getIndexColumn');
        const el = by.css('#tb-vnf table > thead > tr:nth-child(1) > th');
        const listColumn = await element.all(el).getText();
        await console.log('listColumn: ', listColumn);
        const index = listColumn.indexOf('Actions');
        await console.log('index column ', columnName, 'is: ', index + 1);
        return index + 1;
        // let _columnnum: number;
        // _columnnum = 0;
        // for ( let i = 1 ; i <= 7; i++ ) {
        //     const _strColumnLocator = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[1]/th[' + i + ']');
        //     await element(_strColumnLocator).getText().then(function (text) {
        //         if (text === columnName) {
        //             _columnnum = i;
        //         }
        //     });
        // }
        // if (_columnnum === 0 ) {
        //     await console.log('No found ' + columnName + ' in search table');
        // } else {
        //     await console.log('Column ' + _columnnum + ' has ' + columnName + ' as name');
        // }
        // return _columnnum;
    }

    // click show drop list action
    async clickShowDropListAction(columnName: string, row: number) {
        await console.log('start clickShowDropListAction');
        const indexCol = await this.getIndexColumn(columnName);
        const el = by.css('#tb-vnf table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + indexCol + ') > p-dropdown > div');
        await element(el).click();
        await console.log('clicked');
    }

    // select item action drop
    async selectItemActionDropList(item: string) {
        await console.log('start selectItemActionDropList');
        const el = by.cssContainingText('body > div > div > ul > p-dropdownitem > li', item);
        await element(el).click();
        await console.log('selected drop list action item: ', item);
    }

    // get text drop new PreferredVNFM
    async getTextDropNewPreferredVNFM() {
        await console.log('start getTextDropNewPreferredVNFM');
        const el = by.xpath('//*[@id="redistContainer"]//p-dropdown//label');
        const txt = await element(el).getText();
        await console.log('text drop new preferredVNFM: ', txt);
        return txt;
    }

    // get list drop new PreferredVNFM
    async getListDropNewPreferredVNFM() {
        await console.log('start getListDropNewPreferredVNFM');
        let list: any = [];
        const el = by.css('p-dialog p-dropdown p-dropdownitem > li');
        list = await element.all(el).getText();
        list.splice(0, 1);
        await console.log('list drop list new PreferredVNFM: ', list);
        return list;
    }

    // get list VNFMName
    async getListVNFMName() {
        await console.log('start getListVNFMName');
        const el = by.css('table > tbody > tr > td:nth-child(1)');
        const list = await element.all(el).getText();
        await console.log('list vnfm: ', list);
        return list;
    }

    // click button No
    async clickNoButtonDialogConfirm() {
        await console.log('start clickNoButtonDialogConfirm');
        const el = by.xpath('//*[@id="relocate"]//button[@icon="fa fa-times"]');
        await element(el).click();
        await console.log('clicked');
    }

    // click button Yes
    async clickYesButtonDialogConfirm() {
        await console.log('start clickYesButtonDialogConfirm');
        const el = by.css('p-dialog p-footer button:nth-child(2)');
        await element(el).click();
        await console.log('clicked');
    }

    // check yes button enable
    async isYesButtonDialogConfirmEnabled() {
        await console.log('start isYesButtonDialogConfirmEnabled');
        const el = by.css('p-dialog p-footer button:nth-child(2)');
        const check = await element(el).isEnabled();
        await console.log('result: ', check);
        return check;
    }

    // check show dialog update VNFMPreference
    async isShowDialogUpdateVNFMPreference() {
        await console.log('start isShowDialogUpdateVNFMPreference');
        const el = by.cssContainingText('p-dialog div > div.ui-dialog-titlebar > span', 'Update VNFM preference');
        const check = await element(el).isPresent();
        await console.log('result: ', check);
        return check;
    }

    // click drop list new VNFMPreference
    async clickDropListNewVNFMPreference() {
        await console.log('start clickDropListNewVNFMPreference');
        const el = by.css('p-dialog p-dropdown > div');
        await element(el).click();
        await console.log('clicked');
    }

    async getListStatusVNFLifeCycle() {
        await console.log('start getListStatusVNFLifeCycle');
        const el = by.css('#tb-vnf table > tbody > tr > td:nth-child(6)');
        const list = await element.all(el).getText();
        await console.log('list status: ', list);
        return list;
    }

    async getListStatusVNFMReassignment() {
        await console.log('start getListStatusVNFMReassignment');
        const el = by.css('p-dialog table > tbody > tr > td:nth-child(5)');
        const list = await element.all(el).getText();
        await console.log('list status: ', list);
        return list;
    }
    async clickDropListActionVNFMSScreen() {
        await console.log('start clicked show drop list actions');
        const el = by.css('table > tbody > tr:nth-child(1) > td:nth-child(6) > p-dropdown');
        await element(el).click();
        await console.log('clicked show drop list actions');
    }

    async selectItemDropListActionVNFMSScreen(item: string) {
        const el = by.cssContainingText('body > div > div > ul > p-dropdownitem > li', item);
        await element(el).click();
        await console.log('selected item: ', item);
    }

    async showDialogReassignVNFs() {
        await this.clickDropListActionVNFMSScreen();
        await this.selectItemDropListActionVNFMSScreen('Reassign VNFs');
    }

    // Verify Popup Update VNF Perference display
    async VerifyPopupUpdateVNFPerferenceDisplay() {
        await console.log('Verify Popup Update VNF Perference display');
        return element(pupUpdateVNFPerference).isPresent();
    }
}
