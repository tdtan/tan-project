import { by, browser, element, ExpectedConditions } from 'protractor';
import { PageCommon } from './PageCommon.po';

export class VariableElementPageRole {
    // static tabNav = by.css('#vnfms');
    static rowNumber = by.css('#tb-roles table > tbody > tr');
    static searchBox = by.css('#tb-roles input[placeholder=Search]');
    static clearButton = by.css('#tb-roles p-button[icon="fa fa-times"] > button');
    static refreshButton = by.css('#tb-roles p-button[icon="fa fa-refresh"] > button');
    static iconMinus = by.css('#panel-roles span.pi-minus');
    static iconPlus = by.css('#panel-roles span.pi-plus');
    static elMinimize = by.css('#panel-roles > div > div.ui-panel-content-wrapper.ui-panel-content-wrapper-overflown');
    static elExpanded = by.css('#panel-roles > div > div.ui-panel-content-wrapper');
    static entriesInOnePage = by.css('#tb-roles > div > p-paginator > div > p-dropdown > div > label');
    // tslint:disable-next-line:max-line-length
    static iconPerPageOptions = by.css('#tb-roles > div > p-paginator > div > p-dropdown > div > div.ui-dropdown-trigger.ui-state-default.ui-corner-right');
    static PerPageOptions = '#tb-roles p-paginator p-dropdown div > ul > p-dropdownitem > li';
    static columnNumber = by.css('#tb-roles table > thead > tr:nth-child(1) > th');
    // tslint:disable-next-line:max-line-length
    static showHideColumnButton = by.css('#tb-roles p-button[icon="fa fa-table"] > button');
    // tslint:disable-next-line:max-line-length
    static checkboxDialog = by.css('body p-dialog > div > div.ui-dialog-content.ui-widget-content > div > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default');
    // tslint:disable-next-line:max-line-length
    static checkBoxDialogChecked = by.css('body p-dialog > div > div.ui-dialog-content.ui-widget-content > div > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default.ui-state-active');
    static closeDialogButton = by.css('body app-roles > p-dialog p-footer > p-button > button');
    static pagesPaginator = by.css('#tb-roles > div > p-paginator > div > span > a');
    static pagePaginatorActive = by.css('#tb-roles > div > p-paginator > div > span > a.ui-state-active');
    static listItemDropdown = by.css('body > div > div > ul > p-dropdownitem > li');
    static getDynamicCellTable(column: number, row: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + column + ')');
        return el;
    }
    static getDynamicIConSortColumnTable(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > thead > tr:nth-child(1) > th:nth-child(' + column + ') > p-sorticon > i.pi-sort');
        return el;
    }
    static getDynamicIConSortDownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > thead > tr:nth-child(1) > th:nth-child(' + column + ') > p-sorticon > i.pi-sort-down');
        return el;
    }
    static getDynamicIConSortUpColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > thead > tr:nth-child(1) > th:nth-child(' + column + ') > p-sorticon > i.pi-sort-up');
        return el;
    }
    static getDynamicIConDropdownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > thead > tr:nth-child(2) > th:nth-child(' + column + ') > p-dropdown > div > div.ui-dropdown-trigger > span');
        return el;
    }
    static getDynamicItemDropdownColumn(level: number) {
        const el = by.css('body > div > div > ul > p-dropdownitem:nth-child(' + level + ') > li');
        return el;
    }
    static getDynamicTextDropdownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > thead > tr:nth-child(2) > th:nth-child(' + column + ') > p-dropdown > div > label');
        return el;
    }
    static getDynamicCheckboxDialogChecked(level: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('body p-dialog > div > div.ui-dialog-content > div:nth-child(' + level + ') > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default.ui-state-active');
        return el;
    }
    static getDynamicCheckboxDialogNotChecked(level: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('body p-dialog > div > div.ui-dialog-content > div:nth-child(' + level + ') > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default');
        return el;
    }
    static getPageNumberPaginator(page: string) {
        const regExPage = new RegExp('^' + page + '$');
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('#tb-roles p-paginator > div > span > a', regExPage);
        return el;
    }
    static getIconDropdownActions(row: number, column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#tb-roles table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + column + ') > p-dropdown > div > div.ui-dropdown-trigger.ui-state-default.ui-corner-right > span');
        return el;
    }
    static getDynamicItemDropdownActionsByKeyWord(nameItem: string) {
        const regExNameItem = new RegExp('^' + nameItem + '$');
        const el = by.cssContainingText('body > div > div > ul > p-dropdownitem > li', regExNameItem);
        return el;
    }
    static getDynamicRowTable(level: number) {
        const el = by.css('#tb-roles > div > div.ui-table-wrapper.ng-star-inserted > table > tbody > tr:nth-child(' + level + ')');
        return el;
    }
}
// tslint:disable-next-line:max-line-length
const menuitemActive = by.css('body app-topheader > div > div.headeruser > rbn-headeruser > p-menubar > div > p-menubarsub > ul > li.ui-menuitem-active');
// tslint:disable-next-line:max-line-length
const menuitemIcon = by.css('body app-topheader > div > div.headeruser > rbn-headeruser > p-menubar > div > p-menubarsub > ul > li > a > span.ui-submenu-icon.pi.pi-fw.pi-caret-down');
// tslint:disable-next-line:max-line-length
const elLogout = by.cssContainingText('body app-topheader > div > div.headeruser > rbn-headeruser > p-menubar > div > p-menubarsub > ul > li > p-menubarsub > ul > li > a', 'Log out');
// tslint:disable-next-line:max-line-length
const btnLogout = by.css('body app-topheader > div > div.headeruser > rbn-headeruser > p-dialog > div > div.ui-dialog-footer > p-footer > div > button:nth-child(2)');
const btnLogIn = by.css('#login');
const roleNavTab = by.css('#roles');
export class PageRole extends PageCommon {
    constructor() {
        super(VariableElementPageRole);
    }
    async logOut() {
        const checkMenuUser = await element(menuitemActive).isPresent();
        if (!checkMenuUser) {
            await browser.wait(ExpectedConditions.elementToBeClickable(element(menuitemIcon)), 3000);
            await element(menuitemIcon).click();
        }
        await browser.wait(ExpectedConditions.elementToBeClickable(element(elLogout)), 3000);
        await element(elLogout).click();
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnLogout)), 3000);
        await element(btnLogout).click();
        await browser.wait(ExpectedConditions.urlContains('/login'), 22000);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnLogIn)), 3000);
    }

    async verifyRoleNavTabVisible() {
        return await element(roleNavTab).isPresent();
    }

    async clickRowContainKeyWord(column: number, keyWord: string) {
        console.log('begin click');
        // const regExPage = new RegExp('^' + rolename + '$');
        const el = by.cssContainingText('table > tbody > tr > td:nth-child(' + column + ')', keyWord);
        await element(el).click();
        console.log('done click row contain: ', keyWord);
    }

    // Get Record Number
    async getRecordNumber() {
    await console.log('Start get Record Number');
    const txtRecord = by.xpath('//p-table/div/p-paginator/div/div/div/span');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
    const getnum = await element(txtRecord).getText();
    await console.log('Done');
    return getnum;

  }
}
