import { element, by, browser, ExpectedConditions, protractor } from 'protractor';

/*tab left nav*/
// this.elCommon.tabNav = by.css('#vnfms');

/*row table*/
// this.elCommon.rowNumber

/*search box*/
// this.elCommon.searchBox

/*clear button*/
// this.elCommon.clearButton

/*refresh button*/
// this.elCommon.refreshButton

/*icon Minus in panel*/
// this.elCommon.iconMinus

/*icon Plus in panel*/
// this.elCommon.iconPlus

/*element create when panel minimize*/
// this.elCommon.elMinimize

/*element create when panel expanded*/
// this.elCommon.elExpanded

/*tables display entries number in one page as default*/
// this.elCommon.entriesInOnePage

/*icon of per page options */
// this.elCommon.iconPerPageOptions

/*per page options */
// this.elCommon.PerPageOptions

/*column of table*/
// this.elCommon.columnNumber

/*show,Hide column button*/
// this.elCommon.showHideColumnButton

/*check box dialog show/hide button*/
// this.elCommon.checkboxDialog

/*check box dialog show/hide button is checked*/
// this.elCommon.checkBoxDialogChecked

/*button close dialog show/hide button*/
// this.elCommon.closeDialogButton

/*page of table*/
// this.elCommon.pagesPaginator

/*page active of table*/
// this.elCommon.pagePaginatorActive

/*list item dropdown when apPenToBody*/
// this.elCommon.istItemDropdown

/*list item multi select*/
// this.elCommon.multiSelectFilter

/*list item multi select is selected*/
// this.elCommon.multiSelectFilterIsSelected

/*cell of table*/
// this.elCommon.getDynamicCellTable(column: number, row: number)

/*icon sort in column*/
// this.elCommon.getDynamicIConSortColumnTable(column: number)

/*icon sort down in column*/
// this.elCommon.getDynamicIConSortDownColumn(column: number)

/*icon sort up in column*/
// this.elCommon.getDynamicIConSortUpColumn(column: number)

/*icon dropdown in column*/
// this.elCommon.getDynamicIConDropdownColumn(column: number)

/*item dropdown in column*/
// this.elCommon.getDynamicItemDropdownColumn(level: number)

/*text dropdown in column*/
// this.elCommon.getDynamicTextDropdownColumn(column: number)

/*check box is checked in dialog show/hide column*/
// this.elCommon.getDynamicCheckboxDialogChecked(level: number)

/*check box is not checked in dialog show/hide column*/
// this.elCommon.getDynamicCheckboxDialogNotChecked(level: number)

/*get page number by key word*/
// this.elCommon.getPageNumberPaginator(page: string)

/*get icon dropdown in actions column*/
// static getIconDropdownActions(row: number, column: number)

/*get item dropdown in actions column*/
// this.elCommon.getDynamicItemDropdownActionsByKeyWord(nameItem: string)

/*get row table*/
// this.elCommon.getDynamicRowTable(level: number)

/*get column table*/
// this.elCommon.getDynamicColumnTable(column: number)

/*get icon dropdown multi select*/
// this.elCommon.getIconShowMultiSelectItemByColumn(column: number)

/*get item dropdown multi select*/
// this.elCommon.getItemMultiSelect(level: number)

/*get item dropdown multi select is selected*/
// this.elCommon.getItemMultiSelectIsSelected(level: number)

export class PageCommon {
    elCommon: any;
    constructor(el: any) {
        this.elCommon = el;
    }

    // Get the current row number of the table
    async getCurrentNumberRow() {
        const countRow = await element.all(this.elCommon.rowNumber).count();
        await console.log('row of table: ', countRow);
        return countRow;
    }
    // Get the text on the table cell
    async getTextCellOfTable(column: number, row: number) {
        const textCell = await element(this.elCommon.getDynamicCellTable(column, row)).getText();
        await console.log('text of cell(' + column + ',' + row + '): ', textCell);
        return textCell;
    }
    // get list in column to lower case
    async getListCurrentPageColumnToLowerCase(column: number) {
        const numberRow = await this.getCurrentNumberRow();
        const list = [];
        for (let i = 1; i <= numberRow; i++) {
            const roleName = await this.getTextCellOfTable(column, i);
            await list.push(roleName.toLowerCase());
        }
        await console.log('List column ' + column + ': ', list);
        return list;
    }
    // get text on dropdown
    async getTextOnDropdownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = this.elCommon.getDynamicTextDropdownColumn(column);
        const textOnDropdownRole = await element(el).getText();
        await console.log('text selected of dropdown: ', textOnDropdownRole);
        return textOnDropdownRole;
    }
    // get list dropdown column
    async getListDropdownColumn() {
        const list = await element.all(this.elCommon.listItemDropdown).getText();
        await console.log('list dropdown item: ', list.length);
        return list;
    }
    // get Rows Per Page Options
    async getRowsPerPageOptions() {
        const rowsPerPage = await Number(await element(this.elCommon.entriesInOnePage).getText());
        await console.log('entries in one page as default:', rowsPerPage);
        return rowsPerPage;
    }
    // get current number column
    async getCurrentNumberColumn() {
        const countColumn = await element.all(this.elCommon.columnNumber).count();
        await console.log('column of table: ', countColumn);
        return countColumn;
    }
    // get total checkBox dialog
    async getTotalCheckBoxDialog() {
        const totalCheckbox = await element.all(this.elCommon.checkboxDialog).count();
        await console.log('Number checkbox in dialog show/hide column: ', totalCheckbox);
        return totalCheckbox;
    }
    // get number checkBox dialog checked
    async getNumberCheckBoxDialogChecked() {
        const numberCheckbox = await element.all(this.elCommon.checkBoxDialogChecked).count();
        await console.log('Number checkbox checked: ', numberCheckbox);
        return numberCheckbox;
    }
    // get list item dropdown
    async getListItemDropdown() {
        const listItem = await element.all(this.elCommon.listItemDropdown).getText();
        await console.log('list dropdown: ', listItem);
        return listItem;
    }
    // get list number page in table
    async getListNumberPageInTable() {
        const listPagesPaginator = await element.all(this.elCommon.pagesPaginator).getText();
        await console.log('Pages Paginator: ', listPagesPaginator);
        return listPagesPaginator;
    }
    // get number page active in table
    async getNumberPageActiveInTable() {
        return await element(this.elCommon.pagePaginatorActive).getText();
    }
    // get list on all in column
    async getListOnAllPageInColumnToLowerCase(column: number) {
        const listAllDataColumn: any = [];
        const listNumberPageInTable = await this.getListNumberPageInTable();
        if (listNumberPageInTable.length <= 1) {
            const arr = await this.getListCurrentPageColumnToLowerCase(column);
            await console.log('list all column ' + column + ': ', arr);
            return arr;
        } else {
            for (let index = 1; index <= listNumberPageInTable.length; index++) {
                await this.moveNumberPageInTable('' + index);
                const arr = await this.getListCurrentPageColumnToLowerCase(column);
                await listAllDataColumn.push(...arr);
            }
            await this.moveNumberPageInTable('1');
            await console.log('list all column ' + column + ': ', listAllDataColumn);
            return listAllDataColumn;
        }
    }

    // get list item dropdown actions
    async getListDropdownActions(row: number, column: number) {
        await this.clickIconDropdownActions(row, column);
        const list = await element.all(this.elCommon.listItemDropdown).getText();
        console.log('list item dropdown actions: ', list);
        // close dropdown item
        await this.clickIconDropdownActions(row, column);
        return list;
    }
    // get index column contain keyword
    async getIndexColumnContainsKeyWord(keyWord: string) {
        let indexColumn = -1;
        const listColumn = await element.all(this.elCommon.columnNumber).getText();
        for (let index = 0; index < listColumn.length; index++) {
            if (listColumn[index] === keyWord) {
                indexColumn = index + 1;
                break;
            }
        }
        await console.log('column index contain "' + keyWord + '": ', indexColumn);
        return indexColumn;
    }
    // get index column contain keyword
    async getListColumnCurrentPage(column: number) {
        const list = await element.all(this.elCommon.getDynamicColumnTable(column)).getText();
        await console.log('list column in current page: ', list);
        return list;
    }
    // get list multi select is selected
    async getListMultiSelectFilterIsSelected() {
        const list = await element.all(this.elCommon.multiSelectFilterIsSelected).getText();
        await console.log('list multiSelect is selected: ', list);
        return list;
    }
    // get list multi select
    async getListMultiSelectFilter() {
        const list = await element.all(this.elCommon.multiSelectFilter).getText();
        await console.log('list multiSelect: ', list);
        return list;
    }
    // click sort ascending
    async clickSortAscending(column: number) {
        const iconSort = this.elCommon.getDynamicIConSortColumnTable(column);
        const iconSortDown = this.elCommon.getDynamicIConSortDownColumn(column);
        const iconSortUp = this.elCommon.getDynamicIConSortUpColumn(column);
        if (await element(iconSort).isPresent()) {
            await element(iconSort).click();
            await console.log('clicked sort ascending');
        } else if (await element(iconSortDown).isPresent() === false) {
            await element(iconSortUp).click();
            await element(iconSortDown).click();
            await console.log('clicked sort ascending');
        } else if (await element(iconSortDown).isPresent()) {
            await element(iconSortDown).click();
            await console.log('clicked sort ascending');
        }
    }

    // click sort descending
    async clickSortDescending(column: number) {
        const iconSort = this.elCommon.getDynamicIConSortColumnTable(column);
        const iconSortDown = this.elCommon.getDynamicIConSortDownColumn(column);
        const iconSortUp = this.elCommon.getDynamicIConSortUpColumn(column);
        if (await element(iconSort).isPresent()) {
            await element(iconSort).click();
        }
        if (await element(iconSortUp).isPresent()) {
            await element(iconSortUp).click();
        } else {
            await element(iconSortDown).click();
            await element(iconSortUp).click();
        }
        await console.log('clicked sort descending');
    }

    // click show dropdown item
    async clickShowDropdownItemColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const iconDrop = this.elCommon.getDynamicIConDropdownColumn(column);
        await element(iconDrop).click();
        await console.log('show dropdown item');
    }
    // click clear button
    async clickClearButton() {
        await console.log('start click clear button');
        await element(this.elCommon.clearButton).click();
        await console.log('clicked clear button');
    }

    // click refresh button
    async clickRefreshButton() {
        await console.log('start click refresh button');
        await element(this.elCommon.refreshButton).click();
        await console.log('clicked refresh button');
    }
    // click minimize table
    async clickMinimizeTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(this.elCommon.iconMinus)), 3000);
        await element(this.elCommon.iconMinus).click();
        await console.log('clicked Minimize');
    }

    // click expanded table
    async clickExpandedTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(this.elCommon.iconPlus)), 3000);
        await element(this.elCommon.iconPlus).click();
        await console.log('clicked expand');
    }
    // click show hide column button
    async clickShowHideColumnButton() {
        await console.log('start click');
        await element(this.elCommon.showHideColumnButton).click();
        await console.log('clicked show/hide button');
    }
    // click close dialog button
    async clickCloseDialogButton() {
        await console.log('start click');
        await element(this.elCommon.closeDialogButton).click();
        await console.log('closed dialog show/hide column');
    }

    // click icon dropdown action
    async clickIconDropdownActions(row: number, column: number) {
        await console.log('start click');
        await element(this.elCommon.getIconDropdownActions(row, column)).click();
        await console.log('clicked icon dropdown actions');
    }
    async clickRowTable(level: number) {
        await element(this.elCommon.getDynamicRowTable(level)).click();
        await console.log('clicked row table level: ', level);
    }
    // click icon show multi select
    async clickIconShowMultiSelectItemByColumn(column: number) {
        await element(this.elCommon.getIconShowMultiSelectItemByColumn(column)).click();
        await console.log('clicked icon show multiSelect');
    }
    // sort ascending
    async sortListAscending(List: any) {
        const sortList = await List.sort();
        await console.log('sort ascending: ', sortList);
        return sortList;
    }

    // sort descending
    async sortListDescending(List: any[]) {
        const sortList = await List.sort((a, b) => b.localeCompare(a));
        await console.log('Sort descending: ', sortList);
        return sortList;
    }

    // select item
    async selectItemDropdownColumn(level: number) {
        const itemDrop = this.elCommon.getDynamicItemDropdownColumn(level);
        await element(itemDrop).click();
        await console.log('selected item level: ', level);
    }
    // select rows per page options
    async selectRowsPerPageOptions(textOption: string) {
        const regExpTextOption = new RegExp('^' + textOption + '$');
        await console.log('select Rows Per Page Options');
        await element(this.elCommon.iconPerPageOptions).click();
        const el1 = by.cssContainingText(this.elCommon.PerPageOptions, regExpTextOption);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(el1)), 3000);
        await element(el1).click();
        await console.log('done');
    }

    // unSelect all checkBox dialog
    async unSelectAllCheckBoxDialog() {
        const numberCheckBox = await this.getTotalCheckBoxDialog();
        for (let i = 1; i <= numberCheckBox; i++) {
            // tslint:disable-next-line:max-line-length
            const checkboxDialogChecked = this.elCommon.getDynamicCheckboxDialogChecked(i);
            if (await element(checkboxDialogChecked).isPresent()) {
                await element(checkboxDialogChecked).click();
                await browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(element(checkboxDialogChecked))), 3000);
            }
        }
        await console.log('unselected all check box');
    }

    // select all checkBox dialog
    async selectAllCheckBoxDialog() {
        const numberCheckBox = await this.getTotalCheckBoxDialog();
        for (let i = 1; i <= numberCheckBox; i++) {
            // tslint:disable-next-line:max-line-length
            const checkboxDialogChecked = this.elCommon.getDynamicCheckboxDialogChecked(i);
            // tslint:disable-next-line:max-line-length
            const checkboxDialogNotChecked = this.elCommon.getDynamicCheckboxDialogNotChecked(i);
            const isCheck = await element(checkboxDialogChecked).isPresent();
            if (!isCheck) {
                await element(checkboxDialogNotChecked).click();
                await browser.wait(ExpectedConditions.visibilityOf(element(checkboxDialogChecked)), 3000);
            }
        }
        await console.log('selected all check box');
    }
    // select item dropdown bay keyword
    async selectItemDropdownActionsByKeyWord(keyWord: string, row: number, column: number) {
        await this.clickIconDropdownActions(row, column);
        await element(this.elCommon.getDynamicItemDropdownActionsByKeyWord(keyWord)).click();
        await console.log('selected item ', keyWord);
    }
    // select item multi select
    async selectItemMultiSelect(numberItems: number) {
        for (let i = 1; i <= numberItems; i++) {
            const checkItem = await this.checkItemMultiSelectDisplay(i);
            if (!checkItem) {
                await element(this.elCommon.getItemMultiSelect(i)).click();
            }
        }
    }
    // unselect item multi select
    async unselectItemMultiSelect(numberItems: number) {
        for (let i = 1; i <= numberItems; i++) {
            const checkItem = await this.checkItemMultiSelectDisplay(i);
            if (checkItem) {
                await element(this.elCommon.getItemMultiSelect(i)).click();
            }
        }
    }
    // set text on search box
    async setTextOnSearchBox(text: string) {
        await element(this.elCommon.searchBox).clear();
        await element(this.elCommon.searchBox).sendKeys(text);
        await console.log('set text: ', text);
    }

    // clear text on search box
    async clearTextOnSearchBox() {
        await element(this.elCommon.searchBox).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(this.elCommon.searchBox).sendKeys(protractor.Key.BACK_SPACE);
        await console.log('cleared');
    }

    // move page
    async moveNumberPageInTable(page: string) {
        const pageNumberPaginator = this.elCommon.getPageNumberPaginator(page);
        await element(pageNumberPaginator).click();
        await console.log('clicked page: ', page);
    }
    // check item multi select is display
    async checkItemMultiSelectDisplay(level: number) {
        return await element(this.elCommon.getItemMultiSelectIsSelected(level)).isPresent();
    }
    // Verify that the VNFMs tab displayed in left navigation bar
    async verifyVNFMsTabDisplayed() {
        return await element(this.elCommon.tabNav).isPresent();
    }

    // verify sort by keyword
    async verifyDropdownFilterNotAll(textCompare: string, column: number) {
        let flag = true;
        const numberCurrentRow = await this.getCurrentNumberRow();
        for (let i = 1; i <= numberCurrentRow; i++) {
            const textCell = await this.getTextCellOfTable(column, i);
            if (textCell !== textCompare) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    // verify sort by keyword all
    async verifyDropdownFilterAll(rowOnTable: number) {
        const numberCurrentRow = await this.getCurrentNumberRow();
        if (numberCurrentRow === rowOnTable) {
            return true;
        } else {
            return false;
        }
    }
    // verify dropdown filter column
    async verifyDropdownFilterColumn(column: number) {
        const rowTable = await this.getCurrentNumberRow();
        await this.clickShowDropdownItemColumn(column);
        const listItem = await this.getListDropdownColumn();
        if (listItem.length >= 2) {
            await this.selectItemDropdownColumn(2);
            const textOnDrop = await this.getTextOnDropdownColumn(column);
            await expect(await this.verifyDropdownFilterNotAll(textOnDrop, column)).toBeTruthy();
        }
        const isShowListDropdown = await element(this.elCommon.listItemDropdown).isPresent();
        if (!isShowListDropdown) {
            await this.clickShowDropdownItemColumn(column);
        }
        await this.selectItemDropdownColumn(1);
        await expect(await this.verifyDropdownFilterAll(rowTable)).toBeTruthy();
    }

    // verify sort functions column
    async verifySortColumn(column: number) {
        // sort Ascending
        let initialListColumn = await this.getListOnAllPageInColumnToLowerCase(column);
        initialListColumn = await this.sortListAscending(initialListColumn);
        const currentNumberRow = await this.getCurrentNumberRow();
        const sliceInitialListColumn1 = initialListColumn.slice(0, Number(currentNumberRow));
        await this.clickSortAscending(column);
        const sortAscendinglistColumn = await this.getListCurrentPageColumnToLowerCase(column);
        await expect(sliceInitialListColumn1).toEqual(sortAscendinglistColumn);
        // sort Descending
        initialListColumn = await this.sortListDescending(initialListColumn);
        const sliceInitialListColumn2 = initialListColumn.slice(0, Number(currentNumberRow));
        await console.log(sliceInitialListColumn2);
        await this.clickSortDescending(column);
        const sortDescendinglistColumn = await this.getListCurrentPageColumnToLowerCase(column);
        await expect(sliceInitialListColumn2).toEqual(sortDescendinglistColumn);
    }

    // verify filter search box
    async verifyFilterSearchBox(textCompare: string, column: number) {
        let flag = true;
        const numberCurrentRow = await this.getCurrentNumberRow();
        if (numberCurrentRow === 0) {
            flag = false;
            return flag;
        }
        for (let i = 1; i <= numberCurrentRow; i++) {
            const textCell = await this.getTextCellOfTable(column, i);
            const isIncludeTextCompare = await textCell.includes(textCompare);
            if (!isIncludeTextCompare) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    // verify table minimized
    async verifyTableMinimized() {
        return await element(this.elCommon.elMinimize).isPresent();
    }

    // verify table Expanded
    async verifyTableExpanded() {
        if (await element(this.elCommon.elExpanded).isPresent() && await element(this.elCommon.elMinimize).isPresent() === false) {
            return true;
        } else {
            return false;
        }
    }
}
