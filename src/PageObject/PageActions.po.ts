import { browser, ExpectedConditions, element, by } from 'protractor';
const txb_search = by.xpath('//*[@id="tb-vnf"]//div[1]//div[2]//input');
const txtRecord = by.xpath('//*[@id="tb-vnf"]//p-paginator/div/div/div/span');
const txt_Value = by.xpath('//*[@id="tb-vnf"]//div[1]//div[2]/div/input');
const btn_Clean = by.xpath('//*[@id="tb-vnf"]//p-button[@icon="fa fa-times"]');
const btn_Refesh = by.xpath('//*[@id="tb-vnf"]//p-button[@icon="fa fa-refresh"]');
const rowTable = by.css('#tb-vnf table tbody tr');
const iconMinus = by.css('#vnfmsTablePanel span.pi-minus');
const iconPlus = by.css('#vnfmsTablePanel span.pi-plus');
const txtNumberRow = by.xpath('//*[@id="tb-vnf"]/div/p-paginator/div/p-dropdown/div/label');
const btn_ShowHide = by.xpath('//*[@id="tb-vnf"]//p-button[@icon="fa fa-table"]');
// tslint:disable-next-line:max-line-length
const lblNumberRow = by.css('#tb-vnf > div > p-paginator > div > p-dropdown > div > div.ui-dropdown-trigger.ui-state-default.ui-corner-right > span');
export class PageActions {
    // VeriFy Action Tag in Left Nav
    async VerifyLeftNav(level: number) {
        console.log('Start click menu have number: ' + level);
        const sel = 'div.ui-panelmenu-panel:nth-child(' + level + ')';
        await browser.wait(ExpectedConditions.visibilityOf(element(by.css(sel))), 7000);
        const menuName = await element(by.css(sel)).getText();
        await console.log('Current menu: ' + menuName);
        await console.log('Done');
        return menuName;
      }
    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[1]/th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[1]/th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[1]/th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click(). then (function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click(). then (function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');

    }
    // Select filter from talbe in Actions Page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown//label');
        const filChoose = by.xpath('/html/body/div/div/ul//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await browser.sleep(2000);
        await console.log('Done');
    }
    // click icon filter
     async clickFilter(columnnumber: number) {
          // tslint:disable-next-line:max-line-length
        const filtercolumn = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect//span[@class ="ui-multiselect-trigger-icon ui-clickable pi pi-chevron-down"]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();
        await console.log('Done click filter');
     }
     // Select filter from talbe in Actions Page
     async SelectMultiFilter(columnnumber: number, txtChoose: string) {

        // tslint:disable-next-line:max-line-length
        const filtercolumn = by.xpath('//*[@id="tb-vnf"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect//span[@class ="ui-multiselect-trigger-icon ui-clickable pi pi-chevron-down"]');
        const filChoose = by.xpath('/html/body/div/div[2]/ul/p-multiselectitem/li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await browser.sleep(2000);
        await console.log('Done');
    }
    // Get text record in table Live VNF
    async getRecord() {
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 4000);
        const _record = await element(txtRecord).getText();
        await console.log('Text record: ' + _record);
        await console.log('Done get record');
        return _record;
    }
    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await browser.sleep(2000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Done');
    }
     // Set text Search in Actions Page
     async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 4000);
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }
    // Get value from input Text
    async getValue() {
        await console.log('Start get value');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Value)), 4000);
        const value = await element(txt_Value).getAttribute('value');
       // await expect(value).toEqual('');
       return value;
        await console.log('Done get value');
    }
    // Function click button Clear
    async click_Clear() {
    console.log('Start click button Clear');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Clean)), 3000);
    await element(btn_Clean).click();
    console.log('Done Click Button Clears');
    }
    // Function click button Refresh
    async clickRefresh() {
        await browser.sleep(3000);
        console.log('Start click button Refresh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Refesh)), 3000);
        await element(btn_Refesh).click();
        console.log('Done Click Button Refresh');
     }
    // Function click Minimize
    async clickMinimizeTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(iconMinus)), 3000);
        await element(iconMinus).click();
        await browser.sleep(1000);
        await console.log('clicked Minimize');
    }
    // Function click Expand
    async clickExpandedTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(iconPlus)), 3000);
        await element(iconPlus).click();
        await browser.sleep(1000);
        await console.log('clicked expand');
    }
    // Function Verify Minimized
    async verifyTableMinimized() {
        return  await element(rowTable).isDisplayed();
    }

    // verify table Expanded
    async verifyTableExpanded() {
        return await element(rowTable).isDisplayed();
    }
    // Select number page in Configuration Page
    async MoveToPageNumber() {
        await console.log('Start select number page');
        const PageNumber = by.css('#tb-vnf p-paginator .ui-paginator-bottom span.ui-paginator-pages a');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(PageNumber)), 3000);
        await element(PageNumber).click();
        await console.log('Start verify page ');
        // tslint:disable-next-line:max-line-length
        const page = by.css('#tb-vnf p-paginator .ui-paginator-bottom span.ui-paginator-pages a.ui-state-active');
        await browser.wait(ExpectedConditions.visibilityOf(element(page)), 3000);
        await console.log(await element(page).getText());
        await expect(await element(page).getText()).toEqual('1');
        await console.log('Done select number page');
    }
    // Get Number Row display
    async GetNumberRow() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRow)), 4000);
        const getNumRow = await element(txtNumberRow).getText();
        await console.log(' Current Row Number Display: ' + getNumRow);
        return getNumRow;

    }
    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await this.selectNumberRowOnTable(numberRow);
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
          });
    }
    // Select number page on table in Database Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberRow)), 3000);
        await element(lblNumberRow).click();
        await console.log('Start select number row ');
        const numberrow = by.xpath('//*[@id="tb-vnf"]//p-dropdown//li[contains(span,"' + num_row + '")]');
        await element(numberrow).click();
        await console.log('Done select number row display on table');
    }
    // Click button Show Hide Column
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHide)), 3000);
        await element(btn_ShowHide).click();
        await console.log('Done click Show Hide Column');
    }

    // Select Option from Show/ Hide columns List in Actions Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btn_Close = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Close)), 3000);
        await element(btn_Close).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }
 // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await browser.sleep(2000);
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//*[@id="tb-vnf"]//table/thead//th[text()=" ' + strOptionName + ' "]');
        if (await element(tableTH).isPresent()) {
            return true;
        } else {
            return false;
        }
    }
    // verify dropdown action has Pause, Resume, Cancel actions
    async verifyDropDownActions(pause: string, resume: string, cancel: string) {
        const pause_action = by.xpath('/html/body/div/div/ul/li[1]');
        const resume_action = by.xpath('/html/body/div/div/ul/li[2]');
        const cancel_action = by.xpath('/html/body/div/div/ul/li[3]');
        const pause_value = await element(pause_action).getText();
        const resume_value = await element(resume_action).getText();
        const cancel_value = await element(cancel_action).getText();
        if (( pause === pause_value)  && (resume === resume_value) && (cancel === cancel_value)) {
            return true;
        } else {
            return false;
           }

    }
    // verify Search result
    async isAftersearch(txtVerify: string) {
        console.log('start verify Record');
         await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        // expect(element(txtRecord).getText()).toEqual(txtVerify);
        if (await element(txtRecord).getText() === txtVerify) {
            return true;
        } else {
            return false;
        }
        console.log('Done verify Record');
       }
         // Click Select in Configuration Tenant Systems Page
    async clickSelect() {
        const select = by.css('tr:nth-child(1) td.ng-star-inserted p-dropdown');
        console.log('Start click Select');
        await browser.wait(ExpectedConditions.visibilityOf(element(select)), 3000);
        await element(select).click();
        console.log('Done');
    }
    // get text For seach
    async getTextforSearch() {
        await console.log('Start get text');
        // tslint:disable-next-line:max-line-length
        const tdActions = by.xpath('//*[@id="tb-vnf"]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]');
        const valueSearch = await element(tdActions).getText();
        return valueSearch;
        console.log('done get text');
    }
    // getText for Sort Name
     async getTextName(numberColumn: number) {
        await console.log('Start get Text User');
        const rowNumber = by.css('#tb-vnf table > tbody > tr:nth-child(1) > td:nth-child(' + numberColumn + ')');
        await browser.wait(ExpectedConditions.visibilityOf(element(rowNumber)), 5000);
        const actionName = element(rowNumber).getText();
        return actionName;
    }
    // Count for VerifyName
    async countRowSearch() {
        await console.log('Start get Text User');
        const rowNumber = by.css('#tb-vnf table > tbody > tr');
        await browser.wait(ExpectedConditions.visibilityOf(element(rowNumber)), 5000);
        const actionName = element.all(rowNumber).count();
        return actionName;
    }

}
