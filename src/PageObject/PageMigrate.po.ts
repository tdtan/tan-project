import { element, by, ExpectedConditions, browser } from 'protractor';

const btnMigrate = by.xpath('//*[@id="vnfc-function"]/div/div[2]/div/footer/p-toolbar/div/div/button[2]');
const btnCancel = by.xpath('//*[@id="vnfc-function"]/div/div[2]/div/footer/p-toolbar/div/div/button[1]');
const lblflavor = by.xpath('//*[@id="vnfc-function"]//p-dropdown//label');
const txtMigrate = by.xpath('//*[@id="vnfc-function"]/div/div[1]/span');

export class PageMigrate {

    // Click button Migrate
    async clickMigrate() {
        await console.log('Start click Migrate.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnMigrate)), 3000);
        await element(btnMigrate).click();
        await console.log('Done click Migrate');
    }

    // Click button Migrate
    async clickCancel() {
        await console.log('Start click Cancel.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click Cancel');
    }

    // Get text Image Rbuild
    async GetTextFlavor() {
        await browser.wait(ExpectedConditions.visibilityOf(element(lblflavor)), 6000);
        const getTextFlavor = await element(lblflavor).getText();
        await console.log('Text Flavor: ' + getTextFlavor);
        return getTextFlavor;

    }

    // Get text Migrade
    async GetTextMigrade() {
        await console.log('Start Text Migrade');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtMigrate)), 6000);
        const getTextFlavor = await element(txtMigrate).getText();
        return getTextFlavor;

    }

}
