import { element, by, ExpectedConditions, browser } from 'protractor';

const btnNo = by.xpath('//button[@icon="fa fa-times"]');
const btnYes = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txtClUpgrade = by.xpath('//*[@id="upgradeCancel"]/div/div[1]/span');
export class PageCancelUpgrade {

    // Click button Upgrade
    async clickbtnYes() {
        await console.log('Start click Heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnYes)), 3000);
        await element(btnYes).click();
        await console.log('Done click Heal');
    }

    // Click button Cancel
    async clickbtnNo() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnNo)), 3000);
        await element(btnNo).click();
        await console.log('Done click Cancel');
    }

    // Get text in description of option Heal
    async GetTextUpgrade() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtClUpgrade)), 3000);
        const getTextDes = await element(txtClUpgrade).getText();
        await console.log('Text Descrition: ' + getTextDes);
        return getTextDes;

    }


}
