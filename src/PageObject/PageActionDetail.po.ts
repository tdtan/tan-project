import { by, element } from 'protractor';
import { PageCommon } from './PageCommon.po';

export class VariableElementPageActionDetail {
    static rowNumber = by.css('#vnfsActionContainer table > tbody > tr');
    static entriesInOnePage = by.css('#vnfsActionContainer p-paginator p-dropdown > div > label');
    // tslint:disable-next-line:max-line-length
    static iconPerPageOptions = by.css('#vnfsActionContainer p-paginator > div > p-dropdown > div > div.ui-dropdown-trigger.ui-state-default.ui-corner-right');
    static PerPageOptions = '#vnfsActionContainer p-paginator p-dropdown div > ul > p-dropdownitem > li';
    static columnNumber = by.css('#vnfsActionContainer table > thead > tr > th');
    static pagesPaginator = by.css('#vnfsActionContainer > div > p-paginator > div > span > a');
    static pagePaginatorActive = by.css('#vnfsActionContainer > div > p-paginator > div > span > a.ui-state-active');
    static listItemDropdown = by.css('body > div > div > ul > p-dropdownitem > li');
    static getDynamicCellTable(column: number, row: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#vnfsActionContainer p-table div.ui-table-scrollable-body table tr:nth-child(' + row + ') > td:nth-child(' + column + ')');
        return el;
    }
    static getDynamicIConSortColumnTable(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#vnfsActionContainer table > thead > tr > th:nth-child(' + column + ') > p-sorticon > i.pi-sort');
        return el;
    }
    static getDynamicIConSortDownColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#vnfsActionContainer table > thead > tr > th:nth-child(' + column + ') > p-sorticon > i.pi-sort-down');
        return el;
    }
    static getDynamicIConSortUpColumn(column: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#vnfsActionContainer table > thead > tr > th:nth-child(' + column + ') > p-sorticon > i.pi-sort-up');
        return el;
    }
    static getPageNumberPaginator(page: string) {
        const regExPage = new RegExp('^' + page + '$');
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('#vnfsActionContainer > div > p-paginator > div > span > a', regExPage);
        return el;
    }
    static getDynamicRowTable(level: number) {
        const el = by.css('#vnfsActionContainer table > tbody > tr:nth-child(' + level + ')');
        return el;
    }
}

export class PageActionDetail extends PageCommon {
    constructor() {
        super(VariableElementPageActionDetail);
    }
    // click row table action
    async clickRowTableAction(row: number) {
        const el = by.css('#tb-vnf table > tbody > tr:nth-child(' + row + ')');
        await element(el).click();
        await console.log('clicked');
    }

    // check bulk action detail table display
    async isBulkActionDetailTableDisplay() {
        const el = by.css('#vnfsActionContainer');
        return await element(el).isPresent();
    }
}
