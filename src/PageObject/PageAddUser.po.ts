import { element, by, ExpectedConditions, browser } from 'protractor';

const btn_AddUser = by.css('#container-common-table p-button.button-add-action button');
const txb_search = by.xpath('//*[@id="tb-vnf"]//input[@placeholder="Search"]');

const panel = by.xpath('//*[@id="panel-user-actions"]/div/div[1]');
const txb_username = by.css('#panel-user-actions .form-add-user form .rows-edit-form:nth-child(1) .ui-grid-col-8 input');
const txb_password = by.xpath('//*[@id="panel-user-actions"]//form/div/div[2]/div[2]/input');
const txb_confirm = by.xpath('//*[@id="panel-user-actions"]//form/div/div[3]/div[2]/input');
const DrMn_role = by.xpath('//*[@id="panel-user-actions"]//form//p-dropdown[@name="key"]');
const DrMn_tenant = by.xpath('//*[@id="panel-user-actions"]//form//p-dropdown[@name="value"]');
// tslint:disable-next-line:max-line-length
const mis_tenant = by.xpath('//*[@id="panel-user-actions"]//div/div/div[2]/div[2]/div/div[3]/small');

const btn_Cancel = by.xpath('//button[@icon="fa fa-times"]');
const btn_Save = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txt_Record = by.xpath('//*[@id="tb-vnf"]//p-paginator/div/div//span');
// tslint:disable-next-line:max-line-length
const txt_misInfo = by.xpath('//*[text()=" Please select a valid role and tenant "]');
// tslint:disable-next-line:max-line-length
const btn_Add = by.xpath('//button[@class="fa fa-plus-circle plus"]');
const btn_Subtract = by.xpath('//button[@class="fa fa-minus-circle minus"]');
// tslint:disable-next-line:max-line-length
const item_role = by.xpath('//p-dropdownitem//li');
const item_tenant = by.xpath('/html/body/div/div/ul/p-dropdownitem/li');
// tslint:disable-next-line:max-line-length
const rowNumber = by.css('#tb-vnf table > tbody > tr');

export class PageAddUser {

    // Set text Search in Configuration Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 3000);
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        expect(await element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Click button Add User in Add User Page
    async clickAddUser() {
        console.log('Start Click Button Add User');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_AddUser)), 3000);
        await element(btn_AddUser).click();
        console.log('Done Click Button Add User');
    }

    // Verify Empty form is displayed when user click on Add User button
    async verifyAfterClickAddUser() {
        await console.log('Start verify display Empty form');
        // await this.clickAddUser();
        // await browser.sleep(2000);
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_username)), 3000);
        const username = await element(txb_username).getAttribute('value');
        await expect(username).toEqual('');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_password)), 3000);
        const password = await element(txb_username).getAttribute('value');
        await expect(password).toEqual('');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_confirm)), 3000);
        const confirm = await element(txb_username).getAttribute('value');
        await expect(confirm).toEqual('');
        console.log('Done verify display Empty form');
    }

    // Set UserName in Login Page
    async setUserName(username: any) {
        console.log('Start Input User Name.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txb_username)), 3000);
        await element(txb_username).clear();
        await element(txb_username).sendKeys(username);
        console.log('Done Input User Name.');
    }

    // Set password in Add User form
    async setPassword(password: string) {
        console.log('Start set password');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txb_password)), 3000);
        await element(txb_password).clear();
        await element(txb_password).sendKeys(password);
        console.log('Done.');
    }

    // Set password confirm in Add User form
    async setPasswordConfirm(password: string) {
        console.log('Start confirm password.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txb_confirm)), 3000);
        await element(txb_confirm).clear();
        await element(txb_confirm).sendKeys(password);
        console.log('Done.');
    }

    // Click seclect Role
    async clickSelectRole() {
        await console.log('Start click Select Role');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn_role)), 3000);
        await element(DrMn_role).click();
        await console.log('Done click Select Role');
    }

    // Select Role in Add User tab
    async selectRole(strRole: string) {
        await console.log('Start selecting Role');
        //  await this.clickSelectRole();
        await console.log(strRole);
        const SelectRole = by.xpath('//p-dropdownitem[2]//li/span[text()="' + strRole + '"] ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(SelectRole)), 3000);
        await element(SelectRole).click();
        await console.log('Done selecting Role');
    }

    // Click seclect Tenant
    async clickSelectTenant() {
        await console.log('Start click Select Tenan');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn_tenant)), 3000);
        await element(DrMn_tenant).click();
        await console.log('Done click Select Tenant');
    }

    // Select Tenant in Add User tab
    async selectTenant(strTenant: string) {
        await console.log('Start selecting Role');
        await console.log(strTenant);
        const SelectTenant = by.xpath('//li[contains(span,"' + strTenant + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(SelectTenant)), 3000);
        await element(SelectTenant).click();
        await console.log('Done selecting Tenant');
    }

    // Function Add User
    async funcAddUser(user: any, pass: any) {
        console.log('Start Add User.');
        await this.setUserName(user);
        await this.setPassword(pass);
        await this.setPasswordConfirm(pass);
        await this.clickSelectRole();
        const listRole = await this.getListRole();
        await this.selectRole(listRole[0]);
        await this.clickSelectTenant();
        const tenantName = await this.getTextTenantName();
        await this.selectTenant(tenantName);
        await this.verifyAfterClickSave();
        console.log('Done Add User');
    }

    // Click button Cancel Add User
    async clickbuttonCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
        await element(btn_Cancel).click();
        await console.log('Done click Cancel');
    }

    // Verify undisplay Add User tab after click button cancel
    async verifyAfterClickCancel() {
        await console.log('Start Verify undisplay Add User tab after click button cancel');
        // await browser.sleep(5000);
        await expect(element(panel).isPresent()).toBeFalsy();
        await console.log(' Done Verify undisplay Add User tab after click button cancel');
    }

    // Click button Save Add User
    async clickbuttonSave() {
        await console.log('Start click Save');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Save)), 3000);
        await element(btn_Save).click();
        await console.log('Done click Save');
    }

    // Verify undisplay Add User tab after click button save
    async verifyAfterClickSave() {
        await console.log('Verify undisplay Add User tab after click button save');
        await this.clickbuttonSave();
        await expect(element(panel).isPresent()).toBeFalsy();
    }

    // Verify "missing username" message display
    async verifyMissingUsername() {
      const missUsername = by.xpath('//div[text()=" Username is required."]');
      await console.log('Start verify "missing usename" message display');
      await expect(element(missUsername).isPresent()).toBeTruthy();
      await console.log('Done verify "missing required info" message display');
    }

    // Verify "missing password" message display
    async verifyMissingPassword() {
        const missPass = by.xpath('//div[text()=" Password is required. "]');
        await console.log('Start verify "missing password" message display');
        await expect(element(missPass).isPresent()).toBeTruthy();
        await console.log('Done verify "missing required info" message display');
    }

    // Verify "missing password" message display
    async verifyMissingPasswordConfirm() {
        const missPass = by.xpath('//*[text()="Confirm Password is required"]');
        await console.log('Start verify "missing password" message display');
        await expect(element(missPass).isPresent()).toBeTruthy();
        await console.log('Done verify "missing required info" message display');
    }

    // Verify "missing Role and Tenant" message display
    async verifyMissingRoleAndTenant() {
        await console.log('Start verify "missing Role and Tenant" message display');
        await expect(element(txt_misInfo).isPresent()).toBeTruthy();
        await console.log('Done verify "missing Role and Tenant" message display');
    }

    // Verify Error when forget to Select Roles and Tenant
    async verifyDisplayMissingSelectRole() {
        await console.log('Verify display missing info massage');
        await browser.wait(ExpectedConditions.visibilityOf(element(mis_tenant)), 5000);
        const messageError = await element(mis_tenant).getText();
        await expect(messageError).toEqual('Please select a valid role and tenant');

    }
    // Verify that dropdown has value
    async verifyDropdownTenantandRole() {
        await console.log('Verify value of Dropdown');
        await browser.wait(ExpectedConditions.visibilityOf(element(DrMn_role)), 5000);
        await expect(element(DrMn_role).isPresent()).toBeTruthy();
        await browser.wait(ExpectedConditions.visibilityOf(element(DrMn_tenant)), 5000);
        await expect(element(DrMn_tenant).isPresent()).toBeTruthy();


    }
    // Click button Add new Role and Tenant
    async clickAddRoleAndTenant() {
        await console.log('Start click button Add Role');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Add)), 5000);
        await element(btn_Add).click();
        await console.log('Done Click button Add Role');
    }
    // Verify that clik on "+" button can add the new Role and Tenant row
    async verifyNewRoleAndTenant() {
        await console.log('Start Verify new Role');
        await browser.wait(ExpectedConditions.visibilityOf(element(DrMn_role)), 5000);
        const numberRowAndTenant = await element.all(DrMn_role).count();
        return numberRowAndTenant;
    }
    // click button "-"
    async clickSubTractRoleAndTenant() {
        await console.log('Start click button "-" ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Subtract)), 5000);
        await element(btn_Subtract).click();
        await console.log('Done Click button Subtract Role');
    }
    // Verify that the last Role and Tenant row can not be subtracted
    async verifyLastTenantAndRoleCanNotSubtract() {
        await console.log('Start verify the last Role and Tenant row');
        // await browser.sleep(3000);
        await expect(element(btn_Subtract).isEnabled()).toBeFalsy();
        await console.log('Done verify the last Role and Tenant row');
    }
    // Get Total of Roles from Dropdown
    async getNumBerRole() {
        await console.log('Start get Number row');
        await browser.wait(ExpectedConditions.visibilityOf(element(item_role)), 5000);
        const numberRow = await element.all(item_role).count();
        return numberRow;
    }
    // Get List Role from Dropdown
    async getListRole() {
        await console.log('Start get List Role row');
        const listRole = [];
        const numberRow = await this.getNumBerRole();
        await console.log('List role: ', numberRow);
        for (let i = 2; i <= numberRow; i++) {
            // tslint:disable-next-line:max-line-length
            const itemRole = by.xpath('//p-dropdownitem[' + i + ']/li');
            const roleName = await element(itemRole).getText();
            await listRole.push(roleName);
            await console.log(roleName);
        }
        await console.log('List role: ', listRole);
        return listRole;
    }
    // check List Role
    async checkListRole(list1: string[], list2: string[]) {
        let check = false;
        for (let i = 0; i < list1.length; i++) {
            check = false;
            for (let j = 0; j < list2.length; j++) {
                if (list1[i] === list2[j]) {
                    check = true;
                    break;
                }
            }
        }
        return check;
    }
    // get number row of tenant Table
    async getNumberRowOfTenantTable() {
        await console.log('Start get NumberRow');
        await browser.wait(ExpectedConditions.visibilityOf(element(rowNumber)), 5000);
        const numberRow = await element.all(rowNumber).count();
        return numberRow;
    }
    // get Text Tenant Name
    async getTextTenantName() {
        await console.log('Start get Text Tenant');
       const itemTenant = by.xpath('//p-dropdownitem[2]/li');
       await browser.wait(ExpectedConditions.visibilityOf(element(itemTenant)), 5000);
       const tenantName = element(itemTenant).getText();
       return tenantName;
     }

     // Get Total of Tenant from Dropdown
     async getNumBerTenant() {
        await browser.wait(ExpectedConditions.visibilityOf(element(item_tenant)), 5000);
        const numberRow = await element.all(item_tenant).count();
        await console.log(numberRow);
        return (numberRow - 2);
    }

}
