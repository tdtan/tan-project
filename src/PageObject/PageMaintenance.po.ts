import { element, by, ExpectedConditions, browser } from 'protractor';

const DrMn = by.xpath('//*[@id="dropdown-maintenance"]//div[3]/span');
const btn_confirm = by.xpath('//*[@id="dialog-maintenance"]//span[text()="Confirm"]');
const btn_Cancel = by.xpath('//*[@id="dialog-maintenance"]//span[text()="Cancel"]');
const txtMaintenanceMode = by.xpath('//span[text()="VNFM is in Maintenance mode"]');
const dialog = by.xpath('//*[@id="dialog-maintenance"]//span[text()="VNFM Maintenance"]');
const txtMaintenance = by.xpath('//*[@id="admin-maintenance"]/div/div[1]/span');


export class PageMaintenance {

   // Verify show text Maintenance
   async verifyShowTextMaintenance(_textMaintenance: any) {
    console.log('Start get text.');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtMaintenance)), 5000);
    const Maintenance = await element(txtMaintenance).getText();
    expect(Maintenance).toEqual(_textMaintenance);
    await console.log('text: ' + Maintenance);
    await console.log('Done.');
  }

  // Set VNFM in Maintenance Mode
  async setVNFMInMaintenanceMode() {
    await browser.sleep(2000);
    await console.log('Start Set VNFM in Maintenance Mode');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn)), 5000);
    await element(DrMn).click();
    const maintenance = by.xpath('//li[contains(option,"Place VNFM in Maintenance")]');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(maintenance)), 5000);
    await element(maintenance).click();
    await console.log('Done Set VNFM in Maintenance Mode');
  }

  // Verify the dialog is displayed after set VNFM in Maintenance Mode
  async verifyDisplayTheConfirmDialog() {
    await browser.sleep(5000);
    console.log('Verify the dialog is displayed after set VNFM in Maintenance Mode');
    await browser.wait(ExpectedConditions.visibilityOf(element(dialog)), 3000);
    await expect( element(dialog).isPresent()).toBeTruthy();
    await console.log('Done.');
  }

  // Click button confirm to place VNFM in Maintenance Mode
  async clickConfirm() {
    await console.log('Start click confirm');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_confirm)), 3000);
    await element(btn_confirm).click();
    await console.log('Done click confirm');
  }

  // Verify VNFM is in Maintenance Mode
  async verifyVNFMEnterToTheMantenanceMode() {
    await console.log('Verify VNFM is in Maintenance Mode');
    await this.clickConfirm();
    await browser.sleep(20000);
    await browser.wait(ExpectedConditions.visibilityOf(element(txtMaintenanceMode)), 20000);
    await expect( element(txtMaintenanceMode).isPresent()).toBeTruthy();
    await console.log('Done.');
  }

   // Click button Cancel to place VNFM in Maintenance Mode
   async clickCancel() {
    await console.log('Start click Cancel');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
    await element(btn_Cancel).click();
    await console.log('Done click Cancel');
  }

  // Verify exit the confirm dialog after click button cancel
  async verifyExitTheConfirmDialog() {
    await console.log('Verify exit the confirm dialog after click button cancel');
    await this.clickCancel();
    await browser.sleep(5000);
    await expect( element(dialog).isPresent()).toBeFalsy();
  }

  // Remove VNFM from Maintenance Mode
  async removeVNFMFromMaintenanceMode() {
    await console.log('Remove VNFM from Maintenance Mode');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(DrMn)), 3000);
    await element(DrMn).click();
    const exit = by.xpath('//li[contains(option,"Complete  VNFM Maintenance")]');
    await element(exit).click();
    await console.log('Remove VNFM from Maintenance Mode');
  }

  // Verify VNFM Is Removed From Mantenance Mode
  async verifyVNFMIsRemovedFromMantenanceMode() {
    await console.log('Verify VNFM is removed from the Maintenance Mode');
    await this.clickConfirm();
    await browser.sleep(20000);
    await expect( element(txtMaintenanceMode).isPresent()).toBeFalsy();
    await console.log('Done.');
  }



}
