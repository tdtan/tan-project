import { element, by, ExpectedConditions, browser } from 'protractor';

    const txtSecurity = by.css('#container-common-table p-accordiontab a p-header span');
    const btnAddRealm = by.xpath('//*[@id="container-common-table"]//p-accordiontab/div[2]/div/p-button/button');
    const txbSearch = by.xpath('//*[@id="container-common-table"]//p-table/div/div[1]//input[@type="text"]');
    const btnClearSearch = by.xpath('//app-security-realms//p-table/div/div[1]//p-button[@icon="fa fa-times"]');
    const btnClearFilter = by.xpath('//app-security-realms//tr[2]/th[7]/p-button');
    const btnShowFilter = by.xpath('//p-button[@icon="fa fa-sliders"]');
    const drpFilter = by.css('tr th p-dropdown');
    const btnShowColumn = by.xpath('//p-button[@title="Show/Hide Columns"]');
    const btnMinimix = by.xpath('//*[@id="panel-securRealms"]/div/div[1]/a');
    const txtRecord = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator/div/div/div/span');
    const txtNoRecord = by.xpath('//*[text()=" No records found "]');
    const rowTable = by.xpath('//app-security-realms/rbn-common-table//div[2]/table/tbody/tr');
    // const linkEdit = by.css('#container-common-table table tbody tr:nth-child(1)');
    const lblNumPage = by.xpath('//app-security-realms/rbn-common-table//p-paginator//label');
    const btnPage2 = by.xpath('//app-security-realms/rbn-common-table//p-paginator//span/a[2]');
    // tslint:disable-next-line:max-line-length
    const pageActive = by.xpath('//*[@id="tb-securRealms"]//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
    const btnConfirmYes = by.xpath('//button[@icon="pi pi-pw pi-check"]');
    const btnConfirmNo = by.xpath('//button[@icon="fa fa-times"]');
    const pupConfirm = by.xpath('//div[@role="dialog"]');

    const btnSelectActions = by.xpath('//app-security-realms/rbn-common-table//table/tbody/tr[1]/td[6]/p-dropdown');

export class PageSecurityRealms {
    // Clear textbox Search in page Security Realmss
    /**
     * parameter:
     * author: tthiminh
     */
    async ClearTxbSearch() {
        await console.log('Start clear textbox Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).clear();
        await console.log('Done');
    }

    // Set text Search in page Security Realms
    /**
     * parameter: search
     * author: tthiminh
     */
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // Click button Add Realm in page Security Realms
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnAddRealm() {
        console.log('Start click Realm');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnAddRealm)), 5000);
        await element(btnAddRealm).click();
        console.log('Done');
    }

    // Click link in table to edit Realm
    /**
     * parameter:
     * author: tthiminh
     */
    async clicklinkInTableRealm() {
        console.log('Start link in table Realm');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 5000);
        await element(rowTable).click();
        console.log('Done');
    }
    // Click button Show|Hide Filter in Onboarded Page
    async clickShowHideFilter() {
        await console.log('Start click show hide filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done click show hide filter');
    }

    // Click button Clear Filter in Onboarded Page
    async clickClearFilterButton() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearFilter)), 3000);
        await element(btnClearFilter).click();
        await console.log('Done click refesh');
    }
    // Click button Clear in Onboarded Page
    async clickClearSearchButton() {
        await console.log('Start click Clear');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done click Clear');
    }
    // Click button Refesh in Onboarded Page
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done click Show Hide Column');
    }

    // Click button minimized in Onboarded Page
    async clickMinimaxTable() {
        await console.log('Start click minimized table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnMinimix)), 3000);
        await element(btnMinimix).click();
        await console.log('Done click minimized table');
    }

    // Click to move page 2
    /**
     * parameter:
     * author: tthiminh
     */
    async clickNumbertoMovePage() {
        console.log('Start click to move page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPage2)), 5000);
        await element(btnPage2).click();
        console.log('Done');
    }

    // Click Confirm Yes
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnConfirmYes() {
        console.log('Start click button confonfirm Yes');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmYes)), 5000);
        await element(btnConfirmYes).click();
        console.log('Done');
    }

    // Click Confirm No
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnConfirmNo() {
        console.log('Start click button confonfirm No');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmNo)), 5000);
        await element(btnConfirmNo).click();
        console.log('Done');
    }

    // Click button Select Actions
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnSelectActions() {
        console.log('Start click button Select Actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectActions)), 5000);
        await element(btnSelectActions).click();
        console.log('Done');
    }

    // Click Option select Actions
    /**
     * parameter:
     * author: tthiminh
     */
    async clickbtnOptionActions(txtOption: string) {
        const btnOptActions = by.xpath('//li[contains(option,"' + txtOption + '")]');
        console.log('Start click button Option Actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptActions)), 5000);
        await element(btnOptActions).click();
        console.log('Done');
    }

    // get curent page active
    /**
     * parameter:
     * author: tthiminh
     */
    async getCurentPageActive() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(pageActive)), 3000);
        const textPage = await element(pageActive).getText();
        return textPage;
    }

    // get text at header page
    /**
     * parameter:
     * author: tthiminh
     */
    async getTextHeader() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtSecurity)), 3000);
        const textSecurity = await element(txtSecurity).getText();
        return textSecurity;
    }

    // get value txb search
    /**
     * parameter:
     * author: tthiminh
     */
    async getValueSearch() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        const value = await element(txbSearch).getAttribute('value');
        return value;
    }

    // Get Record  Current display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetRecordCurrent() {
       // await browser.sleep(3000);
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        const record = await element(txtRecord).getText();
        await console.log('Record current: ' + record);
        return record;

    }

    // Get Text in filter
    /**
     * parameter:
     * author: tthiminh
     */
    async GetTextInFilter(numTd: number) {
         await console.log('Start get Text in filter');
         const txt_Filter = by.xpath('//*[@id="container-common-table"]//tr[2]/th[' + numTd + ']/p-dropdown//label');
         await browser.wait(ExpectedConditions.visibilityOf(element(txt_Filter)), 3000);
         const record = await element(txt_Filter).getText();
         return record;
     }

    // Get Number Row page default
    /**
     * parameter:
     * author: tthiminh
     */
    async GetNumberRowInPage() {
        await console.log('Start get text row number');
        await browser.wait(ExpectedConditions.visibilityOf(element(lblNumPage)), 3000);
        const getTextNumRow = await element(lblNumPage).getText();
        await console.log('Number Row Display in Page ' + getTextNumRow);
        return getTextNumRow;

    }

    // Get text in Td display
    /**
     * parameter:
     * author: tthiminh
     */
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//app-security-realms/rbn-common-table//table/tbody/tr/td[' + numTd + ']');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Count number row display in page
    /**
     * parameter:
     * author: tthiminh
     */
    async CountNumberRowInpage() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }


    // Select Option from Show/ Hide columns List in Onboarded Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btnClose = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await console.log('Start click close show hide column');
        await element(btnClose).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//app-security-realms/rbn-common-table//th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('/html/body/div/div/ul//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select number row display in page
    async SelectNumberRowDisplay(optionNumber: number) {

        const btnOptionNum = by.xpath('//app-security-realms/rbn-common-table//p-paginator//p-dropdown//span');
        const filChoose = by.xpath('//li[contains(span,"' + optionNumber + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOptionNum)), 3000);
        await console.log('Click lable filter: ' + btnOptionNum);
        await element(btnOptionNum).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Verify sort column
    async verifySortColumn(numTh: number, ) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//app-security-realms/rbn-common-table//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//app-security-realms/rbn-common-table//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//app-security-realms/rbn-common-table//th[' + numTh + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click();
        await expect(element(sortup).isPresent()).toBeTruthy();
        await element(sortup).click();
        await expect(element(sortdown).isPresent()).toBeTruthy();

    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Current record: ' + txtVerify);
        await console.log('Done');
    }

    // Verify display or undisplay table when click minimized
    async verifyTableAfterClickMinimized() {
        await console.log('Start verify table');
        await this.clickMinimaxTable();
        expect(await element(rowTable).isDisplayed()).toBeFalsy();
        await this.clickMinimaxTable();
        expect(await element(rowTable).isDisplayed()).toBeTruthy();
        await console.log('Done verify table');
    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
          });
    }

    // Verify row display on table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyRowTableDisplay() {
        return await element(rowTable).isDisplayed();
    }

    // Verify button Add Realm Display
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyButtonAddRealmDisplay() {
        return await element(btnAddRealm).isDisplayed();
    }

    // Verify Dropdown filter is present in page
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyDropdownFilterIsPresentInPage() {
        await console.log('Start check Dropdown filter is present in page');
        return await element(drpFilter).isPresent();
    }

    // Verify column name display
    /**
     * parameter:strTHName: TH Name in table
     * author: tthiminh
     */
    async verifyColumnDisplayInTable(strTHName: string) {
        await console.log('Verify column ' + strTHName + ' in table');
        const tableTH = by.xpath('//app-security-realms/rbn-common-table//tr[1]/th[text()=" ' + strTHName + ' "]');
        // await browser.wait(ExpectedConditions.visibilityOf(element(tableTH)), 3000);
        return await element(tableTH).isPresent();
    }

    // Verify element display in form
    /**
     * parameter:strEventItem: Item in Function on table
     * author: tthiminh
     */
    async verifyEventDisplayInForm(strEventItem: string) {
        await console.log('Start verify event item is option in Function: ' + strEventItem);
        const elmDisplay = by.xpath('//li[contains(option,"' + strEventItem + '")]');
        return element(elmDisplay).isDisplayed();
    }

    // Verify row display on table
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyPopupConfirmDisplay() {
        return await element(pupConfirm).isPresent();
    }


    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Get number of columns in current search result table
    async getNumberOfColumnsInSearchTable(strTable: string) {
       let _numberOfColumn: number ;
       _numberOfColumn = 0 ;
       const _numOfMaximumColumns = 11;
       await console.log('Start checking if we have any table name equals Text value?');
       for ( let i = 1 ; i <= _numOfMaximumColumns; i++ ) {
        const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
        if (await element(_strColumnLocator).isPresent()) {
            _numberOfColumn = i ;
        }
       }
       return _numberOfColumn ;

    }

    // Get column number based on column name in VFM Settings Page
    async getColumnNumberBasedOnColumnName(strTable: string, columnname: string) {
        let _numCurrentColumns: any;
        let _columnnum: number;
        _columnnum = 0;
        _numCurrentColumns = await this.getNumberOfColumnsInSearchTable(strTable);
        await console.log('Start getting column number based on column Name');
        for ( let i = 1 ; i <= _numCurrentColumns; i++ ) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            await element(_strColumnLocator).getText().then(function (text) {
                if (text === columnname) {
                    _columnnum = i;
                }
            });
        }
        if (_columnnum === 0 ) {
            await console.log('No found ' + columnname + ' in search table');
        } else {
            await console.log('Column ' + _columnnum + ' has ' + columnname + ' as name');
        }
        return _columnnum;
    }

    // Get row number has Column Name Equals Text Value in Onboarded VNFs Page
    async getNumberofRowHasValueofColumnNameEqualsTextValue(strTable: string, intColumnNumber: number, strTextValue: string) {
        let _rownum: number;
        _rownum = 0;
        const record = by.xpath('//*[@id="' + strTable + '"]/div/p-paginator/div/div/div/span');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(record)), 3000);
        const numrecord = await element(record).getText();
        const _intSearchSheetRowNum = Number(numrecord.slice(0, 1));
        await console.log('Current Search table has ' + _intSearchSheetRowNum + ' row(s)');
        for ( let i = 1 ; i <= _intSearchSheetRowNum; i++ ) {
            const TextValueLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + i + ']/td[' + intColumnNumber + ']');
            await element(TextValueLocator).getText().then(function (text) {
                if (text === strTextValue) {
                    _rownum = i;
                }
            });

        }
        if (_rownum === 0 ) {
            await console.log('No found Row has Column Name Equals ' + strTextValue + ' in search table');
        } else {
            await console.log('Row ' + _rownum + ' has value of Column Name equals ' + strTextValue );
        }
        return _rownum;
    }

    // Click Function On Row Has Value of ColumnName Equals TextValue
    // tslint:disable-next-line:max-line-length
    async clickActionOnRowHasValueofColumnNameEqualsTextValue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, functionchoose: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        const _function = by.xpath('//li[contains(span,"' + functionchoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_function)), 4000);
        await element(_function).click();
        await console.log('Done');
    }

    // Click Functions On Row
    // tslint:disable-next-line:max-line-length
    async clickFunctionOnRowHasValueofColumnName(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        await console.log('Done');
    }

}
