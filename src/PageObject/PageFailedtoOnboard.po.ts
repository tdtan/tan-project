import { element, by, ExpectedConditions, browser } from 'protractor';
import { PageError } from './PageError.po';

const txbsearch = by.css('#availableVNFPanel div.table-hdr-btn-grp input[type=text]');
const btnClearTextSeach = by.xpath('//*[@id="availableVNFPanel"]//*[@icon="fa fa-times"]/button');
const btnShowFilter = by.xpath('//*[@id="availableVNFPanel"]//p-button[@icon="fa fa-sliders"]');
const drpFilter = by.css('tr th p-dropdown');
const btnClearFilter = by.xpath('//*[@id="availableVNFPanel"]//tr[2]/th[6]/p-button[@icon="fa fa-times"]/button');
const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
const btnRefesh = by.xpath('//*[@id="availableVNFPanel"]//p-button[@icon="fa fa-refresh"]');
const rowTable = by.css('#availableVNFPanel p-table table thead tr');
const lblNumberPage = by.xpath('//*[@id="availableVNFPanel"]//p-paginator//p-dropdown');
const txtNumberRow = by.xpath('//*[@id="availableVNFPanel"]//p-paginator//p-dropdown//label');
const btn_confirmDelete = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txtRecord = by.xpath('//*[@id="availableVNFPanel"]//p-table//p-paginator/div/div//span');
const txtNoRecord = by.xpath('//*[@id="availableVNFPanel"]//*[contains(text(),"No records found")]');
const btnActionDropdown = by.xpath('//*[@id="availableVNFPanel"]//table/tbody/tr/td[5]/p-dropdown');
const btnBackVNFCatalog = by.css('app-catalog-table p-breadcrumb ul li:nth-child(1) a');

export class PageFailedtoOnboard {
    private pageError = new PageError();
    // Set text Search in Available Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbsearch)), 5000);
        await element(txbsearch).clear();
        await element(txbsearch).sendKeys(user);
        console.log('Done Input search.');
    }

    // Click button Cancel Available VNF
    async clickCancelVNF() {
        await console.log('Start click Cancel.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click cancel');
    }

    // Click button Refesh
    async clickRefesh() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefesh)), 5000);
        await element(btnRefesh).click();
        await console.log('Done click Refesh');
    }

    // Click Actions Dopdown on form Available Catalog
    async clickActionsDopdown() {
        await console.log('Start click Actions Dopdown on form Available Catalog');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnActionDropdown)), 5000);
        await element(btnActionDropdown).click();
        await console.log('Done click Actions Dopdown on form Available Catalog');
    }

    // Click function in Actions Dopdown
    async clickFunctionActionsDopdown(strOption: string) {
        await console.log('Start click choose function in Actions Dopdown');
        const btnFunctionActions = by.xpath('//*[contains(option,"' + strOption + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnFunctionActions)), 5000);
        await element(btnFunctionActions).click();
        await console.log('Done click choose function in Actions Dopdown');
    }

    // Click button Show|Hide Filter
    async clickShowHideFilter() {
        await console.log('Start click Show Hide Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 5000);
        await element(btnShowFilter).click();
        await console.log('Done click Show Hide Filter');
    }

    // Click button confirm Delete in function in table vnfTable
    async clickConfirmDelete() {
        await console.log('Start click Confirm Delete');
        await element(btn_confirmDelete).click();
        await console.log('Done click Confirm Delete');
    }

    // Click button Refesh in Page
    async clickClearTextSearch() {
        await console.log('Start click Clear Text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearTextSeach)), 5000);
        await element(btnClearTextSeach).click();
        await console.log('Done click Clear Text Search');
    }

    // Click button clear filter in Page
    async clickClearFilterInPage() {
        await console.log('Start click clear button filter in Page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearFilter)), 5000);
        await element(btnClearFilter).click();
        await console.log('Done click clear button filter in Page');
    }

    // Click back to page VNF Catalog
    async clickBackToPageVNFCatalog() {
        await console.log('start back to page VNF Catalog ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnBackVNFCatalog)), 3000);
        await element(btnBackVNFCatalog).click();
        await console.log('Done');
    }

    // Get text in filter
    async GetTextInFilter(numTd: number) {
        const txtfilter = by.xpath('//*[@id="availableVNFPanel"]//table/thead/tr[2]/th[' +  numTd + ']/p-dropdown//label');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtfilter)), 3000);
        const getTextFilter = await element(txtfilter).getText();
        await console.log('Text in Filter ' + numTd + ' : ' + getTextFilter);
        return getTextFilter;
    }

    // Get text in Td display
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="availableVNFPanel"]//p-table//div/div[2]//td[' + numTd + ']');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Get Record  Current display
    async GetRecordCurrent() {
        // await browser.sleep(3000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        const record = await element(txtRecord).getText();
        await console.log('Record current: ' + record);
        return record;

    }
    // Get Number Row display
    async GetNumberRow() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRow)), 4000);
        const getNumRow = await element(txtNumberRow).getText();
        await console.log(' Current Row Number Display: ' + getNumRow);
        return getNumRow;

    }

    // Select number page on table in Onboarded Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="availableVNFPanel"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="availableVNFPanel"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect/div');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 5000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 3000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Verify sort column
    async verifySortColumn(numTh: number, ) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="availableVNFPanel"]//table/thead/tr[1]/th[' + numTh + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="availableVNFPanel"]//table/thead/tr[1]/th[' + numTh + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="availableVNFPanel"]//table/thead/tr[1]/th[' + numTh + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click();
        await expect(element(sortup).isPresent()).toBeTruthy();
        await element(sortup).click();
        await expect(element(sortdown).isPresent()).toBeTruthy();

    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
          });
    }

    // Verify Attribute in Search
    async VerifyAttributeInSear() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txbsearch)), 3000);
        return element(txbsearch).getAttribute('value');
    }

    // Verify Dropdown filter is present in page
    async verifyDropdownFilterIsPresentInPage() {
        await console.log('Start check Dropdown filter is present in page');
        return await element(drpFilter).isPresent();
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {

        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Function Upload file CSAR
    async funcUploadCSAR(linkpath: string) {
        await console.log('Start upload CSAR file');
        const path = await require('path');
        const absolutePath = await path.resolve(linkpath);
        const input = await element(by.css('#upload_csar_file_name'));
        await input.sendKeys(absolutePath);

        const btn_Upload = by.xpath('//*[@id="uploadCsarForm"]//button[contains(span,"Upload")]');
        await console.log('Click upload file');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Upload)), 5000);
        await element(btn_Upload).click();
        await browser.sleep(20000);
        await console.log('Done upload CSAR file');
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {

        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Get number of columns in current search result table
    async getNumberOfColumnsInSearchTable(strTable: string) {
       let _numberOfColumn: number ;
       _numberOfColumn = 0 ;
       const _numOfMaximumColumns = 11;
       await console.log('Start checking if we have any table name equals Text value?');
       for ( let i = 1 ; i <= _numOfMaximumColumns; i++ ) {
        const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
        if (await element(_strColumnLocator).isPresent()) {
            _numberOfColumn = i ;
        }
       }
       return _numberOfColumn ;

    }

    // Get column number based on column name in VFM Settings Page
    async getColumnNumberBasedOnColumnName(strTable: string, columnname: string) {
        let _numCurrentColumns: any;
        let _columnnum: number;
        _columnnum = 0;
        _numCurrentColumns = await this.getNumberOfColumnsInSearchTable(strTable);
        await console.log('Start getting column number based on column Name');
        for ( let i = 1 ; i <= _numCurrentColumns; i++ ) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            await element(_strColumnLocator).getText().then(function (text) {
                if (text === columnname) {
                    _columnnum = i;
                }
            });
        }
        if (_columnnum === 0 ) {
            await console.log('No found ' + columnname + ' in search table');
        } else {
            await console.log('Column ' + _columnnum + ' has ' + columnname + ' as name');
        }
        return _columnnum;
    }

    // Get row number has Column Name Equals Text Value in Onboarded VNFs Page
    async getNumberofRowHasValueofColumnNameEqualsTextValue(strTable: string, intColumnNumber: number, strTextValue: string) {
        let _rownum: number;
        _rownum = 0;
        const record = by.xpath('//*[@id="' + strTable + '"]//p-table//p-paginator/div/div//span');
        const numrecord = await element(record).getText();
        const _intSearchSheetRowNum1 = numrecord.slice(0, 9);
        const _intSearchSheetRowNum = Number(_intSearchSheetRowNum1.replace('Showing ', ''));
        await console.log('Current Search table has ' + _intSearchSheetRowNum + ' row(s)');
        for ( let i = 1 ; i <= _intSearchSheetRowNum; i++ ) {
            const TextValueLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + i + ']/td[' + intColumnNumber + ']//span');
            await element(TextValueLocator).getText().then(async function (text) {
                if (text === strTextValue) {
                    _rownum = i;
                }
            });
        }
        if (_rownum === 0 ) {
            await console.log('No found Row has Column Name Equals ' + strTextValue + ' in search table');
        } else {
            await console.log('Row ' + _rownum + ' has value of Column Name equals ' + strTextValue );
        }
        return _rownum;
    }

    // Click Function On Row Has Value of ColumnName Equals TextValue
    // tslint:disable-next-line:max-line-length
    async clickActionOnRowHasValueofColumnNameEqualsTextValue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, functionchoose: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // *[@id="availableVNFPanel"]//table/tbody/tr[1]/td[5]/p-dropdown
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await element(btn_FunctionLocator).click();
        const _function = by.xpath('//li[contains(option,"' + functionchoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_function)), 4000);
        await element(_function).click();
        await console.log('Done');
    }

    async countNumberVnfOnboard() {
        await console.log('begin countNumberVnfOnboard');
        const el = by.css('#availableVNFPanel table tbody tr');
        const rowNumber = await element.all(el).count();
        await console.log('number vnf onboard: ', rowNumber);
        return rowNumber;
    }

    async showDropActionDropDown(row: number) {
        await console.log('begin showDropActionDropDown');
        const el = by.css('#availableVNFPanel table tbody tr:nth-child(1) td:nth-child(5) p-dropdown');
        const isShow = await this.isDropListActionsDisplay();
        if (!isShow) {
            await element(el).click();
            await console.log('displayed drop list action');
        }
    }

    async isDropListActionsDisplay() {
        await console.log('begin isDropListActionsDisplay');
        const el = by.css('body > div > div.ui-dropdown-items-wrapper');
        const check = await element(el).isPresent();
        return check;
    }

    async selectItemDropActions(level: number) {
        await console.log('begin selectItemDropActions');
        const el = by.css('body ul p-dropdownitem:nth-child(' + level + ') li');
        await element(el).click();
        await console.log('done select item: ', level);
    }

    async clickYesConfirmDialog() {
        await console.log('begin clickYesConfirmDialog');
        const el = by.css('p-dialog button[icon="pi pi-pw pi-check"]');
        await element(el).click();
        await console.log('done click yes confirm');
    }

    async clearMessage() {
        await console.log('begin clearMessage');
        const result1 = await this.pageError.verifyMessegeError();
        const result2 = await this.pageError.verifyMessegeWarning();
        const result3 = await this.pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await this.pageError.cliclClearALL();
        }
        await console.log('done');
    }

    async removeAllVnfFailedOnboardVNF() {
        await console.log('begin removeAllVnfFailedOnboardVNF');
        while (await this.countNumberVnfOnboard() > 0) {
            await this.showDropActionDropDown(1);
            await this.selectItemDropActions(3);
            await this.clickYesConfirmDialog();
            await console.log('deleted row 1');
            this.clearMessage();
        }
        this.clearMessage();
        await console.log('deleted all vnf in Failed Onboard VNF page');
    }

}
