import { element, by, browser, ExpectedConditions} from 'protractor';

const txbUser = 'input[type=text]';
const txbPass = 'input[type=password]';
const btnLogin = 'button.sign-in-button';
const btnCheck = 'div.ui-chkbox-box';
const txtMes = '/html/body/app-root/app-login-wrapper/rbn-login/div/div[2]/table/tbody/tr[3]/p';

export class PageLogin {

  // Set UserName in Login Page
  async setUser(user: any) {
    console.log('Start Input User Name.');
    await element(by.css(txbUser)).clear();
    await element(by.css(txbUser)).sendKeys(user);
    console.log('Done Input User Name.');
  }

  // senk key REFRESH
  async setUserRefresh(user: any) {
    console.log('Start Input User Name.');
    await element(by.css(txbUser)).sendKeys(user);
    console.log('Done Input User Name.');
  }

  // Set Password in Login Page
  async setPass(pass: string) {
    console.log('Start Input Password.');
    await element(by.css(txbPass)).clear();
    await element(by.css(txbPass)).sendKeys(pass);
    console.log('Done Input Password.');
  }

  // Click button login in Login Page
  async clickLogin() {
    console.log('Start Click Login.');
    await element(by.css(btnLogin)).click();
    console.log('Done.');
  }

  // Click button check in Login Page
  async clickCheck() {

    console.log('Start Click Check.');
    await element(by.css(btnCheck)).click();
    console.log('Done.');
  }

  // Verify Password Error Message When Login Fail
  async verifyPasswordErrorMessageWhenLoginFail(massage: any) {
    const txtmessage = await element(by.xpath(txtMes)).getText();
    expect(Boolean(txtmessage === massage)).toBe(true);
    // expect(txtmessage).toEqual(massage);

  }

  // Verify UserName Error Message When Login Fail
  async verifyUserNameErrorMessageWhenLoginFail(massage: any) {
    const txtmessage = await element(by.xpath(txtMes)).getText();
    expect(Boolean(txtmessage === massage)).toBe(true);
    // expect(txtmessage).toEqual(massage);

  }

  // Function Login
  async funcLoginInfo(user: any, pass: any) {
    console.log('Start Login.');
    await this.setUser(user);
    await this.setPass(pass);
   // await this.clickCheck();
    await this.clickLogin();
    console.log('Done Login');
  }

  // Function Login suss
  async funcLogin(user: any, pass: any) {
    console.log('Start Login.');
    await this.setUser(user);
    await this.setPass(pass);
   // await this.clickCheck();
    await this.clickLogin();
    await browser.wait(ExpectedConditions.urlContains('vnfm/dashboard'), 22000);
    console.log('Done Login');
  }

  // Function without check box when login fail
  async funcWithoutCheckBox(user: any, pass: any, massage: any) {
    console.log('Start Login.');
    await this.setUser(user);
    await this.setPass(pass);

    await this.clickLogin();
    const txtmessage = await element(by.xpath(txtMes)).getText();
    await expect(txtmessage).toEqual(massage);
  }

}
