import { element, by, ExpectedConditions, browser} from 'protractor';

const txt_AddContinuityPlan = by.xpath('//*[@id="panel-contunuity-pan-actions"]//div[contains(span,"Add Continuity Plan")]');
const txb_ContinuityName = by.xpath('//*[@formcontrolname="ContinuityPlanName"]');
const txb_Description = by.xpath('//*[@formcontrolname="Description"]');
const btn_Cancel = by.xpath('//button[@icon="fa fa-times"]');
const btn_Save = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const btn_AddContinuity = by.xpath('//*[contains(@class,"button-add-action")]/button');
export class PageAddContinuityPlan {

    // Set text Continuity Plan Name
    async setContinuityPlanName(txt_ContinuityPlanName: string) {
        console.log('Start Input Continuity Plan Name.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_ContinuityName)), 3000);
        await element(txb_ContinuityName).clear();
        await element(txb_ContinuityName).sendKeys(txt_ContinuityPlanName);
        console.log('Done Input Continuity Plan Name.');
    }

    // set text Description Continuity Plan
    async setDescriptionContinuityPlan(txt_Description: string) {
        console.log('Start Input text Description Continuity Plan.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_Description)), 3000);
        await element(txb_Description).clear();
        await element(txb_Description).sendKeys(txt_Description);
        console.log('Done Input text Description Continuity Plan.');
    }

    // Click Add continuity plan button
    async ClickAddContinuityPlanButton() {
        await console.log('Start click Add continuity plan button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_AddContinuity)), 3000);
        await element(btn_AddContinuity).click();
        await console.log('Done click Add continuity plan button');
    }

    // Click Cancel button
    async clickCancelButton() {
        await console.log('Start click Cancel Button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
        await element(btn_Cancel).click();
        await console.log('Done click cancel button');
    }

    // Click Save Button
    async clickSaveButton() {
        await console.log('Start click save button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Save)), 3000);
        await element(btn_Save).click();
        await console.log('Done click save button');
    }

    // Click Save Button
    async clickReOderButtonInRow(numRow1: number, numRow2: number) {
        await console.log('Start click Reoder button In Row');
        let btn_REorder1 = await element(by.xpath('//form/div/div[3]/div[2]/p-table//table/tbody/tr[' + numRow1 + ']/td[1]'));
        let btn_REorder2 = await element(by.xpath('//form/div/div[3]/div[2]/p-table//table/tbody/tr[' + numRow2 + ']/td[1]'));
        browser.actions().dragAndDrop( btn_REorder1 , btn_REorder2).mouseUp().perform();
        // browser.actions().clickAndHold(btn_REorder1).moveToElement(btn_REorder2).release().build().perform();
        // await browser.actions().mouseMove( btn_REorder1 ).perform();
        // await browser.actions().mouseDown( btn_REorder1 ).perform();
        // await browser.actions().mouseMove( btn_REorder2 ).perform();
        // await browser.actions().mouseUp().perform();

        // browser.actions().mouseDown(element(btn_REorder1).getWebElement()).perform();
        // browser.actions().mouseMove(element(btn_REorder2).getWebElement()).perform();
        // browser.actions().mouseUp().perform();
        await console.log('Done click Reoder button In Row');
    }

    // Choose VNFM Name on Available VNFMs
    async chooseVNFMNameOnAvailableVNFMs(txtVNFMName: string) {
        await console.log('Start Choose VNFM Name on Available VNFMs');
        const btn_VNFMName = by.xpath('//td[1][text()=" ' + txtVNFMName + ' "]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_VNFMName)), 3000);
        await element(btn_VNFMName).click();
        await console.log('Done Choose VNFM Name on Available VNFMs');
    }

    // Click remove VNFM on Continuity Plan VNFM Sequence table
    async ClickremoveVNFMonContinuityPlanVNFMSequenceTable(txtVNFMName: string) {
        await console.log('Start Click remove VNFM on Continuity Plan VNFM Sequence table');
        const btn_RemoveVNFM = by.xpath('//td[3][text()="' + txtVNFMName + '"]//following-sibling::*//*[contains(@class,"fa-times")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_RemoveVNFM)), 3000);
        await element(btn_RemoveVNFM).click();
        await console.log('Done Click remove VNFM on Continuity Plan VNFM Sequence table');
    }

    // Get Attribute of Continuity Plan Name
    async getAttributeOfContinuityPlanName() {
        await console.log('Start get Attribute of Continuity Plan Name');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_ContinuityName)), 4000);
        return element(txb_ContinuityName).getAttribute('value');
    }

    // Get Attribute of Continuity Plan Name
    async getAttributeOfContinuityPlanDescription() {
        await console.log('Start get Attribute of Continuity Plan Description');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_Description)), 4000);
        return element(txb_Description).getAttribute('value');
    }

    // Verify Add Continuity Plan From Present
    async verifyAddContinuityPlanIsPresent() {
        await console.log('Start verify Add Continuity Plan From Present');
        return element(txt_AddContinuityPlan).isPresent();
    }

    // Verify VNFM Name display on Avalable VNFM table
    async verifyVNFMNameDisplayOnAvalableVNFMTable(txtVNFMName: string) {
        const txt_VNFMName = by.xpath('//td[1][text()=" ' + txtVNFMName + ' "]');
        await console.log('Start verify VNFM Name display on Avalable VNFM table');
        return element(txt_VNFMName).isPresent();
    }

    // Verify VNFM Name Display On Continuity Plan VNFM Sequence Table
    async verifyVNFMNameDisplayOnContinuityPlanVNFMSequenceTable(txtVNFMName: string) {
        const txt_VNFMName = by.xpath('//td[3][text()="' + txtVNFMName + '"]');
        await console.log('Start verify VNFM Name Display On Continuity Plan VNFM Sequence Table');
        return element(txt_VNFMName).isPresent();
    }

    async getListVNFMNameInAvailableVNFMsTable() {
        await console.log('begin get VNFM Name in Available VNFMs Table');
        const lsName: any = [];
        const elColumnVNFMName = by.xpath('//form/div/div[3]/div[1]/p-table//table/tbody//td[1]');
        const list1 = await element.all(elColumnVNFMName).getText();
        for (let index = 0; index < list1.length; index++) {
            if (!lsName.includes(list1[index])) {
                lsName.push(list1[index]);
            }
        }
        return lsName;
    }

    async getListVNFMNameInContinuityPlanVNFMsSequenceTable() {
        await console.log('begin get VNFM Name in Continuity Plan VNFMs Sequence Table');
        const lsName: any = [];
        const elColumnVNFMName = by.xpath('//form/div/div[3]/div[2]/p-table//table/tbody//td[3]');
        const list1 = await element.all(elColumnVNFMName).getText();
        for (let index = 0; index < list1.length; index++) {
            if (!lsName.includes(list1[index])) {
                lsName.push(list1[index]);
            }
        }
        return lsName;
    }

    async getTextVNFMName(numRow: number) {
        await console.log('begin get VNFM Name');
        const elColumnVNFMName = by.xpath('//form/div/div[3]/div[2]/p-table//table/tbody/tr[' + numRow + ']/td[3]');
        const VNFMName = await element.all(elColumnVNFMName).getText();
        return VNFMName;
    }
}
