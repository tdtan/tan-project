import { by, element, browser, ExpectedConditions } from 'protractor';
const bulkActionDropListDisable = by.css('#tb-vnf div.ui-table-caption p-dropdown > div.ui-state-disabled');
// tslint:disable-next-line:max-line-length
const checkBoxSelectedRowTable = by.css('#tb-vnf table > tbody > tr > td:nth-child(1) > p-tablecheckbox > div > div.ui-chkbox-box.ui-state-active');
const checkBoxRowTable = by.css('#tb-vnf table > tbody > tr > td:nth-child(1) > p-tablecheckbox > div > div.ui-chkbox-box');
const checkBoxAll = by.css('#tb-vnf table > thead > tr:nth-child(1) > th:nth-child(1) > p-tableheadercheckbox > div > div.ui-chkbox-box');
// tslint:disable-next-line:max-line-length
const checkBoxAllSelected = by.css('#tb-vnf table > thead > tr:nth-child(1) > th:nth-child(1) > p-tableheadercheckbox > div > div.ui-chkbox-box.ui-state-active');
const actionPopups = by.css('p-dialog div[role="dialog"]');
// tslint:disable-next-line:max-line-length
const iconShowHiheActionDropList = by.css('#tb-vnf div.ui-table-caption p-dropdown div.ui-dropdown-trigger.ui-state-default.ui-corner-right');
const listActionsDrop = by.css('body > div > div > ul > p-dropdownitem > li > option');
const actionDropList = by.css('body div.ui-dropdown-items-wrapper');
const listVNFsAreSelectedInPopups = by.css('body p-dialog form ul > li');
const btnClosePopups = by.css('body p-dialog p-footer button.ui-button-secondary');
const inputBulkaction = by.css('body p-dialog form input[formcontrolname="bulkactionFormName"]');
const areaComments = by.css('body p-dialog form textarea[formcontrolname="bulkactionFormComments"]');
const rowTablePopups = by.css('body p-dialog table > tbody > tr');
const rowSelectedTablePopups = by.css('body p-dialog table > tbody > tr.ui-state-highlight');
const column = by.css('#tb-vnf table > thead > tr:nth-child(1) > th');
const saveButtonPopups = by.css('body p-dialog p-footer > p-button:nth-child(1) > button');

// tslint:disable-next-line:max-line-length
const txtTextPopup = by.css('p-dialog:nth-child(2) div[role="dialog"] div:nth-child(1) > span');
const txtVNFMPreference = by.css('#dialogPrefer div div:nth-child(1) span');
export class PageBulkActions {

    // check ulkActionDropList is disable
    async isBulkActionDropListDisable() {
        return await element(bulkActionDropListDisable).isPresent();
    }

    // get number checkbox select row table
    async getNumberCheckBoxSelectedRowTable() {
        const checkBox = await element.all(checkBoxSelectedRowTable);
        console.log('number checkbox selected: ', checkBox.length);
        return await checkBox.length;
    }

    // get total checkbox in row table
    async getTotalCheckBoxInRowTable() {
        const checkBox = await element.all(checkBoxRowTable);
        console.log('total checkbox in row: ', checkBox.length);
        return await checkBox.length;
    }

    // get Text header Popup
    async getTextHeaderPopup() {
        await console.log('Start get text header popup');
        const txtText = await element(txtTextPopup).getText();
        await console.log('Get text header popup: ' + txtText );
        return txtText;
    }

    // get Text header VNFM Preference
    async getTextHeaderPopupVNFMPreference() {
        await console.log('Start get text header popup VNFM Preference');
        const txtText = await element(txtVNFMPreference).getText();
        await console.log('Get text header popup: ' + txtText );
        return txtText;
    }

    // click checkbox all
    async clickCheckBoxAll() {
        await element(checkBoxAll).click();
        console.log('clicked checkbox all');
    }

    // check select all checkbox
    async isSelectedAllCheckBox() {
        return await element(checkBoxAllSelected).isPresent();
    }

    // select all checkbox table vnf
    async selectAllCheckBoxTableVnf() {
        const check = await this.isSelectedAllCheckBox();
        if (!check) {
            this.clickCheckBoxAll();
            await browser.wait(ExpectedConditions.elementToBeClickable(element(checkBoxAllSelected)), 3000);
        }
        await console.log('selected All CheckBox');
    }

    // unselect all checkbox table vnf
    async unselectAllCheckBoxTableVnf() {
        const check = await this.isSelectedAllCheckBox();
        if (check) {
            this.clickCheckBoxAll();
            await browser.wait(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(element(checkBoxAllSelected))), 3000);
        }
        await console.log('unselected All CheckBox');
    }

    // click icon show/hide action drop list
    async clickIconShowHideActionDropList() {
        await element(iconShowHiheActionDropList).click();
        console.log('clicked icon ShowHide action DropList');
    }

    // check show action drop list
    async isShowActionDropList() {
        return await element(actionDropList).isPresent();
    }

    // show action drop list
    async showActionDropList() {
        const check = await this.isShowActionDropList();
        if (!check) {
            await this.clickIconShowHideActionDropList();
        }
    }

    // hide action drop list
    async hideActionDropList() {
        const check = await this.isShowActionDropList();
        if (check) {
            await this.clickIconShowHideActionDropList();
        }
    }

    // get list action drop
    async getListActionDrop() {
        const list = await element.all(listActionsDrop).getAttribute('value');
        await console.log('list dropdown actions: ', list);
        return list;
    }

    // check show action popups
    async isShowActionsPopups() {
        return await element(actionPopups).isPresent();
    }

    // select item drop action by keyword
    async selectItemDropActionsByKeyWord(keyWord: string) {
        const el = by.cssContainingText('body > div > div > ul > p-dropdownitem > li', keyWord);
        await element(el).click();
        await console.log('selected item "' + keyWord + '"');
    }

    // get list vnf are selected in popups
    async getListVNFsAreSelectedInPopups() {
        const list = await element.all(listVNFsAreSelectedInPopups).getText();
        await console.log('list VNFs are selected in popups: ', list);
        return list;
    }

    // click button close popups
    async clickButtonClosePopups() {
        await element(btnClosePopups).click();
        await console.log('clicked button close popup');
    }

    // set text input BulkactionName
    async setTextInputBulkactionName(text: string) {
        await element(inputBulkaction).clear();
        await element(inputBulkaction).sendKeys(text);
        await console.log('sent text');
    }

    // get text input BulkactionName
    async getTextInputBulkactionName() {
        const text = await element(inputBulkaction).getAttribute('value');
        await console.log('done get text');
        return text;
    }

    // clear text input BulkactionName
    async clearTextInputBulkactionName() {
        await element(inputBulkaction).clear();
        await console.log('cleared');
    }

    // set text area comments
    async setTextareaComments(text: string) {
        await element(areaComments).clear();
        await element(areaComments).sendKeys(text);
        await console.log('sent text');
    }

    // get text area comments
    async getTextareaComments() {
        const text = await element(areaComments).getAttribute('value');
        await console.log('done get text');
        return text;
    }

    // clear text area comments
    async clearTextareaComments() {
        await element(areaComments).clear();
        await console.log('cleared');
    }

    // get row number table popups
    async getNumberRowTablePopups() {
        const row = await element.all(rowTablePopups).count();
        await console.log('number row table popups: ', row);
        return row;
    }

    // get number row is selected in table popups
    async getNumberRowSelectedTablePopups() {
        const row = await element.all(rowSelectedTablePopups).count();
        await console.log('number row selected table popups: ', row);
        return row;
    }

    // click checkbox table popups
    async clickCheckBoxTablePopups(level: number) {
        // tslint:disable-next-line:max-line-length
        const checkBoxTablePopups = by.css('body p-dialog table > tbody > tr:nth-child(' + level + ') > td:nth-child(1) > p-tableradiobutton > div > div.ui-radiobutton-box');
        await element(checkBoxTablePopups).click();
        await console.log('clicked');
    }

    // click checkbox in table vnf
    async clickCheckBoxTableVnfs(level: number) {
        // tslint:disable-next-line:max-line-length
        const checkbox = by.css('#tb-vnf table > tbody > tr:nth-child(' + level + ') > td:nth-child(1) > p-tablecheckbox > div > div.ui-chkbox-box');
        await element(checkbox).click();
        await console.log('clicked');
    }

    // get list column
    async getListColumn() {
        const list = await element.all(column).getText();
        await console.log('list column: ', list);
        return list;
    }

    // get index column contain key word
    async getIndexColumnContainKeyWord(keyWord: string) {
        let index = -1;
        const listColumn = await this.getListColumn();
        for (let i = 0; i < listColumn.length; i++) {
            if (listColumn[i] === keyWord) {
                index = i + 1;
                break;
            }
        }
        await console.log('index column "' + keyWord + '": ', index);
        return index;
    }

    // get list text row is selected by column
    async getListTextRowSelectedByKeyWordColumn(columnName: string) {
        const index = await this.getIndexColumnContainKeyWord(columnName);
        const el = by.css('#tb-vnf table > tbody > tr.ui-state-highlight > td:nth-child(' + index + ')');
        const listText = await element.all(el).getText();
        await console.log('List text selected in column "' + columnName + '": ', listText);
        return listText;
    }

    // get total records table actions
    async getTotalRecordsTableActions() {
        const el = by.css('#tb-vnf > div > p-paginator > div > div > div > span');
        const text = await element(el).getText();
        if (text === 'No records found') {
            return 0;
        }
        const splitText = text.split(' ');
        const TextToNumber = Number(splitText[0]);
        await console.log('total records table actions: ', TextToNumber);
        return TextToNumber;
    }
    // click save button in popups
    async clickSaveButtonPopups() {
        await element(saveButtonPopups).click();
        await console.log('clicked save button');
    }
}
