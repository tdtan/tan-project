import { element, by, ExpectedConditions, browser } from 'protractor';

const btn_showhire = by.xpath('//*[@id="operations-tag-Cloud_Configuration_REST_Service"]/div[2]/a');
const btn_ListOperations = by.xpath('//*[@id="operations-tag-Cloud_Configuration_REST_Service"]/div[3]/a');
const btn_ExpandOperations = by.xpath('//*[@id="operations-tag-Cloud_Configuration_REST_Service"]/div[4]/a');
const firstAPI = by.xpath('//*[@id="operations-Cloud_Configuration_REST_Service-updateUser"]');
const btn_try = by.xpath('//button[text()="Try it out "]');
const txtRestAPI = by.xpath('//*[@id="panel-restapis"]/div/div[1]/span');


export class PageRESTAPIs {

  // Verify show text RESTAPIs
  async verifyShowTextRESTAPIs(_textRESTAPIs: any) {
    console.log('Start get text.');
    await browser.wait(ExpectedConditions.visibilityOf(element(txtRestAPI)), 5000);
    const RESTAPIs = await element(txtRestAPI).getText();
    expect(RESTAPIs).toEqual(_textRESTAPIs);
    await console.log('text: ' + RESTAPIs);
    await console.log('Done.');
  }

  // Click button Show Hire
    async clickShowHire() {
    await console.log('Start click button show hire.');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_showhire)), 5000);
    await element(btn_showhire).click();
    await console.log('Done click button show hire');
  }

  // Verify display or undisplay table when click button show hire
    async verifyTableAfterClickShowHire() {
    await console.log('Start verify table');
    return element(firstAPI).isPresent();
  }

  // Click button List Operations
  async clickListOperations() {
    await console.log('Start click button list operations.');
    await element(btn_ListOperations).click();
    await console.log('Done click button list operations');
  }

  // Verify display table when click button List Operations
  async verifyTableAfterClickListOperation() {
    await console.log('Start verify table');
    await this.clickListOperations();
    // await browser.sleep(2000);
    await browser.wait(ExpectedConditions.visibilityOf(element(firstAPI)), 3000);
    expect(await element(firstAPI).isDisplayed()).toBeTruthy();
  }

  // Click button Expand Operations
  async clickExpandOperations() {
    await console.log('Start click button list operations.');
    await element(btn_ExpandOperations).click();
    await console.log('Done click button list operations');
  }

  // Verify display table when click button Expand Operations
  async verifyTableAfterClickExpandOperation() {
    await console.log('Start verify table');
    await this.clickExpandOperations();
    // await browser.sleep(2000);
    await browser.wait(ExpectedConditions.visibilityOf(element(btn_try)), 3000);
    expect(await element(btn_try).isDisplayed()).toBeTruthy();
  }

}
