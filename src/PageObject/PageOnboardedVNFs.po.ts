import { element, by, ExpectedConditions, browser, protractor } from 'protractor';
import { PageError } from './PageError.po';

    const btnOnboard = by.xpath('//*[@title="Onboard VNF"]/button');
    const txbSearch = by.css('#catalogPanel div.table-hdr-btn-grp input[type=text]');
    const btnShowFilter = by.css('#catalogPanel p-button[icon="fa fa-sliders"]');
    const drpFilter = by.css('tr th p-dropdown');
    const btnClearTextFilter = by.xpath('//*[@id="catalogPanel"]//tr[2]/th[8]/p-button/button');
    const btnRefesh = by.css('#catalogPanel p-button[icon="fa fa-refresh"]');
    const btnClearText = by.xpath('//p-button[@icon="fa fa-times"]/button');
    const btnShowColumn = by.css('#catalogPanel p-button[icon="fa fa-cog"]');
    const txtRecord = by.xpath('//*[@id="catalogPanel"]//p-table//p-paginator/div/div//span');
    const btnMinimix = by.xpath('//*[@id="catalogPanel"]//a[@aria-expanded="true"]');
    const rowTable = by.css('#catalogPanel tbody.ui-table-tbody tr');
    const lblNumberPage = by.xpath('//*[@id="catalogPanel"]//p-paginator//p-dropdown');
    const txtNoRecord = by.xpath('//*[@id="catalogPanel"]//span[text()=" No records found "]');
    const btn_confirmDelete = by.css('#catalogPanel p-footer button[icon="pi pi-pw pi-check"]');
    const txtNumberRow = by.xpath('//*[@id="catalogPanel"]//p-paginator//p-dropdown//label');
    const txtOnboard = by.xpath('//*[@id="catalogPanel"]//p-header/span');
    const txbDescription = by.xpath('//*[@id="catalogPanel"]//table/thead/tr[2]/th[6]/input');

    const btnClearAll = by.xpath('//button[@label="Clear All"]');
    const lcb_divError = by.xpath('/html/body/app-root/rbn-message/p-toast/div/p-toastitem[1]/div/div');
    const btnDropActions = by.xpath('//*[@id="catalogPanel"]//table/tbody/tr/td[7]/p-dropdown');

export class PageOnboardedVNFs {

    private pageError = new PageError();

    // get text Onboard in page header
    async getTextOnboard() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtOnboard)), 3000);
        const textOnb = await element(txtOnboard).getText();
        return textOnb;
    }

    // Set text Search in Onboarded VNF Page
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).clear();
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // clear text Onboard
    async ClearTextDescription() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txbDescription)), 3000);
        await console.log('Clear before input text Description');
        await element(txbDescription).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(txbDescription).sendKeys(protractor.Key.BACK_SPACE);
        await console.log('Done');
    }

    // Set text Search in Onboarded VNF Page
    async settextDescription(description: string) {
        await console.log('Start input text Description');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbDescription)), 3000);
        await element(txbDescription).sendKeys(description);
        await console.log('Done');
    }

    // get Attribute in Search
    async getAttributeSearch() {
        await console.log('Start get Attribute in Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        const value = await element(txbSearch).getAttribute('value');
        await console.log('Done');
        return value;
    }

    // Click button Onboard in Onboarded Page
    async clickOnboardbtn() {
        console.log('Start click Onboard');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOnboard)), 2000);
        await element(btnOnboard).click();
        console.log('Done');
    }

    // Click button Refesh in Onboarded Page
    async clickRefesh() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefesh)), 5000);
        await element(btnRefesh).click();
        await console.log('Done click refesh');
    }

    // Click button Refesh in Onboarded Page
    async clickClearTextSearch() {
        await console.log('Start click Clear Text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearText)), 5000);
        await element(btnClearText).click();
        await console.log('Done click Clear Text Search');
    }

    // Click Clear Button To Clear Text In Filter
    async clickClearTextInFilter() {
        await console.log('Start Clear Button To Clear Text In Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearTextFilter)), 5000);
        await element(btnClearTextFilter).click();
        await console.log('Done Clear Button To Clear Text In Filter');
    }

    // Click button Show|Hide Filter in Onboarded Page
    async clickShowHideFilter() {
        await console.log('Start click show hide filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done click show hide filter');
    }
    // Click button Refesh in Onboarded Page
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done click Show Hide Column');
    }

    // Click dropdown Actions
    async clickDropdownActions() {
        await console.log('Start click dropdown Actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnDropActions)), 5000);
        await element(btnDropActions).click();
        await console.log('Done click dropdown Actions');
    }

    // Click choose function in Actions dropdown
    async clickFuctionInActionsDropdown(strOptionName: string) {
        await console.log('Start click function in Actions dropdown');
        const btnfunction = by.xpath('//*[contains(option,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnfunction)), 3000);
        await element(btnfunction).click();
        await console.log('Done click function in Actions dropdown');
    }

    // Click button minimized in Onboarded Page
    async clickMinimaxTable() {
        await console.log('Start click minimized table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnMinimix)), 3000);
        await element(btnMinimix).click();
        await console.log('Done click minimized table');
    }

    // Click button confirm delete in Onboarded Page
    async clickConfirmDelete() {
        await console.log('Start click confirm delete');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_confirmDelete)), 3000);
        await element(btn_confirmDelete).click();
        await console.log('Done click confirm delete');
    }

    // Get Record  Current display
    async GetRecordCurrent() {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        const record = await element(txtRecord).getText();
        await console.log('Record current: ' + record);
        return record;

    }

    // Get Record  Current display
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="catalogPanel"]//table/tbody/tr/td[' + numTd + ']/div/div/span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Get text in filter
    async GetTextInFilter(numTd: number) {
        const txtfilter = by.xpath('//*[@id="catalogPanel"]//table/thead/tr[2]/th[' +  numTd + ']/p-dropdown//label');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtfilter)), 3000);
        const getTextFilter = await element(txtfilter).getText();
        await console.log('Text in Filter ' + numTd + ' : ' + getTextFilter);
        return getTextFilter;

    }

    // Get Number Row display
    async GetNumberRow() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRow)), 4000);
        const getNumRow = await element(txtNumberRow).getText();
        await console.log(' Current Row Number Display: ' + getNumRow);
        return getNumRow;

    }


    // Select Option from Show/ Hide columns List in Onboarded Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btnClose = by.xpath('//*[@id="catalogPanel"]//p-footer/p-button/button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await element(btnClose).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Select number page on table in Onboarded Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="catalogPanel"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await console.log('Click choose element filter: ' + filChoose);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="catalogPanel"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 3000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Verify sort column
    async verifySortColumn(numTh: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.css('#container-common-table tr:nth-child(1) th:nth-child(' + numTh + ') p-sorticon i[class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.css('#container-common-table tr:nth-child(1) th:nth-child(' + numTh + ') p-sorticon i[class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.css('#container-common-table tr:nth-child(1) th:nth-child(' + numTh + ') p-sorticon i[class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start click sort function');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click();
        await expect(element(sortup).isPresent()).toBeTruthy();
        await element(sortup).click();
        await expect(element(sortdown).isPresent()).toBeTruthy();
        await console.log('Done');
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Current record: ' + txtVerify);
        await console.log('Done');
    }

    // Verify display or undisplay table when click minimized
    async verifyTableAfterClickMinimized() {
        await console.log('Start verify table');
        await this.clickMinimaxTable();
        expect(await element(rowTable).isDisplayed()).toBeFalsy();
        await this.clickMinimaxTable();
        expect(await element(rowTable).isDisplayed()).toBeTruthy();
        await console.log('Done verify table');
    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
          });
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//*[@id="catalogPanel"]/div[1]//table/thead/tr[1]/th[text()=" ' + strOptionName + ' "]');
        if (await element(tableTH).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Verify Messege Error
    async verifyMessegeError() {
        await console.log('Start checking if display messege error');
        if (await element(lcb_divError).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Verify Dropdown filter is present in page
    async verifyDropdownFilterIsPresentInPage() {
        await console.log('Start check Dropdown filter is present in page');
        return await element(drpFilter).isPresent();
    }

    // Click clear All
    async cliclClearALL() {
        await console.log('Start click Clear All');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearAll)), 5000);
        await element(btnClearAll).click();
        await console.log('Done');
    }

    // Get number of columns in current search result table
    async getNumberOfColumnsInSearchTable(strTable: string) {
       let _numberOfColumn: number ;
       _numberOfColumn = 0 ;
       const _numOfMaximumColumns = 11;
       await console.log('Start checking if we have any table name equals Text value?');
       for ( let i = 1 ; i <= _numOfMaximumColumns; i++ ) {
        const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]/div[1]//table/thead/tr[1]/th[' + i + ']');
        if (await element(_strColumnLocator).isPresent()) {
            _numberOfColumn = i ;
        }
       }
       return _numberOfColumn ;

    }

    // Get column number based on column name in VFM Settings Page
    async getColumnNumberBasedOnColumnName(strTable: string, columnname: string) {
        let _numCurrentColumns: any;
        let _columnnum: number;
        _columnnum = 0;
        _numCurrentColumns = await this.getNumberOfColumnsInSearchTable(strTable);
        await console.log('Start getting column number based on column Name');
        for ( let i = 1 ; i <= _numCurrentColumns; i++ ) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//div[1]//table/thead/tr[1]/th[' + i + ']');
            await element(_strColumnLocator).getText().then(async function (text) {
                await console.log('text column ' + i + ': ' + text);
                if (text === columnname) {
                    _columnnum = i;
                }
            });
        }
        if (_columnnum === 0 ) {
            await console.log('No found ' + columnname + ' in search table');
        } else {
            await console.log('Column ' + _columnnum + ' has ' + columnname + ' as name');
        }
        return _columnnum;
    }

    // Get row number has Column Name Equals Text Value in Onboarded VNFs Page
    async getNumberofRowHasValueofColumnNameEqualsTextValue(strTable: string, intColumnNumber: number, strTextValue: string) {
        let _rownum: number;
        _rownum = 0;
        const record = by.xpath('//*[@id="' + strTable + '"]/div[1]//p-table//p-paginator/div/div//span');
        const numrecord = await element(record).getText();
        const _intSearchSheetRowNum1 = numrecord.slice(0, 9);
        const _intSearchSheetRowNum = Number(_intSearchSheetRowNum1.replace('Showing ', ''));
        await console.log('Current Search table has ' + _intSearchSheetRowNum + ' row(s)');
        for ( let i = 1 ; i <= _intSearchSheetRowNum; i++ ) {
            // tslint:disable-next-line:max-line-length
            const TextValueLocator = by.xpath('//*[@id="' + strTable + '"]//div[1]//table/tbody/tr[' + i + ']/td[' + intColumnNumber + ']//span');
            await element(TextValueLocator).getText().then(async function (text) {
                if (text === strTextValue) {
                    _rownum = i;
                }
            });
        }
        if (_rownum === 0 ) {
            await console.log('No found Row has Column Name Equals ' + strTextValue + ' in search table');
        } else {
            await console.log('Row ' + _rownum + ' has value of Column Name equals ' + strTextValue );
        }
        return _rownum;
    }

    // Click Function On Row Has Value of ColumnName Equals TextValue
    // tslint:disable-next-line:max-line-length
    async clickActionOnRowHasValueofColumnNameEqualsTextValue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, functionchoose: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]/div[1]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_FunctionLocator)), 4000);
        await console.log('click list Actions');
        const result1 = this.verifyMessegeError();
        if (result1) {
            this.cliclClearALL();
        }
        await element(btn_FunctionLocator).click();
        const _function = by.xpath('//li[contains(option,"' + functionchoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_function)), 4000);
        await console.log('click option in Actions');
        await element(_function).click();
        await console.log('Done');
    }


    async removeAllVnfOnboard() {
        await console.log('begin removeAllVnfOnboard');
        while (await this.countNumberVnfOnboard() > 0) {
            await this.showDropActionDropDown(1);
            await this.selectItemDropActions(2);
            await this.clickYesConfirmDialog();
            await console.log('deleted row 1');
            this.clearMessage();
        }
        this.clearMessage();
        await console.log('deleted all vnf onboard');
    }

    async countNumberVnfOnboard() {
        await console.log('begin countNumberVnfOnboard');
        const el = by.css('p-table table > tbody > tr');
        const rowNumber = await element.all(el).count();
        await console.log('number vnf onboard: ', rowNumber);
        return rowNumber;
    }

    async showDropActionDropDown(row: number) {
        await console.log('begin showDropActionDropDown');
        const el = by.css('p-table table > tbody > tr:nth-child(' + row + ') > td:nth-child(7) > p-dropdown');
        const isShow = await this.isDropListActionsDisplay();
        if (!isShow) {
            await element(el).click();
            await console.log('displayed drop list action');
        }
    }

    async isDropListActionsDisplay() {
        await console.log('begin isDropListActionsDisplay');
        const el = by.css('body > div > div.ui-dropdown-items-wrapper');
        const check = await element(el).isPresent();
        return check;
    }

    async selectItemDropActions(level: number) {
        await console.log('begin selectItemDropActions');
        const el = by.css('body ul p-dropdownitem:nth-child(' + level + ') li');
        await element(el).click();
        await console.log('done select item: ', level);
    }

    async clickYesConfirmDialog() {
        await console.log('begin clickYesConfirmDialog');
        const el = by.css('p-dialog button[icon="pi pi-pw pi-check"]');
        await element(el).click();
        await console.log('done click yes confirm');
    }

    async clearMessage() {
        await console.log('begin clearMessage');
        const result1 = await this.pageError.verifyMessegeError();
        const result2 = await this.pageError.verifyMessegeWarning();
        const result3 = await this.pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await this.pageError.cliclClearALL();
        }
        await console.log('done');
    }



}
