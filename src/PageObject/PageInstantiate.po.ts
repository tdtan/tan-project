import { by, element } from 'protractor';

const itemsDropFunction = by.css('body div.ui-dropdown-items-wrapper');
export class PageInstantiate {

    // click show item drop function
    async clickShowItemDropFunction(row: number, column: number) {
        const el = by.css('table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + column + ') > p-dropdown > div');
        await element(el).click();
        await console.log('clicked');
    }

    // check item drop function is show
    async isShowItemsDropFunction() {
        return await element(itemsDropFunction).isPresent();
    }

    // show item drop function
    async showItemDropFunction(row: number, column: number) {
        const check = await this.isShowItemsDropFunction();
        if (!check) {
            await this.clickShowItemDropFunction(row, column);
        }
        await console.log('has shown');
    }

    // hide item drop function
    async hideItemDropFunction(row: number, column: number) {
        const check = await this.isShowItemsDropFunction();
        if (check) {
            await this.clickShowItemDropFunction(row, column);
        }
        await console.log('hidden');
    }

    // select item function by keyword
    async selectItemFunctionByKeyWord(row: number, column: number, itemName: string) {
        await this.showItemDropFunction(row, column);
        const el = by.cssContainingText('body div.ui-dropdown-items-wrapper p-dropdownitem li', itemName);
        await element(el).click();
        await console.log('selected item "' + itemName + '"');
    }

    // check InstantiateVNF display
    async isInstantiateVNFDisplay() {
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('#catalogPanel > div.ng-star-inserted > p-panel > div > div.ui-panel-titlebar.ui-widget-header > span', 'Instantiate');
        return await element(el).isPresent();
    }

    // check InstantiateVNFM Preference display
    async isInstantiateVNFMPreferenceDisplay() {
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('#InstPanel > form > div.leftPanel > div:nth-child(12)', 'VNFM Preference');
        return await element(el).isPresent();
    }

    // click show item dropdown in InstantiateVNF
    async clickShowItemDropdownInInstantiate(formcontrolname: string) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#InstPanel > form > div.leftPanel p-dropdown[formcontrolname="' + formcontrolname + '"] > div');
        await element(el).click();
        await console.log('displayed');
    }

    // select item dropdown in InstantiateVNF
    async selectItemDropdownInInstantiateVNFByKeyWord(formcontrolname: string, level: number) {
        await this.clickShowItemDropdownInInstantiate(formcontrolname);
        // tslint:disable-next-line:max-line-length
        const el = by.css('#InstPanel > form > div.leftPanel p-dropdown p-dropdownitem:nth-child(' + level + ') > li');
        await element(el).click();
        await console.log('selected level "' + level + '"');
    }

    // click checkbox in in table VNFMPreference
    async clickCheckBoxTableVNFMPreference(level: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#InstPanel div > div > div > div.ui-table-scrollable-body > table > tbody > tr:nth-child(' + level + ') > td:nth-child(1) > p-tableradiobutton > div > div.ui-radiobutton-box');
        await element(el).click();
        await console.log('clicked checkbox');
    }

    // check checkbox is selected in table VNFMPreference
    async isSelectedCheckBoxTableVNFMPreference(level: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#InstPanel div > div > div > div.ui-table-scrollable-body > table > tbody > tr:nth-child(' + level + ') > td:nth-child(1) > p-tableradiobutton > div > div.ui-radiobutton-box.ui-state-active');
        return await element(el).isPresent();
    }

    // get number checkbox is selected in table VNFMPreference
    async getNumberCheckboxIsSelectedTableVNFMPreference() {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#InstPanel div > div > div > div.ui-table-scrollable-body > table > tbody > tr > td:nth-child(1) > p-tableradiobutton > div > div.ui-radiobutton-box.ui-state-active');
        const numberRow = await element.all(el).count();
        await console.log('number checkbox is selected table vnfm preference: ', numberRow);
        return numberRow;
    }

    // get number row in table VNFMPreference
    async getRowNumberTableVNFMPreference() {
        const el = by.css('#InstPanel div > div > div > div.ui-table-scrollable-body > table > tbody > tr');
        const numberRow = await element.all(el).count();
        await console.log('number row table vnfm preference: ', numberRow);
        return numberRow;
    }

    // select all checkbox TableVNFMPreference
    async selectAllCheckBoxTableVNFMPreference() {
        const rowNumber = await this.getRowNumberTableVNFMPreference();
        for (let i = 1; i <= rowNumber; i++) {
            const check = await this.isSelectedCheckBoxTableVNFMPreference(i);
            if (!check) {
                await this.clickCheckBoxTableVNFMPreference(i);
            }
        }
        await console.log('selected all checkbox');
    }

    // unselect all checkbox TableVNFMPreference
    async unselectAllCheckBoxTableVNFMPreference() {
        const rowNumber = await this.getRowNumberTableVNFMPreference();
        for (let i = 1; i <= rowNumber; i++) {
            const check = await this.isSelectedCheckBoxTableVNFMPreference(i);
            if (check) {
                await this.clickCheckBoxTableVNFMPreference(i);
            }
        }
        await console.log('unselected all checkbox');
    }
}
