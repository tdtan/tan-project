import { element, by, ExpectedConditions, browser } from 'protractor';

const txtShowEvent = by.xpath('//app-vnf-events/rbn-common-table//p-accordion//p-header/span');
const txbSearch = by.xpath('//app-vnf-events/rbn-common-table//p-table/div/div[1]//input');
const txtrecord = by.xpath('//app-vnf-events//p-paginator/div/div//span');
const btnNumRow = by.xpath('//app-vnf-events//p-accordion//p-paginator//p-dropdown');
const ttRowtable = by.xpath('//app-vnf-events//p-accordion//table/tbody/tr');
const txtNoRecord = by.xpath('//app-vnf-events//span[text()=" No records found "]');
const btnShowFilter = by.xpath('//app-vnf-events//p-button[@icon="fa fa-sliders"]');
// tslint:disable-next-line:comment-format
//const btnClear = by.xpath('//app-vnf-events//p-accordiontab/div[1]//p-button[@icon="fa fa-times"]');
const btnClearSearch = by.xpath('//app-vnf-events//p-table//p-button[@icon="fa fa-times"]');
const txbDate = by.xpath('//table/thead/tr[2]/th[2]//button[@icon="fa fa-calendar"]');
const btnPageEnd = by.xpath('//a[@class="ui-paginator-last ui-paginator-element ui-state-default ui-corner-all"]');
// tslint:disable-next-line:max-line-length
const pageEnd = by.xpath('//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
export class PageShowEvents {

    // click button Show Hide Filter  in table
    async clickShowHideFilter() {
        await console.log('Start cilck clear button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done');
    }

    // click button clear search in table
    async clickClearSearch() {
        await console.log('Start cilck clear button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done');
    }

    // Click Date in Show Event Page
    async clickDate() {
        await console.log('Start click date.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbDate)), 4000);
        await element(txbDate).click();
        await console.log('Done click date .');
    }

    // click button page End
    async clickbtnPageEnd() {
        await console.log('Start click button move to page End');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPageEnd)), 3000);
        await element(btnPageEnd).click();
        await console.log('Done');
    }
    // Get text Show Event in form
    async getTextShowEvent() {
        await console.log('Start Get text');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtShowEvent)), 3000);
        const textInstan = await element(txtShowEvent).getText();
        return textInstan;
    }

    // Get number Page End
    async getNumberPageEnd() {
        await console.log('Get Number Page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(pageEnd)), 3000);
        const textInstan = await element(pageEnd).getText();
        return textInstan;
    }
    // Get text record in event table
    async getRecord() {
        await console.log('Start get record in event table.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtrecord)), 4000);
        const record = await element(txtrecord).getText();
        await console.log('Text record:' + record);
        await console.log('Done get record.');
        return record;
    }

    // Set text Search in Show Event Page
    async setSearch(_search: string) {
        await console.log('Start Input search.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 4000);
        await element(txbSearch).clear();
        await element(txbSearch).sendKeys(_search);
        await console.log('Done Input search.');
    }

    // Select number row display in one page envent table
    async selectRowOnPage(rownum: Number) {
        await console.log('Start select number row on one page.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnNumRow)), 3000);
        await element(btnNumRow).click();
        await console.log('Start choose option number.');
        const _numberRow = by.xpath('//li[contains(span,"' + rownum + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_numberRow)), 3000);
        await element(_numberRow).click();
        await console.log('Done.');
    }

    // Select date in event table
    async selectDateInTable(strday: string) {
        await console.log('Start select date ');
        const sel3 = element(by.xpath('//button[contains(span,"' + strday + '")]'));
        sel3.click();
    }

    // Count number Row display on one page
    async countNumberRow() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(ttRowtable)), 3000);
        const count = await element.all(ttRowtable).count();
        await console.log('Number Row on one page: ' + count);
        return count;
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtrecord)), 3000);
        expect(await element(txtrecord).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Select filter from talbe in Live VNF page
    async SelectFilter(columnnumber: number, txtChoose: string) {
        const filtercolumn = by.xpath('//app-vnf-events//th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

}
