// tslint:disable-next-line:quotemark
import { by, browser, element, ExpectedConditions } from "protractor";

const btn_AddRole = by.xpath('//*[@id="panel-roles"]//div/button');
const btn_Cancel = by.css('#panel-role-actions .ui-toolbar-group-right button:nth-child(1)');
const btn_Save = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txt_RoleName = by.xpath('//*[@id="panel-role-actions"]//form/div/div/div[2]/input');
const txt_RoleLabel = by.xpath('//*[@id="panel-role-actions"]//form/div/div/div[1]');
const txt_ErrorMessage = by.xpath('//*[@id="panel-role-actions"]//form/div/div/div[2]/small');
const pemission_control = by.xpath('//*[@id="0"]/div/span');
const select_pemission = by.xpath('//*[@id="1"]/div/span');
const txt_pemission = by.css('#panel-role-actions .right-form .ng-star-inserted div:nth-child(1) div');
const txt_search = by.xpath('//*[@id="tb-roles"]//input[@placeholder="Search"]');
const txt_Record = by.xpath('//*[@id="tb-roles"]//p-paginator/div/div//span');
// function verify empty form
export class PageAddRoles {
  async verifyEmptydisplayed(txtConfig: string) {
    const emptyDisplayed = by.css('#panel-role-actions input[formcontrolname="' + txtConfig + '"].ng-invalid');
    await console.log('Start Verify Empty form is displayed when user clicks On Add Role');
    if (element(emptyDisplayed).isPresent()) {
       return true;
    } else {
      return false;
    }
    await console.log('Done verify empty form is displayed when user click on Add tenant');
  }
// Function click button Add Role
  async clickAddRole() {
       console.log('Start click button Add Role');
       await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_AddRole)), 3000);
       await element(btn_AddRole).click();
       console.log('Done Click Button Add Role');
  }
  // Function click button Cancel
  async clickCancel() {
    console.log('Start click button Cancel');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Cancel)), 3000);
    await element(btn_Cancel).click();
    console.log('Done Click Button Cancel');
  }
  // Function click button Save
  async clickSave() {
    console.log('Start click button Save');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Save)), 3000);
    await element(btn_Save).click();
    console.log('Done Click Button Save');
  }
  // Function click Input Role Name
  async clickInputRoleName() {
      console.log('start click Input Role Name');
       await browser.wait(ExpectedConditions.elementToBeClickable(element(txt_RoleName)), 3000);
       await element(txt_RoleName).click();
       await browser.wait(ExpectedConditions.elementToBeClickable(element(txt_RoleLabel)), 3000);
       await element(txt_RoleLabel).click();
       const errormessage = await element(txt_ErrorMessage).getText();
       return errormessage;
  }
  // Function click button Permission Control
  async clickPermissionControl() {
    console.log('start click pemission control');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(pemission_control)), 3000);
    await element(pemission_control).click();
    await console.log('done click pemission control');
    const pemission = await element(txt_pemission).getText();
    await console.log('get text permission: ' + pemission);
    return pemission;
  }
  // set value for Permission List
  async setPemission() {
    console.log('start click pemission control');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(select_pemission)), 3000);
    await element(select_pemission).click();
    await console.log('done click pemission control');
  }
  // set value for Input Role Name
  async setRoleName(rolename: any) {
    console.log('Start input Role Name');
    await element(txt_RoleName).clear();
    await element(txt_RoleName).sendKeys(rolename);
    console.log('Done input Role Name');
  }
  // set value for input search
  async setSearch(rolename: string) {
    console.log('start input search');
    await browser.wait(ExpectedConditions.visibilityOf(element(txt_search)), 3000);
    await element(txt_search).clear();
    await element(txt_search).sendKeys(rolename);
  }
  // function verify Record Display
  async  verifyRecord() {
    await browser.sleep(3000);
    console.log('start verify Record');
    await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
    const txtRecord = element(txt_Record).getText();
    return txtRecord;
    console.log('Done verify Record');
  }
// function add Role
async funAddRole(rolename: string) {
    console.log('start add role');
    await this.setRoleName(rolename);
    await this.setPemission();
    // await this.clickSave();
    console.log('done add role');
}
}
