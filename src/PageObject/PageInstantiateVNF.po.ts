import { element, by, ExpectedConditions, browser } from 'protractor';
import { PageOnboardedVNFs } from './PageOnboardedVNFs.po';

const txtInstatiate = by.xpath('//p-panel//div[@role="region"]//h3');
const txbVNFName = by.xpath('//*[@id="vnf_instance_name"]');
const drpclould = by.xpath('//*[@id="InstPanel"]//p-dropdown[@formcontrolname="cloudId"]');
const drpTenant = by.xpath('//*[@id="InstPanel"]//p-dropdown[@formcontrolname="tenantId"]');
const drpZone = by.xpath('//*[@id="InstPanel"]//p-dropdown[@formcontrolname="zoneName"]');
const drpVNFMPreferred = by.xpath('//p-dropdown[@formcontrolname="selectedRowVnfm"]');
const txtVNFMName = by.xpath('//p-dropdown[@formcontrolname="selectedRowVnfm"]//label');
const cbxReuseIp = by.xpath('//*[@id="InstPanel"]//p-checkbox[@formcontrolname="reusePreAllocatedFIPs"]');
const messCloud = by.xpath('//*[@id="InstPanel"]//p-accordiontab[1]//div[4]//small[@class="text-error"]');
const messTenant = by.xpath('//*[@id="InstPanel"]//p-accordiontab[1]//div[6]//small[@class="text-error"]');
const countIterm = by.css('ul p-dropdownitem li');
let drp_ContinuityPlan = by.xpath('//p-dropdown[@formcontrolname="continuityPlanId"]');
const txt_Labelcontinuityplan = by.xpath('//*[@id="InstPanel"]//p-accordiontab[1]//div[8]/label');
let txt_OptionDefaultContinuity = by.xpath('//p-dropdown[@formcontrolname="continuityPlanId"]//label');

// Network
const lnkShowNetwork = by.xpath('//*[@id="InstPanel"]//p-accordiontab[2]//a[@role="tab"]');
const dropNetwork_Eth0 = by.xpath('//*[@id="InstPanel"]//p-accordiontab[2]//tr[1]/td[2]//p-dropdown');
const dropNetwork_Eth1 = by.xpath('//*[@id="InstPanel"]//p-accordiontab[2]//tr[2]/td[2]//p-dropdown');
const dropNetwork_Pkt1 = by.xpath('//*[@id="InstPanel"]//p-accordiontab[2]//tr[3]/td[2]//p-dropdown');

// Port
const lnkShowPort = by.xpath('//*[@id="InstPanel"]//p-accordiontab[3]/div[1]/a');
 // tslint:disable-next-line:max-line-length
 const txt_Port1 = by.xpath('//li[@class="ui-dropdown-item ui-corner-all ui-state-disabled"]');

// Volumme
const lnkShowVolume = by.xpath('//*[@id="InstPanel"]//p-accordiontab[5]/div[1]/a');
// flavor
const linkShowFlavor = by.xpath('//*[@id="InstPanel"]//p-accordiontab[4]/div[1]/a');
const drpflavor = by.xpath('//*[@id="InstPanel"]//p-accordiontab[4]//p-dropdown');

// VNF Specific
const linkShowrVNFSpecific = by.xpath('//*[@id="InstPanel"]//p-accordiontab[6]//a');
const txbInitData = by.xpath('//textarea[@formcontrolname="cloudConfigInitData"]');
const btnInstantiate = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
const btnBackVNFCatalog = by.css('app-catalog-table p-breadcrumb ul li:nth-child(1) a');

// SOL
const btn_add_Sol = by.xpath('//*[@id="InstPanel"]//button[@class="fa fa-plus mR10 p"]');
const btn_remove_sol = by.xpath('//*[@id="s1cp_ssbc_btn_3"]');
// tslint:disable-next-line:max-line-length
const row_parameter_sol = by.css('rbn-key-value form .ui-grid-pad:nth-child(1) .ui-grid-row.rows-edit-form');
const row_parameter_sol_removed = by.css('rbn-key-value form div[class="display-false"]');
const txt_Sol_key = by.xpath('//*[@id="s1cp_ssbc_key_root"]');
const txt_Sol_keypair = by.xpath('//*[@id="s1cp_ssbc_valuePair_root"]');

const pageOnboardedVNFs = new PageOnboardedVNFs();
const data = require('../../../e2e/VNFM_DB.e2e.json');
export class PageInstantiateVNF {

    txt_OptionInDropdown         = element.all(by.xpath('//p-dropdownitem//span'));
    // Click check box Reuse Ip
    async clickReuseIp() {
        await console.log('start click Reuse Ip ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxReuseIp)), 3000);
        await element(cbxReuseIp).click();
        await console.log('Done');
    }

    // Click VNF Name
    async clickVNFName() {
        await console.log('start click VNF Name ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbVNFName)), 3000);
        await element(txbVNFName).click();
        await console.log('Done');
    }

    // Click Dropbox Cloud
    async clickdbxCloud() {
        await console.log('start click Dropbox Cloud ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpclould)), 3000);
        await element(drpclould).click();
        await console.log('Done');
    }

    // Click Dropbox Tenant
    async clickdbxTenant() {
        await console.log('start click Dropbox Tenant ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpTenant)), 3000);
        await element(drpTenant).click();
        await console.log('Done');
    }

    // Click show form network
    async clickShowFormNetwork() {
        await console.log('start click show form network ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lnkShowNetwork)), 5000);
        await element(lnkShowNetwork).click();
        await console.log('Done');
    }

    // Click show form Address Port
    async clickShowFormAddressPort() {
        await console.log('start click show form Address Port ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lnkShowPort)), 3000);
        await element(lnkShowPort).click();
        await console.log('Done');
    }

    // Click show form Storage Volumes
    async clickShowStorageVolumesFrom() {
        await console.log('start click show Storage Volumes From');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lnkShowVolume)), 3000);
        await element(lnkShowVolume).click();
        await console.log('Done');
    }

    // Click show form flavor
    async clickShowFormFlavor() {
        await console.log('start click show form flavor ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(linkShowFlavor)), 3000);
        await element(linkShowFlavor).click();
        await console.log('Done');
    }

    // Click show form flavor
    async clickShowVNFSpecificForm() {
        await console.log('start click show form VNF Specific');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(linkShowrVNFSpecific)), 3000);
        await element(linkShowrVNFSpecific).click();
        await console.log('Done');
    }

    // Click Network  in form
    async clickNetworkInForm(numberNetWork: number) {
        await console.log('start click Network ');
        const nekEth = by.xpath('//*[@id="InstPanel"]//p-accordiontab[2]//tr[' + numberNetWork + ']/td[2]//p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(nekEth)), 3000);
        await element(nekEth).click();
        await console.log('Done');
    }

    // Click Volume in form
    async clickPortInForm(numberVolume: number) {
        await console.log('start click Network ');
        const nekEth = by.xpath('//*[@id="InstPanel"]//table/tbody/tr[' + numberVolume + ']/td[3]/p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(nekEth)), 3000);
        await element(nekEth).click();
        await console.log('Done');
    }

    // Click button Instantiate
    async clickInstantiate() {
        await console.log('start click Instantiate');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnInstantiate)), 3000);
        await element(btnInstantiate).click();
        await console.log('Done');
    }

    // Click button Cancel
    async clickCancel() {
        await console.log('start click Cancel ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done');
    }

    // Click back to page VNF Catalog
    async clickBackToPageVNFCatalog() {
        await console.log('start back to page VNF Catalog ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnBackVNFCatalog)), 3000);
        await element(btnBackVNFCatalog).click();
        await console.log('Done');
    }

    // Click VNFM Perferred
    async clickVNFMPerferred() {
        await console.log('start click VNFM Perferred ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpVNFMPreferred)), 3000);
        await element(drpVNFMPreferred).click();
        await console.log('Done');
    }

    // Click VNFM Perferred
    async clickChooseOptionVNFMPerferred(numberStr:  number) {
        await console.log('start click Choose OptionVNFM Perferred ');
        const _optVNFmPerferred = by.xpath('//*[@id="InstPanel"]//ul/p-dropdownitem[' + numberStr + ']/li');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_optVNFmPerferred)), 3000);
        await element(_optVNFmPerferred).click();
        await console.log('Done');
    }

    // Click Dropdowm Port in form
    async ClickDropdownPort(numberTr: number) {
        await console.log('Start Click Dropdowm Port in form');
        const drpPort = by.xpath('//*[@id="InstPanel"]//table/tbody/tr[' + numberTr + ']/td[5]//p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpPort)), 3000);
        await element(drpPort).click();
        await console.log('Done');
    }

    // Click Option Port
    async ClickOptionPort() {
        await console.log('Start Click Option Port');
        const opt_port = by.xpath('/html/body/div/div/ul/p-dropdownitem[2]/li');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_port)), 3000);
        await element(opt_port).click();
        await console.log('Done select port');
    }

    // select continuity plan in form
    async ClickContinuityPlanDropdownInFrom() {
        await console.log('start click continuity plan dropdown in formm');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drp_ContinuityPlan)), 3000);
        await element(drp_ContinuityPlan).click();
        await console.log('Done');
    }

    // Click Option Port
    async ClickOptionContinuityPlan(strOption: string) {
        await console.log('start select option Continuity plan in form');
        const opt_port = by.xpath('//li[contains(span,"' + strOption + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_port)), 3000);
        await element(opt_port).click();
        await console.log('Done');
    }

    // Get text option in dropdown on form
    async getTextOptionInDrodown() {
        await console.log('Start get text option in dropdown on form');
        let lsName: string [] = [];
        const sizeName  = await this.txt_OptionInDropdown.count();
        await console.log('size: ', sizeName);
        for (let index = 1; index <= sizeName; index++) {
            const list1 = await element(by.xpath('//p-dropdownitem[' + index + ']//span')).getText();
            await console.log('Name continuity plan: ', list1);
            if (!lsName.includes(list1)) {
                lsName.push(list1);
            }
        }
        await lsName.sort();
        return lsName;
    }

    // Get text Instantiate in form
    async getTextInstantiate() {
        await console.log('Start get text Instantiate');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtInstatiate)), 3000);
        const textInstan = await element(txtInstatiate).getText();
        return textInstan;
    }

    // get text Init Data
    async gettextInitData() {
        await console.log('Get text init Data');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbInitData)), 3000);
        const textInitData = await element(txbInitData).getText();
        await console.log('Done');
        return textInitData;
    }

    // get Attribute in VNF name
    async getAttributeInitData() {
        await console.log('Start get Attribute in Init Data');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbInitData)), 3000);
        const value = await element(txbInitData).getAttribute('value');
        await console.log('Done');
        return value;
    }

    // get Attribute in VNF name
    async getAttributeVNFName() {
        await console.log('Start get Attribute in VNF Name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbVNFName)), 3000);
        const value = await element(txbVNFName).getAttribute('value');
        await console.log('Done');
        return value;
    }

    // get text IP form
    async getAttributeIPForm(numtxtIp: number) {
        await console.log('Start get Attribute in manual Ip');
        const txtIP = by.xpath('//*[@id="InstPanel"]//table/tbody/tr[' + numtxtIp + ']/td[4]/input');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtIP)), 3000);
        const textIP = await element(txtIP).getAttribute('value');
        await console.log('Done');
        return textIP;
    }

    // get text Continuity Plan
    async gettextContinuityPlan() {
        await console.log('Get text continuity plan');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txt_Labelcontinuityplan)), 3000);
        const textContinuityPlan = await element(txt_Labelcontinuityplan).getText();
        await console.log('Done');
        return textContinuityPlan;
    }

    // get text option Continuity Plan
    async gettextOptionContinuityPlan() {
        await console.log('Get text option continuity plan');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txt_OptionDefaultContinuity)), 3000);
        const textContinuityPlan = await element(txt_OptionDefaultContinuity).getText();
        await console.log('Done');
        return textContinuityPlan;
    }

    // get text Cloud
    async gettextCloud() {
        await console.log('Get text could');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpclould)), 3000);
        const textdrpCloud = await element(drpclould).getText();
        await console.log('Cloud name: ' + textdrpCloud);
        await console.log('Done');
        return textdrpCloud;
    }

    // get text tenant
    async gettextTenant() {
        await console.log('Get text tenant');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpTenant)), 3000);
        const textdrpTenant = await element(drpTenant).getText();
        await console.log('Tenant name: ' + textdrpTenant);
        await console.log('Done');
        return textdrpTenant;
    }

    // get text Message Cloud
    async gettextMessageCloud() {
        await console.log('Get text message cloud');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(messCloud)), 3000);
        const textmesCloud = await element(messCloud).getText();
        await console.log('Done');
        return textmesCloud;
    }

    // get text Message Tenant
    async gettextMessageTenant() {
        await console.log('Get text message tenant');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(messTenant)), 3000);
        const textmesTenant = await element(messTenant).getText();
        await console.log('Done');
        return textmesTenant;
    }

    // get text Dropbox Zone
    async gettextZone() {
        await console.log('Get text zone');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpZone)), 5000);
        const textzone = await element(drpZone).getText();
        await console.log('Done');
        return textzone;
    }

    // get text VNFM Perferred
    async gettextVNFMPerferred() {
        await console.log('Get text VNFM Perferred');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtVNFMName)), 3000);
        const textVNFMName = await element(txtVNFMName).getText();
        await console.log('Done');
        return textVNFMName;
    }

    // get text network
    async gettextNetwork(numtxtNetwork: number) {
        await console.log('Get text network ' + numtxtNetwork);
        const txtNetwork = by.xpath('//*[@id="InstPanel"]//table/tbody/tr[' + numtxtNetwork + ']/td[1]/label');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtNetwork)), 5000);
        const textNetwork = await element(txtNetwork).getText();
        await console.log('Done');
        return textNetwork;
    }

    // get text Port in form
    async gettextPort(numtrPort: number) {
        await console.log('Get text port');
        const txtPort = by.xpath('//*[@id="InstPanel"]//p-accordiontab[3]//table/tbody/tr[' + numtrPort + ']/td[5]//p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtPort)), 3000);
        const textPort = await element(txtPort).getText();
        await console.log('Done');
        return textPort;
    }

    // get text Port disable
    async gettextPortDisable() {
        await console.log('Get text port disable');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txt_Port1)), 3000);
        const textPort = await element(txt_Port1).getText();
        await console.log('Done');
        return textPort;
    }

    // get text Volume in form
    async gettextVolume() {
        await console.log('Get text volume');
        const txtVolume = by.xpath('//*[@id="InstPanel"]//p-accordiontab[5]//p-dropdown//label');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtVolume)), 3000);
        const textVolume = await element(txtVolume).getText();
        await console.log('Done');
        return textVolume;
    }

    // get text Volume in form
    async gettextVolume2() {
        await console.log('Get text volume');
        const txtVolume = by.xpath('//*[@id="mvol_%orchname%"]//label');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtVolume)), 3000);
        const textVolume = await element(txtVolume).getText();
        await console.log('Done');
        return textVolume;
    }

    // get text Volume in form
    async getVolumeAssign() {
        await console.log('Get text volume asign');
        const txtvolume2 = by.css('#InstPanel p-accordiontab:nth-child(9) table tbody tr td:nth-child(1)');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtvolume2)), 3000);
        const textVolume = await element(txtvolume2).getText();
        await console.log('Done');
        return textVolume;
    }

    // Count Iterm In Form
    async countItermInForm() {
        await console.log('start count Iterm In Form');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(countIterm)), 3000);
        const numIterm = await element.all(countIterm).count();
        return numIterm;
    }

    // Set VNF Name
    async setInitData(strInitData: string) {
        await console.log('Start input Init Data');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbInitData)), 3000);
        await element(txbInitData).clear();
        await element(txbInitData).sendKeys(strInitData);
        await console.log('Done');
    }

    // Set VNF Name
    async setVNFName(strVNFName: string) {
        await console.log('Start input VNF name');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbVNFName)), 3000);
        await element(txbVNFName).clear();
        await element(txbVNFName).sendKeys(strVNFName);
        await console.log('Done');
    }

    // select cloud in VNF Config
    async selectCloud(strClouldname: string) {
        await console.log('start select cloud');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpclould)), 5000);
        await element(drpclould).click();
        await console.log('start choose option cloud name');
        const opt_cloudName = by.xpath('//li[contains(span,"' + strClouldname + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_cloudName)), 3000);
        await element(opt_cloudName).click();
        await console.log('Done select clooud');

    }

    // select Tenant in VNF Config
    async selectTenant(strTenantdname: string) {
        await console.log('start select tenant');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpTenant)), 3000);
        await element(drpTenant).click();
        const opt_tenantName = by.xpath('//li[contains(span,"' + strTenantdname + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_tenantName)), 3000);
        await element(opt_tenantName).click();
        await console.log('Done select teant');

    }

    // select Network Eth0 in VNF Config
    async selectNetworkEth0(strEth0: string) {
        await console.log('start select Network Eth0');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(dropNetwork_Eth0)), 5000);
        await element(dropNetwork_Eth0).click();
        const opt_eth0 = by.xpath('//li[contains(span,"' + strEth0 + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_eth0)), 8000);
        await element(opt_eth0).click();
        await console.log('Done select Network Eth0');
    }

    // select Network Eth1 in VNF Config
    async selectNetworkEth1(strEth1: string) {
        await console.log('start select Network Eth1');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(dropNetwork_Eth1)), 3000);
        await element(dropNetwork_Eth1).click();
        const opt_eth1 = by.xpath('//li[contains(span,"' + strEth1 + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_eth1)), 3000);
        await element(opt_eth1).click();
        await console.log('Done select Network Eth1');
    }

    // select Network Pkt1 in VNF Config
    async selectNetworkPkt1(strpkt1: string) {
        await console.log('start select Network pkt1');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(dropNetwork_Pkt1)), 3000);
        await element(dropNetwork_Pkt1).click();
        const opt_eth1 = by.xpath('//li[contains(span,"' + strpkt1 + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_eth1)), 3000);
        await element(opt_eth1).click();
        await console.log('Done select Network pkt1');
    }

    // select Zone in VNF Config
    async selectZone(strzone: string) {
        await console.log('start select Zone');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpZone)), 3000);
        await element(drpZone).click();
        const opt_zone = by.xpath('//li[contains(span,"' + strzone + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_zone)), 3000);
        await element(opt_zone).click();
        await console.log('Done select Zone');
    }

    // select flavor in VNF Config
    async selectflavor(strflavor: string) {
        await console.log('start select flavor');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpflavor)), 3000);
        await element(drpflavor).click();
        const opt_flavor = by.xpath('//li[contains(span,"' + strflavor + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_flavor)), 3000);
        await element(opt_flavor).click();
        await console.log('Done select flavor');
    }


    // select port or address in form
    async selectPortOrAddressInForm(numberTr: number) {
        await console.log('start select Port or address in form');
        const drpAddressOrPort = by.xpath('//*[@id="InstPanel"]//p-accordiontab[3]//table/tbody/tr[' + numberTr + ']/td[4]/p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpAddressOrPort)), 3000);
        await element(drpAddressOrPort).click();
        await console.log('start select option Port in form');
        // const opt_port = by.xpath('//li[contains(span,"' + optionAddorport + '")]');  //ul//*[contains(text(),'Port')]/parent::*
        const opt_port = by.xpath('//ul//*[contains(text(),"Port")]/parent::*');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(opt_port)), 3000);
        await element(opt_port).click();
        await console.log('Done');
    }


    // Verify continuity plan from present
    async verifyContinuityPlanFormDisplay() {
        await console.log('Start verify continuity plan from present');
        return await element(drp_ContinuityPlan).isPresent();
    }

    // Verify element disable
    async verifyElementDisable() {
        await console.log('Start verify element present');
        return await element(txt_Port1).isPresent();
    }

    // Verify undisplay Instatiate form after click button cancel
    async verifyInstaniateFormNotDisplayed() {
        await console.log('Start Verify undisplay Instatiate form after click button cancel');
        // await browser.sleep(5000);
        await expect(element(txtInstatiate).isPresent()).toBeFalsy();
        await console.log(' Done Verify undisplay Instatiate form after click button cancel');
    }

    // Verify element display in form
    async verifyElementDisplayInForm(strelement: string) {
        await console.log('Start verify');
        const elmDisplay = by.xpath('//li[contains(span,"' + strelement + '")]');
        return element(elmDisplay).isPresent();
    }

    // Verify VNFM Perferred Display
    async verifyVNFMPerferredDisplay() {
        await console.log('Start VNFM Perferred Display');
        return element(drpVNFMPreferred).isPresent();
    }

    // Verify element display in form
    async verifyElementdisplayInTD(strelement: string) {
        await console.log('Start verify');
        const elmDisplay = by.xpath('//*[text()="' + strelement + '"]');
        return element(elmDisplay).isPresent();
    }

    // Click Dropdown Cloud
    async clickAddSol() {
        console.log('start click add Sol');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_add_Sol)), 3000);
        await element(btn_add_Sol).click();
        console.log('Done click add Sol');
    }
    // verify add new SOL001 Custom Parameters successfully
    async verifyNewSolParameter() {
        console.log('start verify new Sol');
        await browser.wait(ExpectedConditions.visibilityOf(element(row_parameter_sol)), 3000);
        console.log('start count add Sol');
        const numberSol = await element.all(row_parameter_sol).count();
        return numberSol;
        // await element.all(row_parameter_sol).count().then(async function (count: any) {
        //     await console.log('Number Row Display On Table: ' + count);
        //     expect(Boolean(count >= numberRow)).toBe(true);
        // });
    }
    // click button remove new SOL001 Custom Parameters successfully
    async clickRemoveSolParameter() {
        console.log('start click Remove Sol');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_remove_sol)), 3000);
        await element(btn_remove_sol).click();
        console.log('Done click Remove Sol');
    }
    // verify romve  SOL001 Custom Parameters successfully
    async verifyRomoveSolParameter() {
        await browser.wait(ExpectedConditions.visibilityOf(element(row_parameter_sol_removed)), 3000);
        const numberRowParameter = await element.all(row_parameter_sol_removed).count();
        return numberRowParameter;
    }

    // verify element display when romove SOL001
    async verifyElementDisplayWhenRemoveSOL001() {
        await console.log('Start Verify element when romove SOL001');
        return element(row_parameter_sol_removed).isPresent();
    }
    // Function Edit anything into the new SOL001 custom parameters
    async EditNewParameter(keyvalue: string, keypairvalue: string) {
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Sol_key)), 3000);
        await element(txt_Sol_key).clear();
        await element(txt_Sol_key).sendKeys(keyvalue);
        await browser.waitForAngularEnabled(true);
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Sol_keypair)), 3000);
        await element(txt_Sol_keypair).clear();
        await element(txt_Sol_keypair).sendKeys(keypairvalue);
    }
    // Function Verify Edit anything into the new SOL001 custom parameters successfully
    async isEditInputSuccessfully() {
        const txt_key = await element(txt_Sol_key).getAttribute('value');
        const txt_keypair = await element(txt_Sol_keypair).getAttribute('value');
        if (txt_key !== null && txt_keypair !== null) {
            return true;
        } else {
            return false;
        }
    }
    // function add unlimitted new SOL001 Custom Parameters
    async addUnlimittedSolCustomParameters(number: number) {
        console.log('start add new SOL001');
        for (let i = 0; i < number; i = i + 1) {
            await this.clickAddSol();
        }
        console.log('Done add new SOL001');
    }
    // Click Select in Configuration Tenant Systems Page
    async clickSelect() {
        const select = by.css('tr:nth-child(1) td.ng-star-inserted p-dropdown');
        console.log('Start click Select');
        await browser.wait(ExpectedConditions.visibilityOf(element(select)), 2000);
        await element(select).click();
        console.log('Done');
    }
    // Click Delete in Configuration Tenant Systems Page
    async clickDelete() {
        const Delete = by.css('body > div > div > ul > p-dropdownitem:nth-child(1) > li');
        console.log('Start click Delete');
        await browser.wait(ExpectedConditions.visibilityOf(element(Delete)), 2000);
        await element(Delete).click();
        console.log('Done');
    }


    // Click click Confirm Delete  in Configuration Tenant Systems Page
    async clickConfirmDelete() {
        const confirmDelete = by.xpath('//button//span[text()="Yes"]');
        console.log('Start click Confirm Delete');
        await browser.wait(ExpectedConditions.visibilityOf(element(confirmDelete)), 2000);
        await element(confirmDelete).click();
        console.log('Done');
    }
    // Verify that the VNFM can display the list of flavor in tenant when instantiate VNF
    async  checkDisplayFlavor() {
        const flavorLabel = by.css('#InstPanel form p-dropdown ul > p-dropdownitem > li');
        await browser.wait(ExpectedConditions.visibilityOf(element(flavorLabel)), 3000);
        const numberFlavor = element.all(flavorLabel).count();
        return numberFlavor;
    }
    async clickFlavor() {
        await console.log('start select flavor');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpflavor)), 3000);
        await element(drpflavor).click();
        await console.log('done select flavor');
    }
    async moveMouseToFlavorLabel(flavorHover: string, numberOption: number) {
        await console.log('start move mouse hover label');
        const contentHover = by.css('#InstPanel form p-dropdown ul > p-dropdownitem:nth-child(' + numberOption + ') > li > option');
        await browser.actions().mouseMove(element(contentHover)).perform();
        await expect(await element(contentHover).getAttribute('title')).toEqual(flavorHover);
    }

    async changeInstantiateToEMS() {
        await pageOnboardedVNFs.settextSearch(data.INSTANTIATE.TC_1.TEXTSEARCH_EMS);
        // tslint:disable-next-line:max-line-length
        await pageOnboardedVNFs.clickActionOnRowHasValueofColumnNameEqualsTextValue(data.INSTANTIATE.TC_1.OSTRTABLE_EMS, data.INSTANTIATE.TC_1.OCOLUMNNAME_EMS, data.INSTANTIATE.TC_1.OTEXTVALUE_EMS, data.INSTANTIATE.TC_1.OFUNCTIONCOLUMNNAME_EMS, data.INSTANTIATE.TC_1.OFUNCTION_EMS);
        const txtInstantiate1 = await this.getTextInstantiate();
        await console.log('get text: ' + txtInstantiate1);
        await expect(Boolean(txtInstantiate1 === data.INSTANTIATE.TC_1.OVERIFYTEXT_EMS)).toBe(true);
    }

    // Function deploy EMS
    // tslint:disable-next-line:max-line-length
    async funcDeployEMS(strVNFName: string, strcloudName: string, strtenantName: string, strNetwork1: string, strNetwork2: string, strzone: string) {
        await console.log('Start deploy EMS');
        await this.setVNFName(strVNFName);
        await this.selectCloud(strcloudName);
        await this.selectTenant(strtenantName);
        await this.selectZone(strzone);
        await browser.waitForAngularEnabled(true);
        await this.selectNetworkEth0(strNetwork1);
        await this.selectNetworkEth1(strNetwork2);
    }

    // Function deploy SBC
    // tslint:disable-next-line:max-line-length
    async funcDeploySBC(strVNFName: string, strcloudName: string, strtenantName: string, strNetwork1: string, strNetwork2: string, strNetwork3: string, strzone: string) {
        await console.log('Start deploy SBC');
        await this.setVNFName(strVNFName);
        await this.selectCloud(strcloudName);
        await this.selectTenant(strtenantName);
        await this.selectZone(strzone);
        await this.clickShowFormNetwork();
        await browser.waitForAngularEnabled(true);
        await this.selectNetworkEth0(strNetwork1);
        await this.selectNetworkEth1(strNetwork2);
        await this.selectNetworkPkt1(strNetwork3);

    }
}

