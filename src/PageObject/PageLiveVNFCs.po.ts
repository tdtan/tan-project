import { element, by, ExpectedConditions, browser } from 'protractor';

const txbsearch = by.xpath('//app-vnfcs-list//p-table/div/div[1]//input');
// tslint:disable-next-line:max-line-length
const btnRefresh = by.xpath('//app-vnfcs-list//p-button[@icon="fa fa-refresh"]');
const btnClearnFilter = by.xpath('//app-vnfcs-list//div[2]//div[2]//div[1]//tr[2]/th[7]/p-button');
const btnClearSearch = by.xpath('//app-vnfcs-list//p-table/div/div[1]//p-button[@icon="fa fa-times"]');
const btnShowHide = by.css('#tb-vnfc p-button[icon="fa fa-table"]');
const btnShowHideFilter = by.xpath('//app-vnfcs-list//p-button[@icon="fa fa-sliders"]');
const rowTable = by.xpath('//app-vnfcs-list//div/div[2]/div/div[2]/table/tbody/tr');
const txtRecord = by.xpath('//app-vnfcs-list//p-table//p-paginator/div/div//span');
const txtNoRecord = by.xpath('//*[@id="tb-vnfc"]//span[text()=" No records found "]');
// tslint:disable-next-line:max-line-length
const txtpageheader = by.xpath('/html/body/app-root/app-root-view/div/div[2]/vertical-split-pane/div/div[2]/div/div/app-vnfs-list/app-vnfcs-list/p-panel/div/div[1]/span');
const btnDetailVNFC = by.xpath('//app-vnfcs-list//div[2]/div/div[2]//tr[1]/td[1]//i');
const detailVNFC = by.xpath('//*[@id="table-child"]/div/div/table/tbody/tr[1]/td[1]');

const numRowDefault = by.xpath('//app-vnfcs-list//div/p-paginator/div/p-dropdown/div/label');
const btnNumRow = by.xpath('//app-vnfcs-list//div/p-paginator/div/p-dropdown');



export class PageLiveVNFCs {

    // get text Onboard in page header
    async getTextHeaderPage() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtpageheader)), 3000);
        const textOnb = await element(txtpageheader).getText();
        return textOnb;
    }
    // Set text Search in Available Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbsearch)), 6000);
        await element(txbsearch).clear();
        await element(txbsearch).sendKeys(user);
        console.log('Done Input search.');
    }

    // Click button detail VNFC
    async clickDetailVNFC() {
        await console.log('Start click detail VNFC.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnDetailVNFC)), 3000);
        await element(btnDetailVNFC).click();
        await console.log('Done click detail VNFC');
    }
    // Click button Refesh
    async clickRefreshButton() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefresh)), 3000);
        await element(btnRefresh).click();
        await console.log('Done click Refesh');
    }

    // Click button Clear Search
    async clickClearSearch() {
        await console.log('Start click Clear Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done click Clear Search');
    }

    // Click button Clear input Search
    async clickClearFilterButton() {
        await console.log('Start click Cancel vnfTable');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearnFilter)), 3000);
        await element(btnClearnFilter).click();
        await console.log('Done click Cancel vnfTable');
    }

    // Click button Show Hide Filter
    async clickShowHideFilter() {
        await console.log('Start click Show Hide Filter');
        await element(btnShowHideFilter).click();
        await console.log('Done click Show Hide Filter');
    }

    // Click button Show Hide Column
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await element(btnShowHide).click();
        await console.log('Done click Show Hide Column');
    }

    // click Select Actions
    async clickSelectActions(numTr: number) {
        await console.log('Start click Actions');
        const btnSelectActions = by.xpath('//app-vnfcs-list//div[2]//tr[' + numTr + ']/td[6]/p-dropdown');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectActions)), 3000);
        await element(btnSelectActions).click();
        await console.log('Done');
    }

    // click Option Select Actions
    async clickOptionSelectActions(optAction: string) {
        const btnoptAction = by.xpath('//li[contains(option,"' + optAction + ' ")]');
        await console.log('Start click Option Actions');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnoptAction)), 3000);
        await element(btnoptAction).click();
        await console.log('Done');
    }

    // Count row display one page
    async countNumberRowDisplayOnTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }

    // Get text record in table Live VNF
    async getRecord() {
        // await browser.sleep(3000);
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 4000);
        const _record = await element(txtRecord).getText();
        await console.log('Text record: ' + _record);
        await console.log('Done get record');
        return _record;
    }
    // Get text in Td display
    async GetTextInTd(numTr: number, numTd: number) {
        const txttd = by.xpath('//app-vnfcs-list//table/tbody/tr[' + numTr + ']/td[' + numTd + ']');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txttd)), 10000);
        // await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 60000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;
    }

    // Get Attribute in Search
    async getAttributeInSearch() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txbsearch)), 3000);
        const value = await element(txbsearch).getAttribute('value');
        return value;
    }

    // Get text filter
    async gettextfilter(columnnumber: number) {
        const filtercolumn = by.xpath('//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown//label');
        await browser.wait(ExpectedConditions.visibilityOf(element(filtercolumn)), 3000);
        const value = await element(filtercolumn).getText();
        return value;
    }

    // Get text detail VNFC
    async gettextdetailVNFC() {
        await browser.wait(ExpectedConditions.visibilityOf(element(detailVNFC)), 3000);
        const value = await element(detailVNFC).getText();
        return value;
    }

    // Get Number Row Display Inpage
    async getNumberRowDisplayInpage() {
        await browser.wait(ExpectedConditions.visibilityOf(element(numRowDefault)), 3000);
        const value = await element(numRowDefault).getText();
        return value;
    }

    // Select number row display in page of table VNFC
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnNumRow)), 3000);
        await element(btnNumRow).click();
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }
    // Set text Search in Configuration Tenant Page
    async settextSearchInFilter(search: string) {
        const txbSearchID = by.xpath('/html/body/div/div[1]/div[2]/input');
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbSearchID)), 3000);
        await element(txbSearchID).sendKeys(search);
        await console.log('Done');
    }
    // Click select All in Filter
    async clickCheckBoxSelectAll() {
        const cbxSelectAll = by.xpath('/html/body/div/div[1]/div[1]/div[2]');
        await console.log('Start Click select All in Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxSelectAll)), 3000);
        await element(cbxSelectAll).click();
        await console.log('Done');
    }

    // Click select All in Filter
    async clickLableFilter(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const filtercolumn = by.xpath('//app-vnfcs-list//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        await console.log('Start Click lable Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await element(filtercolumn).click();
        await console.log('Done');
    }

    // Select filter from talbe in Onboarded page
    async SelectOptionFilter(txtChoose: string) {

        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
    }

    // Click select All in Filter
    async clickCloseFilter() {
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');
        await console.log('Start Click Close Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 5000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Live VNF page
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//app-vnfcs-list//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter: ' + filtercolumn);
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter: ' + filChoose);
        await element(filChoose).click();
        await console.log('Done');
    }

    // Verify element display in form
    async verifyFormNotDisplayed() {
        await console.log('Start verify event form is displayed');
        const elmDisplay = by.xpath('//*[@id="vnfc-function"]');
        await browser.sleep(6000);
        await expect(element(elmDisplay).isPresent()).toBeFalsy();
    }

    // Select filter in table Live VNF
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="tb-vnfc"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();
        await browser.waitForAngularEnabled(true);

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 5000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 5000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await browser.sleep(2000);
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Done');
    }


    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//app-vnfcs-list//th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//app-vnfcs-list//th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//app-vnfcs-list//th[' + columnnumber + ']//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click().then(function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click().then(function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');

    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        // await browser.sleep(3000);
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Get number of columns in current search result table
    async getNumberOfColumnsInSearchTable(strTable: string) {
        let _numberOfColumn: number;
        _numberOfColumn = 0;
        const _numOfMaximumColumns = 11;
        await console.log('Start checking if we have any table name equals Text value?');
        for (let i = 1; i <= _numOfMaximumColumns; i++) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            if (await element(_strColumnLocator).isPresent()) {
                _numberOfColumn = i;
            }
        }
        return _numberOfColumn;

    }

    // Get column number based on column name in VFM Settings Page
    async getColumnNumberBasedOnColumnName(strTable: string, columnname: string) {
        let _numCurrentColumns: any;
        let _columnnum: number;
        _columnnum = 0;
        _numCurrentColumns = await this.getNumberOfColumnsInSearchTable(strTable);
        await console.log('Start getting column number based on column Name');
        for (let i = 1; i <= _numCurrentColumns; i++) {
            const _strColumnLocator = by.xpath('//*[@id="' + strTable + '"]//table/thead/tr[1]/th[' + i + ']');
            await element(_strColumnLocator).getText().then(function (text) {
                if (text === columnname) {
                    _columnnum = i;
                }
            });
        }
        if (_columnnum === 0) {
            await console.log('No found ' + columnname + ' in search table');
        } else {
            await console.log('Column ' + _columnnum + ' has ' + columnname + ' as name');
        }
        return _columnnum;
    }

    // Get row number has Column Name Equals Text Value in Onboarded VNFs Page
    async getNumberofRowHasValueofColumnNameEqualsTextValue(strTable: string, intColumnNumber: number, strTextValue: string) {
        let _rownum: number;
        _rownum = 0;
        const record = by.xpath('//*[@id="' + strTable + '"]//p-paginator//span');
        const numrecord = await element(record).getText();
        const _intSearchSheetRowNum = Number(numrecord.slice(0, 1));
        await console.log('Current Search table has ' + _intSearchSheetRowNum + ' row(s)');
        for (let i = 1; i <= _intSearchSheetRowNum; i++) {
            const TextValueLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + i + ']/td[' + intColumnNumber + ']');
            await element(TextValueLocator).getText().then(function (text) {
                if (text === strTextValue) {
                    _rownum = i;
                }
            });

        }
        if (_rownum === 0) {
            await console.log('No found Row has Column Name Equals ' + strTextValue + ' in search table');
        } else {
            await console.log('Row ' + _rownum + ' has value of Column Name equals ' + strTextValue);
        }
        return _rownum;
    }

    // Click Function On Row Has Value of ColumnName Equals TextValue
    // tslint:disable-next-line:max-line-length
    async clickActionOnRowHasValueofColumnNameEqualsTextValue(strTable: string, columnname: string, strTextValue: string, strFunctionColumnName: string, functionchoose: string) {
        let _intColumnNameNumber: number;
        let _intColumnActionNumber: number;
        let _intActionRowNumber: number;

        _intColumnNameNumber = await this.getColumnNumberBasedOnColumnName(strTable, columnname);
        _intActionRowNumber = await this.getNumberofRowHasValueofColumnNameEqualsTextValue(strTable, _intColumnNameNumber, strTextValue);
        _intColumnActionNumber = await this.getColumnNumberBasedOnColumnName(strTable, strFunctionColumnName);
        await console.log('Start taking function');
        await browser.waitForAngularEnabled(true);
        // tslint:disable-next-line:max-line-length
        const btn_FunctionLocator = by.xpath('//*[@id="' + strTable + '"]//table/tbody/tr[' + _intActionRowNumber + ']/td[' + _intColumnActionNumber + ']/p-dropdown//label');
        await element(btn_FunctionLocator).click();
        const _function = by.xpath('//li[contains(option,"' + functionchoose + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(_function)), 4000);
        await element(_function).click();
        await console.log('Done');
    }

    async waitVNFCHealAvailable(row: number, column: number, statusName: string, timeWait: number, messageTimeOut: string) {
        let result = true;
        await console.log('start await status: ', statusName);
        await browser.waitForAngularEnabled(true);
        await browser.wait(async () => {
            console.log('working');
            const statusText = await this.GetTextInTd(row, column);
            await console.log('status table: ', statusText);
            if (statusText === 'Failed') {
                result = false;
                await console.log('Current VNF status is ' + statusText);
                return true;
            } else {
                if (statusText === statusName) { // If status = statusName then give the pass test case
                    return true;
                } else {
                    await browser.sleep(20000); // distance of check time is 1s.
                    return false; // If status !== statusName then continue checking
                }
            }
        }, timeWait, messageTimeOut);
        return result;
    }

    async waitVNFCUpgraded(row: number, column: number, statusName: string, timeWait: number, messageTimeOut: string) {
        let result = true;
        await console.log('start await status: ', statusName);
        await browser.waitForAngularEnabled(true);
        await browser.wait(async () => {
            console.log('working');
            const statusText = await this.GetTextInTd(row, column);
            await console.log('status table: ', statusText);
            if (statusText === 'Failed') {
                result = false;
                await console.log('Current VNF status is ' + statusText);
                return true;
            } else {
                if (statusText === statusName) { // If status = statusName then give the pass test case
                    return true;
                } else {
                    await browser.sleep(20000); // distance of check time is 1s.
                    return false; // If status !== statusName then continue checking
                }
            }
        }, timeWait, messageTimeOut);
        return result;
    }

    async waitVNFCRollBack(row: number, column: number, statusName: string, timeWait: number, messageTimeOut: string) {
        let result = true;
        await console.log('start await status: ', statusName);
        await browser.waitForAngularEnabled(true);
        await browser.wait(async () => {
            console.log('working');
            const statusText = await this.GetTextInTd(row, column);
            await console.log('status table: ', statusText);
            if (statusText === 'Failed') {
                result = false;
                await console.log('Current VNF status is ' + statusText);
                return true;
            } else {
                if (statusText === statusName) { // If status = statusName then give the pass test case
                    return true;
                } else {
                    await browser.sleep(20000); // distance of check time is 1s.
                    return false; // If status !== statusName then continue checking
                }
            }
        }, timeWait, messageTimeOut);
        return result;
    }

    async getTextCellTable(row: number, column: number) {
        // tslint:disable-next-line:max-line-length
        const Row = by.css('app-vnfcs-list p-table tr:nth-child(' + row + ') td:nth-child(' + column + ')');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(Row)), 60000);
        const statusText = await element(Row).getText();
        return statusText;
    }

}
