import { element, by, ExpectedConditions, browser, protractor } from 'protractor';
import { PageError } from './PageError.po';

const txtCloud = by.xpath('//*[@id="container-common-table"]//a/p-header/span');
const txbSearch = by.css('#container-common-table div[class="container-search-input"] input');
const btnAddCloud = by.css('#container-common-table p-button.button-add-action button');
const btnRefesh = by.css('#container-common-table p-button[title="Refresh Table"] button');
const btnClearText = by.xpath('//div[@class="container-search-input"]//p-button[@icon="fa fa-times"]');
const btnShowhideFilter = by.css('#container-common-table p-button[title="Show/Hide Filter Row"] button');
const drpFilter = by.css('tr th p-multiselect');
const btnClearTextFilter = by.xpath('//*[@class=" column-clear"]/p-button[@icon="fa fa-times"]/button');
const btnShowColumn = by.css('#container-common-table p-button[title="Show/Hide Columns"] button');
const lblNumberPage = by.css('#container-common-table p-table p-paginator p-dropdown');
const txtNumberRowDefault = by.css('#container-common-table p-table p-paginator p-dropdown label');
const txtRecord = by.xpath('//*[@id="container-common-table"]//p-paginator/div/div/div/span');
const rowTable = by.css('#container-common-table table tbody tr');
const txtNoRecord = by.xpath('//*[text()=" No records found "]');

const btnSelectAction = by.xpath('//*[@id="container-common-table"]//table/tbody/tr/td[9]/p-dropdown');
const btnOptDeleteInAction = by.xpath('//*[contains(option,"Delete")]');
const btnConfirmDelete = by.xpath('//*[@id="delete-cloud"]//button[2]');
const btnCancelDelete = by.xpath('//*[@id="delete-cloud"]//button[1]');

export class PageVNFMConfCloudSystems {

    private pageError = new PageError();
    // Verify show text Cloud
    async verifyShowTextCloud(_textCloud: any) {
        console.log('Start get text.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtCloud)), 5000);
        const cloud = await element(txtCloud).getText();
        expect(cloud).toEqual(_textCloud);
        await console.log('text: ' + cloud);
        await console.log('Done.');
    }


    // Set text Search in Configuration Cloud Systems Page
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).clear();
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // Set text Search in Configuration Cloud Systems Page
    async clearTextSearch() {
        await console.log('Start clear text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(txbSearch).sendKeys(protractor.Key.BACK_SPACE);
        await console.log('Done');
    }

    // Click button Add Cloud in Configuration Cloud Systems Page
    async clickAddCloudbtn() {
        console.log('Start click Add Cloud');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnAddCloud)), 2000);
        await element(btnAddCloud).click();
        console.log('Done');
    }

    // Click Clear Button To Clear Text Filter
    async clickClearButtonToClearTextFilter() {
        console.log('Start click Clear Button To Clear Text Filter');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnClearTextFilter)), 2000);
        await element(btnClearTextFilter).click();
        console.log('Done');
    }

    // Click button Edit Cloud in Configuration Cloud Systems Page
    async clickEditCloud(level: number) {
        const editCloud = by.css('#container-common-table table tbody tr:nth-child(' + level + ')');
        console.log('Start click Edit Cloud');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(editCloud)), 3000);
        // await browser.wait(ExpectedConditions.visibilityOf(element(editCloud)), 3000);
        await element(editCloud).click();
        console.log('Done');
    }

    // Click button show hide filter
    async clickShowHideFilter() {
        await console.log('Start click show hide filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowhideFilter)), 3000);
        await element(btnShowhideFilter).click();
        await console.log('Done click show hide filter');
    }

    // Click button Refesh in Configuration Cloud Systems Page
    async clickRefesh() {
        await console.log('Start click Refesh');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefesh)), 3000);
        await element(btnRefesh).click();
        await console.log('Done click refesh');
    }

    // Click button clear in Configuration Cloud Systems Page
    async clickButtonClear() {
        await console.log('Start click button clear');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearText)), 3000);
        await element(btnClearText).click();
        await console.log('Done click button clear');
    }

    // Click button Show/Hide Column in Configuration Cloud Systems Page
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done click Show Hide Column');
    }

    // Get text in Td display
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="container-common-table"]//table/tbody/tr/td[' + numTd + ']/div/div/span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }


    // get Attribute in Search
     async getAttributeSearch() {
        await console.log('Start get Attribute in Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        const value = await element(txbSearch).getAttribute('value');
        await console.log('Done');
        return value;
    }

    // get Attribute of element in form
    async getAttributeOfElementInForm(txtConfig: string) {
        const emptyDisplayed = by.css('#panel-actions input[formcontrolname="' + txtConfig + '"].ng-invalid');
        await console.log('Start get Attribute of element in form');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(emptyDisplayed)), 3000);
        const value = await element(emptyDisplayed).getAttribute('value');
        await console.log('Done get Attribute of element in form');
        return value;
    }

    // get text filter in form
    async getTextFilter(columnnumber: number) {
        const filtercolumn = by.css('table tr th:nth-child(' + columnnumber + ') p-multiselect div.ui-multiselect-label-container span');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Start get Text Filter');
        const textFilter = await element(filtercolumn).getText();
        return textFilter;
    }


    // Select Option from Show/ Hide columns List in Configuration Cloud Systems Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[@binary="true"]//label[text()="' + strOptionName + '"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btnClose = by.xpath('//span[text()="Close"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnClose)), 3000);
        await element(btnClose).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Select Option from Show/ Hide columns List in Configuration Cloud Systems Page
    async selectMultiOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        const btnSelectCol = by.xpath('//p-checkbox[@binary="true"]//label[text()="' + strOptionName + '"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        await console.log('Done selecting Option ' + strOptionName + ' from Show Hide Columns List');
    }

    // Click close Show/ Hide columns List in Configuration Cloud Systems Page
    async clickCloseShowHide() {
        const btnClose = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnClose)), 3000);
        await element(btnClose).click();
    }



    // Verify sort column
    async verifysort(columnnumber: number) {
        const sort = by.css('tr.ng-star-inserted th:nth-child(' + columnnumber + ') p-sorticon');
        const sortup = by.xpath('//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        const sortdown = by.xpath('//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        browser.actions().mouseMove(element(sort)).perform();
        await browser.wait(ExpectedConditions.visibilityOf(element(sort)), 3000);
        await element(sort).click().then(function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await browser.wait(ExpectedConditions.visibilityOf(element(sortup)), 3000);
        await element(sortup).click().then(function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');

    }


    // Select filter from talbe in Configuration Cloud Systems page
    async SelectFilter(columnnumber: number, txtChoose: string) {
        await console.log('Start select filter');
        const filtercolumn = by.xpath('//*[@id="tb-cloud"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        await browser.wait(ExpectedConditions.visibilityOf(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();
        await browser.wait(ExpectedConditions.visibilityOf(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();
        await browser.waitForAngularEnabled(true);

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 5000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 5000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Verify text display
    async verifyText(filtercolumn: number, txtVerify: string) {
        const column = by.css('table tbody tr:nth-child(1) td:nth-child(' + filtercolumn + ') span');
        await console.log('Start verify Text');
        await browser.wait(ExpectedConditions.visibilityOf(element(column)), 3000);
        expect(await element(column).getText()).toContain(txtVerify);
        await console.log('Done');
    }


    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toContain(txtVerify);
        await console.log('Done');
    }

    // Check if table has Search value?
    async isTableNoValueAfterSearch() {
        await console.log('Start checking if table has not value after searching?');
        expect(await element(txtNoRecord).isDisplayed()).toBeTruthy();
        await console.log('Done');
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//th[text()=" ' + strOptionName + ' "]');
        return element(tableTH).isDisplayed();
    }

    // Verify Filter Row Display
    async verifyFilterRowDisplay() {
        await console.log('Start verify Filter Row Display');
        return element(drpFilter).isPresent();
    }

    // Select number page on table in Configuration Cloud Systems Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.visibilityOf(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        const rowid = by.xpath('//span[text()="' + num_row + '"]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await this.selectNumberRowOnTable(numberRow);
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
        });
    }


    // Get Record Number
    async getRecordNumber() {
        await console.log('Start get Record Number');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        const getnum = await element(txtRecord).getText();
        await console.log('Done get record: ' + getnum);
        return getnum;

    }

    // Verify user can move to specified page
    async verifyMovePage() {
        const sel = by.css('#panel-cloud span.pi-caret-right');
        await console.log('Start Verify Move Page');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        const getnum = await element(txtRecord).getText();
        const number = Number(getnum.slice(0, 1));
        if (number > 5) {
            await browser.wait(ExpectedConditions.elementToBeClickable(element(sel)), 3000);
            await element(sel).click();
        }
        await console.log('Done');
    }

    // Count row Number
    async countRowNumber() {
        const RowNumber = by.css('#panel-cloud tbody.ui-table-tbody tr');
        await console.log('Start get Record Number');
        await browser.wait(ExpectedConditions.visibilityOf(element(RowNumber)), 3000);
        const number = await element.all(RowNumber).count();
        await console.log('Done');
        return number;

    }

    // Get Number Row display
    async getNumberRow() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRowDefault)), 4000);
        const getNumRow = await element(txtNumberRowDefault).getText();
        await console.log(' Current Row Number Display: ' + getNumRow);
        return getNumRow;

    }

    // Click Select in Configuration Cloud Systems Page
    async clickSelect() {
        await console.log('Start click Select');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectAction)), 5000);
        await element(btnSelectAction).click();
        await console.log('Done');
    }

    // Click Delete in Configuration Cloud Systems Page
    async clickDelete() {
        await console.log('Start click Delete');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnOptDeleteInAction)), 5000);
        await element(btnOptDeleteInAction).click();
        await console.log('Done');
    }

    // Click click Confirm Delete  in Configuration Cloud Systems Page
    async clickConfirmDelete() {
        await console.log('Start click Delete');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnConfirmDelete)), 2000);
        await element(btnConfirmDelete).click();
        await console.log('Done');
    }

    // Click click Confirm Cancel Delete  in Configuration Cloud Systems Page
    async clickConfirmCancelDelete() {
        await console.log('Start click Cancel Delete');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnCancelDelete)), 2000);
        await element(btnCancelDelete).click();
        await console.log('Done');
    }

    async countNumberCloud() {
        await console.log('begin count Number Cloud');
        const el = by.css('p-table table > tbody > tr');
        const rowNumber = await element.all(el).count();
        await console.log('number cloud: ', rowNumber);
        return rowNumber;
    }

    async showDropActionDropDown(row: number) {
        await console.log('begin showDropActionDropDown');
        const el = by.css('tr:nth-child(' + row + ') td.ng-star-inserted p-dropdown');
        const isShow = await this.isDropListActionsDisplay();
        if (!isShow) {
            await element(el).click();
            await console.log('displayed drop list action');
        }
    }

    async isDropListActionsDisplay() {
        await console.log('begin isDropListActionsDisplay');
        const el = by.css('body > div > div.ui-dropdown-items-wrapper');
        const check = await element(el).isPresent();
        return check;
    }

    async removeAllCloud() {
        await console.log('begin remove all Cloud');
        while (await this.countNumberCloud() > 0) {
            await this.showDropActionDropDown(1);
            await this.clickDelete();
            await this.clickConfirmDelete();
            await console.log('deleted row 1');
            this.clearMessage();
        }
        this.clearMessage();
        await console.log('deleted all cloud');
    }

    async clearMessage() {
        await console.log('begin clearMessage');
        const result1 = await this.pageError.verifyMessegeError();
        const result2 = await this.pageError.verifyMessegeWarning();
        const result3 = await this.pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await this.pageError.cliclClearALL();
        }
        await console.log('done');
    }



}

