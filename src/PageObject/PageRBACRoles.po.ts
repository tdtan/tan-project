import { element, by } from 'protractor';
import { PageError } from './PageError.po';

const roleName = by.css('input[formcontrolname="Rolename"]');
const elMessageRoleName = 'input[formcontrolname="Rolename"] + small';
const btnSave = by.css('button[icon="pi pi-pw pi-check"]');
const btnCancel = by.css('button[icon="fa fa-times"]');
const btnAddRole = by.css('#container-common-table p-button.button-add-action button');
const btnBackRolePage = by.xpath('//*[@id="role-action-block"]/p-breadcrumb/div/ul/li[1]/a');
// tslint:disable-next-line:max-line-length
const elTotalSwitchAdministrationPermissions = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(3) > div p-inputswitch');
// tslint:disable-next-line:max-line-length
const elTotalSwitchAdministrationPermissionsEnabled = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(3) > div p-inputswitch > div[aria-checked="true"]');

// tslint:disable-next-line:max-line-length
const elTotalSwitchVNFManagementPermissions = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div p-inputswitch');
// tslint:disable-next-line:max-line-length
const elTotalSwitchVNFManagementPermissionsEnabled = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div p-inputswitch > div[aria-checked="true"]');

function switchAdministrationPermissionsInRow(menuname: string) {
    // tslint:disable-next-line:max-line-length
    // return by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(3) > div.ui-grid.ng-star-inserted > div > div:nth-child(' + row + ') p-inputswitch');
    return by.xpath('//div[text()=" ' + menuname + ' "]/following-sibling::div/p-inputswitch');
}
function switchAdministrationPermissionsEnabled(menuname: string) {
    // tslint:disable-next-line:max-line-length
    // return by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(3) > div.ui-grid.ng-star-inserted > div > div:nth-child(' + row + ') p-inputswitch > div[aria-checked="true"]');
    return by.xpath('//div[text()=" ' + menuname + ' "]/following-sibling::div/p-inputswitch/div[@aria-checked="true"]');
}
export class PageRBACRoles {
    private pageError = new PageError();

    // Test has inputSwitch enabled
    async isHaveInputSwitchOn() {
        await console.log('begin check have inputSwitch On');
        const el = by.css('p-inputswitch > div[aria-checked="true"]');
        const check = await element(el).isPresent();
        await console.log('done');
        return check;
    }

    async isHaveInputSwitchOff() {
        await console.log('begin check have inputSwitch Off');
        const el = by.css('p-inputswitch > div[aria-checked="false"]');
        const check = await element(el).isPresent();
        await console.log('done');
        return check;
    }

    async getValueInputRoleName() {
        await console.log('begin getValueInputRoleName');
        const text = await element(roleName).getAttribute('value');
        await console.log('text role name: ', text);
        return text;
    }

    async clearValueInputRoleName() {
        await console.log('begin clearValueInputRoleName');
        await element(roleName).clear();
        await console.log('done clear text role name');
    }

    async setValueInputRoleName(rolename: string) {
        await console.log('begin setValueInputRoleName');
        await element(roleName).clear();
        await element(roleName).sendKeys(rolename);
        await console.log('done clear text role name');
    }

    async isRoleNameShowMessage(message: string) {
        await console.log('begin isRoleNameShowMessage');
        const el = by.cssContainingText(elMessageRoleName, message);
        const check = await element(el).isPresent();
        await console.log('done');
        return check;
    }

    async clickSaveButton() {
        await console.log('begin clickSaveButton');
        await element(btnSave).click();
        await console.log('done click button save');
    }

    async clickBackButtonToRolePage() {
        await console.log('begin click Back To Role Page');
        await element(btnBackRolePage).click();
        await console.log('done click Back To Role Page');
    }

    async clickCancelButton() {
        await console.log('begin clickCancelButton');
        await element(btnCancel).click();
        await console.log('done click button cancel');
    }

    async clickSwitch(level: number) {
        await console.log('begin clickSwitch');
        const el = by.css('p-inputswitch');
        const items = await element.all(el);
        await items[level].click();
        await console.log('done click switch: ', level);
    }

    async clickBtnAddRole() {
        await console.log('begin clickBtnAddRole');
        await element(btnAddRole).click();
        await console.log('done click button add role');
    }

    async getNumberSwitchAdministrationPermissions() {
        await console.log('begin getNumberSwitchAdministrationPermissions');
        const numberSwitch = await element.all(elTotalSwitchAdministrationPermissions).count();
        await console.log('total switch AdministrationPermissions: ', numberSwitch);
        return numberSwitch;
    }
    async getNumberSwitchAdministrationPermissionsIsOn() {
        await console.log('begin getNumberSwitchAdministrationPermissionsIsOn');
        const numberSwitch = await element.all(elTotalSwitchAdministrationPermissionsEnabled).count();
        await console.log('number switch on of AdministrationPermissions: ', numberSwitch);
        return numberSwitch;
    }
    async getNumberSwitchVNFManagementPermissions() {
        await console.log('begin getNumberSwitchVNFManagementPermissions');
        const numberSwitch = await element.all(elTotalSwitchVNFManagementPermissions).count();
        await console.log('total switch VNFManagementPermissions: ', numberSwitch);
        return numberSwitch;
    }
    async getNumberSwitchVNFManagementPermissionsIsOn() {
        await console.log('begin getNumberSwitchVNFManagementPermissionsIsOn');
        const numberSwitch = await element.all(elTotalSwitchVNFManagementPermissionsEnabled).count();
        await console.log('number switch on VNFManagementPermissions: ', numberSwitch);
        return numberSwitch;
    }

    async verifyRowAdministrationPermissionsEnabled(menuname: string) {
        await console.log('begin verifyRowAdministrationPermissionsEnabled');
        // row = 2: Cloud
        // row = 3: Tenant
        // row = 4: Users
        // row = 5: Settings

        // total switch AdministrationPermissions
        const el = switchAdministrationPermissionsInRow(menuname);

        // switch AdministrationPermissions is On
        // tslint:disable-next-line:max-line-length
        const el1 = switchAdministrationPermissionsEnabled(menuname);

        const items = await element.all(el);
        // on switch all
        await items[0].click();
        const numberSwitchOn1 = element.all(el1).count();
        await expect(numberSwitchOn1).toEqual(items.length);
        await console.log('el : ', items.length);
        await console.log('el1: ', numberSwitchOn1);
        await console.log('done');
    }

    async verifyRowAdministrationPermissionsDisabled(menuname: string) {
        await console.log('begin verifyRowAdministrationPermissionsDisabled');
        // row = 2: Cloud
        // row = 3: Tenant
        // row = 4: Users
        // row = 5: Settings

        // total switch AdministrationPermissions
        // tslint:disable-next-line:max-line-length
        const el = switchAdministrationPermissionsInRow(menuname);

        // switch AdministrationPermissions is On
        // tslint:disable-next-line:max-line-length
        const el1 = switchAdministrationPermissionsEnabled(menuname);
        const items = await element.all(el);

        // off switch all
        await items[0].click();
        const numberSwitchOn2 = element.all(el1).count();
        await expect(numberSwitchOn2).toEqual(0);
        await console.log('done');
    }

    async verifyParentToggleSwitchInRowOfAdministrationPermissions(menuname: string) {
        await console.log('begin verifyParentToggleSwitchInRowOfAdministrationPermissions');
        const items = await element.all(switchAdministrationPermissionsInRow(menuname));
        // for (let index = 1; index < items.length; index++) {
        //     await items[index].click();
        // }
        await items[0].click();
        await expect(await element.all(switchAdministrationPermissionsEnabled(menuname)).count()).toEqual(items.length);
        await console.log('done');
    }

    // async verifyParentToggleSwitchAdministrationPermissionsToEnable() {
    //     await console.log('begin verifyParentToggleSwitchAdministrationPermissionsToEnable');
    //     let numberSwitchAdministrationPermissionsIsOn = await this.getNumberSwitchAdministrationPermissionsIsOn();
    //     const total = await this.getNumberSwitchAdministrationPermissions();
    //     await expect(numberSwitchAdministrationPermissionsIsOn).toEqual(0);
    //     // Cloud
    //     await this.verifyParentToggleSwitchInRowOfAdministrationPermissions(2);
    //     // Tenant
    //     await this.verifyParentToggleSwitchInRowOfAdministrationPermissions(3);
    //     // Users
    //     await this.verifyParentToggleSwitchInRowOfAdministrationPermissions(4);
    //     // Settings
    //     await this.verifyParentToggleSwitchInRowOfAdministrationPermissions(5);
    //     numberSwitchAdministrationPermissionsIsOn = await this.getNumberSwitchAdministrationPermissionsIsOn();
    //     await expect(numberSwitchAdministrationPermissionsIsOn).toEqual(total - 1);
    //     await console.log('done');
    // }

    // async verifyParentToggleSwitchToDisable() {
    //     await console.log('begin verifyParentToggleSwitchToDisable');
    //     const numberSwitchAdministrationPermissionsIsOn1 = await this.getNumberSwitchAdministrationPermissionsIsOn();
    //     const items = await element.all(switchAdministrationPermissionsInRow(2));
    //     items[items.length - 1].click();
    //     const numberSwitchAdministrationPermissionsIsOn2 = await this.getNumberSwitchAdministrationPermissionsIsOn();
    //     await expect(numberSwitchAdministrationPermissionsIsOn2).toEqual(numberSwitchAdministrationPermissionsIsOn1 + 2);
    //     await console.log('done');
    // }

    async getListVNFOnboard() {
        await console.log('begin getListVNFOnboard');
        const rs: any = [];
        const elColumnVNFName = by.css('p-table table > tbody > tr > td:nth-child(1)');
        const list1 = await element.all(elColumnVNFName).getText();
        for (let index = 0; index < list1.length; index++) {
            if (!rs.includes(list1[index])) {
                rs.push(list1[index]);
            }
        }
        await console.log('list VNF onboard: ', rs);
        return rs;
    }
    async getCssValueInToggle(menuName: String) {
        await console.log('begin getCssValueInToggle');
        // tslint:disable-next-line:max-line-length
        const el = by.xpath('//div[text()=" ' + menuName + ' "]/following-sibling::div/p-inputswitch/div');
        const list = await element(el).getCssValue('aria-checked');
        await console.log('Css value in element: ', list);
        return list;
    }


    async getListVNFType() {
        await console.log('begin getListVNFType');
        // tslint:disable-next-line:max-line-length
        const el = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div:nth-child(4) > div:nth-child(2) div.padding-left-60');
        const list = await element.all(el).getText();
        // const list1 = list.replace('VNF Type: ', '');
        await console.log('list vnf type: ', list);
        return list;
    }

    async isPageAddRoleDisplay() {
        await console.log('begin isPageAddRoleDisplay');
        const el = by.css('p-panel[ng-reflect-header="Add Role"]');
        const check = await element(el).isPresent();
        await console.log('done');
        return check;
    }

    async isPageEditRoleDisplay() {
        await console.log('begin isPageEditRoleDisplay');
        const el = by.css('p-panel[ng-reflect-header="Edit Role"]');
        const check = await element(el).isPresent();
        await console.log('done');
        return check;
    }

    async verifyAppearInColumnRole(rolename: string) {
        await console.log('begin verifyAppearInColumnRole');
        let flag = false;
        const el = by.css('table > tbody > tr > td:nth-child(1)');
        const list = await element.all(el).getText();
        for (let i = 0; i < list.length; i++) {
            if (list[i] === rolename) {
                flag = true;
                break;
            }
        }
        if (flag) {
            console.log('have appear', rolename);
        } else {
            console.log('not have appear', rolename);
        }
        return flag;
    }

    async countNumberVnfOnboard() {
        await console.log('begin countNumberVnfOnboard');
        const el = by.css('p-table table > tbody > tr');
        const rowNumber = await element.all(el).count();
        await console.log('number vnf onboard: ', rowNumber);
        return rowNumber;
    }

    async showDropActionDropDown(row: number) {
        await console.log('begin showDropActionDropDown');
        const el = by.css('p-table table > tbody > tr:nth-child(' + row + ') > td:nth-child(7) > p-dropdown');
        const isShow = await this.isDropListActionsDisplay();
        if (!isShow) {
            await element(el).click();
            await console.log('displayed drop list action');
        }
    }

    async isDropListActionsDisplay() {
        await console.log('begin isDropListActionsDisplay');
        const el = by.css('body > div > div.ui-dropdown-items-wrapper');
        const check = await element(el).isPresent();
        return check;
    }

    async selectItemDropActions(level: number) {
        await console.log('begin selectItemDropActions');
        const el = by.css('body ul p-dropdownitem:nth-child(' + level + ') li');
        await element(el).click();
        await console.log('done select item: ', level);
    }

    async clickYesConfirmDialog() {
        await console.log('begin clickYesConfirmDialog');
        const el = by.css('p-dialog button[icon="pi pi-pw pi-check"]');
        await element(el).click();
        await console.log('done click yes confirm');
    }

    async removeAllVnfOnboard() {
        await console.log('begin removeAllVnfOnboard');
        while (await this.countNumberVnfOnboard() > 0) {
            await this.showDropActionDropDown(1);
            await this.selectItemDropActions(2);
            await this.clickYesConfirmDialog();
            await console.log('deleted row 1');
            this.clearMessage();
        }
        this.clearMessage();
        await console.log('deleted all vnf onboard');
    }

    async clearMessage() {
        await console.log('begin clearMessage');
        const result1 = await this.pageError.verifyMessegeError();
        const result2 = await this.pageError.verifyMessegeWarning();
        const result3 = await this.pageError.verifyMessegeInfo();
        if (result1 === true || result2 === true || result3 === true) {
            await this.pageError.cliclClearALL();
        }
        await console.log('done');
    }

    async isTextNoteWasDisplayedVnfType(text: string) {
        await console.log('begin isTextNoteWasDisplayedVnfType');
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div:nth-child(4) > div:nth-child(2)', text);
        const check = await element(el).isPresent();
        await console.log('done');
        return check;
    }

    async isPermissionsPerSelectedVNFTypeDisplay() {
        await console.log('begin isPermissionsPerSelectedVNFTypeDisplay');
        // tslint:disable-next-line:max-line-length
        const el = by.cssContainingText('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div:nth-child(5) > div.ui-grid-row', 'Permissions per Selected VNF Type');
        return await element(el).isPresent();
    }

    async disableAllSwitchVNFType() {
        await console.log('begin disableAllSwitchVNFType');
        // tslint:disable-next-line:max-line-length
        const el = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div:nth-child(4) > div:nth-child(2) > div p-inputswitch[ng-reflect-model="true"]');
        const elVNFType = await element.all(el);
        for (let i = 0; i < elVNFType.length; i++) {
            await elVNFType[i].click();
            await console.log('done click switch vnf type enable: ', i);
        }
        await console.log('disabled All switch VNFType');
    }

    async countSwitchVNFLifeCycleEnable() {
        await console.log('begin countSwitchVNFLifeCycleEnable');
        // tslint:disable-next-line:max-line-length
        const el = by.css('div.ui-panel-content-wrapper > div > div > div > div > div:nth-child(4) > div:nth-child(5) > div:nth-child(2) > div.ui-grid-row p-inputswitch[ng-reflect-model="true"]');
        const numberSwitch = await element.all(el).count();
        await console.log('number switch VNF life cycle enable: ', numberSwitch);
        return numberSwitch;
    }

}
