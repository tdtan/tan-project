import { element, by, browser, ExpectedConditions} from 'protractor';

const btnBulkAction = by.xpath('//*[@id="tb-vnf"]/div/div[1]/span/div/div[1]/p-dropdown/div');
const btnRecord = by.xpath('//*[@id="tb-vnf"]/div/p-paginator/div/div/div/span');

export class PageBulkAction {

     // Click VNF checkbox
    /**
     * parameter:
     * author: tthiminh
     */
    async clickVNFcheckbox(numberTr: number) {
        const cbxVNF = by.xpath('//*[@id="tb-vnf"]/div/div[2]/div/div[2]/table/tbody/tr[' + numberTr + ']/td[1]/p-tablecheckbox');
        await console.log('Start click VNF checkbox ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(cbxVNF)), 3000);
        await element(cbxVNF).click();
        await console.log('Done');
    }

     // Get Record
    /**
     * parameter:
     * author: tthiminh
     */
    async getRecord() {
        await console.log('Start verify element enable');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnRecord)), 3000);
        const txtRecord = await element(btnRecord).getText();
        await console.log('Done');
        return txtRecord;
    }

    // Verify Element Enable
    /**
     * parameter:
     * author: tthiminh
     */
    async verifyElementDisable() {
        await console.log('Start verify element enable');
        return element(btnBulkAction).isEnabled();
    }




}
