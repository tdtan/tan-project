import { element, by, ExpectedConditions, browser } from 'protractor';

const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
const btnUpgrade = by.xpath('//button[@icon="pi pi-pw pi-check"]');
const txtUpgrade = by.xpath('//*[@id="upgrade"]/div/div[1]/span');
const btnSelectVersion = by.xpath('//*[@id="upgrade"]/div/div[2]/div[3]/p-dropdown');
const fmrUpgrade = by.xpath('//*[@id="upgrade"]/div[@role="dialog"]');
export class PageUpgrade {

    // Click button Upgrade
    async clickbtnUpgrade() {
        await console.log('Start click Heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnUpgrade)), 3000);
        await element(btnUpgrade).click();
        await console.log('Done click Heal');
    }

    // Click button Cancel
    async clickbtnCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click Cancel');
    }

    // Click lable select Version
    async clickbtnSelectVersion() {
        await console.log('Start click lable select Version');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectVersion)), 3000);
        await element(btnSelectVersion).click();
        await console.log('Done click lable select Version');
    }

    // Click choose option Version
    async clickoptionVersion(optionVersion: string) {
        const btnoptionVersion = by.xpath('//li[contains(span,"' + optionVersion + '")]');
        await console.log('Start click option heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnoptionVersion)), 3000);
        await element(btnoptionVersion).click();
        await console.log('Done click option heal');
    }

    // Get text in description of option Heal
    async GetTextUpgrade() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtUpgrade)), 3000);
        const getTextDes = await element(txtUpgrade).getText();
        await console.log('Text Descrition: ' + getTextDes);
        return getTextDes;

    }

    // Verify form Upgrade display
    async verifyformUpgrade() {
        await console.log('Start verify form');
        await browser.wait(ExpectedConditions.visibilityOf(element(fmrUpgrade)), 3000);
        return await element(fmrUpgrade).isPresent();

    }


}
