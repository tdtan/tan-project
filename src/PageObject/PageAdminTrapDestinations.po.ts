import { element, by, ExpectedConditions, browser, protractor } from 'protractor';

const btnDelete = by.xpath('//*[@icon="fa fa-trash"]/button');
const btnConfirmYesDelete = by.xpath('//*[@id="trap-destination-delete"]//button[2]');
const btnConfirmNoDelete = by.xpath('//*[@id="trap-destination-delete"]//button[1]');
const pupDelete = by.xpath('//*[@id="trap-destination-delete"]/div[@role="dialog"]');
const txbSearch = by.css('#container-common-table div.container-search-input input');
const txtNoRecord = by.xpath('//*[text()=" No records found "]');
const btnConfirmYes = by.xpath('//*[@icon="pi pi-pw pi-check"]');
const btnShowFilter = by.css('p-button[title="Show/Hide Filter Row"] button');
const btnRefresh = by.css('p-button[title="Refresh Table"] button');
const btnShowColumn = by.css('p-button[title="Show/Hide Columns"] button');
const btnClosePupop = by.xpath('//*[contains(span,"Close")]');
const btnClearSearch = by.css('div.container-search-input p-button[icon="fa fa-times"] button');
const btnClearTextFilter = by.xpath('//table/thead/tr[2]/th[7]/p-button[@icon="fa fa-times"]/button');
const drpFilterRow = by.xpath('//*[@id="container-common-table"]//table//p-dropdown');
const btnAddTrap = by.xpath('//*[contains(@class,"button-add-action")]/button');
const txtRecord = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator/div/div/div/span');
const txtShow1Record = by.xpath('//*[text()=" Showing 1 of 1 record "]');
const Lnk_DownArrow = by.xpath('//*[@id="container-common-table"]//a[@aria-expanded="true"]');
const Lnk_RightArrow = by.xpath('//*[@id="container-common-table"]//a[@aria-expanded="false"]');
const txtNumberRowDisplay = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown/div/label');
const rowTable = by.css('#container-common-table table tbody tr');
const lblNumberPage = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown');
const btnPage2Number = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator/div/span/a[2]');
const btnSwitchPage = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator//a[3]');

const btnEditTrap = by.xpath('//*[@id="container-common-table"]//table/tbody/tr/td[1]/div/div/span');
const txtEditTrap = by.xpath('//*[@id="trapDestination"]//div/span[text()="Edit Trap Destination"]');
const pupDeleteTrapAlarm = by.xpath('(//*[contains(@class,"ui-toast")]//*[contains(text(),"has been deleted from VNFM")])[1]');
const pupAddTrapAlarm = by.xpath('(//*[contains(@class,"ui-toast")]//*[contains(text(),"has been added to VNFM")])[1]');
const pupEditTrapAlarm = by.xpath('(//*[contains(@class,"ui-toast")]//*[contains(text(),"has been updated to VNFM")])[1]');
export class PageAdminTrapDestinations {
    // Click Delete button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickDeleteButton() {
        await console.log('Start Click Delete button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnDelete)), 3000);
        await element(btnDelete).click();
        await console.log('Done');
    }

    // Click Confirm Yes Delete button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickConfirmYesDeleteButton() {
        await console.log('Start Confirm Yes Delete button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmYesDelete)), 3000);
        await element(btnConfirmYesDelete).click();
        await console.log('Done');
    }

    // Click Confirm No Delete button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickConfirmNoDeleteButton() {
        await console.log('Start Click Confirm No Delete button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmNoDelete)), 3000);
        await element(btnConfirmNoDelete).click();
        await console.log('Done');
    }

    // Click Option in Action
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickOptionInSelectAction(txtOption: string) {
        await console.log('Start click Option in Action');
        const btnOption = by.xpath('//li[contains(span,"' + txtOption + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOption)), 3000);
        await element(btnOption).click();
        await console.log('Done');
    }

    // Click Confirm Yes button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickConfirmYesButton() {
        await console.log('Start Click Confirm Yes button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnConfirmYes)), 3000);
        await element(btnConfirmYes).click();
        await console.log('Done');
    }

    // Click Refresh button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickRefreshButton() {
        await console.log('Start Click Refresh button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnRefresh)), 3000);
        await element(btnRefresh).click();
        await console.log('Done');
    }
    // Click Filter button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickShowCloumnButton() {
        await console.log('Start Click Show Cloumn button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowColumn)), 3000);
        await element(btnShowColumn).click();
        await console.log('Done');
    }

    // Click Filter button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickFilterButton() {
        await console.log('Start Click Filter button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowFilter)), 3000);
        await element(btnShowFilter).click();
        await console.log('Done');
    }

    // Click Filter Role Column
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickFilterRoleColumn() {
        await console.log('Start Click Filter Role Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(drpFilterRow)), 3000);
        await element(drpFilterRow).click();
        await console.log('Done');
    }

    // Click link on row switch to edit page
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickLinkOnRow() {
        await console.log('Start Click link on row switch to edit page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnEditTrap)), 3000);
        await element(btnEditTrap).click();
        await console.log('Done');
    }

    // Click Choose Option Filter In Role Column
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickChooseOptionFilterInRoleColumn() {
        await console.log('Start Click Choose Option Filter In Role Column');
        const btnOption = by.xpath('//li[contains(span,"user_test")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnOption)), 3000);
        await element(btnOption).click();
        await console.log('Done');
    }

    // Click Clear search button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickClearSearchButton() {
        await console.log('Start Click Clear search button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done');
    }

    // Click Clear Filter button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickClearFilterButton() {
        await console.log('Start Click Clear Filter button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearTextFilter)), 3000);
        await element(btnClearTextFilter).click();
        await console.log('Done');
    }

    // Click Close Button In Show Column
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickCloseButtonInShowColumn() {
        await console.log('Start Click Close Button In Show Column');
        const btnClose = by.xpath('//*[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClose)), 3000);
        await element(btnClose).click();
        await console.log('Done');
    }

    // Click Page 2 In Table
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickPage2InTable() {
        await console.log('Start Click Page 2 In Table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPage2Number)), 3000);
        await element(btnPage2Number).click();
        await console.log('Done');
    }

    // Click Add Trap Button
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickAddTrapButton() {
        await console.log('Start Click Add Trap Button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnAddTrap)), 3000);
        await element(btnAddTrap).click();
        await console.log('Done');
    }


    // Click Down Arrow
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickDownArrow() {
        await console.log('Start Click Down Arrow');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(Lnk_DownArrow)), 3000);
        await element(Lnk_DownArrow).click();
        await console.log('Done');
    }

    // Click Right Arrow
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickRightArrow() {
        await console.log('Start Click Right Arrow');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(Lnk_RightArrow)), 3000);
        await element(Lnk_RightArrow).click();
        await console.log('Done');
    }

    // Click close show column pupop
    /**
     * parameter:
     * author: tthiminh
     */
    async ClickCloseButton() {
        await console.log('Start Click Close Button');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClosePupop)), 3000);
        await element(btnClosePupop).click();
        await console.log('Done');
    }

    // Clear text in textbox search
    /**
     * parameter:
     * author: tthiminh
     */
    async ClearTextSearch() {
        await console.log('Start Clear text in textbox search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(txbSearch).sendKeys(protractor.Key.BACK_SPACE);
        await console.log('Done');
    }

    // Count row display one page
    async countNumberRowDisplayOnTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }

    async countNumberRoleInPage() {
        await console.log('begin count Number Role In Page');
        const el = by.css('p-table table > tbody > tr');
        const rowNumber = await element.all(el).count();
        await console.log('number role in page: ', rowNumber);
        return rowNumber;
    }

    // Set text Search in Role Page
    async settextSearch(search: string) {
        await console.log('Start input text Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbSearch)), 3000);
        await element(txbSearch).sendKeys(search);
        await console.log('Done');
    }

    // Select Option from Show/ Hide columns List in Configuration Tenant Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Multiple Option Name from Show Hide Columns List');
        const btnSelectCol = by.xpath('//*[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.visibilityOf(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        await console.log('Done selecting Multiple Option Name from Show Hide Columns List');
    }

    // Select number page on table in Onboarded Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }

    // Action filter
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 5000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
        await console.log('Done');
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }


    // Get text record
    async getTextRecord() {
        await console.log('Start get text record');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtRecord)), 3000);
        return element(txtRecord).getText();

    }

    // Get text label filter
    async getTextLabelFilter(NumColumn: number) {
        await console.log('Start get text label filter');
        // tslint:disable-next-line:max-line-length
        const txtLabelInFilter = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[' + NumColumn + ']/p-dropdown/div/label');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtLabelInFilter)), 3000);
        return element(txtLabelInFilter).getText();

    }

    // Get text in column
    async getTextInColumn(txtColumn: number) {
        await console.log('Start get text in Column');
        const txt_Column = by.xpath('//*[@id="container-common-table"]//table/tbody/tr[1]/td[' + txtColumn + ']/div/div/span');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txt_Column)), 3000);
        return element(txt_Column).getText();

    }

    // Get Text Number Row Display In Page
    async getTextNumberRowDisplayInPage() {
        await console.log('Start get Text Number Row Display In Page');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtNumberRowDisplay)), 3000);
        return element(txtNumberRowDisplay).getText();

    }

    // Get Attribute of textbox search
    async getAttributeOfTextboxSearch() {
        await console.log('Start get Attribute of textbox search');
        await browser.wait(ExpectedConditions.visibilityOf(element(txbSearch)), 4000);
        return element(txbSearch).getAttribute('value');
    }

    // Get text in info message when add trap destination
    async getTextInMessageWhenAddTrapDestination() {
        await console.log('Start get Text In Message When Add Trap Destination');
        const txtMessageInfo = by.xpath('//div[@class="ui-toast-detail"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtMessageInfo)), 6000);
        return element(txtMessageInfo).getText();
    }


    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="container-common-table"]//th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 5000);
        await element(sort).click().then(function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click().then(function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');
    }

    // Verify column display in page
    async verifyColumnDisplay(columnName: string) {

        const txtTH = by.xpath('//th[text()=" ' + columnName + ' "]');
        await console.log('Start verify column display in Pages ');
        return element(txtTH).isDisplayed();
    }

    // Verify display 1 record in page
    async verifyDisplay1RecordInPage() {
        await console.log('Start verify display 1 record in page ');
        return element(txtShow1Record).isPresent();
    }

    // Verify show 1 record is Display in page
    async verifyShow1RecordIsDisplayInPage() {
        await console.log('Start verify show 1 record is Display in page ');
        return element(txtShow1Record).isDisplayed();
    }

    // Verify display dropdown filter in page
    async verifyDisplayDropdownFilterInPage() {
        await console.log('Start verify display display dropdown filter in page ');
        return element(drpFilterRow).isPresent();
    }

    // Verify Switch Button Display
    async verifySwitchButtonDisplay() {
        await console.log('Start Verify Switch Button Display');
        return element(btnSwitchPage).isEnabled();
    }

    // Verify Edit Trap Destination Page display
    async verifyEditTrapDestinationPageDisplay() {
        await console.log('Start Verify Edit Trap Destination Page display');
        return element(txtEditTrap).isPresent();
    }

    // Verify Add Trap Destination button Display
    async verifyAddTrapDestinationButtonDisplay() {
        await console.log('Start Verify Add Trap Destination button Display');
        return element(btnAddTrap).isDisplayed();
    }

    // Verify Delete Pupop Display In Page
    async verifyDeletePupopDisplayInPage() {
        await console.log('Start Verify Delete Pupop Display In Page');
        return element(pupDelete).isPresent();
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//table/thead/tr[1]/th[text()=" ' + strOptionName + ' "]');
        return await element(tableTH).isDisplayed();
    }

    // Verify Alarm Display When Trap To Delete
    async verifyAlarmDisplayWhenTrapToDelete() {
        await console.log('Start verify Alarm Display When Trap To Delete');
        await browser.wait(ExpectedConditions.presenceOf(element(pupDeleteTrapAlarm)), 6000);
        return await element(pupDeleteTrapAlarm).isPresent();
    }

    // Verify Alarm Present In page When Trap Edit
    async verifyAlarmPresentInPageWhenTrapEdit() {
        await console.log('Start verify Alarm Present In page When Trap Edit');
        await browser.wait(ExpectedConditions.presenceOf(element(pupEditTrapAlarm)), 6000);
        return await element(pupEditTrapAlarm).isPresent();
    }

    // Verify Alarm Display When Trap Added
    async verifyAlarmDisplayWhenTrapAdded() {
        await console.log('Start verify Alarm Display When Trap Added');
        await browser.wait(ExpectedConditions.presenceOf(element(pupAddTrapAlarm)), 10000);
        // const pupAddTrapAlarm1 = by.xpath('(//*[contains(@class,"ui-toast")]//*[contains(text(),"has been added to VNFM")])[1]');
        return await element(pupAddTrapAlarm).isPresent();
    }

}
