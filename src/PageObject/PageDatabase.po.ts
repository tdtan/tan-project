import { element, by, ExpectedConditions, browser } from 'protractor';

const txb_search = by.css('#container-common-table div[class="container-search-input"] input');
const btn_Refesh = by.xpath('//*[@id="container-common-table"]//p-button[@title="Refresh Table"]/button');
const btn_ClearText = by.xpath('//p-button[@icon="fa fa-times"]/button');
const btn_ShowHide = by.xpath('//*[@id="container-common-table"]//p-button[@title="Show/Hide Columns"]/button');
const btn_ShowHideFilter = by.xpath('//*[@id="container-common-table"]//p-button[@title="Show/Hide Filter Row"]/button');
const drp_InTable = by.css('tr th p-dropdown');
const btn_ClearTextFilter = by.xpath('//*[@id="container-common-table"]//tr[2]/th[6]/p-button/button');
const rowTable = by.css('#container-common-table table tbody tr');
const lblNumberRow = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown');
const NumberRow = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown//label');
const txt_Record = by.xpath('//*[@id="container-common-table"]//p-paginator/div/div/div/span');
const txtDatabase = by.xpath('//*[@id="container-common-table"]//p-header/span');

export class PageDatabase {

    // Verify show text Database
    async verifyShowTextDatabase(_textDatabase: any) {
        console.log('Start get text.');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtDatabase)), 5000);
        const Database = await element(txtDatabase).getText();
        expect(Database).toEqual(_textDatabase);
        await console.log('text: ' + Database);
        await console.log('Done.');
    }

    // Set text Search in Database Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await element(txb_search).clear();
        await element(txb_search).sendKeys(user);
        console.log('Done Input search.');
    }

    // Get Attribute In Search
    async getAttributeInSearch() {
        await console.log('Start Get Attribute In Search');
        await browser.wait(ExpectedConditions.visibilityOf(element(txb_search)), 4000);
        return await element(txb_search).getAttribute('value');
    }

    // Click button minimized
    async clickShowHideFilter() {
        await console.log('Start click Show hide filter.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHideFilter)), 4000);
        await element(btn_ShowHideFilter).click();
        await console.log('Done');
    }

    // Click button Refesh
    async clickRefesh() {
        await console.log('Start click Refesh');
        await element(btn_Refesh).click();
        await console.log('Done click Refesh');
    }

    // Click Clear button to clear text search
    async clickClearTextSearch() {
        await console.log('Start click Clear button to clear text search');
        await element(btn_ClearText).click();
        await console.log('Done click Clear button to clear text search');
    }


    // Click button Show Hide Column
    async clickShowHideColumn() {
        await console.log('Start click Show Hide Column');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ShowHide)), 3000);
        await element(btn_ShowHide).click();
        await console.log('Done click Show Hide Column');
    }

    // Click Clear button in Filter
    async clickClearButtonInFilter() {
        await console.log('Start click Clear button in Filter');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_ClearTextFilter)), 3000);
        await element(btn_ClearTextFilter).click();
        await console.log('Done click Clear button in Filter');
    }

    // Select Option from Show/ Hide columns List in Database Page
    async selectOptionNamefromShowHideColumnsList(strOptionName: string) {
        await console.log('Start selecting Option Name from Show Hide Columns List');
        await this.clickShowHideColumn();
        const btnSelectCol = by.xpath('//p-checkbox[contains(label,"' + strOptionName + '")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectCol)), 3000);
        await element(btnSelectCol).click();
        const btn_Close = by.xpath('//button[contains(span,"Close")]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btn_Close)), 3000);
        await element(btn_Close).click();
        await console.log('Done selecting Option Name from Show Hide Columns List');
    }

    // Verify Show/ Hide columns when select column show
    async verifyShowHidecolumns(strOptionName: string) {
        await console.log('Start verify column show or hide in table');
        const tableTH = by.xpath('//*[@id="container-common-table"]//table/thead/tr[1]/th[text()=" ' + strOptionName + ' "]');
        if (await element(tableTH).isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    // Set table display 5 entries as default
    async setEntriesDefault() {
        await console.log('Start set 5 rows on table');
        await browser.wait(ExpectedConditions.visibilityOf(element(NumberRow)), 3000);
        await expect(element(NumberRow).getText()).toEqual('5');
        await console.log('Done set 5 rows on table');
    }

    // Verify table display maximum 5 entries as default
    async verifyDisplayEntriesDefault() {
        await console.log('Start verify display maximum 5 rows on table');
        await browser.wait(ExpectedConditions.visibilityOf(element(rowTable)), 3000);
        await console.log('Done verify display maximum 5 rows on table');
    }

    // Select number page on table in Database Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberRow)), 3000);
        await element(lblNumberRow).click();
        const numberrow = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(numberrow).click();
        await console.log('Done select number row display on table');
    }

    // Verify row display on 1 page in table
    async verifyNumberRowDisplayOnTable(numberRow: number) {
        await this.selectNumberRowOnTable(numberRow);
        await element.all(rowTable).count().then(async function (count) {
            await console.log('Number Row Display On Table: ' + count);
            expect(Boolean(count <= numberRow)).toBe(true);
          });
    }

    // Select number page in Database Page
    async MoveToPageNumber(page_num: number) {
        await console.log('Start select number page');
        const PageNumber = by.xpath('//*[@id="database-vnf"]//a[text()="' + page_num + '"]');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(PageNumber)), 3000);
        await element(PageNumber).click();
        // tslint:disable-next-line:max-line-length
        const page = by.xpath('//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
        await browser.wait(ExpectedConditions.visibilityOf(element(page)), 3000);
        await expect(element(page).getText()).toEqual(page_num);
        await console.log('Done select number page');
    }

    // Get text record in tab Database
    async getRecord() {
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        const record = await element(txt_Record).getText();
        await console.log('Done get record');
        return record;
    }

    // Get Value In Filter
    async getValueInFilter() {
        await console.log('Begin Get Value In Filter');
        const txt_ValueFilter = by.xpath('//*[@id="container-common-table"]//tr[2]/th[5]/p-dropdown/div/label');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_ValueFilter)), 3000);
        const record = await element(txt_ValueFilter).getText();
        await console.log('Done Get Value In Filter');
        return record;
    }

    // Get text in Td display
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="container-common-table"]//table/tbody/tr[1]/td[' + numTd + ']/div/div/span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 5000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Record)), 3000);
        await expect(element(txt_Record).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // verify Dropdown Filter Display
    async verifyDropdownFilterDisplay() {
        await console.log('Start verify Dropdown Filter Display');
        return element(drp_InTable).isPresent();
    }

    // Select filter from talbe in Database page
    async SelectFilter(columnnumber: number, txtChoose: string) {
        const filtercolumn = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();
        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
        await console.log('Done');
    }



}
