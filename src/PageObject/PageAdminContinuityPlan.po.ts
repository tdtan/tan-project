import { element, by, browser, ExpectedConditions } from 'protractor';
import { PageError } from '../PageObject/PageError.po';
import { PageAddContinuityPlan } from './PageAddContinuityPlan.po';
// ExpectedConditions, browser, protractor

export class PageAdminContinuityPlan {
    pageError = new PageError();
    add = new PageAddContinuityPlan();
    // PageAdminContinuityPlan
    /**
     * parameter:
     * author: tdtan
     */
    add_btn             = element(by.xpath('//span[contains(text(),"Add Plan")]'));
    continuity_planname = element(by.xpath('//input[@formcontrolname="ContinuityPlanName"]'));
    header_PlanName     = element(by.xpath('//thead/tr/th[contains(text(),"Plan Name")]'));
    header_Description  = element(by.xpath('//thead/tr/th[contains(text(),"Description")]'));
    header_Delete       = element(by.xpath('//thead/tr/th[contains(text(),"Delete")]'));
    sort_default_icon   = element(by.xpath('//thead/tr/th[1]/p-sorticon/i'));
    sort_up_icon        = element(by.xpath('//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]'));
    sort_down_icon      = element(by.xpath('//i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]'));
    filter_icon         = element(by.xpath('//p-button[@title="Show/Hide Filter Row"]//button'));
    clear_filter_icon   = element(by.xpath('//tr[2]//th[@class=" column-clear"]/p-button'));
    search_global_txt   = element(by.xpath('//div[@class="container-search-input"]//input'));
    table_row           = element.all(by.xpath('//tbody[@class="ui-table-tbody"]/tr/td[1]//span'));
    plan_name_txt       = element(by.xpath('//thead[@class="ui-table-thead"]/tr[2]/th[1]/input'));
    description_txt     = element(by.xpath('//thead[@class="ui-table-thead"]/tr[2]/th[2]/input'));
    show_hide_col_icon  = element(by.xpath('//p-button[@title="Show/Hide Columns"]/button'));
    plan_name_checkbox  = element(by.xpath('//div[@class="container-column-dialog"]//div[1]//div[1]//div[2]'));
    description_checkbox = element(by.xpath('//div[@class="container-column-dialog"]//div[2]//div[1]//div[2]'));
    delete_checkbox     = element(by.xpath('//div[@class="container-column-dialog"]//div[3]//div[1]//div[2]'));
    x_icon              = element(by.xpath('//span[@class="pi pi-times"]'));
    close_column_popup  = element(by.xpath('//span[contains(text(),"Close")]'));
    current_number_page = element(by.xpath('//p-dropdown'));
    paging_option       = element(by.xpath('//div[@class="ui-dropdown-items-wrapper"]'));
    value_paging_option = element.all(by.xpath('//div[@class="ui-dropdown-items-wrapper"]//p-dropdownitem'));
    delete_icon         = element(by.xpath('//tbody//tr[1]//p-button[@icon="fa fa-trash"]/button'));
    delete_popup        = element(by.xpath('//p-dialog[@id="continuity-plan-delete"]//div[@role="dialog"]'));
    no_button_popup     = element(by.xpath('//div[@role="dialog"]//button[@icon="fa fa-times"]'));
    yes_button_popup    = element(by.xpath('//button[@icon="pi pi-pw pi-check"]'));
    CP_name             = element.all(by.xpath('//tbody/tr/td[1]'));
    btnClearAll         = element(by.xpath('//button[@label="Clear All"]'));
    lcb_divInfo         = element(by.xpath('//*[text()="Info"]'));
    txt_ContinuityPlan  = element.all(by.xpath('//td[1]//span'));
    txtNoRecord         = element(by.xpath('//*[text()=" No records found "]'));
    msg_detail          = element(by.xpath('//p-toastitem[1]//div[@class="ui-toast-detail"]'));
    
    // was deleted from VNFM
    // Continuity Plan '11111' added to VNFM
    // Http failure response for https:

    async ClickAddButton() {
        await browser.wait(ExpectedConditions.elementToBeClickable(this.add_btn), 5000, ' Locator is not displayed on UI ');
        await this.add_btn.click();
    }
    async ClickSortIconPlanNameHeader() {
        await browser.wait(ExpectedConditions.elementToBeClickable(this.sort_default_icon), 5000, ' Locator is not displayed on UI ');
        await this.sort_default_icon.click();

    }
    async ClickOnPlanNameHeader() {
        await browser.wait(ExpectedConditions.elementToBeClickable(this.header_PlanName), 5000, 'Locator is not displayed on UI');
        await this.header_PlanName.click();
    }
    async ClickOnDescriptionHeader() {
        await browser.wait(ExpectedConditions.elementToBeClickable(this.header_Description), 5000, 'Locator is not displayed on UI');
        await this.header_Description.click();
    }
    async ClickOnFilterIcon() {
        await browser.wait(ExpectedConditions.elementToBeClickable(this.filter_icon), 5000, 'Locator is not displayed on UI');
        await this.filter_icon.click();
    }
    async CheckGlobalSearch(keyword: string) {
        let flag: boolean = true;
        let size = await this.table_row.count();
        for (let i = 1; i <= size; i++) {
            let plan_txt = await element(by.xpath('//tbody[@class="ui-table-tbody"]/tr[' + i + ']/td[1]//span')).getText();
            let des_txt = await element(by.xpath('//tbody[@class="ui-table-tbody"]/tr[' + i + ']/td[2]//span')).getText();
           // await console.log(plan_txt,des_txt);
            if ( (plan_txt.includes(keyword)) || (des_txt.includes(keyword)) ) {
                return flag = true;
            } else {
                return flag = false;
                break;
            }
        }
        return flag;
    }
    async CheckPlanNameFilter(keyword: string) {
        let flag: boolean = true;
        let size = await this.table_row.count();
        for (let i = 1; i <= size; i++) {
            let plan_txt = await element(by.xpath('//tbody[@class="ui-table-tbody"]/tr[' + i + ']/td[1]//span')).getText();
            // await console.log('plan_txt',plan_txt);
            // let des_txt = await element(by.xpath('//tbody[@class="ui-table-tbody"]/tr[' + i + ']/td[2]//span')).getText();
            if ( plan_txt.includes(keyword) ) {
                return flag = true;
            } else {
                return flag = false;
                break;
            }
        }
        return flag;
    }
    async CheckDescriptionFilter(keyword: string) {
        let flag: boolean = true;
        let size = await this.table_row.count();
        for (let i = 1; i <= size; i++) {
            // let plan_txt = await element(by.xpath('//tbody[@class="ui-table-tbody"]/tr[' + i + ']/td[1]//span')).getText();
            let des_txt = await element(by.xpath('//tbody[@class="ui-table-tbody"]/tr[' + i + ']/td[2]//span')).getText();
            // await console.log('des_txt',des_txt);
            if ( des_txt.includes(keyword)) {
                return flag = true;
            } else {
                return flag = false;
                break;
            }
        }
        return flag;
    }

    async VerifyCurrentPageNumber(num: number) {
        let current_number = parseInt(await this.current_number_page.getText());
        await expect(num).toEqual(current_number);
        await console.log('The current number of record per page is', current_number);
    }
    async VerifyPagingOptionDisplay() {
        await this.current_number_page.click();
        expect(this.paging_option.isDisplayed()).toBe(true);
        await console.log('The Paging Option displays on UI');
    }
    async ClickValueOfPagingOption(num: number) {

        await this.current_number_page.click();
        let size = await this.value_paging_option.count();
        for (let i = 0; i < size; i++) {
            let value_selected = parseInt( await this.value_paging_option.get(i).getText());
            // await console.log('value',value);
            if (value_selected == num) {
                await this.value_paging_option.get(i).click();
                let new_number = parseInt(await this.current_number_page.getText());
                // await console.log('the select number is',value_selected);
                // await console.log('the number displays on page number is',new_number);
                expect(value_selected).toEqual(new_number);
                break;
            }
        }

    }
    async AddCP(PlanName: string, CpDescription: string) {
        await this.add.setContinuityPlanName(PlanName);
        await this.add.setDescriptionContinuityPlan(CpDescription);
        const ltVNFM = await this.add.getListVNFMNameInAvailableVNFMsTable();
        await console.log('List VNFM Name In Available VNFMs Table: ', ltVNFM);
        for (let index = 0; index < ltVNFM.length; index++) {
            await this.add.chooseVNFMNameOnAvailableVNFMs(ltVNFM[index]);
        }
        await this.add.clickSaveButton();
        await browser.waitForAngularEnabled(false);
        await browser.wait(await ExpectedConditions.visibilityOf(this.msg_detail),10000,'message add is not displayed');
        await expect(await this.msg_detail.getText()).toMatch('added to VNFM');
        await this.btnClearAll.click();
    }
    async VerifyCPisDelete(CPName: string) {
        let array_name: string [] = [];
        await this.search_global_txt.sendKeys(CPName);
        await element(by.xpath('//tbody/tr/td[3]//button')).click();
        await browser.wait(await ExpectedConditions.elementToBeClickable(this.yes_button_popup), 10000, 'Locator is not displayed on UI');
        await this.yes_button_popup.click();
        await browser.waitForAngularEnabled(false);
        
        await browser.wait(await ExpectedConditions.visibilityOf(this.msg_detail),10000,'message delete is not displayed');
        await expect(await this.msg_detail.getText()).toMatch('was deleted from VNFM');
        await this.btnClearAll.click();
        await this.search_global_txt.sendKeys(' ');
        let size = await this.CP_name.count();
        for (let i = 1; i <= size; i ++) {
            let name = await element(by.xpath('//tbody/tr[' + i + ']/td[1]')).getText();
            await array_name.push(name);
        }
        await expect(await array_name.includes(CPName)).toBe(false);
    }

    async getListNameContinuityPlanInTable() {
        await console.log('begin get List Name Continuity Plan In Table');
        let lsName: string [] = [];
        const sizeName  = await this.txt_ContinuityPlan.count();
        await console.log('Number: ', sizeName);
        for (let index = 1; index <= sizeName; index++) {
            const list1 = await element(by.xpath('//tbody/tr[' + index + ']/td[1]//span')).getText();
            await console.log('Name: ', list1);
            if (!lsName.includes(list1)) {
                lsName.push(list1);
            }
        }
        return lsName;
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await this.txtNoRecord.isPresent()) {
            return false;
        } else {
            return true;
        }
    }

}
