import { element, by, ExpectedConditions, browser, protractor } from 'protractor';

const btnSaveCloud = by.xpath('//*[@id="panel-actions"]//button[@icon="pi pi-pw pi-check"]');
const btnCancel = by.xpath('//*[@id="panel-actions"]//button[@icon="fa fa-times"]');
const btnGetVersion = by.xpath('//button[contains(span,"Get Version")]');
const txtAddCloud = by.xpath('//*[@id="panel-actions"]//span[text()="Add Cloud"]');
// tslint:disable-next-line:max-line-length
const dropInputVersionDisable = by.css('p-panel form > div > div > div > div.ui-g-2 > p-dropdown > div.ui-state-disabled');
export class PageAddCloud {

  // Click button Save Cloud in add cloud Page
  async clickSaveCloud() {
    console.log('Start click Save Cloud');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSaveCloud)), 5000);
    await element(btnSaveCloud).click();
    console.log('Done');
  }


  // Click button Cancel in add cloud Page
  async clickCancel() {
    console.log('Start click Cancel');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnCancel)), 2000);
    await element(btnCancel).click();
    console.log('Done');
  }


  // Click button Get Version in add cloud Page
  async clickGetVersion() {
    console.log('Start click button Get Version');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnGetVersion)), 2000);
    await element(btnGetVersion).click();
    console.log('Done');
  }

  // Verify show text Add Cloud
  async verifyShowTextAddCloud() {
    console.log('Start verify show text Add Cloud.');
    await expect(element(txtAddCloud).isPresent()).toBeTruthy();
    await console.log('Done.');
  }

  // Verify text Add Cloud not display
  async verifyAddCloudNoPresent() {
    console.log('Start verify text Add Cloud not display.');
    await expect(element(txtAddCloud).isPresent()).toBeFalsy();
    await console.log('Done.');
  }

  // Verify button Save is disable
  async verifyButtonSaveDisable() {
    console.log('Start Verify button Save is disable.');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnSaveCloud)), 4000);
    return element(btnSaveCloud).isEnabled();

  }

  // Verify button get version is disable
  async verifyButtonGetVersionDisable() {
    console.log('Start Verify button get version is disable.');
    await browser.wait(ExpectedConditions.visibilityOf(element(btnGetVersion)), 4000);
    return element(btnGetVersion).isEnabled();

  }

  // Set input cloud config in add cloud Page
  async inputCloudConfig(level: number, info: string) {
    const config = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    await console.log('Start input Cloud Config');
    await browser.wait(ExpectedConditions.visibilityOf(element(config)), 3000);
    await element(config).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    await element(config).sendKeys(protractor.Key.BACK_SPACE);
    await element(config).sendKeys(info);
    await console.log('Done');
  }

  // Click cloud config in add cloud Page
  async clickCloudConfig(level: number) {
    const config = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[' + level + ']/div[2]/input');
    console.log('Start click cloud config');
    await browser.wait(ExpectedConditions.visibilityOf(element(config)), 4000);
    await element(config).click();
    console.log('Done');
  }

  // Click location in add cloud Page
  async clickLoca(level: number) {
    const location = by.xpath('//*[@id="panel-actions"]//div/form//div[3]/div[' + level + ']/div[2]/input');
    console.log('Start click location');
    await browser.wait(ExpectedConditions.visibilityOf(element(location)), 4000);
    await element(location).click();
    console.log('Done');
  }


  // Select version cloud from localter span
  async selectVersionCloud(level: number, version: string) {
    await console.log('Start click lable to select version');
    const selectVersion = by.xpath('//*[@id="panel-actions"]//form/div/div[1]/div[' + level + ']//p-dropdown');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(selectVersion)), 4000);
    await element(selectVersion).click();
    const se = by.xpath('//span[text()="' + version + '"]');
    await console.log('Click option version');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(se)), 4000);
    await element(se).click();
    console.log('Done');
  }

  // Select version cloud from localter div
  async selectVersionCloud2(level: number, version: string) {
    await console.log('Start click lable to select version');
    const selectVersion = by.xpath('//*[@id="panel-actions"]//form/div/div[1]/div[' + level + ']//p-dropdown');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(selectVersion)), 4000);
    await element(selectVersion).click();
    const se = by.xpath('//div[text()="' + version + '"]');
    await console.log('Click option version');
    await browser.wait(ExpectedConditions.elementToBeClickable(element(se)), 4000);
    await element(se).click();
    console.log('Done');
  }

  /*
    // Click cloud in  add cloud Page
    async clickCloud(level: number) {
      const cloud = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[' + level + ']/div[2]/input');
      console.log('Start click cloud');
      await browser.wait(ExpectedConditions.visibilityOf(element(cloud)), 2000);
      await element(cloud).click();
      console.log('Done');
    }
  */
  // Select Protocol in add cloud Page
  async selectProtocol(txtChoose: string) {
    const selectProtocol = by.xpath('//*[@id="panel-actions"]//div/form//div[1]/div[15]/div[2]/p-dropdown');
    const Protocol = by.xpath('//span[text()="' + txtChoose + '"]');
    await browser.wait(ExpectedConditions.visibilityOf(element(selectProtocol)), 3000);
    await console.log('Click lable filter');
    await element(selectProtocol).click();
    await browser.wait(ExpectedConditions.visibilityOf(element(Protocol)), 3000);
    await console.log('Click choose element filter');
    await element(Protocol).click();
    // await browser.sleep(2000);
    await console.log('Done');
  }





  // Verify Empty form is displayed when user click on Add cloud
  async verifyEmptyDisplayed(txtConfig: string) {
    const emptyDisplayed = by.css('#panel-actions input[formcontrolname="' + txtConfig + '"].ng-invalid');
    await console.log('Start verify Empty form is displayed when user click on Add cloud');
    await expect(element(emptyDisplayed).isPresent()).toBeTruthy();
    await console.log('Done verify empty form is displayed when user click on Add cloud');
  }


  // Verify "missing required info" message display
  async verifyMissingRequiredInfo(message: string) {
    const missingMessage = by.xpath('//div[text()=" ' + message + ' is required"]');
    await console.log('Start verify "missing required info" message display');
    await expect(element(missingMessage).isPresent()).toBeTruthy();
    await console.log('Done verify "missing required info" message display');
  }


  // Verify "invalid info" message display
  async verifyInvalidInfo(message: string) {
    const invalidMessage = by.xpath('//*[text()=" ' + message + '"]');
    await console.log('Start verify "invalid info" message display');
    await expect(element(invalidMessage).isPresent()).toBeTruthy();
    await console.log('Done verify "invalid info" message display');
  }

  // Verify "invalid info" message not display
  async verifyInvalidInfoNoPresent(message: string) {
    const invalidMessage = by.xpath('//*[text()=" ' + message + '"]');
    await console.log('Start verify "invalid info" message not display');
    await expect(element(invalidMessage).isPresent()).toBeFalsy();
    await console.log('Done verify "invalid info" message not display');
  }


  // Verify version
  async verifyVersion(message: string) {
    const version = by.css('#panel-actions [aria-label="' + message + '"][placeholder="Version"]');
    await console.log('Start verify version');
    await expect(element(version).isPresent()).toBeTruthy();
    await console.log('Done verify version');
  }

  /**
  * author ncdinh
  */
  async ClickBtnAddCloud() {
    // tslint:disable-next-line:max-line-length
    const el = by.css('#panel-cloud > div > div.ui-panel-content-wrapper> div > div > button');
    await element(el).click();
    await console.log('clicked');
  }

  async getListTextRowOfTable(row: number) {
    // tslint:disable-next-line:max-line-length
    const el = by.css('#tb-cloud > div > div.ui-table-scrollable-wrapper > div > div.ui-table-scrollable-body > table > tbody > tr:nth-child(' + row + ') > td');
    const listText = await element.all(el).getText();
    await console.log('list text in row: ', listText);
    return listText;
  }

  async getIpFromText(text: string) {
    const subText = text.split(' ');
    return subText[0];
  }

  async countInputVersionDisable() {
    // tslint:disable-next-line:max-line-length
    const listInputVersionDisable = await element.all(dropInputVersionDisable).getText();
    console.log('number input version disable: ', listInputVersionDisable.length);
    return listInputVersionDisable.length;
  }

  async clickRowTable(row: number) {
    // tslint:disable-next-line:max-line-length
    const elRowTable = by.css('#tb-cloud > div > div.ui-table-scrollable-wrapper > div > div.ui-table-scrollable-body > table > tbody > tr:nth-child(' + row + ')');
    await element(elRowTable).click();
    console.log('clicked row: ', row);
  }
}
