import { element, by, ExpectedConditions, browser, protractor } from 'protractor';

const txbsearch = by.xpath('//*[@class="container-search-input"]/input');
const btnClearSearch = by.css('#container-common-table div.container-search-input p-button[icon="fa fa-times"] button');
const btnRefesh = by.xpath('//*[@title="Refresh Table"]/button');
const btnClearTextFilter = by.css('tr th p-button[icon="fa fa-times"] button');
const btnShowhideFilter = by.xpath('//*[@title="Show/Hide Filter Row"]');
const rowTable = by.css('#container-common-table table tbody tr');
const txtRecord = by.xpath('//p-accordion//p-accordiontab//p-table//p-paginator/div/div/div/span');
const txtNoRecord = by.xpath('//*[text()=" No records found "]');
const btnClearDate = by.xpath('/html/body/div/div[2]/div/div[2]/button');
const btnDate = by.xpath('//*[@icon="fa fa-calendar"]');
const btnLastday = by.xpath('//*[contains(span,"Last day")]');
const txbDescription = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[6]/input');
const txtNumberRow = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown//label');
const lblNumberPage = by.xpath('//*[@id="container-common-table"]//p-paginator//p-dropdown/div');
const page2 = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator//span/a[2]');
// tslint:disable-next-line:max-line-length
const currentpage = by.xpath('//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ng-star-inserted ui-state-active"]');
// tslint:disable-next-line:max-line-length
const txxpage = by.xpath('//a[@class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all ui-state-active ng-star-inserted"]');
const btnPageEnd = by.xpath('//*[@id="container-common-table"]//p-table//p-paginator/div/a[4]');

// tslint:disable-next-line:max-line-length
const lbl_defaultEvent = by.xpath('//p-selectbutton/div/div[@class="ui-button ui-widget ui-state-default ui-button-text-only ng-star-inserted ui-state-active"]/span');
const lblSysterm = by.xpath('//p-selectbutton/div/div[@aria-label="System Alerts"]');
const lblVNFCrycle = by.xpath('//p-selectbutton/div/div[@aria-label="VNF Lifecycle Events"]');
const lblAll = by.xpath('//p-selectbutton/div/div[@aria-label="All"]');
const lblTypeinTable = by.xpath('//*[@id="alertTable"]//table/thead/tr[2]/th[3]/p-dropdown/div/label');


export class PageDashboard {

    // Set text Search in Available Page
    async setSearch(user: string) {
        console.log('Start Input search.');
        await element(txbsearch).clear();
        await element(txbsearch).sendKeys(user);
        console.log('Done Input search.');
    }

    // Set text Descrition
    async settextDescription(user: string) {
        console.log('Start Input Description.');
        await element(txbDescription).sendKeys(user);
        console.log('Done Input Description.');
    }

    // Clear Description
    async clearDescription() {
        console.log('Start clear Description.');
        await element(txbDescription).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await element(txbDescription).sendKeys(protractor.Key.BACK_SPACE);
        console.log('Done clear Description.');
    }

    // Click Show Hide filter
    async clickShowHidefilter() {
        await console.log('Start click Show Hide filter.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnShowhideFilter)), 3000);
        await element(btnShowhideFilter).click();
        await console.log('Done');
    }

    // Click All Event
    async clickAllEvent() {
        await console.log('Start click All Event.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblAll)), 3000);
        await element(lblAll).click();
        await console.log('Done');
    }

    // Click All Event
    async cliclSystermAlert() {
        await console.log('Start click Systerm Alert.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblSysterm)), 3000);
        await element(lblSysterm).click();
        await console.log('Done');
    }

    // Click VNF Life Crycle Event
    async clickVNFLifeCrycleEvent() {
        await console.log('Start click VNF Life Crycle Event.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblVNFCrycle)), 3000);
        await element(lblVNFCrycle).click();
        await console.log('Done');
    }

    // Click Page End
    async clickPageEnd() {
        await console.log('Start click to number page end.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnPageEnd)), 3000);
        await element(btnPageEnd).click();
        await console.log('Done');
    }

    // Click clear Date
    async clickClearDate() {
        await console.log('Start click clear Date.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearDate)), 3000);
        await element(btnClearDate).click();
        await console.log('Done click clear Date');
    }

    // Click Last Day
    async clickLastDay() {
        await console.log('Start click Last Day.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnLastday)), 3000);
        await element(btnLastday).click();
        await console.log('Done click Last Day');
    }

    // Click page 2
    async clickPage2() {
        await console.log('Start click page 2.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(page2)), 3000);
        await element(page2).click();
        await console.log('Done click page 2');
    }

    // Click Date
    async clickDate() {
        await console.log('Start click Date.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnDate)), 3000);
        await element(btnDate).click();
        await console.log('Done click Date');
    }

    // Click button Refesh
    async clickRefesh() {
        await console.log('Start click Refesh');
        await element(btnRefesh).click();
        await console.log('Done click Refesh');
    }

    // Click Clear button to clear text filter
    async clickClearButtonToClearTextFilter() {
        await console.log('Start click Clear button to clear text filter');
        await element(btnClearTextFilter).click();
        await console.log('Done click Clear button to clear text filter');
    }

    // Click clear Search
    async clickClearSearch() {
        await console.log('Start click clear clear Search.');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnClearSearch)), 3000);
        await element(btnClearSearch).click();
        await console.log('Done click clear clear Search');
    }

    // get Attribute in Search
    async getAttributeSearch() {
        await console.log('Start get Attribute in Search');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(txbsearch)), 3000);
        const value = await element(txbsearch).getAttribute('value');
        await console.log('Done');
        return value;
    }

    // Get text record in table Live VNF
    async getRecord() {
        await console.log('Start get record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 4000);
        const _record = await element(txtRecord).getText();
        await console.log('Text record: ' + _record);
        await console.log('Done get record');
        return _record;
    }

    // Get text in dropdown filter
    async getTextInDropdownFilter(numTd: number) {
        await console.log('Start get text in dropdown filter');
        const txt_Filter = by.xpath('//*[@id="container-common-table"]//tr[2]/th[' + numTd + ']/p-dropdown//label');
        await browser.wait(ExpectedConditions.visibilityOf(element(txt_Filter)), 4000);
        const _record = await element(txt_Filter).getText();
        await console.log('Done get text in dropdown filter');
        return _record;
    }

    // Get text in Td display
    async GetTextInTd(numTd: number) {
        const txttd = by.xpath('//*[@id="container-common-table"]//table/tbody/tr[1]/td['  + numTd + ']//span');
        await browser.wait(ExpectedConditions.visibilityOf(element(txttd)), 3000);
        const getTextTd = await element(txttd).getText();
        await console.log('Text in Tr1, Td' + numTd + ' : ' + getTextTd);
        return getTextTd;

    }

    // Get default display Alert
    async GetDefaultDisplayAlert() {
        await console.log('Start get default Alert in Dashboard');
        await browser.wait(ExpectedConditions.visibilityOf(element(lbl_defaultEvent)), 3000);
        const getText = await element(lbl_defaultEvent).getText();
        await console.log('Text ' + getText);
        return getText;

    }

    // Get text in filter Type in table
    async GetTextInFilterTypeInTable() {
        await console.log('Start get text in filter Type in table');
        await browser.wait(ExpectedConditions.visibilityOf(element(lblTypeinTable)), 3000);
        const getText = await element(lblTypeinTable).getText();
        await console.log('Text ' + getText);
        return getText;

    }

    // Get Number Row display
    async GetNumberRow() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtNumberRow)), 4000);
        const getNumRow = await element(txtNumberRow).getText();
        await console.log(' Current Row Number Display: ' + getNumRow);
        return getNumRow;

    }
    // Get page number current display
    async GetPageCurrentdisplay() {
        await browser.wait(ExpectedConditions.visibilityOf(element(currentpage)), 4000);
        const getNumpage = await element(currentpage).getText();
        await console.log(' Current Row Number Display: ' + getNumpage);
        return getNumpage;

    }

    // Get page number current display
    async GetPageCurrentdisplay2() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txxpage)), 4000);
        const getNumpage = await element(txxpage).getText();
        await console.log(' Current Row Number Display: ' + getNumpage);
        return getNumpage;

    }

    // Count row display one page
    async countNumberRowDisplayOnTable() {
        await browser.wait(ExpectedConditions.elementToBeClickable(element(rowTable)), 3000);
        const count = await element.all(rowTable).count();
        await console.log('Number Row Display On Table: ' + count);
        return count;
    }

    // Select number page on table in Onboarded Page
    async selectNumberRowOnTable(num_row: number) {
        await console.log('Start select number row display on table');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(lblNumberPage)), 3000);
        await element(lblNumberPage).click();
        // tslint:disable-next-line:max-line-length
        const rowid = by.xpath('//li[contains(span,"' + num_row + '")]');
        await element(rowid).click();
        await console.log('Done select number row display on table');
    }


    // Select filter from talbe event
    async SelectFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 5000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 5000);
        await console.log('Click choose element filter');
        await element(filChoose).click();
        await console.log('Done');
    }

    // Select filter from talbe in Onboarded page
    async SelectFilterWithCheckbox(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="container-common-table"]//table/thead/tr[2]/th[' + columnnumber + ']/p-multiselect');
        const filChoose = by.xpath('//li[contains(span,"' + txtChoose + '")]');
        const txtClose = by.xpath('/html/body/div/div[1]/a/span');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filChoose)), 3000);
        await console.log('Click choose element filter');
        await element(filChoose).click();

        await browser.wait(ExpectedConditions.elementToBeClickable(element(txtClose)), 3000);
        await element(txtClose).click();
        await console.log('Done');
    }

    // Select date in event table
    async selectDateInTable(strday: number) {
        await console.log('Start select date ');
        const sel3 = element(by.xpath('//tr//a[text()="' + strday + '"]'));
        sel3.click();
    }

    // Check if table has Search value?
    async isTableHasValueAfterSearch() {
        await console.log('Start checking if table has value after searching?');
        if (await element(txtNoRecord).isPresent()) {
            return false;
        } else {
            return true;
        }
    }

    // Verify in element display in form
    async verifyElementDisplayWhenFilter(columnnumber: number, txtChoose: string) {

        const filtercolumn = by.xpath('//*[@id="alertTable"]//table/thead/tr[2]/th[' + columnnumber + ']/p-dropdown/div/label');
        const filChoose = by.xpath('/html/body/div/div/ul/li[contains(span,"' + txtChoose + '")]');

        await browser.wait(ExpectedConditions.elementToBeClickable(element(filtercolumn)), 3000);
        await console.log('Click lable filter');
        await element(filtercolumn).click();

        if (await element(filChoose).isPresent()) {
            return true;
        } else {
            return false;
        }

    }

    // Verify Event Display in Chart
    async verifylblAlertDisplay(txtevent: string) {
        await console.log('Start Event Display in Chart');
        const lblEvent = by.xpath('//p-selectbutton/div/div[@aria-label="' + txtevent + '"]');
        return element(lblEvent).isPresent();
    }

    // Verify sort column
    async verifysort(columnnumber: number) {
        // tslint:disable-next-line:max-line-length
        const sort = by.xpath('//*[@id="container-common-table"]//table/thead/tr[1]/th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort"]');
        // tslint:disable-next-line:max-line-length
        const sortup = by.xpath('//*[@id="container-common-table"]//table/thead/tr[1]/th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-up"]');
        // tslint:disable-next-line:max-line-length
        const sortdown = by.xpath('//*[@id="container-common-table"]//table/thead/tr[1]/th[' + columnnumber + ']/p-sorticon/i[@class="ui-sortable-column-icon pi pi-fw pi-sort-down"]');
        await console.log('Start verify sort on column number: ' + columnnumber);
        await browser.wait(ExpectedConditions.elementToBeClickable(element(sort)), 3000);
        await element(sort).click().then(function () {
            expect(element(sortup).isPresent()).toBeTruthy();
        });
        await element(sortup).click().then(function () {
            expect(element(sortdown).isPresent()).toBeTruthy();
        });
        await console.log('Done verify sort');

    }

    // Verify Record display
    async verifyRecord(txtVerify: string) {
        await console.log('Start verify record');
        await browser.wait(ExpectedConditions.visibilityOf(element(txtRecord)), 3000);
        expect(await element(txtRecord).getText()).toEqual(txtVerify);
        await console.log('Done');
    }

    // Verify element display in form
    async verifyElementDisplayInForm() {
        await console.log('Start verify');
        await expect(element(currentpage).isPresent()).toBeTruthy();
    }

    // Function get color in row
    async getColorInTableAlert() {
        const sel1 = '//*[@id="alertTable"]//table/tbody/tr[1]';
        await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath(sel1))), 3000);
        // mã màu RGB/RGBA
        const valueColor = await element(by.xpath(sel1)).getCssValue('background-color');
        console.log('Color id RGB/RGBA: ', valueColor);
        const valueColorHex = this.rgb2hex(valueColor);
        console.log('Color id Hex 1 :', valueColorHex);
        return valueColorHex;
    }

    // Function to convert RGB/RGBA to hex:
    async rgb2hex(rgb: any) {
        rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? '#' +
            ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
            ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
            ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
    }

    // Function count total number row in table
    async totalNumberRow() {

    }
}
