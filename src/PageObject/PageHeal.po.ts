import { element, by, ExpectedConditions, browser } from 'protractor';

const btnHeal = by.xpath('//*[@id="vnfc-function"]/div/div[2]/div/footer/p-toolbar/div/div/button[2]');
const btnCancel = by.xpath('//*[@id="vnfc-function"]/div/div[2]/div/footer/p-toolbar/div/div/button[1]');
const btnSelectHeal = by.xpath('//*[@id="vnfc-function"]//p-dropdown[@optionlabel="label"]');
const txtDescription = by.xpath('//*[@id="vnfc-function"]//p[@class="ng-star-inserted"]');
const lblImage = by.css('#vnfc-function div:nth-child(3) p-dropdown label');
const lblFlavor = by.css('#vnfc-function div:nth-child(4) p-dropdown label');
export class PageHeal {

    // Click button Heal
    async clickbtnHeal() {
        await console.log('Start click Heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnHeal)), 3000);
        await element(btnHeal).click();
        await console.log('Done click Heal');
    }

    // Click button Cancel
    async clickbtnCancel() {
        await console.log('Start click Cancel');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done click Cancel');
    }

    // Click lable select Heal
    async clickbtnSelectheal() {
        await console.log('Start click lable select Heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnSelectHeal)), 3000);
        await element(btnSelectHeal).click();
        await console.log('Done click lable select Heal');
    }

    // Click choose option heal
    async clickoptionHeal(optionHeal: string) {
        const btnoptionHeal = by.xpath('//li[contains(span,"' + optionHeal + '")]');
        await console.log('Start click option heal');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnoptionHeal)), 3000);
        await element(btnoptionHeal).click();
        await console.log('Done click option heal');
    }

    // Get text in description of option Heal
    async GetTextdescription() {
        await browser.wait(ExpectedConditions.visibilityOf(element(txtDescription)), 3000);
        const getTextDes = await element(txtDescription).getText();
        await console.log('Text Descrition: ' + getTextDes);
        return getTextDes;

    }

    // Get text Flavor
    async GetTextFlavor() {
        await browser.wait(ExpectedConditions.visibilityOf(element(lblFlavor)), 6000);
        const getTextflavor = await element(lblFlavor).getText();
        await console.log('Text Flavor: ' + getTextflavor);
        return getTextflavor;

    }

    // Get text Image
    async GetTextImage() {
        await browser.wait(ExpectedConditions.visibilityOf(element(lblImage)), 6000);
        const getTextImage = await element(lblImage).getText();
        await console.log('Text Image: ' + getTextImage);
        return getTextImage;

    }

    // Verify text Show Or Hide
    async verifyTextShowOrHide(numOption: number) {
        await console.log('Start verify text Show Or Hide');
        const txttd = by.xpath('//*[@id="vnfc-function"]/div/div[2]/div/div[2]/p-tabview/div/div/p-tabpanel[' + numOption + ']/div/p');
        if (element(txttd).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }


}
