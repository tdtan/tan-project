import { by, element, browser, ExpectedConditions } from 'protractor';

const vnfmsTab = by.cssContainingText('body rbn-leftnav > p-panelmenu p-panelmenusub > ul > li', 'VNFMs');
const itemsDropFunction = by.css('body div.ui-dropdown-items-wrapper');
const btnCancel = by.xpath('//button[@icon="fa fa-times"]');
export class PageVNFMPreferenceVNF {

    // click show item drop function
    async clickShowItemDropFunction(row: number, column: number) {
        const el = by.css('#tb-vnf table > tbody > tr:nth-child(' + row + ') > td:nth-child(' + column + ') > p-dropdown > div');
        await element(el).click();
        await console.log('clicked');
    }
    // check item drop function is show
    async isShowItemsDropFunction() {
        return await element(itemsDropFunction).isPresent();
    }
    // show item drop function
    async showItemDropFunction(row: number, column: number) {
        const check = await this.isShowItemsDropFunction();
        if (!check) {
            await this.clickShowItemDropFunction(row, column);
        }
        await console.log('has shown');
    }
    // hide item drop function
    async hideItemDropFunction(row: number, column: number) {
        const check = await this.isShowItemsDropFunction();
        if (check) {
            await this.clickShowItemDropFunction(row, column);
        }
        await console.log('hidden');
    }
    // select item function by keyword
    async selectItemFunctionByKeyWord(row: number, column: number, itemName: string) {
        await this.showItemDropFunction(row, column);
        const el = by.cssContainingText('body div.ui-dropdown-items-wrapper p-dropdownitem li', itemName);
        await element(el).click();
        await console.log('selected item "' + itemName + '"');
    }
    // check vnfm preference is display
    async isVNFMPreferenceDisplay() {
        const el = by.css('app-vnf-relocate p-dialog div[role="dialog"]');
        return await element(el).isPresent();
    }
    // click checkbox table in dialog vnfm preference
    async clickCheckBoxTableDialog(row: number, levelTable: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#redistContainer > form p-table:nth-child(' + levelTable + ') > div > div > div > div.ui-table-scrollable-body > table > tbody > tr:nth-child(' + row + ') > td:nth-child(1) > p-tableradiobutton div.ui-radiobutton-box');
        await element(el).click();
        await console.log('clicked');
    }
    // check checkbox is selected table in dialog vnfm preference
    async isSelectedCheckBoxTableDialog(row: number, levelTable: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#redistContainer > form p-table:nth-child(' + levelTable + ') > div > div > div > div.ui-table-scrollable-body > table > tbody > tr:nth-child(' + row + ') > td:nth-child(1) > p-tableradiobutton div.ui-radiobutton-box.ui-state-active');
        return await element(el).isPresent();
    }

    async countCheckBoxTableDialog(levelTable: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#redistContainer > form p-table:nth-child(' + levelTable + ') > div > div > div > div.ui-table-scrollable-body > table > tbody > tr > td:nth-child(1) > p-tableradiobutton div.ui-radiobutton-box');
        const numberCheckBox = await element.all(el).count();
        await console.log('number check box:', numberCheckBox);
        return numberCheckBox ;
    }
    // select all checkbox table in dialog vnfm preference
    async selectAllCheckBoxTableDialog(levelTable: number) {
        const numberRow = await this.getRowNumberTableDialog(levelTable);
        for (let i = 1; i <= numberRow; i++) {
            const isSelectedCheckBox = await this.isSelectedCheckBoxTableDialog(i, levelTable);
            if (!isSelectedCheckBox) {
                await this.clickCheckBoxTableDialog(i, levelTable);
            }
        }
        await console.log('select all checkbox');
    }
    // unselect all checkbox table in dialog vnfm preference
    async unselectAllCheckBoxTableDialog(levelTable: number) {
        const numberRow = await this.getRowNumberTableDialog(levelTable);
        for (let i = 1; i <= numberRow; i++) {
            const isSelectedCheckBox = await this.isSelectedCheckBoxTableDialog(i, levelTable);
            if (isSelectedCheckBox) {
                await this.clickCheckBoxTableDialog(i, levelTable);
            }
        }
        await console.log('unselect all checkbox');
    }
    // get number row table is selected in dialog vnfm preference
    async getRowNumberSelectedTableDialog(levelTable: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#redistContainer > form p-table:nth-child(' + levelTable + ') > div > div > div > div.ui-table-scrollable-body > table > tbody > tr.ui-state-highlight');
        const number = await element.all(el).count();
        await console.log('number row selected: ', number);
        return number;
    }

    // get number row table in dialog vnfm preference
    async getRowNumberTableDialog(levelTable: number) {
        // tslint:disable-next-line:max-line-length
        const el = by.css('#redistContainer > form p-table:nth-child(' + levelTable + ') > div > div > div > div.ui-table-scrollable-body > table > tbody > tr');
        const number = await element.all(el).count();
        await console.log('number row table: ', number);
        return number;
    }
    // check VNFMs tab displayed in left navigation bar
    async isVNFMsTabDisplayed() {
        return await element(vnfmsTab).isPresent();
    }
    async getIndexColumnContainKeyWord(keyword: string) {
        const el = by.css('#tb-vnf table > thead > tr:nth-child(1) > th');
        const listColumns = await element.all(el).getText();
        const index = listColumns.indexOf(keyword);
        await console.log('index column contain ' + keyword + ' is: ', index + 1);
        return index + 1;
    }
    // Click button Cancel
    async clickCancel() {
        await console.log('start click Cancel ');
        await browser.wait(ExpectedConditions.elementToBeClickable(element(btnCancel)), 3000);
        await element(btnCancel).click();
        await console.log('Done');
    }
}
