// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  SELENIUM_PROMISE_MANAGER: false, // This must be false if using async/await keywords
  allScriptsTimeout: 300000,
  specs: [

    // './src/**/*.e2e-spec.ts'

    './src/TC_UI/login.e2e-spec.ts',
    './src/TC_UI/leftnav.e2e-spec.ts',
    './src/TC_UI/Configuration.e2e-spec.ts',
    './src/TC_UI/EditConfiguration.e2e-spec.ts',
    './src/TC_UI/Database.e2e-spec.ts',
    './src/TC_UI/RestAPIs.e2e-spec.ts',
    './src/TC_UI/Maintenance.e2e-spec.ts',
    './src/TC_UI/Dashboard.e2e-spec.ts',
    './src/TC_UI/header.e2e-spec.ts',
    './src/TC_UI/AvailableVNFCatalog.e2e-spec.ts',
    './src/TC_UI/OnboardedVNFCatalog.e2e-spec.ts',

    './src/TC_UI/VNFMConfigurationPO.e2e-spec.ts',
    './src/TC_UI/Tenant.e2e-spec.ts',
    "./src/TC_UI/AddRole.e2e-spec.ts",
    './src/TC_UI/AddUsers.e2e-spec.ts',
    './src/TC_UI/Users.e2e-spec.ts',
    './src/TC_UI/EditUser.e2e-spec.ts',
    './src/TC_UI/InstantiateVNF.e2e-spect.ts',
    './src/TC_UI/Action.e2e-spec.ts',
    './src/TC_UI/LiveVNFs.e2e-spec.ts',
    './src/TC_UI/LiveVNFC.e2e-spect.ts',
    './src/TC_UI/UIValidation.e2e-spec.ts'


  ],


  //  capabilities: { browserName: 'firefox', 'moz:firefoxOptions': { args: ['-headless'] } },

  // /* multiCapabilities: [{ 'browserName': 'chrome', 'chromeOptions': { 'args': ['--headless'] } },
  //                   { 'browserName': 'firefox', 'moz:firefoxOptions': { args: ['-headless'] } }],
  //  */                 

  // directConnect: true,
  // baseUrl: 'http://172.29.8.30:8080/',
  // framework: 'jasmine2',
  // jasmineNodeOpts: {
  //   showColors: true,
  //   defaultTimeoutInterval: 15000,
  //   print: function () { }

  capabilities: {

    'browserName': 'chrome',
    // 'chromeOptions': {
    //   'args': ['--disable-web-security', '--user-data-dir=/.e2e-chrome-profile']
    // }

    //  'browserName': 'firefox',
    //  'moz:firefoxOptions': {
    //    'args': ['--safe-mod']
    //  }

  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 40000,
    print: function () { }
  },

  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({
      spec: {
        displayStacktrace: true,
        displayFailuredSpec: true,
        displaySuiteNumber: true,
        displaySpecDuration: true
      }
    }));

    var jasmineReporters = require('jasmine-reporters');
    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      savePath: 'testresults',
      filePrefix: 'protractor-results'
    }));

  },

  // Suites, use --suite from command line
  suites: {
    smoke: '**/smoketest.e2e-spec.ts',
    all: [
      // './src/**/*.e2e-spec.ts'  
      './src/TC_UI/login.e2e-spec.ts',
      // './src/TC_UI/header.e2e-spec.ts',
      // './src/TC_UI/leftnav.e2e-spec.ts',
      // './src/TC_UI/AdminSettings.e2e-spec.ts',
      // './src/TC_UI/AdminEditSettings.e2e-spec.ts',
      // './src/TC_UI/AdminDatabase.e2e-spec.ts',
      // './src/TC_UI/AdminRestAPIs.e2e-spec.ts',
      // './src/TC_UI/OnboardedVNFCatalog.e2e-spec.ts',
      // './src/TC_UI/FailedOnboardVNFCatalog.e2e-spec.ts',
      // './src/TC_UI/Dashboard.e2e-spec.ts',
      // './src/TC_UI/AdminVNFMs.e2e-spec.ts',
      // './src/TC_UI/SecurityRealms.e2e-spec.ts',
      // './src/TC_UI/VNFMConfigurationPO.e2e-spec.ts',
      // './src/TC_UI/Tenant.e2e-spec.ts',
      // './src/TC_UI/PageRBACRoles.e2e-spec.ts',
      // './src/TC_UI/PageAdminRole.e2e-spec.ts',
      // './src/TC_UI/AddUsers.e2e-spec.ts',
      // './src/TC_UI/Users.e2e-spec.ts',
      // './src/TC_UI/EditUser.e2e-spec.ts',
      // './src/TC_UI/InstantiateVNF.e2e-spec.ts',
      // './src/TC_UI/PageUpdateVNFMPreferenceRedesign.e2e-spec.ts',
      // './src/TC_UI/PageAdminVNFMReassign.e2e-spec.ts',
      // './src/TC_UI/LiveVNFs.e2e-spec.ts',
      // './src/TC_UI/LiveVNFC.e2e-spect.ts',
      // './src/TC_UI/PageDashboard-spec.ts',
      // './src/TC_UI/PageAddCloud.e2e-spec.ts',
      // './src/TC_UI/PageEditCloud.e2e-spec.ts',
      //'./src/TC_UI/VNFMLogging.e2e-spec.ts',
      // './src/TC_UI/PageAdminTrapDestinations.e2e-spec.ts',
      // './src/TC_UI/PageAddAdminContinuityPlan.e2e-spec.ts',
      './src/TC_UI/AdminContinuityPlan.e2e-spec.ts',
      // './src/TC_UI/ClearResounce.e2e-spec.ts',
     
    ],
  },
};
